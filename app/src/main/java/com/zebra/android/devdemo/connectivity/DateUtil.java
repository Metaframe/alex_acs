package com.zebra.android.devdemo.connectivity;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

public class DateUtil {
	private final String TAG = "DateUtil";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm:ss";
	public static final String DATEFORMAT_DD_MON_YYYY = "dd-MMM-yyyy";
	public static final String DATEFORMAT_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String DATEFORMAT_YYYY_MM_DD_hh_mm = "yyyy/MM/dd hh:mm a";

	public Timestamp dateString2Calendar(String s) throws ParseException {
		// Log.v(TAG, "Convert to timestamp " + s);
		Timestamp ts = null;
		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date d1;
		try {
			d1 = df.parse(s);
			ts = new Timestamp(d1.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in timestamp conversion " + s, e);
			throw e;
		}
		return ts;
	}

	public Timestamp dateString2Calendar(String s, String dateFormat) throws ParseException {
		// Log.v(TAG, "Convert to timestamp " + s);
		Timestamp ts = null;
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		Date d1;
		try {
			d1 = df.parse(s);
			ts = new Timestamp(d1.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in timestamp conversion " + s, e);
			throw e;
		}
		return ts;
	}

	public String timeStamp2dateString(Timestamp ts) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_DD_MON_YYYY);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}

	public String timeStamp2dateString(Timestamp ts, String f) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(f);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}

	public String timeStamp2datetimeString(Calendar c) {
		SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_hh_mm);
		Date d1;
		d1 = (Date) c.clone();
		return df.format(d1);
	}

	public String timeStampLongValue2DateString(long tsValue) {
		String timeStampString = null;
		Date dt = new Date(tsValue);
		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		timeStampString = df.format(dt);
		return timeStampString;

	}

	public String timeStamp2dateString(Calendar c) {
		// Log.v(TAG, "Convert to timestamp " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_DD_MON_YYYY);
		Date d1;
		d1 = (Date) c.getTime();
		return df.format(d1);
	}

	public Timestamp getCurrentTimeStamp() {
		// Log.v(TAG, "get current timestamp");
		Timestamp ts = null;
		Date d1 = new Date();
		ts = new Timestamp(d1.getTime());
		return ts;
	}

	public Date getCurrentDate() {
		// Log.v(TAG, "get current Date");
		Timestamp ts = null;
		Date d1 = new Date();
		return d1;
	}

	public Timestamp ConvertDateToTimestamp(Date dt) {
		// Log.v(TAG, "get current timestamp");
		Timestamp ts = null;
		// if the dt is <1900, don't set the timestamp, somehow the rs.getDate
		// will still return value even if it is
		// null in the DB
		if (dt != null) {
			if (dt.getYear() > 100)
				ts = new Timestamp(dt.getTime());
		}

		return ts;
	}

	public String timeStamp2dateString2(Timestamp ts) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}
}
