package com.zebra.android.devdemo.connectivity;

import java.io.Serializable;

/*
 *Change History
 ***************************************************************
 *Date              Updated By      Remarks
 *************************************************************** 
 *June 6, 2011      Ryan            Created
 *
 */
public class DropdownItemVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String itemDescription;
	private String itemId;
	private String itemAmt;

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemAmt() {
		return itemAmt;
	}

	public void setItemAmt(String itemAmt) {
		this.itemAmt = itemAmt;
	}

	public DropdownItemVO() {

	}

	public DropdownItemVO(String id, String desc, String amt) {
		itemDescription = desc;
		itemId = id;
		itemAmt = amt;
	}

}
