/********************************************** 
 * CONFIDENTIAL AND PROPRIETARY 
 *
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * 
 * Copyright ZIH Corp. 2010
 *
 * ALL RIGHTS RESERVED 
 ***********************************************/
package com.zebra.android.devdemo.connectivity;

import java.util.ArrayList;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.view.BaseView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zebra.android.comm.BluetoothPrinterConnection;
import com.zebra.android.comm.TcpPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnectionException;
import com.zebra.android.devdemo.util.DemoSleeper;
import com.zebra.android.devdemo.util.SettingsHelper;
import com.zebra.android.printer.GraphicsUtil;
import com.zebra.android.printer.PrinterLanguage;
import com.zebra.android.printer.ZebraPrinter;
import com.zebra.android.printer.ZebraPrinterFactory;
import com.zebra.android.printer.ZebraPrinterLanguageUnknownException;

public class ConnectivityDemo extends BaseView implements LocationListener { // implements
																				// LocationListener

	private ZebraPrinterConnection zebraPrinterConnection;
	private RadioButton btRadioButton, btNoticeTypeNOCRB;
	private ZebraPrinter printer;
	private TextView statusField;
	private EditText macAddress, ipDNSAddress, portNumber;
	private EditText vehicleNumber, nricNumber, datetimeOffence, placeOffence, amtOffence;
	private Button testButton;
	private Spinner typeOffence;
	private SignatureArea signatureArea = null;
	private int nocNo, nooNo;

	// For GPS
	private EditText longitude, latitude;
	public static int iPosition;
	public static int iMode = 0;
	private LocationManager mLoc;
	private static final Integer MINIMUM_UPDATE_INTERVAL = 30000; // update
																	// every 5
																	// seconds
	private static final Integer MINIMUM_UPDATE_DISTANCE = 10; // update every
																// 10 meters

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.connection_screen_with_status);

		setupWidgets();

		// the location manager allows access to the current location and GPS
		// status
		mLoc = (LocationManager) getSystemService(LOCATION_SERVICE);
		// 00:03:7A:19:BA:BE
		nocNo = 0;
		nooNo = 0;
	}

	private void setupWidgets() {
		try {
			ArrayList<DropdownItemVO> areaList = new ArrayList();
			DropdownItemVO vo;
			vo = new DropdownItemVO("0", "Select Type of Offence", "");
			areaList.add(vo);
			vo = new DropdownItemVO("1", "(1) : Illegal parking", "$50");
			areaList.add(vo);
			vo = new DropdownItemVO("2", "(2) : Speeding", "$100");
			areaList.add(vo);
			vo = new DropdownItemVO("3", "(3) : Driving without licence", "$250");
			areaList.add(vo);
			vo = new DropdownItemVO("4", "(4) : Expired licence", "$150");
			areaList.add(vo);
			vo = new DropdownItemVO("5", "(5) : Hazardous driving", "$500");
			areaList.add(vo);

			ipDNSAddress = (EditText) this.findViewById(R.id.ipAddressInput);
			ipDNSAddress.setText(SettingsHelper.getIp(this));

			portNumber = (EditText) this.findViewById(R.id.portInput);
			portNumber.setText(SettingsHelper.getPort(this));

			macAddress = (EditText) this.findViewById(R.id.macInput);
			// macAddress.setText(SettingsHelper.getBluetoothAddress(this));
			macAddress.setText("00:03:7A:19:BA:BE");

			statusField = (TextView) this.findViewById(R.id.statusText);
			btRadioButton = (RadioButton) this.findViewById(R.id.bluetoothRadio);

			btNoticeTypeNOCRB = (RadioButton) this.findViewById(R.id.nocRadio);

			vehicleNumber = (EditText) this.findViewById(R.id.noticeVehicleInput);
			// noticeNumber.setText(SettingsHelper.getNoticeNumber(this));

			nricNumber = (EditText) this.findViewById(R.id.nricInput);

			datetimeOffence = (EditText) this.findViewById(R.id.datetimeInput);
			try {
				DateUtil dUtil = new DateUtil();
				String dt = dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp(), "yyyy-MM-dd HH:mm a");
				datetimeOffence.setText(dt);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			placeOffence = (EditText) this.findViewById(R.id.placeOfOffenceInput);

			typeOffence = (Spinner) this.findViewById(R.id.spinner1);

			CustomArrayAdapter statusAdapter = new CustomArrayAdapter(getApplicationContext(), areaList);
			typeOffence.setAdapter(statusAdapter);
			typeOffence.setOnItemSelectedListener(new MyOnItemSelectedListener());

			amtOffence = (EditText) this.findViewById(R.id.txtAmt);

			signatureArea = (SignatureArea) this.findViewById(R.id.sigArea);

			longitude = (EditText) this.findViewById(R.id.txtLongitude);
			latitude = (EditText) this.findViewById(R.id.txtLatitude);

			testButton = (Button) this.findViewById(R.id.testButton);
			testButton.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					nocNo++;
					nooNo++;
					// if (validateFields()) {
					new Thread(new Runnable() {
						public void run() {
							enableTestButton(false);
							Looper.prepare();
							doConnectionTest();
							Looper.loop();
							Looper.myLooper().quit();
						}
					}).start();
					// } else {
					// }
				}
			});

			RadioGroup radioGroup = (RadioGroup) this.findViewById(R.id.radioGroup);
			radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (checkedId == R.id.bluetoothRadio) {
						toggleEditField(macAddress, true);
						toggleEditField(portNumber, false);
						toggleEditField(ipDNSAddress, false);
					} else {
						toggleEditField(portNumber, true);
						toggleEditField(ipDNSAddress, true);
						toggleEditField(macAddress, false);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void toggleEditField(EditText editText, boolean set) {
		/*
		 * Note: Disabled EditText fields may still get focus by some other
		 * means, and allow text input. See
		 * http://code.google.com/p/android/issues/detail?id=2771
		 */
		editText.setEnabled(set);
		editText.setFocusable(set);
		editText.setFocusableInTouchMode(set);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (zebraPrinterConnection != null && zebraPrinterConnection.isConnected()) {
			disconnect();
		}
	}

	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			// Only refresh list if the selected status is not the same//Can
			// consider using text for more precision

			Object obj = parent.getItemAtPosition(pos);
			DropdownItemVO vo;
			if (obj != null) {
				vo = (DropdownItemVO) obj;
				amtOffence.setText(vo.getItemAmt());
			}
		}

		// @Override
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
			amtOffence.setText("");
		}
	}

	private void enableTestButton(final boolean enabled) {
		runOnUiThread(new Runnable() {
			public void run() {
				testButton.setEnabled(enabled);
			}
		});
	}

	private boolean isBluetoothSelected() {
		return btRadioButton.isChecked();
	}

	public ZebraPrinter connect() {
		setStatus("Connecting...", Color.YELLOW);
		zebraPrinterConnection = null;
		if (isBluetoothSelected()) {
			zebraPrinterConnection = new BluetoothPrinterConnection(getMacAddressFieldText());
			SettingsHelper.saveBluetoothAddress(this, getMacAddressFieldText());
		} else {
			try {
				int port = Integer.parseInt(getTcpPortNumber());
				zebraPrinterConnection = new TcpPrinterConnection(getTcpAddress(), port);
				SettingsHelper.saveIp(this, getTcpAddress());
				SettingsHelper.savePort(this, getTcpPortNumber());
			} catch (NumberFormatException e) {
				setStatus("Port Number Is Invalid", Color.RED);
				return null;
			}
		}

		try {
			zebraPrinterConnection.open();
			setStatus("Connected", Color.GREEN);
		} catch (ZebraPrinterConnectionException e) {
			e.printStackTrace();
			setStatus("Comm Error! Disconnecting", Color.RED);
			DemoSleeper.sleep(1000);
			disconnect();
		}

		ZebraPrinter printer = null;

		if (zebraPrinterConnection.isConnected()) {
			try {
				printer = ZebraPrinterFactory.getInstance(zebraPrinterConnection);
				setStatus("Determining Printer Language", Color.YELLOW);
				PrinterLanguage pl = printer.getPrinterControlLanguage();
				setStatus("Printer Language " + pl, Color.BLUE);
			} catch (ZebraPrinterConnectionException e) {
				setStatus("Unknown Printer Language", Color.RED);
				printer = null;
				DemoSleeper.sleep(1000);
				disconnect();
			} catch (ZebraPrinterLanguageUnknownException e) {
				setStatus("Unknown Printer Language", Color.RED);
				printer = null;
				DemoSleeper.sleep(1000);
				disconnect();
			}
		}

		return printer;
	}

	public void disconnect() {
		try {
			setStatus("Disconnecting", Color.RED);
			if (zebraPrinterConnection != null) {
				zebraPrinterConnection.close();
			}
			setStatus("Not Connected", Color.RED);
		} catch (ZebraPrinterConnectionException e) {
			setStatus("COMM Error! Disconnected", Color.RED);
		} finally {
			enableTestButton(true);
		}
	}

	private void setStatus(final String statusMessage, final int color) {
		runOnUiThread(new Runnable() {
			public void run() {
				statusField.setBackgroundColor(color);
				statusField.setText(statusMessage);
			}
		});
		DemoSleeper.sleep(1000);
	}

	private String getMacAddressFieldText() {
		return macAddress.getText().toString();
	}

	private String getTcpAddress() {
		return ipDNSAddress.getText().toString();
	}

	private String getTcpPortNumber() {
		return portNumber.getText().toString();
	}

	private String getVehicleNumber() {
		return vehicleNumber.getText().toString();
	}

	private String getNricNumber() {
		return nricNumber.getText().toString();
	}

	private String getDatetimeOffence() {
		return datetimeOffence.getText().toString();
	}

	private String getPlaceOffence() {
		return placeOffence.getText().toString();
	}

	private String getLatitude() {
		return latitude.getText().toString();
	}

	private String getLongitude() {
		return longitude.getText().toString();
	}

	private void doConnectionTest() {
		printer = connect();
		if (printer != null) {
			sendTestLabel();
		} else {
			disconnect();
		}
	}

	private void sendTestLabel() {
		try {
			byte[] configLabel = getConfigLabel();
			zebraPrinterConnection.write(configLabel);
			setStatus("Sending Data", Color.BLUE);
			DemoSleeper.sleep(1500);

			// For signature
			GraphicsUtil g = printer.getGraphicsUtil();
			Bitmap image = signatureArea.getBitmap();
			// Bitmap image = scaleBitmap(signatureArea.getBitmap());

			g.printImage(image, 0, 0, image.getWidth(), (int) image.getHeight(), false);
			DemoSleeper.sleep(1500);

			if (zebraPrinterConnection instanceof BluetoothPrinterConnection) {
				String friendlyName = ((BluetoothPrinterConnection) zebraPrinterConnection).getFriendlyName();
				setStatus(friendlyName, Color.MAGENTA);
				DemoSleeper.sleep(500);
				friendlyName = null;
			}
		} catch (ZebraPrinterConnectionException e) {
			setStatus(e.getMessage(), Color.RED);
		} finally {
			disconnect();
		}
	}

	/*
	 * Returns the command for a test label depending on the printer control
	 * language The test label is a box with the word "TEST" inside of it
	 * 
	 * _________________________ | | | | | TEST | | | | |
	 * |_______________________|
	 */
	private byte[] getConfigLabel() {
		PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();

		byte[] configLabel = null;
		if (printerLanguage == PrinterLanguage.ZPL) {
			configLabel = "^XA^FO17,16^GB379,371,8^FS^FT65,255^A0N,135,134^FDTEST^FS^XZ".getBytes();
		} else if (printerLanguage == PrinterLanguage.CPCL) {

			// Get values to print
			DropdownItemVO vo1;
			vo1 = (DropdownItemVO) typeOffence.getSelectedItem();

			// SettingsHelper.saveNoticeNumber(this, getNoticeNumber());
			// noticeNumber.setText(SettingsHelper.getNoticeNumber(this));

			// String cpclConfigLabel = "! 0 200 200 406 1\r\n" +
			// "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 4\r\n" +
			// "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";

			// TEXT or T command
			// {command} {font} {size} {x} {y} {data}
			String cpclConfigLabel = "";
			cpclConfigLabel = "! 0 200 200 390 1\r\n" + "ON-FEED IGNORE\r\n"; // 406
			// cpclConfigLabel += "BOX 5 20 380 380 2\r\n";
			cpclConfigLabel += "T 7 0 10 10 Notice No.\r\n";
			cpclConfigLabel += "T 7 0 210 10 :\r\n";

			if (btNoticeTypeNOCRB.isChecked() == true) {
				cpclConfigLabel += "T 7 0 230 10 NOC-" + (nocNo) + "\r\n";
			} else {
				cpclConfigLabel += "T 7 0 230 10 NOO-" + (nooNo) + "\r\n";
			}

			cpclConfigLabel += "T 7 0 10 40 Vehicle No.\r\n";
			cpclConfigLabel += "T 7 0 210 40 :\r\n";
			cpclConfigLabel += "T 7 0 230 40 " + getVehicleNumber() + "\r\n";

			cpclConfigLabel += "T 7 0 10 70 Nric/Passport No.\r\n";
			cpclConfigLabel += "T 7 0 210 70 :\r\n";
			cpclConfigLabel += "T 7 0 230 70 " + getNricNumber() + "\r\n";

			cpclConfigLabel += "T 7 0 10 100 Date & Time of\r\n";
			cpclConfigLabel += "T 7 0 10 120 Offence\r\n";
			cpclConfigLabel += "T 7 0 210 100 :\r\n";
			cpclConfigLabel += "T 7 0 230 100 " + getDatetimeOffence().substring(0, 10) + "\r\n";
			cpclConfigLabel += "T 7 0 230 120 " + getDatetimeOffence().substring(11, 19) + "\r\n";

			cpclConfigLabel += "T 7 0 10 150 Place of Offence\r\n";
			cpclConfigLabel += "T 7 0 210 150 :\r\n";
			cpclConfigLabel += "T 7 0 30 170 - " + getPlaceOffence() + "\r\n";

			cpclConfigLabel += "T 7 0 10 200 Type of Offence\r\n";
			cpclConfigLabel += "T 7 0 210 200 :\r\n";
			cpclConfigLabel += "T 7 0 30 220 " + vo1.getItemDescription() + "\r\n";

			cpclConfigLabel += "T 7 0 10 250 Composition amt\r\n";
			cpclConfigLabel += "T 7 0 210 250 :\r\n";
			cpclConfigLabel += "T 7 0 230 250 " + vo1.getItemAmt() + "\r\n";

			cpclConfigLabel += "LINE 10 280 370 280 2\r\n";

			cpclConfigLabel += "T 7 0 10 300 Lat : " + getLatitude() + "\r\n";
			cpclConfigLabel += "T 7 0 10 330 Long: " + getLongitude() + "\r\n";

			cpclConfigLabel += "PRINT\r\n";

			configLabel = cpclConfigLabel.getBytes();

			// vo1 = null;
		}
		return configLabel;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	private Boolean validateFields() {
		try {
			DropdownItemVO vo1;
			vo1 = (DropdownItemVO) typeOffence.getSelectedItem();

			if (getNricNumber().trim().length() == 0)
				return false;
			else if (getPlaceOffence().trim().length() == 0)
				return false;
			else if (vo1.getItemId().equals("0"))
				return false;

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// GPS start
	// @Override
	/**
	 * onResume is is always called after onStart, even if the app hasn't been
	 * paused
	 */
	protected void onResume() {
		// add a location listener and request updates every 10000ms or 10m
		mLoc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MINIMUM_UPDATE_INTERVAL, MINIMUM_UPDATE_DISTANCE, this);
		// GPS_PROVIDER
		super.onResume();
	}

	// @Override
	protected void onPause() {
		// GPS, as it turns out, consumes battery like crazy
		mLoc.removeUpdates(this);
		super.onPause();
	}

	// @Override
	// protected void onStop() {
	// // may as well just finish since saving the state is not important for
	// this toy app
	// // finish(); commented if not annotation will go back to faultnewview
	// instead of replaceimage
	// super.onStop();
	// }

	public void onLocationChanged(Location loc) {
		// display some information based on the current position
		StringBuilder sbLong = new StringBuilder("");
		StringBuilder sbLat = new StringBuilder("");

		sbLong.append(loc.getLongitude());
		if (!sbLong.equals(null)) {
			longitude.setText(sbLong.toString());
		}

		sbLat.append(loc.getLatitude());
		if (!sbLat.equals(null)) {
			latitude.setText(sbLat.toString());
		}
	}

	public void onProviderDisabled(String provider) {
		// called if/when the GPS is disabled in settings
		Toast.makeText(this, "GPS disabled", Toast.LENGTH_LONG).show();

		// end program since we depend on GPS
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setMessage("This app requires GPS. Please activate it first!");
		alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				finish();
			}
		});
		alertbox.show();
	}

	public void onProviderEnabled(String provider) {
		Toast.makeText(this, "GPS enabled", Toast.LENGTH_LONG).show();
	}

	// @Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// called upon GPS status changes
		switch (status) {
		case LocationProvider.OUT_OF_SERVICE:
			Toast.makeText(this, "Status changed: out of service", Toast.LENGTH_LONG).show();
			break;
		case LocationProvider.TEMPORARILY_UNAVAILABLE:
			Toast.makeText(this, "Status changed: temporarily unavailable", Toast.LENGTH_LONG).show();
			break;
		case LocationProvider.AVAILABLE:
			Toast.makeText(this, "Status changed: available", Toast.LENGTH_LONG).show();
			break;
		}
	}

	// GPS stop
	private Bitmap scaleBitmap(Bitmap bitmap) {
		int iWidth = bitmap.getWidth();
		int iHeight = bitmap.getHeight();
		int newWidth = 0, newHeight = 0;
		float fFactor = (float) iHeight / (float) iWidth;

		if (iWidth <= iHeight) {
			newWidth = 300;
			newHeight = 100;
			// newHeight = (int) (newWidth * fFactor);
		} else {
			newWidth = 181;
			newHeight = (int) (newWidth * fFactor);
		}
		float fScaleWidth = ((float) newWidth) / iWidth;
		float fScaleHeight = ((float) newHeight) / iHeight;

		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth, iHeight, matrix, true);

		return resizedBitmappreview;
	}
	// private Bitmap scaleBitmap(Bitmap bitmap) {
	// int iWidth = bitmap.getWidth();
	// int iHeight = bitmap.getHeight();
	// int newWidth = 0, newHeight = 0;
	// float fFactor = (float) iHeight / (float) iWidth;
	//
	// if (iWidth <= iHeight) {
	// newWidth = 200;
	// newHeight = (int) (newWidth * fFactor);
	// } else {
	// newWidth = 400;
	// newHeight = (int) (newWidth * fFactor);
	// }
	// float fScaleWidth = ((float) newWidth) / iWidth;
	// float fScaleHeight = ((float) newHeight) / iHeight;
	//
	// Matrix matrix = new Matrix();
	// // resize the bit map
	// matrix.postScale(fScaleWidth, fScaleHeight);
	//
	// Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth,
	// iHeight, matrix, true);
	//
	// return resizedBitmappreview;
	// }
}
