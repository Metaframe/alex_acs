package com.surbana.common.dateslider;

public class SliderConstant {
	public static final int DEFAULTDATESELECTOR_ID = 0;
	public static final int ALTERNATIVEDATESELECTOR_ID = 1;
	public static final int CUSTOMDATESELECTOR_ID = 2;
	public static final int MONTHYEARDATESELECTOR_ID = 3;
	public static final int TIMESELECTOR_ID = 4;
	public static final int DATETIMESELECTOR_ID = 5;
}
