package sg.com.surbana.acs.einsp.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import sg.com.surbana.acs.einsp.R;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.ianywhere.ultralitejni12.ConfigPersistent;
import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.DatabaseManager;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *22 jul 2011		Ryan		    release connection before obtaining new connection
 *25 jul 2011		Ryan		    support creation of DB on SD card
 *08 Jan 2013	    Ryan  			Prevent occasional  system crashing when resuming the system after switching to another system.
 */

public class DbUtil {
	private final String TAG = "DbUtil";
	private static Connection _conn;
	private ConfigPersistent config;
	private PreparedStatement _ps_order;
	private ResultSet _rs_order;
	private static Context mContext;
	public static String DB_DIRECTORY;
	protected static final int _debugLevel = 0; // 0 none, 1 teststream minimal,
												// 2 all
												// private static final String
												// DB_NAME = "custdb_v12.udb";
	// private int DEFAULT_DB=R.raw.custdb_v12;

	private int DEFAULT_DB = R.raw.tenant_custdb_v12_eng;
	private static final String DB_NAME = "acs_einsp_data_eng_v4.udb"; //llm eng fault 02/10/2013

	// "; //PRELOADED - admin DB ONLY FOR
	// DEMO
	//
	// private int DEFAULT_DB = R.raw.custdb_v12_sync_truncated;
	// private static final String DB_NAME = "custdb_v12_sync_truncated.udb"; //
	// PRELOADED
	// -
	// seko
	// DB
	// ONLY
	// FOR
	// DEMO

	public DbUtil(Context con) {
		mContext = con;
	}

	public DbUtil() {

	}

	/**
	 * Connect to the database. If necessary, create it and the tables.
	 * 
	 * @param args
	 *            <description>
	 * @return true on success
	 * @throws IOException
	 */
	public boolean initialize() throws ULjException, IOException {

		// connect to the database, creating first if needed
		// DB_DIRECTORY = mContext.getFilesDir().getPath();

		DB_DIRECTORY = Environment.getExternalStorageDirectory().toString();
		// DB_DIRECTORY = Environment.getExternalStorageDirectory().toString()+
		// File.separator + "eInspection";
		if (!isDBExists()) {
			Log.v(TAG, "DB file not exists, create new file");
			// createDBFile();
			createDBFileOnExternal();
		}

		if (config == null) {
			config = createConfig(DB_DIRECTORY + File.separator + DB_NAME);
			config.setLazyLoadIndexes(true);
		}

		try {
			Log.v(TAG, "Try connect to DB file");
			releaseConnection(_conn); // Ryan : 20110722 - Release connection
										// before getting new connection
			_conn = DatabaseManager.connect(config);
			Log.v(TAG, "DB File connected");
		} catch (ULjException e) {
			// should throw exception unless use backup plan (i.e. create tables
			// using script)
			e.printStackTrace();
			throw e;
		}

		return true;
	}

	public boolean refreshNewDB() throws ULjException, IOException {

		// connect to the database, creating first if needed
		// DB_DIRECTORY = mContext.getFilesDir().getPath();

		DB_DIRECTORY = Environment.getExternalStorageDirectory().toString();
		// DB_DIRECTORY = Environment.getExternalStorageDirectory().toString()+
		// File.separator + "eInspection";
		releaseConnection(_conn);
		config = null;

		createDBFileOnExternal(true);

		config = createConfig(DB_DIRECTORY + File.separator + DB_NAME);
		config.setLazyLoadIndexes(true);

		try {
			Log.v(TAG, "Try connect to DB file");
			_conn = DatabaseManager.connect(config);
			Log.v(TAG, "DB File connected");
		} catch (ULjException e) {
			// should throw exception unless use backup plan (i.e. create tables
			// using script)
			e.printStackTrace();
			throw e;
		}

		return true;
	}

	protected static ConfigPersistent createConfig(String fname) throws ULjException {
		return DatabaseManager.createConfigurationFileAndroid(fname, mContext);
	}

	/**
	 * To release DB connection
	 * 
	 * @param con
	 */
	private void releaseConnection(Connection con) {
		try {
			if (con != null)
				con.release();
		} catch (Exception ex) {
			Log.e(TAG, "Error occur when trying to release connection");
			ex.printStackTrace();
		}

	}

	/**
	 * Execute a single-value query.
	 * 
	 * @param query
	 *            SQL query to be executed
	 * @return <code>null</code>, when no value is obtained; otherwise, the
	 *         obtained value
	 */
	public String executeSingleValueQuery(String query) throws ULjException {
		Log.v(TAG, "Executing single value query : " + query);

		String retn = null;
		if (_conn == null)// check if connection is null
			_conn = getNewConnection();

		PreparedStatement ps = _conn.prepareStatement(query);
		try {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				retn = rs.getString(1);
			}
			if (rs != null)
				rs.close();
		} finally {
			if (ps != null)
				ps.close();
		}
		Log.v(TAG, "Return : " + retn);

		return retn;
	}

	/**
	 * Execute a single-value query.
	 * 
	 * @param query
	 *            SQL query to be executed
	 * @return <code>null</code>, when no value is obtained; otherwise, the
	 *         obtained value
	 */
	public int executeSingleIntValueQuery(String query) throws ULjException {
		Log.v(TAG, "Executing single value query : " + query);

		int retn = 0;
		if (_conn == null)// check if connection is null
			_conn = getNewConnection();

		PreparedStatement ps = _conn.prepareStatement(query);
		try {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				retn = rs.getInt(1);
			}
			if (rs != null)
				rs.close();
		} finally {
			if (ps != null)
				ps.close();
		}
		Log.v(TAG, "Return : " + retn);

		return retn;
	}

	/**
	 * Execute a SQL statement.
	 * 
	 * @param do_commit
	 *            if <code>true</code>,perform a commit operation after
	 *            executing the SQL statement
	 * @param sql_statemt
	 *            SQL statement to be executed
	 */
	public void executeStatement(boolean do_commit, String sql_stmt) throws ULjException {
		try {
			if (_conn == null)// check if connection is null
				_conn = getNewConnection();
			PreparedStatement ps = _conn.prepareStatement(sql_stmt);
			ps.execute();
			if (do_commit) {
				_conn.commit();
			}
			if (ps != null)
				ps.close();
		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + sql_stmt, ejl);
			_conn.rollback();
			throw ejl;
		}
	}

	public ConfigPersistent getConfig() {
		return config;
	}

	private boolean isDBExists() {
		java.io.File file = new java.io.File(DB_DIRECTORY, DB_NAME);
		Log.i(TAG, "Check if DB exists at " + DB_DIRECTORY + File.separator + DB_NAME);
		if (file.exists())
			return true;
		else
			return false;
	}

	private void createDBFileOnExternal() throws IOException {
		createDBFileOnExternal(false);
	}

	private void createDBFileOnExternal(boolean isRefresh) throws IOException {
		java.io.File eInsDirectory = new java.io.File(DB_DIRECTORY);

		java.io.File eInsDBFile = new java.io.File(DB_DIRECTORY, DB_NAME);

		try {
			if (!eInsDirectory.exists())
				eInsDirectory.mkdir();

			if (isRefresh) {
				if (eInsDBFile.exists()) {
					eInsDBFile.delete();
				}

			}
			FileOutputStream os = new FileOutputStream(eInsDBFile, true);

			InputStream is = mContext.getResources().openRawResource(DEFAULT_DB);

			byte[] buff = new byte[4096];
			int n;
			for (;;) {
				n = is.read(buff);
				if (n < 0)
					break;
				os.write(buff, 0, n);
			}
		} catch (IOException e) {
			// ...
			e.printStackTrace();
			throw e;
		}
	}

	private int createDBFile() throws IOException {

		// The DB files will be copied under system directory for android within
		// the application package
		try {
			InputStream is = mContext.getResources().openRawResource(DEFAULT_DB);
			FileOutputStream os = mContext.openFileOutput(DB_NAME, Context.MODE_PRIVATE);
			byte[] buff = new byte[4096];
			int n;
			for (;;) {
				n = is.read(buff);
				if (n < 0)
					break;
				os.write(buff, 0, n);
			}
		} catch (IOException e) {
			// ...
			e.printStackTrace();
			throw e;
		}
		return 1;
	}

	public static Connection getConnection() {
		// to put null check, and if null, to reconnection
		if (_conn != null)
			return _conn;
		else {
			_conn = getNewConnection();
			return _conn;
		}
	}

	private static Connection getNewConnection() {
		Connection con = null;
		try {
			//Ryan:20130108 : Prevent occasional  system crashing when resuming the system after switching to another system. 
			if(DB_DIRECTORY==null)
				DB_DIRECTORY = Environment.getExternalStorageDirectory().toString();

			ConfigPersistent config2 = createConfig(DB_DIRECTORY + File.separator + DB_NAME);
			config2.setLazyLoadIndexes(true);
			System.out.println(DB_DIRECTORY + File.separator + DB_NAME);
			System.out.println(config2==null);
			con = DatabaseManager.connect(config2);
		} catch (ULjException e) {
			// should throw exception unless use backup plan (i.e. create tables
			// using script)
			e.printStackTrace();
		}
		return con;
	}
}
