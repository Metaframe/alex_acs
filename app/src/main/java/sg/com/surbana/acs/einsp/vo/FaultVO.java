package sg.com.surbana.acs.einsp.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 9, 2011		Ryan			Created
 *
 */
public class FaultVO implements Serializable {
	private static final long serialVersionUID = -6114205509027782382L;
	private final String TAG = "FaultVO.java";

	// For Fault Search Criteria
	private String faultId;
	private String detailDesc;
	private String faultDesc;
	private String imageFileName;
	private byte[] imageData;
	private String imageCaption;
	private String remarks;

	private int locationId;
	private int faultCategoryId;
	private int priorityId;
	private int faultStatusId;
	private int lastModifiedByUserInfoId;
	private int faultReportedByUserInfoId;

	private Timestamp faultDate;
	private Timestamp lastModifiedDate;

	private int checklistGropuId;
	private int checklistItemId;
	private int serviceChecklistResultId;

	private int faultType;

	private int faultSubCatBuildingElement;
	private int faultSubCatFaultType;
	private int faultSubCatFaultNature;

	private String ComplainantName;
	private String ComplainantContactNo;
	private String ComplainantCompany;
    
	private int faultContrStatusId;
	
	private boolean repeated;//llm 02/10/2013 eng fault
	private boolean routed;//llm 02/10/2013 eng fault
	private int  selectedoic;
	
	
	public int getSelectedoic() {
		return selectedoic;
	}

	public void setSelectedoic(int selectedoic) {
		this.selectedoic = selectedoic;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isRouted() {
		return routed;
	}

	public void setRouted(boolean routed) {
		this.routed = routed;
	}

	public boolean isRepeated() {
		return repeated;
	}

	public void setRepeated(boolean repeated) {
		this.repeated = repeated;
	}

	public int getFaultContrStatusId() {
		return faultContrStatusId;
	}

	public void setFaultContrStatusId(int faultContrStatusId) {
		this.faultContrStatusId = faultContrStatusId;
	}

	public String getComplainantName() {
		return ComplainantName;
	}

	public void setComplainantName(String complainantName) {
		ComplainantName = complainantName;
	}

	public String getComplainantContactNo() {
		return ComplainantContactNo;
	}
	public void setComplainantContactNo(String complainantContactNo) {
		ComplainantContactNo = complainantContactNo;
	}

	public String getComplainantCompany() {
		return ComplainantCompany;
	}

	public void setComplainantCompany(String complainantCompany) {
		ComplainantCompany = complainantCompany;
	}

	public int getFaultSubCatBuildingElement() {
		return faultSubCatBuildingElement;
	}

	public void setFaultSubCatBuildingElement(int faultSubCatBuildingElement) {
		this.faultSubCatBuildingElement = faultSubCatBuildingElement;
	}

	public int getFaultSubCatFaultType() {
		return faultSubCatFaultType;
	}

	public void setFaultSubCatFaultType(int faultSubCatFaultType) {
		this.faultSubCatFaultType = faultSubCatFaultType;
	}

	public int getFaultSubCatFaultNature() {
		return faultSubCatFaultNature;
	}

	public void setFaultSubCatFaultNature(int faultSubCatFaultNature) {
		this.faultSubCatFaultNature = faultSubCatFaultNature;
	}

	public String getImageCaption() {
		return imageCaption;
	}

	public void setImageCaption(String imageCaption) {
		this.imageCaption = imageCaption;
	}

	public int getFaultType() {
		return faultType;
	}

	public void setFaultType(int faultType) {
		this.faultType = faultType;
	}

	public int getChecklistGropuId() {
		return checklistGropuId;
	}

	public void setChecklistGropuId(int checklistGropuId) {
		this.checklistGropuId = checklistGropuId;
	}

	public int getChecklistItemId() {
		return checklistItemId;
	}

	public void setChecklistItemId(int checklistItemId) {
		this.checklistItemId = checklistItemId;
	}

	public int getServiceChecklistResultId() {
		return serviceChecklistResultId;
	}

	public void setServiceChecklistResultId(int serviceChecklistResultId) {
		this.serviceChecklistResultId = serviceChecklistResultId;
	}

	public String getFaultId() {
		return faultId;
	}

	public void setFaultId(String faultId) {
		this.faultId = faultId;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public String getFaultDesc() {
		return faultDesc;
	}

	public void setFaultDesc(String faultDesc) {
		this.faultDesc = faultDesc;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getFaultCategoryId() {
		return faultCategoryId;
	}

	public void setFaultCategoryId(int faultCategoryId) {
		this.faultCategoryId = faultCategoryId;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public int getFaultStatusId() {
		return faultStatusId;
	}

	public void setFaultStatusId(int faultStatusId) {
		this.faultStatusId = faultStatusId;
	}

	public int getLastModifiedByUserInfoId() {
		return lastModifiedByUserInfoId;
	}

	public void setLastModifiedByUserInfoId(int lastModifiedByUserInfoId) {
		this.lastModifiedByUserInfoId = lastModifiedByUserInfoId;
	}

	public int getFaultReportedByUserInfoId() {
		return faultReportedByUserInfoId;
	}

	public void setFaultReportedByUserInfoId(int faultReportedByUserInfoId) {
		this.faultReportedByUserInfoId = faultReportedByUserInfoId;
	}

	public Timestamp getFaultDate() {
		return faultDate;
	}

	public void setFaultDate(Timestamp faultDate) {
		this.faultDate = faultDate;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
