package sg.com.surbana.acs.einsp.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.bo.MonthlyMeterBO;
import sg.com.surbana.acs.einsp.dao.FaultDAO;
import sg.com.surbana.acs.einsp.dao.MonthlyMeterDAO;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.CommonFunction;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.util.SynchronizationUtils;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterImagesVO;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ianywhere.ultralitejni12.ULjException;

public class MonthlyMeterImagesView extends Activity implements GestureDetector.OnGestureListener, OnTouchListener, OnClickListener, OnLongClickListener {
	private final String TAG = "MonthlyMeterImagesView.java";

	private Button imgBtnCapture;
	private ImageButton imgBtnDel, imgBtnNoDel, imgBtnSave, imgBtnClose, imgBtnCancel;
	private TextView txtGPSLocation, txtImageDate, lblGPSLocation;
	private Button imgBtnScan;
	private ImageView[] imgViewThumbNail;
	private ImageView imgViewPreview, imgEdit;
	private static final int CAMERA_PIC_REQUEST = 1337;
	private static final int SCAN_BARCODE_REQUEST = 1111;
	private int iPreviewInd = 0;

	private Bitmap thumbnail;
	public static Bitmap[] bitmap = null;

	private View vTemp;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;

	private GestureDetector gestures;

	private LinearLayout linearLayoutPreview, linearLayoutThumb, linearLayoutCaption, linearLayoutSave;

	// private Boolean bFirst = false, bSwipe = false;
	private Dialog dialogEdit;
	private int iPhoto = 0;
	private Context context;

	static String strtaskId;
	private String scannedBarcode;

	SynchronizationUtils syncUtil = new SynchronizationUtils();
	public static NotificationManager NOTIFICATION_MANAGER;

	private int locationid = 0;

	private TextView lblFaultID, txtCaption;
	public static String faultid = "";

	int iIndexTemp = -1;
	public static int iIndex = -1;

	static Boolean flagPhoto = false;

	private int meterno, resultid,  terminalid, Status,monthlymeterno,readingno = 0;
	private ProgressDialog dialogWait;
	private String strWebURL = "http://202.42.163.6/Upload_Dir/CustomerService/";

	// juehua image enhance
	private Uri mImageCaptureUri;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// juehua 20120229 for merge chin hua's tablet xml to leeming's code
		if (Build.MODEL.equalsIgnoreCase("GT-P7500")) {
			setContentView(R.layout.tablet_monthlymeter_images);
		} else {
			setContentView(R.layout.monthlymeter_images);
		}
	
		String serName = Context.NOTIFICATION_SERVICE;
		NOTIFICATION_MANAGER = (NotificationManager) getSystemService(serName);
		CommonFunction cFunction = new CommonFunction();
		try {
			strWebURL = cFunction.getHandheldConfig(AppsConstant.CONFIG_IMAGE_WEB) + cFunction.getHandheldConfig(AppsConstant.CONFIG_EQUIPMENT_LIST_URL);
		} catch (ULjException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		iIndex = -1;
		iIndexTemp = -1;
		ReplaceMonthlyMeterImage.strCaption = null;
		ReplaceMonthlyMeterImage.bitmapReplace = null;
		ReplaceMonthlyMeterImage.gpslocation = null;
		ReplaceMonthlyMeterImage.imageDate = null;
		bitmap = null;

		context = this.getBaseContext();

		Intent myLocalIntent = getIntent();
		Bundle myBundle = myLocalIntent.getExtras();
		if (myBundle != null) {
			try {
				monthlymeterno = myBundle.getInt(AppsConstant.INTENT_DATA_MONTHLY_METER_NO);
			} catch (Exception e) {
			}
			try {
				readingno = myBundle.getInt(AppsConstant.INTENT_DATA_MONTHLY_METER_READINGNO);
			} catch (Exception e) {
			}
			try {
				meterno = myBundle.getInt(AppsConstant.INTENT_DATA_METER_NO);
			} catch (Exception e) {
			}
			try {
				resultid = myBundle.getInt(AppsConstant.INTENT_DATA_SERVICE_RESULT_ID);
			} catch (Exception e) {
			}
			try {
				locationid = myBundle.getInt(AppsConstant.INTENT_DATA_BUILDING_ID);
			} catch (Exception e) {
			}
			try {
				monthlymeterno = myBundle.getInt(AppsConstant.INTENT_DATA_MONTHLY_METER_NO);
			} catch (Exception e) {
			}
			try {
				terminalid = myBundle.getInt(AppsConstant.INTENT_DATA_TERMINAL_ID);
			} catch (Exception e) {
			}
			try {
				Status = myBundle.getInt(AppsConstant.INTENT_DATA_MONTHLY_METER_STATUS);
			} catch (Exception e) {
			}
			
			

		}

		try {

			lblFaultID = (TextView) findViewById(R.id.lblFaultID);

			gestures = new GestureDetector(MonthlyMeterImagesView.this, this);

			imgViewPreview = (ImageView) findViewById(R.id.imageViewPreview);
			imgViewPreview.setOnTouchListener(this);
			imgViewPreview.setOnLongClickListener(this);

			imgBtnSave = (ImageButton) findViewById(R.id.imgbtnSave);
			imgBtnSave.setOnClickListener(this);

			imgBtnCancel = (ImageButton) findViewById(R.id.imgBtnCancel);
			imgBtnCancel.setOnClickListener(this);

			txtCaption = (TextView) findViewById(R.id.txtCaption);
			txtCaption.setInputType(0);
			txtCaption.setEnabled(false);
			txtCaption.setFocusable(false);
			// Capture button to capture new image
			imgBtnCapture = (Button) findViewById(R.id.btncamera);
			imgBtnCapture.setOnClickListener(this);
			imgBtnScan = (Button) findViewById(R.id.imgBtnScan);
			if (Status==1)
			{
				imgBtnCapture.setVisibility(View.GONE);
			}

			// Five thumbnail for captured images.
			imgViewThumbNail = new ImageView[5];
			imgViewThumbNail[0] = (ImageView) findViewById(R.id.imageView1);
			imgViewThumbNail[1] = (ImageView) findViewById(R.id.imageView2);
			imgViewThumbNail[2] = (ImageView) findViewById(R.id.imageView3);
			imgViewThumbNail[3] = (ImageView) findViewById(R.id.imageView4);
			imgViewThumbNail[4] = (ImageView) findViewById(R.id.imageView5);

			for (int i = 0; i < 5; i++) {
				imgViewThumbNail[i].setOnLongClickListener(this);
				imgViewThumbNail[i].setOnClickListener(this);
			}

			// allocate array to store images
			if (bitmap == null)
				bitmap = new Bitmap[5];

			linearLayoutPreview = (LinearLayout) findViewById(R.id.linearLayoutPreview);
			linearLayoutThumb = (LinearLayout) findViewById(R.id.linearLayoutThumb);
			linearLayoutCaption = (LinearLayout) findViewById(R.id.linearLayoutCaption);
			linearLayoutSave = (LinearLayout) findViewById(R.id.linearLayoutSave);
			// if (ReplaceTenantNoticeImage.iMode != 0 )
			// {
			try {

				// path=path +"/"+module+"/"+id + "/"+type;
				// Ryan-20111213

				String webImageSubURI = terminalid + "/" + AppsConstant.MODULE_MONTHLY_METER + "/" + monthlymeterno + "/image";

				// search existing images
				Vector<MonthlyMeterImagesVO> listFaultImages = new Vector<MonthlyMeterImagesVO>();

				if (iIndex == -1 && listFaultImages.size() <= 0) {
					MonthlyMeterDAO tDAO = new MonthlyMeterDAO();
					MonthlyMeterImagesVO vo = new MonthlyMeterImagesVO();
					vo.setMonthlyMeterNo(monthlymeterno);
					vo.setReadingNo(readingno);
					vo.setMeterNo(meterno);
					vo.setTerminalId(terminalid);
				
					listFaultImages = tDAO.getMonthlyMeterImages(vo);
					iIndex = listFaultImages.size() - 1;
					iIndexTemp = listFaultImages.size() - 1;

					if (listFaultImages.size() > 0) {
						linearLayoutPreview.setVisibility(View.VISIBLE);
						linearLayoutThumb.setVisibility(View.VISIBLE);
						linearLayoutCaption.setVisibility(View.VISIBLE);

						if (ReplaceMonthlyMeterImage.bitmapReplace == null)
							ReplaceMonthlyMeterImage.bitmapReplace = new Bitmap[5];

						if (ReplaceMonthlyMeterImage.strCaption == null)
							ReplaceMonthlyMeterImage.strCaption = new String[5];

						if (ReplaceMonthlyMeterImage.gpslocation == null)
							ReplaceMonthlyMeterImage.gpslocation = new String[5];

						if (ReplaceMonthlyMeterImage.imageDate == null)
							ReplaceMonthlyMeterImage.imageDate = new String[5];
						Bitmap bitmapTemp = null;
						// if( noticeStatus>=2 ){
						// if (listFaultImages.size()==5)
						// {
						// imgBtnCapture.setEnabled(false);
						// }
						// }
						for (int i = 0; i < listFaultImages.size(); i++) {
							if (listFaultImages.elementAt(i).getImageFileName() != null) {
								if (listFaultImages.elementAt(i).getImageData().length > 0) {
									bitmapTemp = BitmapFactory.decodeByteArray(listFaultImages.elementAt(i).getImageData(), 0, listFaultImages.elementAt(i)
											.getImageData().length);
									Bitmap resizedBitmapThumbail = Bitmap.createScaledBitmap(bitmapTemp, 75, 75, true);
									ReplaceMonthlyMeterImage.bitmapReplace[i] = bitmapTemp;
									imgViewThumbNail[i].setImageBitmap(resizedBitmapThumbail);
									imgViewThumbNail[i].setTag(i);

								} else {// get pic from web

									// dialogWait = ProgressDialog.show(this,
									// "e-Inspection System",
									// "Downloading...", true);

									BitmapFactory.Options bmOptions;
									bmOptions = new BitmapFactory.Options();
									bmOptions.inSampleSize = 1;

									// String strFileURL
									// =strWebURL+"Tenant Notice/"+Integer.toString(terminalid)+"/"
									// +Integer.toString(noticeid)+"/images/"
									// +listFaultImages.elementAt(i).getImageFileName();
									//

									// string strFileURL
									// =AppsConstant.WEB_IMAGE_ROOT_URL+webImageSubURI+"/"+listFaultImages.elementAt(i).getImageFileName();
									String strFileURL = main.WEBSERVER_IMAGEPATH + "/" + webImageSubURI + "/" + listFaultImages.elementAt(i).getImageFileName();

									System.out.println("Retrieving image from  " + strFileURL);
									bitmapTemp = LoadImage(strFileURL, bmOptions, dialogHandler);

									Bitmap resizedBitmappreview = bitmapTemp;
									ReplaceMonthlyMeterImage.bitmapReplace[i] = resizedBitmappreview;
									Bitmap resizedBitmapThumbail = Bitmap.createScaledBitmap(bitmapTemp, 75, 75, true);
									ReplaceMonthlyMeterImage.bitmapReplace[i] = bitmapTemp;
									imgViewThumbNail[i].setImageBitmap(resizedBitmapThumbail);
									imgViewThumbNail[i].setTag(i);

									// dialogWait.dismiss();

								}
								ReplaceMonthlyMeterImage.strCaption[i] = listFaultImages.elementAt(i).getImageCaption().toString();

								String tmpLoc = "";
								tmpLoc = listFaultImages.elementAt(i).getLongitude() + "," + listFaultImages.elementAt(i).getLatitude();
								ReplaceMonthlyMeterImage.gpslocation[i] = tmpLoc;
								DateUtil dUtil = new DateUtil();
								// ReplaceTenantNoticeImage.imageDate[i]=
								// dUtil.timeStamp2dateString2(listFaultImages.elementAt(i).getLastUpdateDate());

								if (i == 0) {
									imgViewPreview.setTag(0);
									imgViewPreview.setImageBitmap(bitmapTemp);
									Bitmap resizedBitmappreview1 = scaleBitmap(bitmapTemp);
									imgViewPreview.setImageBitmap(resizedBitmappreview1);
									txtCaption.setText(listFaultImages.elementAt(i).getImageCaption().toString());
								}

							}
						}// end for
						bitmap = ReplaceMonthlyMeterImage.bitmapReplace;

					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// showExceptionDialog("Unable to load image from web", e);

				if (dialogWait != null)
					dialogWait.dismiss();
				e.printStackTrace();
			}
			// }
			// search existing images

			if (ReplaceMonthlyMeterImage.bitmapReplace == null || ReplaceMonthlyMeterImage.bitmapReplace.length == 0) {
				linearLayoutPreview.setVisibility(View.GONE);
				linearLayoutThumb.setVisibility(View.GONE);
				linearLayoutCaption.setVisibility(View.GONE);
			} else {

				setImage(ReplaceMonthlyMeterImage.bitmapReplace);
			}

			lblFaultID.setText("Utilities Recording");

			
			if (Status==1)
			{
				imgBtnCapture.setVisibility(View.GONE);
			}
			else
			{
				imgBtnCapture.setVisibility(View.VISIBLE);
			}
				imgBtnScan.setVisibility(View.VISIBLE);
				linearLayoutSave.setVisibility(View.VISIBLE);
				txtCaption.setEnabled(true);
				txtCaption.setFocusable(true);
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the captured image from camera
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
			try {
				// To get the captured image
				// thumbnail = (Bitmap) data.getExtras().get("data");
				// Bitmap resizedBitmappreview = scaleBitmap(thumbnail);//
				// Bitmap.createScaledBitmap(thumbnail,
				// // 400,
				// // 370,
				// // true);

				// juehua image enhance
				Bitmap pic = getCorrectlyOrientedImage(getApplicationContext(), mImageCaptureUri);
				// Bitmap resizedBitmappreview = scaleBitmap(pic);
				ImageView imgViewPreviewTemp = new ImageView(this);
				// Bitmap resizedBitmappreview =
				// scaleBitmap(resizedBitmappreview);
				imgViewPreviewTemp.setImageBitmap(pic);

				ReplaceMonthlyMeterImage.imgViewTemp = imgViewPreviewTemp;
				ReplaceMonthlyMeterImage.bitmapReplace = bitmap;
				ReplaceMonthlyMeterImage.flagSetImage = true;

				Intent intentReplace = new Intent(MonthlyMeterImagesView.this, ReplaceMonthlyMeterImage.class);
				startActivityForResult(intentReplace, 123);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == CAMERA_PIC_REQUEST && resultCode != RESULT_OK) {
			if (ReplaceMonthlyMeterImage.bitmapReplace != null) {
				setImage(ReplaceMonthlyMeterImage.bitmapReplace);
				// if (iIndex == 4)
				// imgBtnCapture.setEnabled(false);
			}
		}

		else if (requestCode == 123 && resultCode == Activity.RESULT_OK) {

			if (ReplaceMonthlyMeterImage.bitmapReplace != null) {
				if (ReplaceMonthlyMeterImage.iMode == 0)
					iIndexTemp++;
				else if (ReplaceMonthlyMeterImage.iMode != 0)
					iIndexTemp++;
				iIndex = iIndexTemp;
				setImage(ReplaceMonthlyMeterImage.bitmapReplace);
				// if(iIndex==4)
				// imgBtnCapture.setEnabled(false);
			}

		}

		else if (requestCode == SCAN_BARCODE_REQUEST && resultCode == Activity.RESULT_OK) {

			Bundle extras = data.getExtras();
			scannedBarcode = (String) extras.get(CaptureActivity.INTENT_RESULT_LOCATION_CODE);

			String locCode = "";
			locCode = scannedBarcode;

			FaultDAO faultDao = new FaultDAO();
			try {
				LocationVO lvo = new LocationVO();
				lvo = faultDao.getLocIdbyLocCode(locCode);

				locationid = Integer.parseInt(lvo.getLocationId());
			} catch (ULjException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/**
	 * sets the captured image
	 */
	private void setImage(Bitmap[] bitmaptemp) {

		for (int i = 0; i < 5; i++) {
			if (bitmaptemp[i] != null) {

				imgViewThumbNail[i].setImageBitmap(Bitmap.createScaledBitmap(bitmaptemp[i], 75, 75, true));
				imgViewThumbNail[i].setTag(i);
			} else
				imgViewThumbNail[i].setImageBitmap(null);
		}

		if (iIndex >= 0) {
			if (linearLayoutPreview.getVisibility() != View.VISIBLE || linearLayoutThumb.getVisibility() != View.VISIBLE) {
				linearLayoutThumb.setVisibility(View.VISIBLE);
				linearLayoutPreview.setVisibility(View.VISIBLE);
				linearLayoutCaption.setVisibility(View.VISIBLE);
			}

			Bitmap resizedBitmappreview;
			if (monthlymeterno > 0) {
				// juehua image enhance
				// resizedBitmappreview =
				// Bitmap.createScaledBitmap(bitmaptemp[iIndex], 400, 350,
				// true);
				resizedBitmappreview = bitmaptemp[iIndex];
			} else {
				resizedBitmappreview = bitmaptemp[iIndex];
				iPreviewInd = iIndex;
			}

			imgViewPreview.setImageBitmap(resizedBitmappreview);
			imgViewPreview.setTag(iIndex);
			txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[iIndex].toString());

		}

		else {
			imgViewPreview.setImageBitmap(null);
			linearLayoutPreview.setVisibility(View.GONE);
			linearLayoutCaption.setVisibility(View.GONE);
			flagPhoto = false;
		}

	}

	
	/**
	 * creates the scaled bitmap for preview of the image
	 * 
	 * @param bitmap
	 * @return
	 */
	private Bitmap scaleBitmap(Bitmap bitmap) {
		int iWidth = bitmap.getWidth();
		int iHeight = bitmap.getHeight();
		int newWidth = 0, newHeight = 0;
		float fFactor = (float) iHeight / (float) iWidth;

		if (iWidth <= iHeight) {
			newWidth = 300;
			newHeight = (int) (newWidth * fFactor);// 370;
		} else {
			newWidth = 400;
			newHeight = (int) (newWidth * fFactor);// 370;
		}
		float fScaleWidth = ((float) newWidth) / iWidth;
		float fScaleHeight = ((float) newHeight) / iHeight;

		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth, iHeight, matrix, true);

		return resizedBitmappreview;
	}

	public void onClick(View v) {

		try {
			if (v == imgBtnCapture) // if login button is clicked
			{

				if ((ReplaceMonthlyMeterImage.bitmapReplace == null) || (ReplaceMonthlyMeterImage.bitmapReplace[4] == null)) {
					Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

					// juehua image enhance
					ContentValues values = new ContentValues();
					mImageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
					cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

					startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
				}

				else {
					Toast toast = Toast.makeText(MonthlyMeterImagesView.this, "Maximum 5 images are allowed.", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 40, 60);
					toast.show();
				}

			} else if (v == imgViewThumbNail[0]) {
				iPreviewInd = 0;
				imgViewPreview.setImageBitmap(ReplaceMonthlyMeterImage.bitmapReplace[0]);
				imgViewPreview.setTag(0);
				txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[0].toString());
			} else if (v == imgViewThumbNail[1]) {
				iPreviewInd = 1;
				// Bitmap resizedBitmappreview =
				// scaleBitmap(ReplaceImage.bitmapReplace[1]);
				imgViewPreview.setImageBitmap(ReplaceMonthlyMeterImage.bitmapReplace[1]);
				imgViewPreview.setTag(1);
				txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[1].toString());
			} else if (v == imgViewThumbNail[2]) {
				iPreviewInd = 2;
				imgViewPreview.setImageBitmap(ReplaceMonthlyMeterImage.bitmapReplace[2]);
				imgViewPreview.setTag(2);
				txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[2].toString());

			} else if (v == imgViewThumbNail[3]) {
				iPreviewInd = 3;
				imgViewPreview.setImageBitmap(ReplaceMonthlyMeterImage.bitmapReplace[3]);
				imgViewPreview.setTag(3);
				txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[3].toString());
			} else if (v == imgViewThumbNail[4]) {
				iPreviewInd = 4;
				imgViewPreview.setImageBitmap(ReplaceMonthlyMeterImage.bitmapReplace[4]);
				imgViewPreview.setTag(4);
				txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[4].toString());
			} else if (v == imgBtnDel) // delete button of alert is selected,
			{

				for (int i = iPhoto; i < ReplaceMonthlyMeterImage.bitmapReplace.length; i++) {

					if ((i + 1) < ReplaceMonthlyMeterImage.bitmapReplace.length) // removes
																					// the
																					// selected
																					// image
																					// from
																					// array
					{
						ReplaceMonthlyMeterImage.bitmapReplace[i] = ReplaceMonthlyMeterImage.bitmapReplace[i + 1];
						ReplaceMonthlyMeterImage.bitmapReplace[i + 1] = null;
						ReplaceMonthlyMeterImage.strCaption[i] = ReplaceMonthlyMeterImage.strCaption[i + 1];
						ReplaceMonthlyMeterImage.strCaption[i + 1] = null;
						ReplaceMonthlyMeterImage.gpslocation[i] = ReplaceMonthlyMeterImage.gpslocation[i + 1];
						ReplaceMonthlyMeterImage.gpslocation[i + 1] = null;
						ReplaceMonthlyMeterImage.imageDate[i] = ReplaceMonthlyMeterImage.imageDate[i + 1];
						ReplaceMonthlyMeterImage.imageDate[i + 1] = null;

					} else {
						ReplaceMonthlyMeterImage.bitmapReplace[i] = null;
						ReplaceMonthlyMeterImage.strCaption[i] = null;
						ReplaceMonthlyMeterImage.gpslocation[i] = null;
						ReplaceMonthlyMeterImage.imageDate[i] = null;

						linearLayoutPreview.setVisibility(View.GONE);
						linearLayoutCaption.setVisibility(View.GONE);
						linearLayoutThumb.setVisibility(View.GONE);
					}
				}
				dialogEdit.dismiss();
				iIndex--;
				iIndexTemp--;
				if (iIndex < 4)
					imgBtnCapture.setEnabled(true);
				setImage(ReplaceMonthlyMeterImage.bitmapReplace);

			} else if (v == imgBtnNoDel) // delete button of alert is selected,
			{
				dialogEdit.dismiss();
			} else if (v == imgBtnClose) {
				dialogEdit.dismiss();
			}

			else if (v == imgBtnCancel) // save button clicked.
			{

				// TenantNoticeDAO tdao = new TenantNoticeDAO();
				// int cntimg = 0;
				// cntimg = tdao.SelectImageCount(terminalid, readingno,
				// noticeid);
				// if (cntimg <= 0) {
				// tdao.resetTenantNoticeChecklistResults(terminalid, noticeid,
				// readingno, 1);
				// // TNChecklistView.TEMP_CHECKLIST_RESULT=null;
				// }

				super.onBackPressed();
			} else if (v == imgBtnSave) // save button clicked.
			{

				DateUtil dUtil = new DateUtil();

				MonthlyMeterBO tBO = new MonthlyMeterBO(MonthlyMeterImagesView.this);
				try {

					Log.d(TAG, "save monthly meter images - start");
					MonthlyMeterImagesVO voImg = new MonthlyMeterImagesVO();
					if (ReplaceMonthlyMeterImage.bitmapReplace != null) {
						// delete images from existing task
						MonthlyMeterDAO delimg = new MonthlyMeterDAO();
						voImg.setTerminalId(terminalid);
						voImg.setMonthlyMeterNo(monthlymeterno);
						voImg.setReadingNo(readingno);
						delimg.deleteImages(voImg);
					}

					MonthlyMeterDAO tDAO = new MonthlyMeterDAO();
					if (ReplaceMonthlyMeterImage.bitmapReplace != null) {
						for (int i = 0; i < ReplaceMonthlyMeterImage.bitmapReplace.length; i++) {
							if (ReplaceMonthlyMeterImage.bitmapReplace[i] != null) {

								// convert bitmap to bytes before send to voImg
								ByteArrayOutputStream bos = new ByteArrayOutputStream();
								ReplaceMonthlyMeterImage.bitmapReplace[i].compress(Bitmap.CompressFormat.JPEG, 50, bos);

								byte[] bitmapdata = bos.toByteArray();
								voImg.setTerminalId(terminalid);
								voImg.setMonthlyMeterNo(monthlymeterno);
								voImg.setReadingNo(readingno);
								voImg.setImageData(bitmapdata);
								voImg.setImageFileName("gotimage");
								voImg.setImageCaption(ReplaceMonthlyMeterImage.strCaption[i].toString());
								voImg.setLastUpdateDate(dUtil.getCurrentTimeStamp());
								voImg.setLastUpdatedBy(main.iCurrentUserInfoId);
								voImg.setMeterNo(meterno)	;
								voImg.setResultid(resultid);
							

								if (ReplaceMonthlyMeterImage.gpslocation[i] != null && !ReplaceMonthlyMeterImage.gpslocation[i].equals(",")
										&& !ReplaceMonthlyMeterImage.gpslocation[i].equals("")) {

									String[] gpsloc = ReplaceMonthlyMeterImage.gpslocation[i].toString().split(",");

									voImg.setLongitude(Float.parseFloat(gpsloc[0]));
									voImg.setLatitude(Float.parseFloat(gpsloc[1]));
								}

								tBO.createNewImage(voImg, i);

							}
						}
					}
					Log.d(TAG, "save fault images - end");

				} catch (ULjException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				iIndex = -1;
				iIndexTemp = -1;
				bitmap = null;
				ReplaceMonthlyMeterImage.bitmapReplace = null;
				ReplaceMonthlyMeterImage.strCaption = null;
				ReplaceMonthlyMeterImage.gpslocation = null;
				ReplaceMonthlyMeterImage.imageDate = null;

				finish(); // Ryan : cannot call finish as the sycnhing process
							// may be in progress, calling synching will cause
							// the error for the dialog box

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	/**
	 * handles long click of thumbnail image.
	 * it shows dialog for edit or delete.
	 */
	public boolean onLongClick(View v) {
		if (Status != 1)//completed disable edit image
		{
			try {
				// if (!bSwipe) {
				ImageView imgTemp = (ImageView) v;
				iPhoto = (Integer) imgTemp.getTag();
				// to show dialog on long click of thumbnail image.
				dialogEdit = new Dialog(this);
				Window window = dialogEdit.getWindow();
				window.requestFeature(Window.FEATURE_NO_TITLE);
				dialogEdit.setContentView(R.layout.dialogedit);
				dialogEdit.show();

				// replace and delete button of dialog
				imgBtnDel = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogDelete);

				imgBtnNoDel = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogNoDelete);

				// imgBtnReplace = (ImageButton) dialogEdit
				// .findViewById(R.id.imgBtnDialogEdit);

				imgBtnClose = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogClose);

				lblGPSLocation = (TextView) dialogEdit.findViewById(R.id.lblGPSLocation);
				txtGPSLocation = (TextView) dialogEdit.findViewById(R.id.txtGPSLocation);

				txtImageDate = (TextView) dialogEdit.findViewById(R.id.txtImageDate);

				imgBtnDel.setOnClickListener(this);
				imgBtnNoDel.setOnClickListener(this);

				// imgBtnReplace.setOnClickListener(this);
				imgBtnClose.setOnClickListener(this);

				// To get selected image
				imgEdit = new ImageView(getBaseContext());
				Bitmap bitmapTemp = ReplaceMonthlyMeterImage.bitmapReplace[iPhoto];
				imgEdit.setImageBitmap(bitmapTemp);

				if (ReplaceMonthlyMeterImage.gpslocation[iPhoto] != null && !ReplaceMonthlyMeterImage.gpslocation[iPhoto].equals(",")) {
					txtGPSLocation.setVisibility(View.VISIBLE);
					lblGPSLocation.setVisibility(View.VISIBLE);
					txtGPSLocation.setText("(" + ReplaceMonthlyMeterImage.gpslocation[iPhoto] + ")");
				} else {
					lblGPSLocation.setVisibility(View.GONE);
					txtGPSLocation.setText("");
					txtGPSLocation.setVisibility(View.GONE);

				}

				txtImageDate.setText(ReplaceMonthlyMeterImage.imageDate[iPhoto]);
				// }

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * to detect swipe gesture
	 */
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		try {
			if (vTemp == imgViewPreview) {
				// bSwipe = true;
				// left to right swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

					if (iPreviewInd < iIndex) {
						Animation sideInLeft = AnimationUtils.loadAnimation(MonthlyMeterImagesView.this, R.anim.slide_left_in);
						imgViewPreview.setAnimation(sideInLeft);
						imgViewPreview.startAnimation(sideInLeft);

						Bitmap resizedBitmappreview = Bitmap.createScaledBitmap(ReplaceMonthlyMeterImage.bitmapReplace[iPreviewInd + 1], 400, 350, true);
						imgViewPreview.setImageBitmap(resizedBitmappreview);
						txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[iPreviewInd + 1].toString());
						iPreviewInd = iPreviewInd + 1;
					}
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					// right to left swipe
					if (iPreviewInd > 0) {
						Animation sideInRight = AnimationUtils.loadAnimation(MonthlyMeterImagesView.this, R.anim.slide_left_out);
						imgViewPreview.setAnimation(sideInRight);
						imgViewPreview.startAnimation(sideInRight);

						Bitmap resizedBitmappreview = Bitmap.createScaledBitmap(ReplaceMonthlyMeterImage.bitmapReplace[iPreviewInd - 1], 400, 350, true);
						imgViewPreview.setImageBitmap(resizedBitmappreview);
						txtCaption.setText(ReplaceMonthlyMeterImage.strCaption[iPreviewInd - 1].toString());
						iPreviewInd = iPreviewInd - 1;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		vTemp = null;
		return true;
	}

	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Touch event of the activity
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (vTemp == imgViewPreview)
			return gestures.onTouchEvent(event);

		return false;
	}

	/**
	 * touch event of the image view preview
	 */
	public boolean onTouch(View v, MotionEvent event) {
		vTemp = null;
		// bSwipe = false;
		if (v == imgViewPreview) // if touched view is imageviewpreview
		{
			vTemp = imgViewPreview;
			if (gestures.onTouchEvent(event)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * check for mandatory field
	 * 
	 * @return
	 */

	public void doBarCodeScanning(View v) {
		Intent intent = new Intent(this, CaptureActivity.class);
		startActivityForResult(intent, SCAN_BARCODE_REQUEST); // Must use
																// startActivityForResult
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			iIndex = -1;
			iIndexTemp = -1;
			bitmap = null;
			ReplaceMonthlyMeterImage.bitmapReplace = null;
			ReplaceMonthlyMeterImage.strCaption = null;
			ReplaceMonthlyMeterImage.gpslocation = null;
			ReplaceMonthlyMeterImage.imageDate = null;

			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	public Handler dialogHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// check sync status
			// dialogWait.dismiss();

			Log.d(TAG, "dismissing dialog.............");
			// Exception ex = (Exception)
			// msg.getData().getSerializable("error");

		}
	};

	private Bitmap LoadImage(String URL, BitmapFactory.Options options, Handler handler) throws Exception {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			in.close();
		} catch (Exception e1) {
			e1.printStackTrace();
			showMessageDialog("Image not found");
			throw e1;
		}
		handler.sendEmptyMessage(0);
		return bitmap;
	}

	private InputStream OpenHttpConnection(String strURL) throws Exception {
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inputStream = httpConn.getInputStream();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return inputStream;
	}

	protected void showMessageDialog(String messageToShow) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayMessage(this, messageToShow);
		dUtil = null;
	}

	// juehua image enhance
	public Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
		InputStream is = context.getContentResolver().openInputStream(photoUri);
		BitmapFactory.Options dbo = new BitmapFactory.Options();
		dbo.inJustDecodeBounds = false;
		// Bitmap srcBitmap1 = BitmapFactory.decodeStream(is, null, dbo);

		dbo.inSampleSize = 4;
		Bitmap srcBitmap = BitmapFactory.decodeStream(this.getContentResolver().openInputStream(photoUri), null, dbo);
		
		is.close();
		int orientation = 0;
		String realPath = getRealPathFromURI(mImageCaptureUri);
		Log.v(TAG, "realPath: " + realPath);
		ExifInterface exif = new ExifInterface(realPath); // Since API Level 5
		String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		switch (Integer.parseInt(exifOrientation)) {
		case 3:
			orientation = 180;
			break;
		case 6:
			orientation = 90;
			break;
		case 8:
			orientation = 270;
			break;
		}
		Log.v(TAG, "orientation: " + orientation);
		if (orientation > 0) {
			Matrix matrix = new Matrix();
			matrix.postRotate(orientation);
			srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
		}
		File file = new File(realPath);
		boolean deleted = file.delete();
		Log.v(TAG, "realPath deleted: " + deleted);
		//Log.v(TAG, "image saved: " + saveBitmap2file(srcBitmap,"test"));
		
		return srcBitmap;
	}

	// juehua image enhance
	public String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(contentUri, proj, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	static boolean saveBitmap2file(Bitmap bmp, String filename) {
		CompressFormat format = Bitmap.CompressFormat.JPEG;
		int quality = 100;
		OutputStream stream = null;
		try {
			stream = new FileOutputStream("/mnt/sdcard/" + filename);
		} catch (FileNotFoundException e) {
			Log.e("", "", e);
		}
		return bmp.compress(format, quality, stream);
	}
}
