package sg.com.surbana.acs.einsp.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *July 6, 2011		Lee Ming			Created
 *
 */
public class MonthlyMeterImagesVO implements Serializable {
	private static final long serialVersionUID = -6114205509027782382L;
	private final String TAG = "MonthlyMeterImagesVO.java";

	private int TerminalId;
	private int MonthlyMeterNo;
	
	private int MeterNo;
	private int ReadingNo;
	
	private String imageFileName;
	private byte[] imageData;
	private String imageCaption;
	private float latitude;
	private float longitude;
	
	private Timestamp lastUpdateDate;
	private int lastUpdatedBy;
	private int resultid;

	
	
	
	
	public int getTerminalId() {
		return TerminalId;
	}

	public void setTerminalId(int terminalId) {
		TerminalId = terminalId;
	}

	public int getMonthlyMeterNo() {
		return MonthlyMeterNo;
	}

	public void setMonthlyMeterNo(int monthlyMeterNo) {
		MonthlyMeterNo = monthlyMeterNo;
	}

	public int getResultid() {
		return resultid;
	}

	public void setResultid(int resultid) {
		this.resultid = resultid;
	}
	
	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

	public String getImageCaption() {
		return imageCaption;
	}

	public void setImageCaption(String imageCaption) {
		this.imageCaption = imageCaption;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	
	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public int getMeterNo() {
		return MeterNo;
	}

	public void setMeterNo(int meterNo) {
		MeterNo = meterNo;
	}

	public int getReadingNo() {
		return ReadingNo;
	}

	public void setReadingNo(int readingNo) {
		ReadingNo = readingNo;
	}

}
