package sg.com.surbana.acs.einsp.view.annotateshapes;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Aug 16, 2011		Ryan			Created
 *
 */
public class ShapeCircle extends BaseShape {
	private final String TAG = "AbstractShape.java";

	Matrix mat = new Matrix();

	public ShapeCircle(Canvas can, Paint p) {
		super(can, p);
	}

	public ShapeCircle() {
		super();
	}

	public void drawMe(Canvas can, float touchX, float touchY) {
		x = touchX;
		y = touchY;
		shpCanvas = can;
		drawMe(can);
	}

	public void drawMe() {

		shpPath.reset();
		shpPath.moveTo(x, y);
		shpPath.lineTo(x + 50, y - 50);
		shpPath.lineTo(x + 50, y - 30);
		shpPath.moveTo(x + 50, y - 50);
		shpPath.lineTo(x + 30, y - 50);
		// mPath.quadTo(mX, mX, mX+10, mY+10);
		// mat.setRotate(rotationAngle);
		shpPath.transform(mat);

		// shpCanvas.drawPath(shpPath,shpPaint);
	}

	public void drawMe(Canvas can) {

		//
		shpPath.reset();
		shpPath.moveTo(x, y);
		shpPath.lineTo(x + 50, y - 50);
		shpPath.lineTo(x + 50, y - 30);
		shpPath.moveTo(x + 50, y - 50);
		shpPath.lineTo(x + 30, y - 50);
		// mPath.quadTo(mX, mX, mX+10, mY+10);
		// mat.setRotate(rotationAngle);
		// shpPath.transform(mat);
		// can.drawCircle(x, y, 30, shpPaint);
		can.drawCircle(x, y, 30 * rscale, shpPaint);

	}

}
