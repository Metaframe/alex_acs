package sg.com.surbana.acs.einsp.view;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.bo.LoginBO;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.CommonFunction;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.util.SynchronizationUtils;
import sg.com.surbana.acs.einsp.vo.UserInfoVO;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *28 Jul 2011		Ryan			Use common Menu
 *09 Apr 2013 		Ryan			use doLogout() function in base class to prevent error when logging out.
 *
 */
public class HomeView extends BaseView {
	private final String TAG = "HomeView.java";

	Button btnCallCheckList;
	//llm 22/04/2013 commercial fault
	Button btnCallFault;
	Button btnLogout;
	Button btnInformation;
	
	boolean isDownloadOnly = false; // All the download scripts in the screen
									// should be synced with
									// isDownloadOnly=false
	Button btnEngChecklist;
	
	SynchronizationUtils syncUtil = new SynchronizationUtils();

	// Juehua 20140708 delete gallery 6 months ago
	public static int monthsBetween(Date minuend, Date subtrahend) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(minuend);
		int minuendMonth = cal.get(Calendar.MONTH);
		int minuendYear = cal.get(Calendar.YEAR);
		cal.setTime(subtrahend);
		int subtrahendMonth = cal.get(Calendar.MONTH);
		int subtrahendYear = cal.get(Calendar.YEAR);
		return ((minuendYear - subtrahendYear) * (cal.getMaximum(Calendar.MONTH) + 1)) + (minuendMonth - subtrahendMonth);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Juehua 20140708 delete gallery 6 months ago
		new Thread() {
			public void run() {
				try {
					String path = Environment.getExternalStorageDirectory().toString() + "/DCIM/Camera";
					File f = new File(path);
					File file[] = f.listFiles();
					if (file != null) {
						for (int i = 0; i < file.length; i++) {
							if (monthsBetween(new Date(), new Date(file[i].lastModified())) >= 6) {
								file[i].delete();
							}
						}
					}
				} catch (Exception e) {
					Log.e(TAG, "", e);
				}
			}
		}.start();

		// Juehua 20140214 done for prevent AES home page change to FMC one when crash
		if (main.strCurrentUserId == null) {
			finish();
		}

		CommonFunction localCommonFunction = new CommonFunction();
		UserInfoVO localUserInfoVO = new UserInfoVO();
		try {
			localUserInfoVO = localCommonFunction.getUserInfoById(main.strCurrentUserId);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		Log.v(TAG, "go to home engineering");
		setContentView(R.layout.home_engineering);

		btnEngChecklist = (Button) findViewById(R.id.btnEngChecklist);
			

	}



	public void doSync(View v) {
		SynchronizationUtils syncUtil = new SynchronizationUtils();

		boolean isDownloadOnly = false;
		Log.d(TAG, "Do Sync");
		CommonFunction comFun = new CommonFunction();
		if (comFun.isOnline(this.getApplicationContext())) {
			try {
				syncUtil.synchronizedDoSyncForVersion(AppsConstant.VERS_SYNC, AppsConstant.SUB_VERS_SYNC, isDownloadOnly, DbUtil.getConnection(),
						HomeView.this, null);
			} catch (Exception e) {
				Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
			}

		} else {
			Toast toast = Toast
					.makeText(getBaseContext(), "Internet connection not detected.\nSynchronizing data to server cannot be done.", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 40, 60);
			toast.show();

		}
	}



	@Override
	public void onBackPressed() { 
	
		 if (main.iCurrentUserGroupId!=1)
		 {
			 showLogoutDialog();
		 }
	
		 else
		 super.onBackPressed(); //if dashboard included
	}

	private void showLogoutDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(HomeView.this);
		builder.setMessage("Are you sure you want to exit from Mobile Inspection System").setTitle("Mobile Inspection System").setCancelable(true)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
					
						doLogout();
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();

	}

	public void gotoMonthlyMeter(View v) {
		
		try {
			Intent intentToGoAfterSync = new Intent(HomeView.this, ScheduleView.class);
			Bundle myData = new Bundle();
			myData.putString("frButton", "MonthlyMeter");
			intentToGoAfterSync.putExtras(myData);

			intentToGoAfterSync.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			Log.d(TAG, "INIT *****");

//
			startActivity(intentToGoAfterSync);
//			syncUtil.synchronizedDoSyncForVersion(AppsConstant.VERS_DOWNLOAD_MONTHLY_METER, AppsConstant.SUB_VERS_MONTHLY_METER_DOWNLOAD, isDownloadOnly,
//					DbUtil.getConnection(), HomeView.this, intentToGoAfterSync);
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	
	public void doRefresh(View v)
	{
	
		
		DbUtil dbUtil = new DbUtil();
		try {
			dbUtil.refreshNewDB();
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoginBO bo = new LoginBO();
		try {
			//false bcoz already delete udb above; need to reinsert userid
			bo.doProcessIfUserExist(false, main.strCurrentUserId);
		} catch (ULjException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("Calling Init Function");
		try {
			syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_INIT, AppsConstant.SUB_VERS_INIT, isDownloadOnly,
					DbUtil.getConnection(), HomeView.this, initCallBack);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//initCallBack
		
	}
	
	Handler initCallBack = new Handler() {
		public void handleMessage(Message msg) {
			Bundle bun;
			bun = msg.getData();
			if (bun != null) {
				if (bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != 0 && bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != AppsConstant.HANDLER_VALUE_STATUS_ERROR) {
					// juehua for auth imei
					LoginBO loginBO = new LoginBO();
					try {
						if (!loginBO.authenticateIMEI(main.strDeviceID)) {
							showMessageDialog("Your device is not authorized to use this system");
							
						} else {
							//checkUserAuthentication();
							showMessageDialog("Synchronization done");
						}
					} catch (ULjException e) {
						// should not catch error again
					}
					
				} else {
					showMessageDialog("Unable to perform Init synchronization, please contact adminitrator if problem persist");
					
				}
			}
		}
	};
	
}
