package sg.com.surbana.acs.einsp.vo;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 16, 2011		Ryan			Created
 *
 */
public class ChecklistResultVO {
	private final String TAG = "ChecklistResultVO.java";
	int resultId;
	int checklistItemId;
	int groupId;
	int status;
	String remarks;
	String serviceDescription;

	public int getResultId() {
		return resultId;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public int getChecklistItemId() {
		return checklistItemId;
	}

	public void setChecklistItemId(int checklistItemId) {
		this.checklistItemId = checklistItemId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

}
