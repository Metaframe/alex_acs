package sg.com.surbana.acs.einsp.view;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.dao.FaultDAO;
import sg.com.surbana.acs.einsp.util.ApiConnectionUtil;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.CommonFunction;
import sg.com.surbana.acs.einsp.util.CustomArrayAdapterWithText;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.util.RequestCommand;
import sg.com.surbana.acs.einsp.util.SynchronizationUtils;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.ImageMapVO;
import sg.com.surbana.acs.einsp.vo.ImageSendDataVO;
import sg.com.surbana.acs.einsp.vo.MeterSendVO;
import sg.com.surbana.acs.einsp.vo.ScheduleList2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleListSendVO;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ianywhere.ultralitejni12.ULjException;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jul 28, 2011		Ryan			Created
 *									Base View class to contain common function of all view
 *30 Nov 2012		Ryan			Added Handler to handle the logout action after synching access log
 *03 Dec 2012		Ryan			change the access modifier of doLogout() to 'Protected' to allow access from child class
 *12 Feb 2013		Ryan			Set to use the network time if it is not set
 *
 */
public class BaseView extends Activity {
	private final String TAG = "BaseView.java";
	private Dialog dialogLocation;
	private Spinner btnTerminal, btnLevel, btnZone, btnArea, btnLocation;
	private ImageButton imgBtnDialogLocationClose, imgBtnDialogLocationOK, imgBtnDialogLocationCancel;

	protected int spnTerminalid, spnLevelid, spnAreaid, spnZoneid, spnLocationId, spnTenantId;
	protected int spnServiceId;//llm 14/03/2013 get disable terminal spinner when is terminal check

	private Spinner btnCompany, btnStation, btnHydrant;
	protected int spnCompanyID, spnStationID, spnHydrantID;
	private Dialog dialogHydrant;
	protected int spnGateID;
	private Dialog dialogGate;
	private ImageButton imgBtnDialogHydrantClose, imgBtnDialogHydrantOK, imgBtnDialogHydrantCancel;

	private ProgressDialog loadingProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		loadingProgressDialog = new ProgressDialog(this);
		loadingProgressDialog.setCancelable(false);
		loadingProgressDialog.setMessage("loading . . .");

		ApiConnectionUtil apiConnectionUtil = new ApiConnectionUtil(this);
		apiConnectionUtil.getImageVO(main.strCurrentUserId);
		try {
			if(apiConnectionUtil.getScheduleListDAO().countOf() > 0) {
				QueryBuilder<ScheduleList2VO, Integer> scheduleList2VOIntegerQueryBuilder = apiConnectionUtil.getScheduleListDAO().queryBuilder();
				List<ScheduleList2VO> scheduleList2VOList = scheduleList2VOIntegerQueryBuilder.query();
				DeleteBuilder<ScheduleList2VO, Integer> deleteScheduleBuilder = apiConnectionUtil.getScheduleListDAO().deleteBuilder();
				DateUtil dateUtil = new DateUtil();
				for(ScheduleList2VO vo : scheduleList2VOList) {
					if(vo.getActualEnd() != null && vo.getActualEnd() != "") {
						Date date = new Date(dateUtil.dateString2Calendar(vo.getActualEnd(), "MM/dd/yyyy HH:mm:ss").getTime());
						Calendar actualEnd = Calendar.getInstance();
						actualEnd.setTime(date);
						actualEnd.add(Calendar.DATE, 1);
						Calendar currentDay = Calendar.getInstance();
						currentDay.setTime(dateUtil.getCurrentDate());
						if (actualEnd.after(currentDay)) {
							apiConnectionUtil.getScheduleListDAO().delete(vo);
						}
					}
				}
			}
			if(apiConnectionUtil.getScheduleListSendDAO().countOf() > 0) {
				QueryBuilder<ScheduleListSendVO, Integer> scheduleQueryBuilder = apiConnectionUtil.getScheduleListSendDAO().queryBuilder();
				List<ScheduleListSendVO> scheduleListSendVOs = scheduleQueryBuilder.where().eq("isOffline",false).query();
				for(ScheduleListSendVO vo : scheduleListSendVOs) {
					apiConnectionUtil.updateScheduleTime(vo);
				}
			}
			if(apiConnectionUtil.getImageSendDAO().countOf() > 0) {
				QueryBuilder<ImageSendDataVO, Integer> imagesendQueryBuilder = apiConnectionUtil.getImageSendDAO().queryBuilder();
				List<ImageSendDataVO> imageSendDataVOs = imagesendQueryBuilder.where().eq("isOffline",false).query();
				for(ImageSendDataVO vo : imageSendDataVOs) {
					apiConnectionUtil.sendImageData(vo);
				}
			}
			if(apiConnectionUtil.getMeterSendDAO().countOf() > 0) {

				List<MeterSendVO> meterSendVOs = apiConnectionUtil.getMeterSendDAO().queryBuilder().where().eq("isOffline",false).query();
				for(MeterSendVO vo : meterSendVOs) {
					if(apiConnectionUtil.isExistingMeterNo(vo.getsMonthlyMeterNo(), vo.getsReadingNo(), vo.getSMeterNo(), vo.getSChecklistResultId())) {
						apiConnectionUtil.updateReadingHistory(vo);
					} else {
						apiConnectionUtil.insertReadingHistory(vo);
					}
				}
			}
			RequestCommand requestCommand = new RequestCommand(this);
			if(apiConnectionUtil.getImageMapDAO().countOf() > 0) {
				List<ImageMapVO> imageMapVOs = apiConnectionUtil.getImageMapDAO().queryBuilder().where().eq("isOffline",false).query();
				for(ImageMapVO vo : imageMapVOs) {
					if(vo.getFileName().contains("_Signature")) {
						if (apiConnectionUtil.isExistingMeterNo(vo.getMonthlyMeterNo(), vo.getReadingNo(), vo.getMeterNo(), vo.getCheckResultID())) {
							requestCommand.add(apiConnectionUtil.ImageUpload(AppsConstant.URL_UPDATE_SIGN_IMAGE, vo.imageToMap()));
						} else {
							requestCommand.add(apiConnectionUtil.ImageUpload(AppsConstant.URL_INSERT_SIGN_IMAGE, vo.imageToMap()));
						}
					} else {
						requestCommand.add(apiConnectionUtil.ImageUpload(AppsConstant.URL_METER_IMAGE, vo.imageToMap()));
					}
				}

				requestCommand.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.homemenu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			super.onOptionsItemSelected(item);
			int itemID = item.getItemId();
			// title to display is taken from current menu item
			String msg = item.getTitle().toString();
			if (msg.equals("Logout"))
				doLogout();
			if (msg.equals("About"))
				showinfo();
			if (msg.equals("Home"))
				goHome();

		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();

		}
		return false;
	}// onOptionsItemSelected

	//Ryan : change the access modifier to 'Protected' to allow access from child class
	protected void doLogout() {
		try {
	   //Ryan : Use handler to complete logout action 
			
//			finish();
//			Intent intent = new Intent(this, main.class);
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			// insert logout into accesslog table llm 23/11/12
			
			try {
				SynchronizationUtils syncUtil = new SynchronizationUtils();
				syncUtil.setSyncHost(); //llm 19/07/2013
				CommonFunction comFunction = new CommonFunction();
				comFunction.insertAccessLog(0, "Logout");
				syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_UPLOAD_ACCESS_LOG, AppsConstant.SUB_VERS_ACCESS_LOG_UPLOAD, false,
						DbUtil.getConnection(),BaseView.this, processLogoutHandler);

			} catch (ULjException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//llm 2015/01/20 if mobilink down. allow to logout
				if(e.getErrorCode()==-1305)
				{
					finish();
					Intent intent = new Intent(BaseView.this, main.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
			}
			// insert logout into accesslog table llm 23/11/12
//			startActivity(intent);


		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	private void goHome() {
		try {
			//finish();
			Intent intent = new Intent(this, HomeView.class);
			//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		     //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	
	private void showinfo() {
		try {
			AlertDialog diaBox = makeAndShowDialogBox("Mobile Inspection System", "Copyright 2011\n" + "All rights reserved\n" + "Surbana Technologies Pte Ltd\n"
					+ "168 Jln Bukit Merah\n" + "#01-01 Surbana One\n" + "Singapore 150168\n" + "Tel: +65 6248 1540");
			diaBox.show();

		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	private AlertDialog makeAndShowDialogBox(String sTitle, String sMsg) {

		AlertDialog myQuittingDialogBox =

		new AlertDialog.Builder(this)
		// set message, title, and icon
				.setTitle(sTitle).setMessage(sMsg)

				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// whatever should be done when answering "NO" goes here
						// msg = "Cancel " + Integer.toString(whichButton);
						// txtMsg.setText(msg);
					}
				})

				.create();

		return myQuittingDialogBox;
	}

	protected void showExceptionDialog(String errorMsg, Exception e) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayException(this, errorMsg, e);
		dUtil = null;
	}

	protected void showMessageDialog(String messageToShow) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayMessage(this, messageToShow);
		dUtil = null;
	}

	protected void showMessageDialogWithConfirmation(String messageToShow) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayAlertDialog(this, "Confirmation", messageToShow);
		dUtil = null;
	}


	public void showLocationSelection(View v) {

		dialogLocation = new Dialog(this);
		Window window = dialogLocation.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		dialogLocation.setContentView(R.layout.dialoglocation);
		dialogLocation.show();

		// replace and delete button of dialog
		btnTerminal = (Spinner) dialogLocation.findViewById(R.id.btnTerminal);
		btnLevel = (Spinner) dialogLocation.findViewById(R.id.btnLevel);
		btnZone = (Spinner) dialogLocation.findViewById(R.id.btnZone);
		btnArea = (Spinner) dialogLocation.findViewById(R.id.btnArea);
		btnLocation = (Spinner) dialogLocation.findViewById(R.id.btnLocation);

		imgBtnDialogLocationClose = (ImageButton) dialogLocation.findViewById(R.id.imgBtnDialogLocationClose);
		imgBtnDialogLocationOK = (ImageButton) dialogLocation.findViewById(R.id.imgBtnDialogLocationOK);
		imgBtnDialogLocationCancel = (ImageButton) dialogLocation.findViewById(R.id.imgBtnDialogLocationCancel);

		imgBtnDialogLocationClose.setOnClickListener(new doCancelLocation());
		imgBtnDialogLocationCancel.setOnClickListener(new doCancelLocation());
		
		if ( v.getTag()!=null)
		{
			//llm 03/07/2013 add location button for job start adhoc inspection
			if (v.getTag().equals("LocationFilter") || v.getTag().equals("btnJobStartLocation"))
			{
				imgBtnDialogLocationOK.setTag(imgBtnDialogLocationOK);
				imgBtnDialogLocationOK.setOnClickListener(new doSelectLocationFilter());
			}	
		}
		else
		{
			imgBtnDialogLocationOK.setOnClickListener(new doSelectLocation());
		}
		btnTerminal.setOnItemSelectedListener(new terminalOnItemSelectedListener());
		btnLevel.setOnItemSelectedListener(new levelOnItemSelectedListener());
		btnArea.setOnItemSelectedListener(new areaOnItemSelectedListener());
		btnZone.setOnItemSelectedListener(new zoneOnItemSelectedListener());

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> terminalListing;
		terminalListing = getTerminalList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> levelListing;
		levelListing = getLevelList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> areaListing;
		areaListing = getAreaList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> zoneListing;
		zoneListing = getZoneList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> locationListing;
		
		//llm 21/05/2013 getlocation with tenant name for commercial user
		if (AppsConstant.USER_GROUP_COMMERCIAL == main.iCurrentUserGroupId)
		{locationListing = getLocationWithTenantList();}
		else
		{locationListing = getLocationList();}
		
		
		CustomArrayAdapterWithText terminalAdapter = new CustomArrayAdapterWithText(getApplicationContext(), terminalListing);
		CustomArrayAdapterWithText levelAdapter = new CustomArrayAdapterWithText(getApplicationContext(), levelListing);
		CustomArrayAdapterWithText areaAdapter = new CustomArrayAdapterWithText(getApplicationContext(), areaListing);
		CustomArrayAdapterWithText zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), zoneListing);
		CustomArrayAdapterWithText locationAdapter = new CustomArrayAdapterWithText(getApplicationContext(), locationListing);

		btnTerminal.setAdapter(terminalAdapter);
		btnLevel.setAdapter(levelAdapter);
		btnArea.setAdapter(areaAdapter);
		btnZone.setAdapter(zoneAdapter);
		btnLocation.setAdapter(locationAdapter);
		
		//llm 14/03/2013 for commercial user terminal checklist
		if (spnTerminalid != 0) {
			// set selected value of zoneid
			int pos2 = 0;
			for (int i = 0; i < terminalListing.size(); i++) {
				DropdownItemVO a = (DropdownItemVO) btnTerminal.getItemAtPosition(i);
				int tmpid2 = Integer.parseInt(a.getItemId());
				if (tmpid2 == spnTerminalid) {
					pos2 = i;
					i = terminalListing.size() - 1;
				}
			}
			btnTerminal.setSelection(pos2);
		}
		
		//llm 14/03/2013 to disable terminal selection when is terminal check
		if (spnServiceId==AppsConstant.TERMINAL_CHECKLIST_SERVICE_ID)
		{btnTerminal.setEnabled(false);}
	}
	
	public void showLocationSelection2(View v) {

		dialogLocation = new Dialog(this);
		Window window = dialogLocation.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		dialogLocation.setContentView(R.layout.dialoglocation);
		dialogLocation.show();

		// replace and delete button of dialog
		btnTerminal = (Spinner) dialogLocation.findViewById(R.id.btnTerminal);
		btnLevel = (Spinner) dialogLocation.findViewById(R.id.btnLevel);
		btnZone = (Spinner) dialogLocation.findViewById(R.id.btnZone);
		btnArea = (Spinner) dialogLocation.findViewById(R.id.btnArea);
		btnLocation = (Spinner) dialogLocation.findViewById(R.id.btnLocation);

		imgBtnDialogLocationClose = (ImageButton) dialogLocation.findViewById(R.id.imgBtnDialogLocationClose);
		imgBtnDialogLocationOK = (ImageButton) dialogLocation.findViewById(R.id.imgBtnDialogLocationOK);
		imgBtnDialogLocationCancel = (ImageButton) dialogLocation.findViewById(R.id.imgBtnDialogLocationCancel);

		imgBtnDialogLocationClose.setOnClickListener(new doCancelLocation());
		imgBtnDialogLocationCancel.setOnClickListener(new doCancelLocation());
		
		if ( v.getTag()!=null)
		{
			//llm 03/07/2013 add location button for job start adhoc inspection
			if (v.getTag().equals("LocationFilter") || v.getTag().equals("btnJobStartLocation"))
			{
				imgBtnDialogLocationOK.setTag(imgBtnDialogLocationOK);
				imgBtnDialogLocationOK.setOnClickListener(new doSelectLocationFilter());
			}	
		}
		else
		{
			imgBtnDialogLocationOK.setOnClickListener(new doSelectLocation());
		}
		btnTerminal.setOnItemSelectedListener(new terminalOnItemSelectedListener());
		btnLevel.setOnItemSelectedListener(new levelOnItemSelectedListener());
		btnArea.setOnItemSelectedListener(new areaOnItemSelectedListener());
		btnZone.setOnItemSelectedListener(new zoneOnItemSelectedListener());

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> terminalListing;
		terminalListing = getFaultTerminalList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> levelListing;
		levelListing = getFaultLevelList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> areaListing;
		areaListing = getFaultAreaList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> zoneListing;
		zoneListing = getFaultZoneList();

		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> locationListing;
		locationListing = getFaultLocationList();
		
		
		CustomArrayAdapterWithText terminalAdapter = new CustomArrayAdapterWithText(getApplicationContext(), terminalListing);
		CustomArrayAdapterWithText levelAdapter = new CustomArrayAdapterWithText(getApplicationContext(), levelListing);
		CustomArrayAdapterWithText areaAdapter = new CustomArrayAdapterWithText(getApplicationContext(), areaListing);
		CustomArrayAdapterWithText zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), zoneListing);
		CustomArrayAdapterWithText locationAdapter = new CustomArrayAdapterWithText(getApplicationContext(), locationListing);

		btnTerminal.setAdapter(terminalAdapter);
		btnLevel.setAdapter(levelAdapter);
		btnArea.setAdapter(areaAdapter);
		btnZone.setAdapter(zoneAdapter);
		btnLocation.setAdapter(locationAdapter);
		
		//llm 14/03/2013 for commercial user terminal checklist
		if (spnTerminalid != 0) {
			// set selected value of zoneid
			int pos2 = 0;
			for (int i = 0; i < terminalListing.size(); i++) {
				DropdownItemVO a = (DropdownItemVO) btnTerminal.getItemAtPosition(i);
				int tmpid2 = Integer.parseInt(a.getItemId());
				if (tmpid2 == spnTerminalid) {
					pos2 = i;
					i = terminalListing.size() - 1;
				}
			}
			btnTerminal.setSelection(pos2);
		}
		
		//llm 14/03/2013 to disable terminal selection when is terminal check
		if (spnServiceId==AppsConstant.TERMINAL_CHECKLIST_SERVICE_ID)
		{btnTerminal.setEnabled(false);}
		

	}


	public class levelOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			try {
				ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> listing;

				DropdownItemVO x = (DropdownItemVO) btnLevel.getSelectedItem();
				String tmpid = x.getItemId();

				listing = getZoneList();
				CustomArrayAdapterWithText zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnZone.setAdapter(zoneAdapter);

				listing = getLocationList();
				CustomArrayAdapterWithText locationAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnLocation.setAdapter(locationAdapter);

				if (!tmpid.equals("0")) {
					listing = getAreaList(Integer.parseInt(tmpid));
					CustomArrayAdapterWithText areaAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
					btnArea.setAdapter(areaAdapter);

					if (spnAreaid != 0) {
						// set selected value of spnAreaid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnArea.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnAreaid) {
								pos2 = i;
								i = listing.size() - 1;
							}

						}
						btnArea.setSelection(pos2);

					}

					if (spnZoneid != 0) {
						// set selected value of spnZoneid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnZone.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnZoneid) {
								pos2 = i;
								i = listing.size() - 1;
							}
						}
						btnZone.setSelection(pos2);

					}

					if (spnLocationId != 0) {
						// set selected value of zoneid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnLocation.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnLocationId) {
								pos2 = i;
								i = listing.size() - 1;
							}
						}
						btnLocation.setSelection(pos2);
					}

				}
				else
				{
					btnArea.setSelection(0);
					btnZone.setSelection(0);
					btnLocation.setSelection(0);
					
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		public void onNothingSelected(AdapterView<?> parent) { // Do nothing.
		}
	}

	public class areaOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			try {
				ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> listing;
				DropdownItemVO x = (DropdownItemVO) btnArea.getSelectedItem();
				String tmpid = x.getItemId();

				listing = getZoneList();
				CustomArrayAdapterWithText zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnZone.setAdapter(zoneAdapter);

				listing = getLocationList();
				CustomArrayAdapterWithText locationAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnLocation.setAdapter(locationAdapter);

				if (!tmpid.equals("0")) {
					listing = getZoneList(Integer.parseInt(tmpid));
					zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
					btnZone.setAdapter(zoneAdapter);

					if (spnZoneid != 0) {
						// set selected value of spnZoneid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnZone.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnZoneid) {
								pos2 = i;
								i = listing.size() - 1;
							}
						}
						btnZone.setSelection(pos2);
					}

					if (spnLocationId != 0) {
						// set selected value of spnLocationId
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnLocation.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnLocationId) {
								pos2 = i;
								i = listing.size() - 1;
							}
						}
						btnLocation.setSelection(pos2);
					}

				}
				else
				{
					
					btnZone.setSelection(0);
					btnLocation.setSelection(0);
					
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		public void onNothingSelected(AdapterView<?> parent) { // Do nothing.
		}
	}

	public class zoneOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			try {
				ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> locationListing;

				DropdownItemVO x = (DropdownItemVO) parent.getSelectedItem();
				String tmpid = x.getItemId();

				if (!tmpid.equals("0")) {
					//llm 21/05/2013 getlocation with tenant name for commercial user
					if (AppsConstant.USER_GROUP_COMMERCIAL == main.iCurrentUserGroupId)
					{locationListing = getLocationWithTenantList(Integer.parseInt(tmpid));}
					else
					{locationListing = getLocationList(Integer.parseInt(tmpid));}

					CustomArrayAdapterWithText zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), locationListing);
					btnLocation.setAdapter(zoneAdapter);

					if (spnLocationId != 0) {
						// set selected value of spnLocationId
						int pos2 = 0;
						for (int i = 0; i < locationListing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnLocation.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnLocationId) {
								pos2 = i;
								i = locationListing.size() - 1;
							}
						}
						btnLocation.setSelection(pos2);
					}
					
				}
				else
				{
					btnLocation.setSelection(0);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		public void onNothingSelected(AdapterView<?> parent) { // Do nothing.
		}
	}

	private ArrayList<DropdownItemVO> getLevelList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Level");
		list.add(vo);

		return list;
	}
	
	private ArrayList<DropdownItemVO> getFaultLevelList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Level");
		list.add(vo);

		return list;
	}


	private ArrayList<DropdownItemVO> getLevelList(int terminalid) {

		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		
			
		try {
			
				list = faultDao.getLevel(terminalid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	private ArrayList<DropdownItemVO> getFaultLevelList(int terminalid) {

		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
	
			
		try {
		
				list = faultDao.getLevel(terminalid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	private ArrayList<DropdownItemVO> getAreaList(int levelid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
	
		try {
			
				list = faultDao.getArea(levelid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	private ArrayList<DropdownItemVO> getFaultAreaList(int levelid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
	
		try {
				list = faultDao.getFaultArea(levelid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	private ArrayList<DropdownItemVO> getAreaList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Area");
		list.add(vo);

		return list;
	}
	private ArrayList<DropdownItemVO> getFaultAreaList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Area");
		list.add(vo);

		return list;
	}

	private ArrayList<DropdownItemVO> getZoneList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Zone");
		list.add(vo);

		return list;
	}
	private ArrayList<DropdownItemVO> getFaultZoneList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Zone");
		list.add(vo);

		return list;
	}

	private ArrayList<DropdownItemVO> getZoneList(int areaid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		
		try {
			
				list = faultDao.getZone(areaid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	private ArrayList<DropdownItemVO> getFaultZoneList(int areaid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		
		try {
			
				list = faultDao.getFaultZone(areaid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	private ArrayList<DropdownItemVO> getLocationList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Location");
		list.add(vo);

		return list;
	}
	
	private ArrayList<DropdownItemVO> getFaultLocationList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Location");
		list.add(vo);

		return list;
	}

	private ArrayList<DropdownItemVO> getLocationList(int zoneid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		
		try {
			
				list = faultDao.getLocation(zoneid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	private ArrayList<DropdownItemVO> getFaultLocationList(int zoneid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		
		try {
			
				list = faultDao.getFaultLocation(zoneid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	//llm 21/05/2013 getlocation with tenant name for commercial user
	private ArrayList<DropdownItemVO> getLocationWithTenantList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "Select Location");
		list.add(vo);

		return list;
	}
	private ArrayList<DropdownItemVO> getLocationWithTenantList(int zoneid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		try {
			list = faultDao.getLocationWithTenantName(zoneid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
//llm 21/05/2013 getlocation with tenant name for commercial user
	
	public class terminalOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			try {
				ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> listing;

				DropdownItemVO x = (DropdownItemVO) btnTerminal.getSelectedItem();
				String tmpid = x.getItemId();

				listing = getAreaList();
				CustomArrayAdapterWithText areaAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnArea.setAdapter(areaAdapter);

				listing = getZoneList();
				CustomArrayAdapterWithText zoneAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnZone.setAdapter(zoneAdapter);

				listing = getLocationList();
				CustomArrayAdapterWithText locationAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
				btnLocation.setAdapter(locationAdapter);

				if (!tmpid.equals("0")) {
					listing = getLevelList(Integer.parseInt(tmpid));
					CustomArrayAdapterWithText levelAdapter = new CustomArrayAdapterWithText(getApplicationContext(), listing);
					btnLevel.setAdapter(levelAdapter);

					if (spnLevelid != 0) {

						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnLevel.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnLevelid) {
								pos2 = i;
								i = listing.size() - 1;
							}

						}
						btnLevel.setSelection(pos2);

					}

					if (spnAreaid != 0) {
						// set selected value of spnAreaid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnArea.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnAreaid) {
								pos2 = i;
								i = listing.size() - 1;
							}

						}
						btnArea.setSelection(pos2);

					}

					if (spnZoneid != 0) {
						// set selected value of spnZoneid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnZone.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnZoneid) {
								pos2 = i;
								i = listing.size() - 1;
							}
						}
						btnZone.setSelection(pos2);

					}

					if (spnLocationId != 0) {
						// set selected value of zoneid
						int pos2 = 0;
						for (int i = 0; i < listing.size(); i++) {
							DropdownItemVO a = (DropdownItemVO) btnLocation.getItemAtPosition(i);
							int tmpid2 = Integer.parseInt(a.getItemId());
							if (tmpid2 == spnLocationId) {
								pos2 = i;
								i = listing.size() - 1;
							}
						}
						btnLocation.setSelection(pos2);
					}

				}
				else
				{
					btnLevel.setSelection(0);
					btnArea.setSelection(0);
					btnZone.setSelection(0);
					btnLocation.setSelection(0);
					
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	private ArrayList<DropdownItemVO> getTerminalList() {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
		
		try {
		
				list = faultDao.getTerminal();
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	private ArrayList<DropdownItemVO> getFaultTerminalList() {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		FaultDAO faultDao = new FaultDAO();
	
		try {
				list = faultDao.getFaultTerminal();
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	private class doSelectLocation implements OnClickListener {
		public void onClick(View v) {
					
			if (btnTerminal.getSelectedItemPosition() == 0) {
				Toast toast = Toast.makeText(BaseView.this, "Please Select Building", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 40, 60);
				toast.show();
			} else if (btnLevel.getSelectedItemPosition() == 0) {
				Toast toast = Toast.makeText(BaseView.this, "Please Select Level", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 40, 60);
				toast.show();
			} else if (btnArea.getSelectedItemPosition() == 0 && View.VISIBLE == btnArea.getVisibility()) {
				Toast toast = Toast.makeText(BaseView.this, "Please Select Area", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 40, 60);
				toast.show();
			} else if (btnZone.getSelectedItemPosition() == 0 && View.VISIBLE == btnZone.getVisibility()) {
				Toast toast = Toast.makeText(BaseView.this, "Please Select Zone", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 40, 60);
				toast.show();
			} else if (btnLocation.getSelectedItemPosition() == 0 && View.VISIBLE == btnLocation.getVisibility()) {
				Toast toast = Toast.makeText(BaseView.this, "Please Select Location", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 40, 60);
				toast.show();
			} else {

				spnTerminalid = Integer.parseInt(((DropdownItemVO) btnTerminal.getSelectedItem()).getItemId());
				spnLevelid = Integer.parseInt(((DropdownItemVO) btnLevel.getSelectedItem()).getItemId());
				spnAreaid = Integer.parseInt(((DropdownItemVO) btnArea.getSelectedItem()).getItemId());
				spnZoneid = Integer.parseInt(((DropdownItemVO) btnZone.getSelectedItem()).getItemId());
				spnLocationId = Integer.parseInt(((DropdownItemVO) btnLocation.getSelectedItem()).getItemId());
				
				
				try {
					TextView blocation;
					blocation = (TextView) findViewById(R.id.lblLocation);
					CommonFunction comFunction = new CommonFunction();
					if (blocation != null)
					{
						blocation.setText(comFunction.getLocationCodeById(spnLocationId));
					}
				} catch (ULjException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

//				try {
//					TextView bUnitNo;
//					bUnitNo = (TextView) findViewById(R.id.txtUnitNumber);
//					if (bUnitNo != null) {
//						CommonFunction comFunction = new CommonFunction();
//						if (bUnitNo != null)
//							bUnitNo.setText(comFunction.getLocationCodeById(spnLocationId));
//					}
//				} catch (ULjException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				try {
//					Spinner bTenant;
//					bTenant = (Spinner) findViewById(R.id.spnTenantName);
//					// Handle page withh no Tenant
//					if (bTenant != null) {
//						CommonFunction comFunction = new CommonFunction();
//						spnTenantId = Integer.parseInt(comFunction.getTenantIDbyLocation(spnLocationId));
//						bTenant.setSelection(getPositionForId(getTenantListList(), String.valueOf(spnTenantId)));
//
//					}
//
//				} catch (NumberFormatException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				} catch (ULjException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}

				dialogLocation.dismiss();

			
			}
		}

	}

	private class doCancelLocation implements OnClickListener {
		public void onClick(View v) {
			
			dialogLocation.dismiss();
		}
	}

	private ArrayList<DropdownItemVO> getTenantListList() throws ULjException {
		CommonFunction cFunction = new CommonFunction();

		try {
			return cFunction.getTenantListList();
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}

	private int getPositionForId(List<DropdownItemVO> vos, String id) {
		int returnValue = 0;
		for (DropdownItemVO vo : vos) {
			if (id.equals(vo.getItemId()))
				return returnValue;
			returnValue++;
		}
		return 0;
	}
	
	
	private class doSelectLocationFilter implements OnClickListener {
		public void onClick(View v) {
					
			
				spnTerminalid = Integer.parseInt(((DropdownItemVO) btnTerminal.getSelectedItem()).getItemId());
				spnLevelid = Integer.parseInt(((DropdownItemVO) btnLevel.getSelectedItem()).getItemId());
				spnAreaid = Integer.parseInt(((DropdownItemVO) btnArea.getSelectedItem()).getItemId());
				spnZoneid = Integer.parseInt(((DropdownItemVO) btnZone.getSelectedItem()).getItemId());
				spnLocationId = Integer.parseInt(((DropdownItemVO) btnLocation.getSelectedItem()).getItemId());
			
			
				try {
					TextView blocation;
					blocation = (TextView) findViewById(R.id.lblLocation);
					CommonFunction comFunction = new CommonFunction();
					if (blocation != null)
					{
						String strLocationCode="";
						if (spnLocationId>0)
							strLocationCode=comFunction.getLocationCodeById(spnLocationId);
						else
						{
							
							if (spnZoneid>0)
							{strLocationCode=comFunction.getLocationCodeByZoneId(spnZoneid);}
							else if (spnAreaid>0)
							{strLocationCode=comFunction.getLocationCodeByAreaId(spnAreaid);}
							else if (spnLevelid>0)
							{strLocationCode=comFunction.getLocationCodeByLevelId(spnLevelid);}
							else if (spnTerminalid>0)
							{
								strLocationCode= comFunction.getTerminalById(spnTerminalid)+"/";
								//strLocationCode="T"+Integer.toString(spnTerminalid)+"/";
							}
							
						}
						//llm 03/07/2013 add location button for job start adhoc inspection
						if ( v.getTag()!=null)
						{
							if ( v.getTag().equals(imgBtnDialogLocationOK))
							{
								//llm 11/07/2013
								//showConfirmLocationDialog(strLocationCode);
								
							
								blocation = (TextView) findViewById(R.id.lblLocation);
								
								TextView bJobStart;
								bJobStart = (TextView) findViewById(R.id.txtJobStart);
								
								if (blocation != null)
								{
									
										blocation.setText(strLocationCode);
										//ChecklistView.getJobStartTimestamp(blocation);
										DateUtil dUtil = new DateUtil();
										
										if (bJobStart!=null)
										{
											bJobStart.setText(dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp(), DateUtil.DATE_TIME_FORMAT));
										}
										
										dialogLocation.dismiss();
										
									
								}
								}
							else
							{
								blocation.setText(strLocationCode);
								dialogLocation.dismiss();
							}
						}
						else
						{
							blocation.setText(strLocationCode);
							dialogLocation.dismiss();
						}
							
						
					}	
					

				} catch (ULjException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				

			
		}

	}


	protected void onResume() {
		// TODO Auto-generated method stub
		CommonFunction comFunction = new CommonFunction();
		try {
//			
			//20130219:Ryan : reset the phone to use the network time if is not not currently set as such
			if(android.provider.Settings.System.getInt(getContentResolver(), android.provider.Settings.System.AUTO_TIME)!=1){
				Toast.makeText(getBaseContext(), "setting autotime to yes", Toast.LENGTH_LONG).show();
				android.provider.Settings.System.putInt(getContentResolver(), android.provider.Settings.System.AUTO_TIME, 1);
				
			}

 

			if (comFunction.isAirplaneModeOn(this.getApplicationContext()))
			{
				try {
					AlertDialog diaBox = makeAndShowDialogBox("Mobile Inspection System", "Flight Mode is On.\nPlease turn off Flight Mode.");
					diaBox.show();
					super.onResume();

				} catch (Exception e) {
					Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
			else
				super.onResume();

				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Ryan : handler to perform logout after updating access log
	Handler processLogoutHandler = new Handler() { 
		public void handleMessage(Message msg) {
			finish();
			Intent intent = new Intent(BaseView.this, main.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);		
		}
	};

	
	

	//llm 03/07/2013 add location button for job start adhoc inspection
	private void showConfirmLocationDialog(final String strLocationCode) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Start inspection for location :\n"+strLocationCode+ " ? ").setTitle("Mobile Inspection System").setCancelable(true)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						CommonFunction comFunction = new CommonFunction();
						TextView blocation;
						blocation = (TextView) findViewById(R.id.lblLocation);
						
						TextView bJobStart;
						bJobStart = (TextView) findViewById(R.id.txtJobStart);
						
						if (blocation != null)
						{
							
								blocation.setText(strLocationCode);
							
								DateUtil dUtil = new DateUtil();
								
								if (bJobStart!=null)
								{
									bJobStart.setText(dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp(), DateUtil.DATE_TIME_FORMAT));
								}
								
								dialogLocation.dismiss();
								
							
						}
						
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();

	}
	public void showProgressDialog() {
		if (loadingProgressDialog != null && !loadingProgressDialog.isShowing()) {
			loadingProgressDialog.show();
		}
	}

	public void dismissProgressDialog() {
		if (loadingProgressDialog != null && loadingProgressDialog.isShowing()) {
			loadingProgressDialog.dismiss();
		}
	}

	public void promptProgressDialogWithDelay() {

		showProgressDialog();
		(new Handler()).postDelayed(new Runnable() {
			@Override
			public void run() {
				dismissProgressDialog();
			}
		}, 3000);
	}
}
