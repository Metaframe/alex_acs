package sg.com.surbana.acs.einsp.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ALRED on 3/27/2017.
 */

@DatabaseTable(tableName = "MeterReading2VO")
public class MeterReading2VO implements Parcelable{
    @DatabaseField
    @SerializedName("monthlymeterno")
    @Expose
    private String monthlymeterno;
    @DatabaseField
    @SerializedName("ReadingNo")
    @Expose
    private String readingNo;
    @DatabaseField
    @SerializedName("MeterNo")
    @Expose
    private String meterNo;
    @DatabaseField
    @SerializedName("MeterOnOffReadingNo")
    @Expose
    private String meterOnOffReadingNo;
    @DatabaseField
    @SerializedName("LastUpdatedDate")
    @Expose
    private String lastUpdatedDate;
    @DatabaseField
    @SerializedName("LastUpdatedBy")
    @Expose
    private String lastUpdatedBy;
    @DatabaseField
    @SerializedName("locationid")
    @Expose
    private String locationid;
    @DatabaseField
    @SerializedName("ChecklistResultID")
    @Expose
    private String checklistResultID;
    @DatabaseField
    @SerializedName("PrevConsumption")
    @Expose
    private String prevConsumption;
    @DatabaseField
    @SerializedName("PrevReading")
    @Expose
    private String prevReading;
    @DatabaseField
    @SerializedName("CurrReading")
    @Expose
    private String currReading;
    @DatabaseField
    @SerializedName("Consumption")
    @Expose
    private String consumption;
    @DatabaseField
    @SerializedName("TakenByID")
    @Expose
    private String takenByID;
    @DatabaseField
    @SerializedName("TenantID")
    @Expose
    private String tenantID;
    @DatabaseField
    @SerializedName("TenantNo")
    @Expose
    private String tenantNo;
    @DatabaseField
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @DatabaseField
    @SerializedName("MeterRound")
    @Expose
    private String meterRound;
    @DatabaseField
    @SerializedName("unitno")
    @Expose
    private String unitno;
    @DatabaseField
    @SerializedName("customername")
    @Expose
    private String customername;
    @DatabaseField
    @SerializedName("tenantname")
    @Expose
    private String tenantname;
    @DatabaseField
    @SerializedName("tenantemail")
    @Expose
    private String tenantemail;
    @DatabaseField
    @SerializedName("SignFileName")
    @Expose
    private String signFileName;
    @DatabaseField
    @SerializedName("signimage")
    @Expose
    private String signimage;

    public String getMonthlymeterno() {
        return monthlymeterno;
    }

    public void setMonthlymeterno(String monthlymeterno) {
        this.monthlymeterno = monthlymeterno;
    }

    public String getReadingNo() {
        return readingNo;
    }

    public void setReadingNo(String readingNo) {
        this.readingNo = readingNo;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public String getMeterOnOffReadingNo() {
        return meterOnOffReadingNo;
    }

    public void setMeterOnOffReadingNo(String meterOnOffReadingNo) {
        this.meterOnOffReadingNo = meterOnOffReadingNo;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }

    public String getChecklistResultID() {
        return checklistResultID;
    }

    public void setChecklistResultID(String checklistResultID) {
        this.checklistResultID = checklistResultID;
    }

    public String getPrevConsumption() {
        return prevConsumption;
    }

    public void setPrevConsumption(String prevConsumption) {
        this.prevConsumption = prevConsumption;
    }

    public String getPrevReading() {
        return prevReading;
    }

    public void setPrevReading(String prevReading) {
        this.prevReading = prevReading;
    }

    public String getCurrReading() {
        return currReading;
    }

    public void setCurrReading(String currReading) {
        this.currReading = currReading;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getTakenByID() {
        return takenByID;
    }

    public void setTakenByID(String takenByID) {
        this.takenByID = takenByID;
    }

    public String getTenantID() {
        return tenantID;
    }

    public void setTenantID(String tenantID) {
        this.tenantID = tenantID;
    }

    public String getTenantNo() {
        return tenantNo;
    }

    public void setTenantNo(String tenantNo) {
        this.tenantNo = tenantNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getMeterRound() {
        return meterRound;
    }

    public void setMeterRound(String meterRound) {
        this.meterRound = meterRound;
    }

    public String getUnitno() {
        return unitno;
    }

    public void setUnitno(String unitno) {
        this.unitno = unitno;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getTenantname() {
        return tenantname;
    }

    public void setTenantname(String tenantname) {
        this.tenantname = tenantname;
    }

    public String getTenantemail() {
        return tenantemail;
    }

    public void setTenantemail(String tenantemail) {
        this.tenantemail = tenantemail;
    }

    public String getSignFileName() {
        return signFileName;
    }

    public void setSignFileName(String signFileName) {
        this.signFileName = signFileName;
    }

    public String getSignimage() {
        return signimage;
    }

    public void setSignimage(String signimage) {
        this.signimage = signimage;
    }

    public static final Parcelable.Creator<MeterReading2VO> CREATOR = new Creator<MeterReading2VO>() {
        @Override
        public MeterReading2VO createFromParcel(Parcel source) {
            MeterReading2VO meterReading2VO = new MeterReading2VO();
            meterReading2VO.monthlymeterno= source.readString();
            meterReading2VO.readingNo= source.readString();
            meterReading2VO.meterNo= source.readString();
            meterReading2VO.meterOnOffReadingNo= source.readString();
            meterReading2VO.lastUpdatedDate= source.readString();
            meterReading2VO.lastUpdatedBy= source.readString();
            meterReading2VO.locationid= source.readString();
            meterReading2VO.checklistResultID= source.readString();
            meterReading2VO.prevConsumption= source.readString();
            meterReading2VO.prevReading= source.readString();
            meterReading2VO.currReading= source.readString();
            meterReading2VO.consumption= source.readString();
            meterReading2VO.takenByID= source.readString();
            meterReading2VO.tenantID= source.readString();
            meterReading2VO.tenantNo= source.readString();
            meterReading2VO.remarks= source.readString();
            meterReading2VO.meterRound= source.readString();
            meterReading2VO.unitno= source.readString();
            meterReading2VO.customername= source.readString();
            meterReading2VO.tenantname= source.readString();
            meterReading2VO.tenantemail= source.readString();
            meterReading2VO.signFileName= source.readString();
            meterReading2VO.signimage= source.readString();
            meterReading2VO.signimage= source.readString();
            return meterReading2VO;
        }

        @Override
        public MeterReading2VO[] newArray(int size) {
            return new MeterReading2VO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(monthlymeterno);
        parcel.writeString(readingNo);
        parcel.writeString(meterNo);
        parcel.writeString(meterOnOffReadingNo);
        parcel.writeString(lastUpdatedDate);
        parcel.writeString(lastUpdatedBy);
        parcel.writeString(locationid);
        parcel.writeString(checklistResultID);
        parcel.writeString(prevConsumption);
        parcel.writeString(prevReading);
        parcel.writeString(currReading);
        parcel.writeString(consumption);
        parcel.writeString(takenByID);
        parcel.writeString(tenantID);
        parcel.writeString(tenantNo);
        parcel.writeString(remarks);
        parcel.writeString(meterRound);
        parcel.writeString(unitno);
        parcel.writeString(customername);
        parcel.writeString(tenantname);
        parcel.writeString(tenantemail);
        parcel.writeString(signFileName);
        parcel.writeString(signimage);
        parcel.writeString(signimage);
    }
}
