package sg.com.surbana.acs.einsp.vo;

import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Sep 7, 2011		Ryan			Created
 *
 */
public class SignoffVO {
	private final String TAG = "SignoffVO.java";
	int signoffNo;
	String hoName;
	String hoDesignation;
	String hoTelephone;
	String hoOrganization;

	Timestamp hoDate;
	byte[] hoSignature;
	String aName;
	String aDesignation;
	String aTelephone;
	Timestamp aDate;
	byte[] aSignature;
	String handoverNo;
	String aSignatureFileName;
	String hoSignatureFileName;
	String aOrganization;
	Timestamp lastUpdatedDate;
	int lastUpdatedBy;

	public String getaSignatureFileName() {
		return aSignatureFileName;
	}

	public void setaSignatureFileName(String sSignatureFileName) {
		this.aSignatureFileName = sSignatureFileName;
	}

	public String getHoSignatureFileName() {
		return hoSignatureFileName;
	}

	public void setHoSignatureFileName(String hoSignatureFileName) {
		this.hoSignatureFileName = hoSignatureFileName;
	}

	public Timestamp getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public int getSignoffNo() {
		return signoffNo;
	}

	public void setSignoffNo(int signoffNo) {
		this.signoffNo = signoffNo;
	}

	public String getHoName() {
		return hoName;
	}

	public void setHoName(String hoName) {
		this.hoName = hoName;
	}

	public String getHoDesignation() {
		return hoDesignation;
	}

	public void setHoDesignation(String hoDesignation) {
		this.hoDesignation = hoDesignation;
	}

	public String getHoTelephone() {
		return hoTelephone;
	}

	public void setHoTelephone(String hoTelephone) {
		this.hoTelephone = hoTelephone;
	}

	public Timestamp getHoDate() {
		return hoDate;
	}

	public void setHoDate(Timestamp hoDate) {
		this.hoDate = hoDate;
	}

	public byte[] getHoSignature() {
		return hoSignature;
	}

	public void setHoSignature(byte[] hoSignature) {
		this.hoSignature = hoSignature;
	}

	public String getaName() {
		return aName;
	}

	public void setaName(String aName) {
		this.aName = aName;
	}

	public String getaDesignation() {
		return aDesignation;
	}

	public void setaDesignation(String aDesignation) {
		this.aDesignation = aDesignation;
	}

	public String getaTelephone() {
		return aTelephone;
	}

	public void setaTelephone(String aTelephone) {
		this.aTelephone = aTelephone;
	}

	public Timestamp getaDate() {
		return aDate;
	}

	public void setaDate(Timestamp aDate) {
		this.aDate = aDate;
	}

	public byte[] getaSignature() {
		return aSignature;
	}

	public void setaSignature(byte[] aSignature) {
		this.aSignature = aSignature;
	}

	public String getHandoverNo() {
		return handoverNo;
	}

	public void setHandoverNo(String handoverNo) {
		this.handoverNo = handoverNo;
	}

	public String getHoOrganization() {
		return hoOrganization;
	}

	public void setHoOrganization(String hoOrganization) {
		this.hoOrganization = hoOrganization;
	}

	public String getaOrganization() {
		return aOrganization;
	}

	public void setaOrganization(String aOrganization) {
		this.aOrganization = aOrganization;
	}

}
