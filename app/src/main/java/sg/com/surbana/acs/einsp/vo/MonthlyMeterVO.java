package sg.com.surbana.acs.einsp.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.Timestamp;


/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Mar 15, 2012		Ryan			Created
 *
 */
@DatabaseTable(tableName = "MonthlyMeterVO")
public class MonthlyMeterVO {
	private final String TAG = "MonthlyMeterVO.java";
	Integer MonthlyMeterNo;
	@DatabaseField
	@SerializedName("MeterNo")
	@Expose
	Integer MeterNo;
	@DatabaseField
	@SerializedName("SerialNo")
	@Expose
	String SerialNo;
	@DatabaseField
	@SerializedName("MeterTypeDescription")
	@Expose
	String  MeterType ;
	@DatabaseField
	@SerializedName("LocationID")
	@Expose
	Integer LocationID;
	public static final String LOCATIONID = "LocationID";
	@DatabaseField
	@SerializedName("InstallationDate")
	@Expose
	String InstallationDate;
	@DatabaseField
	@SerializedName("RemovalDate")
	@Expose
	String  RemovalDate;
	@DatabaseField
	@SerializedName("InitialReading")
	@Expose
	Double InitialReading;
	@DatabaseField
	@SerializedName("LastReading")
	@Expose
	Double LastReading;
	@DatabaseField
	@SerializedName("MultiplyFactor")
	@Expose
	Float MultiplyFactor;
	@DatabaseField
	@SerializedName("unitid")
	@Expose
	String UnitID;
	@DatabaseField
	@SerializedName("metercategory")
	@Expose
	String MeterCategory ;
	@DatabaseField
	@SerializedName("meterclass")
	@Expose
	String MeterClass;
	@DatabaseField
	@SerializedName("meterstatus")
	@Expose
	String MeterStatus;
	@DatabaseField
	@SerializedName("pucno")
	@Expose
	String PUCNo;
	Float prevConsumption;
	String prevReading;
	String currReading;
	Float Consumption;
	Integer ChecklistNo;
	Integer TenantID;
	Timestamp LastUpdatedDate;
	Integer LastupdatedBy;
	Integer TakenBy;
	Integer ReadingNo;
	Timestamp TakenOn;
	
	String Aname;
	Timestamp Adate;
	byte[] aSignature;
	String AckBySigFilename;
	String TenantNo;
	String Remarks;
	String Atelephone;
	String ADesignation;
	@DatabaseField
	@SerializedName("sublocation")
	@Expose
	String subLocation;

	Boolean meterRound;
	
	String UnitNo;
	String CustomerName;
	String TenantName;
	String TenantEmail;
	
	
	
	public String getUnitNo() {
		return UnitNo;
	}
	public void setUnitNo(String unitNo) {
		UnitNo = unitNo;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getTenantName() {
		return TenantName;
	}
	public void setTenantName(String tenantName) {
		TenantName = tenantName;
	}
	public String getTenantEmail() {
		return TenantEmail;
	}
	public void setTenantEmail(String tenantEmail) {
		TenantEmail = tenantEmail;
	}
	public Integer getMonthlyMeterNo() {
		return MonthlyMeterNo;
	}
	public void setMonthlyMeterNo(Integer monthlyMeterNo) {
		MonthlyMeterNo = monthlyMeterNo;
	}
	public Integer getMeterNo() {
		return MeterNo;
	}
	public void setMeterNo(Integer meterNo) {
		MeterNo = meterNo;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getMeterType() {
		return MeterType;
	}
	public void setMeterType(String meterType) {
		MeterType = meterType;
	}
	public Integer getLocationID() {
		return LocationID;
	}
	public void setLocationID(Integer locationID) {
		LocationID = locationID;
	}
//	public Timestamp getInstallationDate() {
//		return InstallationDate;
//	}
//	public void setInstallationDate(Timestamp installationDate) {
//		InstallationDate = installationDate;
//	}
//	public Timestamp getRemovalDate() {
//		return RemovalDate;
//	}
//	public void setRemovalDate(Timestamp removalDate) {
//		RemovalDate = removalDate;
//	}
	public Double getInitialReading() {
		return 0.0;
	}
	public void setInitialReading(Double initialReading) {
		InitialReading = initialReading;
	}
	public Double getLastReading() {
		return LastReading;
	}
	public void setLastReading(Double lastReading) {
		LastReading = lastReading;
	}
	public Float getMultiplyFactor() {
		return MultiplyFactor;
	}
	public void setMultiplyFactor(Float multiplyFactor) {
		MultiplyFactor = multiplyFactor;
	}
	public String getUnitID() {
		return UnitID;
	}
	public void setUnitID(String unitID) {
		UnitID = unitID;
	}
	public String getMeterCategory() {
		return MeterCategory;
	}
	public void setMeterCategory(String meterCategory) {
		MeterCategory = meterCategory;
	}
	public String getMeterClass() {
		return MeterClass;
	}
	public void setMeterClass(String meterClass) {
		MeterClass = meterClass;
	}
	public String getMeterStatus() {
		return MeterStatus;
	}
	public void setMeterStatus(String meterStatus) {
		MeterStatus = meterStatus;
	}
	public String getPUCNo() {
		return PUCNo;
	}
	public void setPUCNo(String pUCNo) {
		PUCNo = pUCNo;
	}
	public Float getPrevConsumption() {
		return prevConsumption;
	}
	public void setPrevConsumption(Float prevConsumption) {
		this.prevConsumption = prevConsumption;
	}
	public String getPrevReading() {
		return prevReading;
	}
	public void setPrevReading(String prevReading) {
		this.prevReading = prevReading;
	}
	public String getCurrReading() {
		return currReading;
	}
	public void setCurrReading(String currReading) {
		this.currReading = currReading;
	}
	public Float getConsumption() {
		return Consumption;
	}
	public void setConsumption(Float consumption) {
		Consumption = consumption;
	}
	public Integer getChecklistNo() {
		return ChecklistNo;
	}
	public void setChecklistNo(Integer checklistNo) {
		ChecklistNo = checklistNo;
	}
	public Integer getTenantID() {
		return TenantID;
	}
	public void setTenantID(Integer tenantID) {
		TenantID = tenantID;
	}
	public Timestamp getLastUpdatedDate() {
		return LastUpdatedDate;
	}
	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}
	public Integer getLastupdatedBy() {
		return LastupdatedBy;
	}
	public void setLastupdatedBy(Integer lastupdatedBy) {
		LastupdatedBy = lastupdatedBy;
	}
	public Integer getTakenBy() {
		return TakenBy;
	}
	public void setTakenBy(Integer takenBy) {
		TakenBy = takenBy;
	}
	public Integer getReadingNo() {
		return ReadingNo;
	}
	public void setReadingNo(Integer readingNo) {
		ReadingNo = readingNo;
	}
	public String getAname() {
		return Aname;
	}
	public void setAname(String aname) {
		Aname = aname;
	}
	public Timestamp getAdate() {
		return Adate;
	}
	public void setAdate(Timestamp adate) {
		Adate = adate;
	}
	public byte[] getaSignature() {
		return aSignature;
	}
	public void setaSignature(byte[] aSignature) {
		this.aSignature = aSignature;
	}
	public Timestamp getTakenOn() {
		return TakenOn;
	}
	public void setTakenOn(Timestamp takenOn) {
		TakenOn = takenOn;
	}
	public String getAckBySigFilename() {
		return AckBySigFilename;
	}
	public void setAckBySigFilename(String ackBySigFilename) {
		AckBySigFilename = ackBySigFilename;
	}
	public String getTenantNo() {
		return TenantNo;
	}
	public void setTenantNo(String tenantNo) {
		TenantNo = tenantNo;
	}
	public String getRemarks() {
		return Remarks;
	}
	public void setRemarks(String remarks) {
		Remarks = remarks;
	}
	public String getAtelephone() {
		return Atelephone;
	}
	public void setAtelephone(String atelephone) {
		Atelephone = atelephone;
	}
	public String getADesignation() {
		return ADesignation;
	}
	public void setADesignation(String aDesignation) {
		ADesignation = aDesignation;
	}
	public String getSubLocation() {
		return subLocation;
	}
	public void setSubLocation(String subLocation) {
		this.subLocation = subLocation;
	}
	public Boolean getMeterRound() {
		return meterRound;
	}
	public void setMeterRound(Boolean meterRound) {
		this.meterRound = meterRound;
	}
	

	
	
}
