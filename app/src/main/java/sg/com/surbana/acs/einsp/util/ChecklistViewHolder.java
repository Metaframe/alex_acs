package sg.com.surbana.acs.einsp.util;

import sg.com.surbana.acs.einsp.R;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *06 Sept 2011		Ryan			Created
 *
 */
public class ChecklistViewHolder {
	private final String TAG = "ChecklistViewHolder";
	EditText edQuantity;
	EditText edReplace;
	EditText edRepair;
	EditText edReturn;
	EditText edRemarks;
	EditText edSelectedValue;
	Spinner spnValue;
	Spinner spnStatus;

	public int ref;
	Button tButton = null;

	ChecklistViewHolder(View base) {
		this.tButton = (Button) base.findViewById(R.id.lstCondition);
//		this.edQuantity = (EditText) base.findViewById(R.id.lstQuantity);
//		this.spnValue = (Spinner) base.findViewById(R.id.lstChecklistVal);
//		this.spnStatus = (Spinner) base.findViewById(R.id.lstChecklistStatus);
//		this.edReturn = (EditText) base.findViewById(R.id.lstReturn);
		this.edRemarks = (EditText) base.findViewById(R.id.lstRemarks);
	}
}
