package sg.com.surbana.acs.einsp.vo;
import java.io.Serializable;


public class ServerIPVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -281183006344908787L;

	private final String TAG = "ServerIPVO";

	String IP1;
	String IP2;
	String EXPIRY_DATE_1;
	String EXPIRY_DATE_2;
	int PORT1;
	int PORT2;
	int VERSION; //llm 23/07/2013 add version
	
	
	public int getVERSION() {
		return VERSION;
	}
	public void setVERSION(int vERSION) {
		VERSION = vERSION;
	}
	public int getPORT1() {
		return PORT1;
	}
	public void setPORT1(int pORT1) {
		PORT1 = pORT1;
	}
	public int getPORT2() {
		return PORT2;
	}
	public void setPORT2(int pORT2) {
		PORT2 = pORT2;
	}
	public String getIP1() {
		return IP1;
	}
	public void setIP1(String iP1) {
		IP1 = iP1;
	}
	public String getIP2() {
		return IP2;
	}
	public void setIP2(String iP2) {
		IP2 = iP2;
	}
	public String getEXPIRY_DATE_1() {
		return EXPIRY_DATE_1;
	}
	public void setEXPIRY_DATE_1(String eXPIRY_DATE_1) {
		EXPIRY_DATE_1 = eXPIRY_DATE_1;
	}
	public String getEXPIRY_DATE_2() {
		return EXPIRY_DATE_2;
	}
	public void setEXPIRY_DATE_2(String eXPIRY_DATE_2) {
		EXPIRY_DATE_2 = eXPIRY_DATE_2;
	}
	
	
	
}