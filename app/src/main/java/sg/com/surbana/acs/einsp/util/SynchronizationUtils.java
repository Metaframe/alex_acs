package sg.com.surbana.acs.einsp.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import org.apache.http.util.ByteArrayBuffer;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.view.HomeView;
import sg.com.surbana.acs.einsp.vo.ServerIPVO;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.StreamHTTPParms;
import com.ianywhere.ultralitejni12.SyncParms;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *06 Jul 2011		Ryan			set wait(200) to sync with notification
 *07 Jul 2011		Ryan			Display error message when sync occur
 *20 Jul 2011		Ryan 			set sync to false if error to release the lock
 *22 Jul 2011		Ryan 			pass mHanlder to sync thread
 *29 Jul 2011		Ryan			adopt callback handler mechanism to allow caller function to decide 'post' sync process
 */
public class SynchronizationUtils {
	private final String TAG = "SynchronizationUtils";
	// public static String SYNC_HOST="10.0.2.2"; //When connecting using
	// emulator
	// public static String SYNC_HOST="192.168.1.185";//When connecting using
	// android device
	// // android device -galxy
	// // tab

	// public static String SYNC_HOST = "192.168.43.119";// When connecting
	// using

	// // public static String SYNC_HOST="210.24.129.138"; //Demo server

	/*
	 * IMPORTANT : USE THIS ONLY FOR RELEASING THE APP TO ANDROID MARKET
	 * 
	 * live string
	
	 */
	public static String SYNC_HOST ="210.24.129.9";
	public static int SYNC_PORT = 2461;
	public static int SYNC_PORT_2 = 2461;
	

 
	/*
	 * lcoal string
	 * 
	 * // public static String SYNC_HOST="192.168.0.104"; //Galaxy Note public static String SYNC_HOST="192.168.0.104"; //router public static int public static
	 * int SYNC_PORT = 2439;//Galaxy Note public static int SYNC_PORT_2 = // 2439;//Galaxy Note FOR IFS public static int SYNC_PORT_2 = 2439;
	 */

	/*
	 * Test string
	 */
//	 public static String SYNC_HOST = "210.24.129.3";
//	 public static int SYNC_PORT = 2464;
//	 public static int SYNC_PORT_2 = 2464;

	/*
	 * STG string

*/
//	 public static String SYNC_HOST = "210.24.129.3";
//	 public static int SYNC_PORT = 2439;
//	 public static int SYNC_PORT_2 = 2439; 
	 	
	// public static
	// String SYNC_HOST = "192.168.43.141";// When connecting usinghy4x445

	// public static boolean isWirelessMode = true;

//	 public static int SYNC_PORT = 2439;
//	 public static int SYNC_PORT_2 = 2439;
//	 public static String SYNC_HOST = "192.168.43.119";
//	// public static String SYNC_HOST = "192.168.0.103";
//	 public static String SYNC_HOST_2 = "192.168.43.119";

	public static String mbUser = "ompstest";
	public static String mbPassword = "ompstest";
	ProgressDialog dialog = null;
	Context context;

	private Handler mHandler = new Handler();

	RemoteViews contentView;
	NotificationManager notificationManager;

	private static int NOTIFICATION_ID = 1;

	public static boolean isSynching = false;

	/**
	 * Provide synchronized interfaced to prevent concurrency. Ensure only one synchronization happen on the mobile device at a time, MB server will throw error
	 * if two different threads trying to do synchronization
	 * 
	 * @param version
	 * @param publication
	 * @param downloadOnly
	 * @param conn
	 * @param context
	 * @param intent
	 * @return
	 * @throws ULjException
	 */

	public boolean synchronizedDoSyncForVersion(String version, String publication, boolean downloadOnly, Connection conn, Context context, Intent intent)
			throws ULjException {
		return synchronizedDoSyncForVersion(version, publication, downloadOnly, conn, context, intent, null);
	}

	public synchronized boolean synchronizedDoSyncForVersion(String version, String publication, boolean downloadOnly, Connection conn, Context context,
			Intent intent, Handler callbackHandler) throws ULjException {
		while (isSynching) {
			System.out.println("Synching now.. waittt");
			try {
				wait(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("GOOOOOOOOOOOOOOOOOOOOOOO");
		isSynching = true;

		boolean result = doSyncForVersion(version, publication, downloadOnly, conn, context, intent, callbackHandler);
		// isSynching = false;
		notifyAll();
		return result;
	}

	public synchronized boolean synchronizedSyncForVersionWithNotification(String version, String publication, Connection conn, Context context,
			boolean uploadOnly) throws ULjException {
		while (isSynching) {
			try {
				wait(200); // 20110706-Ryan:Set wait to 0.2 second
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		isSynching = true;

		boolean result = doSyncForVersionWithNotification(version, publication, conn, context, null, NOTIFICATION_ID++, uploadOnly);
		notifyAll();
		return result;

	}

	/**
	 * Note: The intent parameter is used to inform the synchronization util on which screen the user should be forward to after it has done the process if
	 * intent is null, user will not be forwarded to any page
	 * 
	 * @param version
	 * @param publication
	 * @param downloadOnly
	 * @param conn
	 * @param context
	 * @param intent
	 * @return
	 * @throws ULjException
	 */

	public boolean doSyncForVersion(String version, String publication, boolean downloadOnly, Connection conn, Context context, Intent intent)
			throws ULjException {
		return (doSyncForVersion(version, publication, downloadOnly, conn, context, intent, null));
	}

	public boolean doSyncForVersion(String version, String publication, boolean downloadOnly, Connection conn, Context context, Intent intent,
			Handler callbackHandler) throws ULjException {
		try {

			Log.v(TAG, SYNC_HOST + "-" + SYNC_PORT + " Do Sync for " + version);
			SyncParms syncParms = conn.createSyncParms(mbUser, version);
			StreamHTTPParms streamParms = syncParms.getStreamParms();

			if (AppsConstant.VERS_DOWNLOAD_IFS.equals(version)) {
				streamParms.setHost(SYNC_HOST);
				streamParms.setPort(SYNC_PORT_2);
			} else {
				streamParms.setHost(SYNC_HOST);
				streamParms.setPort(SYNC_PORT);
			}

			// if(AppsConstant.VERS_UPDATE_HANDOVER.equals(version)||AppsConstant.VERS_INIT.equals(version)||
			// AppsConstant.VERS_DOWNLOAD_HANDOVER.equals(version)||
			// AppsConstant.VERS_UPDATE_TAKEOVER.equals(version)||
			// AppsConstant.VERS_DOWNLOAD_TAKEOVER.equals(version)){
			// //synch to local machine
			//
			// streamParms.setHost(SYNC_HOST_2);
			// streamParms.setPort(SYNC_PORT_2);
			// }else{
			// //sycn to demo server
			// streamParms.setHost(SYNC_HOST);
			// streamParms.setPort(SYNC_PORT);
			// }

			if (context != null)
				dialog = ProgressDialog.show(context, "", "in progress. . .", true);
			this.context = context;
			//syncParms.setSyncObserver(new AppSyncObserver(dialog)); // need to
																	// set the
																	// observer
																	// to
																	// capture
																	// the
																	// progress

			syncParms.setPassword(mbPassword);
			if (publication != null)
				syncParms.setPublications(publication);

			syncParms.setDownloadOnly(downloadOnly);

			if (callbackHandler == null) {
				callbackHandler = new Handler() {
					public void handleMessage(Message msg) {
						Exception ex = (Exception) msg.getData().getSerializable("error");
						if (ex != null)
							displayErrorMessage(ex);
					}
				};
			}

			Log.v(TAG, "BEFORE DOING SYNCHRONIZATION*****");

			System.out.println("start time is : " + new Date());
			SyncThread st = new SyncThread(conn, syncParms, dialog, intent, context, callbackHandler);
			st.start();

			Log.v(TAG, "**********AFTER DOING SYNCHRONIZATION*****");

		} catch (ULjException e) {
			Log.e(TAG, "Error when sycn " + version, e);
			e.printStackTrace();
			SynchronizationUtils.isSynching = false; // Ryan-20110720:set sync
														// to false if error to
														// release the lock

			throw e;
		} catch (Exception e) {
			if (dialog != null)
				dialog.dismiss();

			Log.e(TAG, "Error when sycn " + version, e);
			SynchronizationUtils.isSynching = false; // Ryan-20110720:set sync
														// to false if error to
														// release the lock

			e.printStackTrace();
		}

		return true;
	}

	public boolean synchronizedDoSyncForVersionWithoutScreenTransition(String version, String publication, boolean downloadOnly, Connection conn,
			Context context, Handler callbackHandler) throws ULjException {

		return synchronizedDoSyncForVersion(version, publication, downloadOnly, conn, context, null, callbackHandler);
	}

	public boolean doSyncForVersionWithNotification(String version, String publication, Connection conn, Context context, Notification notify,
			int notificationId, boolean uploadOnly) throws ULjException {
		try {
			Log.v(TAG, SYNC_HOST + "-" + SYNC_PORT + " Do Sync for " + version);
			SyncParms syncParms = conn.createSyncParms(mbUser, version);
			StreamHTTPParms streamParms = syncParms.getStreamParms();
			Notification notification;
			//
			// if(AppsConstant.VERS_UPDATE_HANDOVER.equals(version)||AppsConstant.VERS_INIT.equals(version)||
			// AppsConstant.VERS_UPDATE_HANDOVER.equals(version)||
			// AppsConstant.VERS_UPDATE_HANDOVER.equals(version)||
			// AppsConstant.VERS_UPDATE_HANDOVER.equals(version)){
			// //synch to local machine
			// streamParms.setHost(SYNC_HOST_2);
			// streamParms.setPort(SYNC_PORT_2);
			// }else{
			// streamParms.setHost(SYNC_HOST);
			// streamParms.setPort(SYNC_PORT);
			// }
			//

			if (AppsConstant.VERS_UPDATE_IFS.equals(version)) {
				// synch with IFS mobilink server
				streamParms.setHost(SYNC_HOST);
				streamParms.setPort(SYNC_PORT_2);
			} else {
				streamParms.setHost(SYNC_HOST);
				streamParms.setPort(SYNC_PORT);
			}

			// syncParms.setUploadOnly(uploadOnly);
			syncParms.setDownloadOnly(false);
			if (notify == null) {
				notification = notificationBuilder(context);
				System.out.println("Notification ID is " + notificationId);
			} else {
				notification = notify;
			}

			main.NOTIFICATION_MANAGER.notify(notificationId, notification);
			this.context = context;

			if (publication != null)
				syncParms.setPublications(publication);

			syncParms.setSyncObserver(new AppSyncObserver(notification, notificationId)); // need
																							// to
																							// set
																							// the
																							// observer
																							// to
																							// capture
																							// the
																							// progress

			syncParms.setPassword(mbPassword);
			mHandler = new Handler() {
				public void handleMessage(Message msg) {
					Exception ex = (Exception) msg.getData().getSerializable(AppsConstant.HANDLER_KEY_ERROR);
					if (ex != null)
						displayErrorMessage(ex);
				}
			};
			SyncThread st = new SyncThread(conn, syncParms, notification, notificationId, mHandler);
			st.start();

			// conn.synchronize(syncParms);

			Log.v(TAG, "**********AFTER DOING SYNCHRONIZATION*****");
		} catch (ULjException e) {
			Log.e(TAG, "Error when sycn " + version, e);
			SynchronizationUtils.isSynching = false; // Ryan-20110720:set sync
														// to false if error to
														// release the lock
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			Log.e(TAG, "Erryr when sycn " + version, e);
			SynchronizationUtils.isSynching = false; // Ryan-20110720:set sync
														// to false if error to
														// release the lock
			e.printStackTrace();
		}

		return true;
	}

	private Notification notificationBuilder(Context cont) {

		int icon = R.drawable.caglogo;
		String tickerText = " Mobile Inspection System sync in progress...";
		long when = System.currentTimeMillis();
		Notification notification = new Notification(icon, tickerText, when);

		contentView = new RemoteViews(cont.getPackageName(), R.layout.custom_notification);
		contentView.setTextViewText(R.id.text, "Waiting for synchronization.....");
		notification.contentView = contentView;

		Intent i = new Intent(cont, HomeView.class).setAction(Intent.ACTION_MAIN).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(cont, 0, i, 0);

		notification.contentIntent = contentIntent;

		return notification;
	}

	public boolean testConnection(String uid, String pwd, Connection conn) throws ULjException {
		try {
			Log.v(TAG, "testConnection");

			SyncParms syncParms = conn.createSyncParms(uid, AppsConstant.VERS_INIT);
			StreamHTTPParms streamParms = syncParms.getStreamParms();
			streamParms.setHost(SYNC_HOST);
			streamParms.setPort(SYNC_PORT);
			syncParms.setPassword(pwd);
			syncParms.setPingOnly(true);
			// syncParms.setSyncObserver(new AppSyncObserver(null)); // need to
			// set the observer to capture the progress
			conn.synchronize(syncParms);
		} catch (ULjException e) {
			Log.e(TAG, "Erryr when testing connection", e);
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}

		return true;
	}

	// public boolean doSyncForVersionInit(Connection conn, Context context)
	// throws ULjException {
	//
	//
	// Notification notify = notificationBuilder(context);
	// // notificationManager.notify(1, notify);
	//
	// int nextNotificationId = NOTIFICATION_ID++;
	// main.NOTIFICATION_MANAGER.notify(nextNotificationId, notify );
	//
	//
	// return synchronizedSync(AppsConstant.VERS_INIT, conn,
	// context,notify, nextNotificationId);
	//
	// }
	//
	// public boolean doSyncForVersionUpdate(Connection conn) throws
	// ULjException {
	// return doSyncForVersionUpdate(conn, null);
	// }
	//
	// public boolean doSyncForVersionUpdate(Connection conn, Context context)
	// throws ULjException {
	//
	//
	// Notification notify = notificationBuilder(context);
	// // notificationManager.notify(1, notify);
	//
	// int nextNotificationId = NOTIFICATION_ID++;
	// main.NOTIFICATION_MANAGER.notify(nextNotificationId, notify );
	//
	// return synchronizedSync(AppsConstant.VERS_UPDATE, conn,
	// context, notify, nextNotificationId);
	// }

	public static void setMbUserDetails(String uid, String pwd) {
		mbUser = uid;
		// mbPassword = pwd;
		mbPassword = null; // password is not required to be set for synching,
							// it is already checked upon login (follow existing
							// logic)
	}

	public static void setMbHostDetails(String hostName, int port) {
		SYNC_HOST = hostName;
		SYNC_PORT = port;
	}

	public void displayErrorMessage(Exception ex) {
		DisplayUtil dsUtil = new DisplayUtil();
		dsUtil.displayException(context, "Error occur during synchronization", ex);
		ex = null;
		dsUtil = null;
	}
	
	private synchronized boolean synchronizedDoSyncForVersionIPPT(String version, String publication, boolean uploadOnly, Connection conn, Context context,
			Intent intent, Handler callbackHandler) throws ULjException {
		while (isSynching) {
			System.out.println("Synching now.. waittt");
			try {
				wait(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("GOOOOOOOOOOOOOOOOOOOOOOO");
		isSynching = true;

		boolean result = doSyncForVersionIPPT(version, publication, uploadOnly, conn, context, intent, callbackHandler);
		// isSynching = false;
		notifyAll();
		return result;
	}

	public boolean doSyncForVersionIPPT(String version, String publication, boolean uploadOnly, Connection conn, Context context, Intent intent,
			Handler callbackHandler) throws ULjException {
		try {

			Log.v(TAG, SYNC_HOST + "-" + SYNC_PORT + " Do Sync for " + version);
			SyncParms syncParms = conn.createSyncParms(mbUser, version);
			StreamHTTPParms streamParms = syncParms.getStreamParms();

			if (AppsConstant.VERS_DOWNLOAD_IFS.equals(version)) {
				streamParms.setHost(SYNC_HOST);
				streamParms.setPort(SYNC_PORT_2);
			} else {
				streamParms.setHost(SYNC_HOST);
				streamParms.setPort(SYNC_PORT);
			}

			// if(AppsConstant.VERS_UPDATE_HANDOVER.equals(version)||AppsConstant.VERS_INIT.equals(version)||
			// AppsConstant.VERS_DOWNLOAD_HANDOVER.equals(version)||
			// AppsConstant.VERS_UPDATE_TAKEOVER.equals(version)||
			// AppsConstant.VERS_DOWNLOAD_TAKEOVER.equals(version)){
			// //synch to local machine
			//
			// streamParms.setHost(SYNC_HOST_2);
			// streamParms.setPort(SYNC_PORT_2);
			// }else{
			// //sycn to demo server
			// streamParms.setHost(SYNC_HOST);
			// streamParms.setPort(SYNC_PORT);
			// }

			if (context != null)
				dialog = ProgressDialog.show(context, "", "in progress. . .", true);
			this.context = context;
			syncParms.setSyncObserver(new AppSyncObserver(dialog)); // need to
																	// set the
																	// observer
																	// to
																	// capture
																	// the
																	// progress

			syncParms.setPassword(mbPassword);
			if (publication != null)
				syncParms.setPublications(publication);

			syncParms.setUploadOnly(uploadOnly);

			if (publication == AppsConstant.SUB_VERS_UPLOAD_IPPT_SESSION)
				syncParms.setTableOrder("tblAesIPPTSession,tblAesIPPTSessionParticipants");
			
			if (callbackHandler == null) {
				callbackHandler = new Handler() {
					public void handleMessage(Message msg) {
						Exception ex = (Exception) msg.getData().getSerializable("error");
						if (ex != null)
							displayErrorMessage(ex);
					}
				};
			}

			Log.v(TAG, "BEFORE DOING SYNCHRONIZATION*****");

			System.out.println("start time is : " + new Date());
			SyncThread st = new SyncThread(conn, syncParms, dialog, intent, context, callbackHandler);
			st.start();

			Log.v(TAG, "**********AFTER DOING SYNCHRONIZATION*****");

		} catch (ULjException e) {
			Log.e(TAG, "Error when sycn " + version, e);
			e.printStackTrace();
			SynchronizationUtils.isSynching = false; // Ryan-20110720:set sync
														// to false if error to
														// release the lock

			throw e;
		} catch (Exception e) {
			if (dialog != null)
				dialog.dismiss();

			Log.e(TAG, "Error when sycn " + version, e);
			SynchronizationUtils.isSynching = false; // Ryan-20110720:set sync
														// to false if error to
														// release the lock

			e.printStackTrace();
		}

		return true;
	}

	public static String getMbUserDetails() {
		return mbUser;
	}
	
	public boolean CheckMobilinkAlive(String synchhost)//llm 17/07/2013
	{
		Connection conn;
		conn = DbUtil.getConnection();
		
		Boolean alive=true;
		
		
		try {
			SyncParms syncParms = conn.createSyncParms(mbUser, AppsConstant.VERS_CHECK_LOGIN);
			StreamHTTPParms streamParms = syncParms.getStreamParms();
			syncParms.setPublications( AppsConstant.SUB_VERS_CHECKLIST_DOWNLOAD);
			syncParms.setPassword(null);
			streamParms.setHost(synchhost);
			streamParms.setPort(SYNC_PORT);
			
			conn.synchronize(syncParms);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			if(e.getErrorCode()==-1305)
			{
				alive=false;
			}
			else 
			{
				alive = true;
			}
					
		}
		return alive;
	}
	
	public void setSyncHost() throws Exception // llm 17/07/2013. djh 20140210
	{
		Connection conn;
		conn = DbUtil.getConnection();

		DateUtil dateUtil = new DateUtil();
		CommonFunction cfun = new CommonFunction();
		String synchost1="";
		String synchost2="";
		String synchostexp1="";
		String synchostexp2="";
		int syncport1=SYNC_PORT;
		int syncport2=SYNC_PORT;
		
		ServerIPVO svo = new ServerIPVO();
		ServerIPVO svolocal = new ServerIPVO(); // llm 23/07/2013 add version
		
//testing string
		synchost1="202.42.51.23";//"210.24.129.9" LIVE
		synchost2="202.42.51.23";//"210.24.129.3" STG
		synchostexp1="2199-08-17 00:00:00.0";
		synchostexp2="2199-08-17 00:00:00.0";
		syncport1=2464;//2461 LIVE 2464 dev
		syncport2=2464;//
//testing string
		
		Date exp1 = null;
		Date exp2 = null;
		if (synchost1 != null)// use server text file
		{
			Timestamp currentDate = dateUtil.getCurrentTimeStamp();

			if (synchostexp1 != null) {
				exp1 = dateUtil.dateString2Calendar(synchostexp1, "yyyy-MM-dd");
			}

			if (synchostexp2 != null) {
				exp2 = dateUtil.dateString2Calendar(synchostexp2, "yyyy-MM-dd");
			}

			// if (CheckMobilinkAlive(SYNC_HOST)==false)
			// {
			if (synchostexp1.length() > 0 && exp1 != null && exp1.after(currentDate)) {
				if (synchost1.length() > 0) {
					SYNC_HOST = synchost1;
					if (syncport1 > 0)
						SYNC_PORT = syncport1;

					if (testConnection(mbUser, null, conn) == false) // llm 06/08/2013 use test connection instead of setsynchost
					// if (CheckMobilinkAlive(synchost1)==false)
					{
						SYNC_HOST = synchost2;
						if (syncport2 > 0)
							SYNC_PORT = syncport2;
					}
				}
			} else {
				if (synchostexp2.length() > 0 && exp1 != null && exp2.after(currentDate))
					if (synchost2.length() > 0) {
						SYNC_HOST = synchost2;
						if (syncport2 > 0)
							SYNC_PORT = syncport2;
						// if (CheckMobilinkAlive(synchost2)==false) //llm 06/08/2013 use test connection instead of setsynchost

						if (testConnection(mbUser, null, conn) == false) {
							SYNC_HOST = synchost1;
							if (syncport1 > 0)
								SYNC_PORT = syncport1;
						}

					}
			}
			// }
		} else // use local txt file //llm 23/07/2013 add version
		{
			synchost1 = svolocal.getIP1();
			synchost2 = svolocal.getIP2();
			synchostexp1 = svolocal.getEXPIRY_DATE_1();
			synchostexp2 = svolocal.getEXPIRY_DATE_2();
			syncport1 = svolocal.getPORT1();
			syncport2 = svolocal.getPORT2();

			if (synchost1 != null) {
				Timestamp currentDate = dateUtil.getCurrentTimeStamp();

				if (synchostexp1 != null) {
					exp1 = dateUtil.dateString2Calendar(synchostexp1, "yyyy-MM-dd");
				}

				if (synchostexp2 != null) {
					exp2 = dateUtil.dateString2Calendar(synchostexp2, "yyyy-MM-dd");
				}

				if (synchostexp1.length() > 0 && exp1 != null && exp1.after(currentDate)) {
					if (synchost1.length() > 0) {
						SYNC_HOST = synchost1;
						if (syncport1 > 0)
							SYNC_PORT = syncport1;

						if (testConnection(mbUser, null, conn) == false) // llm 06/08/2013 use test connection instead of setsynchost
						// if (CheckMobilinkAlive(synchost1)==false)
						{
							SYNC_HOST = synchost2;
							if (syncport2 > 0)
								SYNC_PORT = syncport2;
						}
					}
				} else {
					if (synchostexp2.length() > 0 && exp1 != null && exp2.after(currentDate))
						if (synchost2.length() > 0) {
							SYNC_HOST = synchost2;
							if (syncport2 > 0)
								SYNC_PORT = syncport2;
							if (testConnection(mbUser, null, conn) == false)
							// if (CheckMobilinkAlive(synchost2)==false) //llm 06/08/2013 use test connection instead of setsynchost
							{
								SYNC_HOST = synchost1;
								if (syncport1 > 0)
									SYNC_PORT = syncport1;
							}
						}
				}
			}
		}
	}
	
	//llm 23/07/2013 add version
	public void DownloadFromUrl(String DownloadUrl, String fileName) {
		  try {
		       File root = android.os.Environment.getExternalStorageDirectory();  
		       File dir = new File (root.getAbsolutePath() + "/txt");
		       if(dir.exists()==false) {
		            dir.mkdirs();
		       }

		       URL url = new URL(DownloadUrl); //you can write here any link
		       File file = new File(dir, fileName);

		       long startTime = System.currentTimeMillis();
		       Log.d("DownloadManager", "download url:" + url);

		       /* Open a connection to that URL. */
		       URLConnection ucon = url.openConnection();

		       /*
		        * Define InputStreams to read from the URLConnection.
		        */
		       InputStream is = ucon.getInputStream();
		       BufferedInputStream bis = new BufferedInputStream(is);

		       /*
		        * Read bytes to the Buffer until there is nothing more to read(-1).
		        */
		       ByteArrayBuffer baf = new ByteArrayBuffer(5000);
		       int current = 0;
		       while ((current = bis.read()) != -1) {
		          baf.append((byte) current);
		       }

		       /* Convert the Bytes read to a String. */
		       FileOutputStream fos = new FileOutputStream(file);
		       fos.write(baf.toByteArray());
		       fos.flush();
		       fos.close();
		       Log.d("DownloadManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
		  } catch (IOException e) {
		   Log.d("DownloadManager", "Error: " + e);
		  }
		 }
}
