/*
 * Copyright (C) 2011 Daniel Berndt - Codeus Ltd  -  DateSlider
 * 
 * Class for setting up the dialog and initialsing the underlying
 * ScrollLayouts
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;
import java.util.Calendar;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import sg.com.surbana.acs.einsp.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NumberSpinnerDialog extends Dialog {

	// private static String TAG = "DATESLIDER";
	private ArrayList<Integer> wheelsId = new ArrayList();

	protected OnNumberSetListener onNumberSetListener;
	protected Calendar mTime;
	protected TextView mTitleText;
	protected LinearLayout mLayout;
	protected View vw;

	public NumberSpinnerDialog(Context context, OnNumberSetListener l) {
		super(context);
		this.onNumberSetListener = l;
	}

	public NumberSpinnerDialog(Context context, OnNumberSetListener l, View v) {
		super(context);
		this.onNumberSetListener = l;
		vw = v;
	}

	/**
	 * Set up the dialog with all the views and their listeners
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			long time = savedInstanceState.getLong("time", mTime.getTimeInMillis());
			mTime.setTimeInMillis(time);
		}

		this.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.numbers_scroller_8);
		this.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialogtitle);

		mTitleText = (TextView) this.findViewById(R.id.dateSliderTitleText);
		mLayout = (LinearLayout) findViewById(R.id.dateSliderMainLayout);

		Button okButton = (Button) findViewById(R.id.dateSliderOkButton);
		okButton.setOnClickListener(okButtonClickListener);
		mTitleText.setText("Number Picker");

		Button cancelButton = (Button) findViewById(R.id.dateSliderCancelButton);
		cancelButton.setOnClickListener(cancelButtonClickListener);

		wheelsId.add(R.id.num_1);
		wheelsId.add(R.id.num_2);
		wheelsId.add(R.id.num_3);
		wheelsId.add(R.id.num_4);
		wheelsId.add(R.id.num_5);
		wheelsId.add(R.id.num_6);
		wheelsId.add(R.id.num_7);
		wheelsId.add(R.id.num_8);

		initAllWhile();

	}

	private android.view.View.OnClickListener okButtonClickListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			// return value to correct view
			// set title value
			if (onNumberSetListener != null)
				onNumberSetListener.onDateSet(NumberSpinnerDialog.this, readAllNumbers(), vw);
			dismiss();
		}
	};

	private android.view.View.OnClickListener cancelButtonClickListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			dismiss();
		}
	};

	@Override
	public Bundle onSaveInstanceState() {
		Bundle savedInstanceState = super.onSaveInstanceState();
		if (savedInstanceState == null)
			savedInstanceState = new Bundle();
		return savedInstanceState;
	}

	/**
	 * This method sets the title of the dialog
	 */
	protected void setTitle() {
		if (mTitleText != null) {
			mTitleText.setText("This show new vlaue" + readAllNumbers());
		}
	}

	/**
	 * Defines the interface which defines the methods of the OnDateSetListener
	 */
	public interface OnNumberSetListener {
		/**
		 * this method is called when a date was selected by the user
		 * 
		 * @param view
		 *            the caller of the method
		 * 
		 */
		public void onDateSet(NumberSpinnerDialog view, String value, View v);
	}

	private void initAllWhile() {
		for (int id : wheelsId) {
			initWheel(id);
		}
	}

	private String readAllNumbers() {
		String numbers = "";
		for (int id : wheelsId) {
			numbers = numbers + String.valueOf(getWheel(id).getCurrentItem());
		}

		return numbers;
	}

	/**
	 * Initializes wheel
	 * 
	 * @param id
	 *            the wheel widget Id
	 */
	private void initWheel(int id) {
		WheelView wheel = getWheel(id);
		wheel.setViewAdapter(new NumericWheelAdapter(this.getContext(), 0, 9));
		wheel.setCyclic(true);
		wheel.setInterpolator(new AnticipateOvershootInterpolator());
	}

	/**
	 * Returns wheel by Id
	 * 
	 * @param id
	 *            the wheel Id
	 * @return the wheel with passed Id
	 */
	private WheelView getWheel(int id) {
		return (WheelView) findViewById(id);
	}

}
