package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import sg.com.surbana.acs.einsp.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

public class SpecialAdapter extends SimpleAdapter {
	private final String TAG = "SpecialAdapter.class";
	private Context con;
	private ImageView statusImage;

	// private int[] colors = new int[] {
	// HomeView.CAG_RESOURCE.getColor(R.color.col_white),
	// HomeView.CAG_RESOURCE.getColor(R.color.col_light_purple) };
	private int[] colors = new int[] { R.drawable.list_item_selector_white, R.drawable.list_item_selector_purple };
	private int[] statusImages = new int[] { R.drawable.status_green, R.drawable.status_green_square, R.drawable.status_red, R.drawable.status_yellow_half };

	public SpecialAdapter(Context context, ArrayList<HashMap<String, Object>> list, int resource, String[] from, int[] to) {
		super(context, list, resource, from, to);
		con = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		HashMap map = (HashMap) getItem(position);
		int statusId = 0;// (Integer)map.get(AppsConstant.HASHMAP_KEY_STATUS_ID);
		Date revisedDate = null;// (Date)map.get(AppsConstant.HASHMAP_KEY_REVISED_DATE);//use
								// date as it is the same value
		Date actualStart = null;// (Date)map.get(AppsConstant.HASHMAP_KEY_ACTUAL_START);
		Date actualEnd = null;// (Date)map.get(AppsConstant.HASHMAP_KEY_ACTUAL_END);

		statusImage = (ImageView) view.findViewById(R.id.lstStatusCode);
		if (statusImage != null) {
			int statusIndex = processStatusImageCode(statusId, revisedDate, actualStart, actualEnd);
			statusImage.setBackgroundResource(statusImages[statusIndex]);
		}
		int colorPos = position % colors.length;
		view.setBackgroundResource(colors[colorPos]);
		// view.setBackgroundDrawable(HomeView.CAG_RESOURCE.getDrawable(img[colorPos]));
		// view.setBackgroundColor(colors[colorPos]);
		return view;
	}

	private int processStatusImageCode(int statusId, Date revisedDate, Date actualStart, Date actualEnd) {
		int statusIndex = 3;
		Date currentDate = new Date();

		if (statusId == 3)
			statusIndex = 3;
		else if (actualStart != null && actualEnd != null) {
			statusIndex = 0;
		} else if (actualStart != null && actualEnd == null) {
			statusIndex = 1;
		}
		// }else if(currentDate !=null && currentDate.after(revisedDate) &&
		// actualStart==null){
		// statusIndex=2;
		// }

		return statusIndex;

	}
}
