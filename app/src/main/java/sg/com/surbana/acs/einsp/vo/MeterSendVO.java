package sg.com.surbana.acs.einsp.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ALRED on 3/29/2017.
 */

@DatabaseTable(tableName = "MeterSendVO")
public class MeterSendVO implements Serializable {
    @DatabaseField(id = true)
    @SerializedName("sMonthlyMeterNo")
    @Expose
    private String sMonthlyMeterNo;
    @DatabaseField
    @SerializedName("sReadingNo")
    @Expose
    private String sReadingNo;
    @DatabaseField
    @SerializedName("sMeterNo")
    @Expose
    private String sMeterNo;
    @DatabaseField
    @SerializedName("slastupdatedDate")
    @Expose
    private String slastupdatedDate;
    @DatabaseField
    @SerializedName("slastupdatedBy")
    @Expose
    private String slastupdatedBy;
    @DatabaseField
    @SerializedName("sLocationID")
    @Expose
    private String sLocationID;
    @DatabaseField
    @SerializedName("sChecklistResultId")
    @Expose
    private String sChecklistResultId;
    @DatabaseField
    @SerializedName("sPrevConsumption")
    @Expose
    private String sPrevConsumption;
    @DatabaseField
    @SerializedName("sCurrReading")
    @Expose
    private String sCurrReading;
    @DatabaseField
    @SerializedName("sPrevReading")
    @Expose
    private String sPrevReading;
    @DatabaseField
    @SerializedName("sConsumption")
    @Expose
    private String sConsumption;
    @DatabaseField
    @SerializedName("sTakenByID")
    @Expose
    private String sTakenByID;
    @DatabaseField
    @SerializedName("sTenantID")
    @Expose
    private String sTenantID;
    @DatabaseField
    @SerializedName("sTenantNo")
    @Expose
    private String sTenantNo;
    @DatabaseField
    @SerializedName("sRemarks")
    @Expose
    private String sRemarks;
    @DatabaseField
    @SerializedName("sMeterRound")
    @Expose
    private String sMeterRound;
    @DatabaseField
    @SerializedName("sCustomerName")
    @Expose
    private String sCustomerName;
    @DatabaseField
    @SerializedName("sTenantName")
    @Expose
    private String sTenantName;
    @DatabaseField
    @SerializedName("sTenantEmail")
    @Expose
    private String sTenantEmail;
    @DatabaseField
    private String sSignFileName;
    @DatabaseField
    @SerializedName("sUnitNo")
    @Expose
    private String sUnitNo;
    @DatabaseField
    @SerializedName("result")
    @Expose
    private String result;

    @DatabaseField
    private boolean isOffline;

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    public String getsMonthlyMeterNo() {
        return sMonthlyMeterNo;
    }

    public void setSMonthlyMeterNo(String sMonthlyMeterNo) {
        this.sMonthlyMeterNo = sMonthlyMeterNo;
    }

    public String getsReadingNo() {
        return sReadingNo;
    }

    public void setsReadingNo(String sReadingNo) {
        this.sReadingNo = sReadingNo;
    }

    public String getSMeterNo() {
        return sMeterNo;
    }

    public void setsMeterNo(String sMeterNo) {
        this.sMeterNo = sMeterNo;
    }

    public String getSlastupdatedDate() {
        return slastupdatedDate;
    }

    public void setSlastupdatedDate(String slastupdatedDate) {
        this.slastupdatedDate = slastupdatedDate;
    }

    public String getSlastupdatedBy() {
        return slastupdatedBy;
    }

    public void setSlastupdatedBy(String slastupdatedBy) {
        this.slastupdatedBy = slastupdatedBy;
    }

    public String getSLocationID() {
        return sLocationID;
    }

    public void setsLocationID(String sLocationID) {
        this.sLocationID = sLocationID;
    }

    public String getSChecklistResultId() {
        return sChecklistResultId;
    }

    public void setsChecklistResultId(String sChecklistResultId) {
        this.sChecklistResultId = sChecklistResultId;
    }

    public String getSPrevConsumption() {
        return sPrevConsumption;
    }

    public void setsPrevConsumption(String sPrevConsumption) {
        this.sPrevConsumption = sPrevConsumption;
    }

    public String getSCurrReading() {
        return sCurrReading;
    }

    public void setsCurrReading(String sCurrReading) {
        this.sCurrReading = sCurrReading;
    }

    public String getSPrevReading() {
        return sPrevReading;
    }

    public void setsPrevReading(String sPrevReading) {
        this.sPrevReading = sPrevReading;
    }

    public String getSConsumption() {
        return sConsumption;
    }

    public void setsConsumption(String sConsumption) {
        this.sConsumption = sConsumption;
    }

    public String getSTakenByID() {
        return sTakenByID;
    }

    public void setsTakenByID(String sTakenByID) {
        this.sTakenByID = sTakenByID;
    }

    public String getSTenantID() {
        return sTenantID;
    }

    public void setsTenantID(String sTenantID) {
        this.sTenantID = sTenantID;
    }

    public String getSTenantNo() {
        return sTenantNo;
    }

    public void setsTenantNo(String sTenantNo) {
        this.sTenantNo = sTenantNo;
    }

    public String getSRemarks() {
        return sRemarks;
    }

    public void setsRemarks(String sRemarks) {
        this.sRemarks = sRemarks;
    }

    public String getSMeterRound() {
        return sMeterRound;
    }

    public void setsMeterRound(String sMeterRound) {
        this.sMeterRound = sMeterRound;
    }

    public String getSCustomerName() {
        return sCustomerName;
    }

    public void setsCustomerName(String sCustomerName) {
        this.sCustomerName = sCustomerName;
    }

    public String getSTenantName() {
        return sTenantName;
    }

    public void setsTenantName(String sTenantName) {
        this.sTenantName = sTenantName;
    }

    public String getSTenantEmail() {
        return sTenantEmail;
    }

    public void setsTenantEmail(String sTenantEmail) {
        this.sTenantEmail = sTenantEmail;
    }

    public String getSSignFileName() {
        return sSignFileName;
    }

    public void setsSignFileName(String sSignFileName) {
        this.sSignFileName = sSignFileName;
    }

    public String getSUnitNo() {
        return sUnitNo;
    }

    public void setsUnitNo(String sUnitNo) {
        this.sUnitNo = sUnitNo;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
