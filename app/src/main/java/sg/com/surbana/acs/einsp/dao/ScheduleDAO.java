package sg.com.surbana.acs.einsp.dao;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import sg.com.surbana.acs.einsp.vo.TimeCheckVO;
import android.util.Log;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 9, 2011		Ryan			Created
 *
 */
public class ScheduleDAO extends BaseDAO {
	private final String TAG = "FaultDAO.java";

	// public void insertFault(FaultVO vo) throws ULjException{
	// Log.v(TAG, "Save Case");
	//
	// Connection conn = DbUtil.getConnection();
	// StringBuffer query = new StringBuffer("Insert into tblFaultDetails  ");
	// query.append("(FaultID, FaultDate, LocationID, DetailDesc, FaultDesc, FaultCategoryID, PriorityID, ImageData,");
	// query.append("ImageFileName, FaultStatusID, LastModifiedDate, LastModifiedByUserInfoID, FaultReportedByUserInfoID) ");
	// query.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?) ");
	//
	// PreparedStatement ps = conn.prepareStatement(query.toString());
	//
	// try {
	// int index=1;
	// ps.set(index++, formatString(vo.getFaultId()));
	// ps.set(index++, vo.getFaultDate());
	// ps.set(index++, vo.getLocationId());
	// ps.set(index++, vo.getDetailDesc());
	// ps.set(index++, vo.getFaultDesc());
	// ps.set(index++, vo.getFaultCategoryId());
	// ps.set(index++, vo.getPriorityId());
	// ps.set(index++, "");
	// ps.set(index++, "");
	// ps.set(index++, vo.getFaultStatusId());
	// ps.set(index++, vo.getLastModifiedDate());
	// ps.set(index++, vo.getLastModifiedByUserInfoId());
	// ps.set(index++, vo.getFaultReportedByUserInfoId());
	// ps.execute();
	// conn.commit();
	// Log.d(TAG, "Fault created successfully");
	//
	// }catch (ULjException ejl){
	// Log.e(TAG, "Error in executing query " + query.toString(), ejl);
	// conn.rollback();
	// throw ejl;
	// }finally{
	// ps.close();
	// }
	// }
	//
	public ArrayList<ScheduleListVO> searchScheduleByCriteria(int taskStatus, Date revisedDate, boolean isCheckDateRequired, String barcode)
			throws ULjException {
		ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
		ScheduleListVO schduleListVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		// change for demo llm 22/08/2011
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		// bufQuery.append("SELECT top 10 a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		bufQuery.append("a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, a.PersonInChargeID, l.locationcode, cl.TemplateDescription, a.ServiceMaintProgID, f.frequencyDesc   "); //commercial
		bufQuery.append("FROM tblServiceServiceHistory a ");
		bufQuery.append("LEFT JOIN tblLocation l ");
		bufQuery.append("ON l.locationid = a.buildingId ");
		bufQuery.append("LEFT JOIN tblChecklist cl ");
		bufQuery.append("ON cl.templateID = a.ChecklistTemplateID ");
		bufQuery.append("LEFT JOIN tblterminal t ");
		bufQuery.append("ON t.terminalid = a.buildingid ");
		bufQuery.append("LEFT JOIN tblMstFrequency f  ");
		bufQuery.append("ON f.frequencyId =  a.FrequencyID ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND A.serviceMaintProgID in (1,20,21,22,23,6) ");
	
														// IS22: Filter out
														// tenant notice items
														// from the list

		if (isNotNullOrEmpty(barcode))
			bufQuery.append("AND  l.locationCode  = ? ");

		if (isCheckDateRequired)
			bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) = 0   )");

		switch (taskStatus) {
		case 1:
			bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
			break;
		case 2:
			bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
			break;
		case 4:
			bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
			break;
		case 3:
			bufQuery.append("AND a.StatusID = 3  ");
			break;

		}

		bufQuery.append("ORDER BY a.RevisedStart DESC, a.StatusID DESC ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (isNotNullOrEmpty(barcode))
				ps.set(ind++, barcode);

			if (isCheckDateRequired)
				ps.set(ind++, revisedDate);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setServiceDescription(rs.getString(columnIndex++));
				schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
				schduleListVo.setChecklistTemplateId(rs.getInt(columnIndex++));
				schduleListVo.setChecklistTemplateVersion(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
				schduleListVo.setLocation(rs.getString(columnIndex++));
				schduleListVo.setChecklistName(rs.getString(columnIndex++));

	        	//commercial-terminal checklist
            	schduleListVo.setServiceId(rs.getInt(columnIndex++));
           	 
            	if(schduleListVo.getServiceId()==AppsConstant.TERMINAL_CHECKLIST_SERVICE_ID)
            		schduleListVo.setLocation("Terminal " + schduleListVo.getBuildingId());
            		//            		schduleListVo.setLocation(rs.getString(columnIndex++));


            	schduleListVo.setFrequency(rs.getString(columnIndex++));
            	
				ScheduleList.add(schduleListVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return ScheduleList;
	}

	public ScheduleListVO searchScheduleByChecklistResultId(int checklistResultId) throws ULjException {
		ScheduleListVO schduleListVo = null;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.FrequencyId, a.statusId, a.remarks, a.serviceId ");
		bufQuery.append("FROM tblServiceServiceHistory a ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			ps.set(ind++, checklistResultId);

			rs = ps.executeQuery();
			if (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setFrequencyId(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setRemarks(rs.getString(columnIndex++));
				schduleListVo.setServiceMainProgId(rs.getInt(columnIndex++)); 
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);

		}

		return schduleListVo;
	}

	/**
	 * updateTarget = 1-> start time updateTarget = 2-> end time
	 * 
	 * @param vo
	 * @param updateTarget
	 * @throws ULjException
	 */
	public void updateScheduleTime(ScheduleListVO vo, int updateTarget) throws ULjException {
		StringBuffer bufQuery = new StringBuffer();
		PreparedStatement ps = null;
		Connection conn = DbUtil.getConnection();

		bufQuery.append("UPDATE ");
		bufQuery.append("tblEngServiceServiceHistory a ");
		bufQuery.append("SET ");
		if (updateTarget == 1)
			bufQuery.append("ActualStart =?, ");
		if (updateTarget == 2)
			bufQuery.append("ActualEnd=?, ");
		bufQuery.append("StatusID=?, ");
		bufQuery.append("remarks=?, ");
		bufQuery.append("LastModified=?  ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_STARTTIME)
				ps.set(ind++, vo.getActualStart());
			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_ENDTIME)
				ps.set(ind++, vo.getActualEnd());
			ps.set(ind++, vo.getStatusId());
			ps.set(ind++, vo.getRemarks());
			ps.set(ind++, vo.getLastModifiedDate());
			ps.set(ind++, vo.getChecklistResultId());

			ps.execute();
			conn.commit();

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}

	}

	public ArrayList<ScheduleListVO> searchScheduleExcludeStatus(int taskStatus, Date revisedDate, boolean isCheckDateRequired, String barcode)
			throws ULjException {
		ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
		ScheduleListVO schduleListVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		// change for demo llm 22/08/2011
		// bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		bufQuery.append("SELECT top 10 a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		bufQuery.append("a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, a.PersonInChargeID, l.locationcode, cl.TemplateDescription ");
		bufQuery.append("FROM tblServiceServiceHistory a ");
		bufQuery.append("LEFT JOIN tblLocation l ");
		bufQuery.append("ON l.locationid = a.buildingId ");
		bufQuery.append("LEFT JOIN tblChecklist cl ");
		bufQuery.append("ON cl.templateID = a.ChecklistTemplateID ");
		bufQuery.append("WHERE 1=1 ");

		if (isNotNullOrEmpty(barcode))
			bufQuery.append("AND  l.locationCode  = ? ");

		if (isCheckDateRequired)
			bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) = 0   ) ");

		switch (taskStatus) {
		case 1:
			bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
			break;
		case 2:
			bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
			break;
		case 4:
			bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
			break;
		case 6:
			bufQuery.append("AND a.StatusID = 3  ");
			break;
		// for demo 25/08/2011 case1+case2 =
		case 99:
			bufQuery.append("AND (a.ActualEnd IS NOT NULL AND a.StatusID != 3) or ( a.ActualStart IS NULL AND a.StatusID != 3 )");
			break;

		}

		bufQuery.append("ORDER BY a.RevisedStart DESC, a.StatusID DESC ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (isNotNullOrEmpty(barcode))
				ps.set(ind++, barcode);

			if (isCheckDateRequired)
				ps.set(ind++, revisedDate);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setServiceDescription(rs.getString(columnIndex++));
				schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
				schduleListVo.setChecklistTemplateId(rs.getInt(columnIndex++));
				schduleListVo.setChecklistTemplateVersion(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
				schduleListVo.setLocation(rs.getString(columnIndex++));
				schduleListVo.setChecklistName(rs.getString(columnIndex++));

				ScheduleList.add(schduleListVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return ScheduleList;
	}

	public ArrayList<ScheduleListVO> searchTenantScheduleByCriteria(int taskStatus, Date revisedDate, boolean isCheckDateRequired, String barcode)
			throws ULjException {
		ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
		ScheduleListVO schduleListVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		// change for demo llm 22/08/2011
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		// bufQuery.append("SELECT top 10 a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		bufQuery.append("a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, a.PersonInChargeID, l.locationcode, cl.TemplateDescription ");
		bufQuery.append("FROM tblTenantServiceServiceHistory a ");
		bufQuery.append("LEFT JOIN tblLocation l ");
		bufQuery.append("ON l.locationid = a.buildingId ");
		bufQuery.append("LEFT JOIN tblChecklist cl ");
		bufQuery.append("ON cl.templateID = a.ChecklistTemplateID ");
		bufQuery.append("WHERE 1=1 ");

		if (isNotNullOrEmpty(barcode))
			bufQuery.append("AND  l.locationCode  = ? ");

		if (isCheckDateRequired)
			bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) = 0   )");

		switch (taskStatus) {
		case 1:
			bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
			break;
		case 2:
			bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
			break;
		case 4:
			bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
			break;
		case 6:
			bufQuery.append("AND a.StatusID = 3  ");
			break;

		}

		bufQuery.append("ORDER BY a.RevisedStart DESC, a.StatusID DESC ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (isNotNullOrEmpty(barcode))
				ps.set(ind++, barcode);

			if (isCheckDateRequired)
				ps.set(ind++, revisedDate);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setServiceDescription(rs.getString(columnIndex++));
				schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
				schduleListVo.setChecklistTemplateId(rs.getInt(columnIndex++));
				schduleListVo.setChecklistTemplateVersion(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
				schduleListVo.setLocation(rs.getString(columnIndex++));
				// schduleListVo.setChecklistName(rs.getString(columnIndex++));
				schduleListVo.setChecklistName("Weekly inspection");
				schduleListVo.setTenantName("Burger King");

				ScheduleList.add(schduleListVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return ScheduleList;
	}

	public void updateTenantScheduleTime(ScheduleListVO vo, int updateTarget) throws ULjException {
		StringBuffer bufQuery = new StringBuffer();
		PreparedStatement ps = null;
		Connection conn = DbUtil.getConnection();

		bufQuery.append("UPDATE ");
		bufQuery.append("tblTenantServiceServiceHistory a ");
		bufQuery.append("SET ");
		if (updateTarget == 1)
			bufQuery.append("ActualStart =?, ");
		if (updateTarget == 2)
			bufQuery.append("ActualEnd=?, ");
		bufQuery.append("StatusID=?, ");
		bufQuery.append("remarks=?, ");
		bufQuery.append("LastModified=?  ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_STARTTIME)
				ps.set(ind++, vo.getActualStart());
			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_ENDTIME)
				ps.set(ind++, vo.getActualEnd());
			ps.set(ind++, vo.getStatusId());
			ps.set(ind++, vo.getRemarks());
			ps.set(ind++, vo.getLastModifiedDate());
			ps.set(ind++, vo.getChecklistResultId());

			ps.execute();
			conn.commit();

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}

	}

	public ScheduleListVO searchTenantScheduleByChecklistResultId(int checklistResultId) throws ULjException {
		ScheduleListVO schduleListVo = null;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.FrequencyId, a.statusId, a.remarks ");
		bufQuery.append("FROM tblTenantServiceServiceHistory a ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			ps.set(ind++, checklistResultId);

			rs = ps.executeQuery();
			if (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setFrequencyId(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setRemarks(rs.getString(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);

		}

		return schduleListVo;
	}

	public ArrayList<ScheduleListVO> searchToiletScheduleByCriteria(int taskStatus, Date revisedDate, boolean isCheckDateRequired, String barcode)
			throws ULjException {
		ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
		ScheduleListVO schduleListVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		// change for demo llm 22/08/2011
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
		bufQuery.append("a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, a.PersonInChargeID, l.locationcode, cl.TemplateDescription,  l.location,lv.levelDesc ");// cl.DropDownGroupId,
		bufQuery.append("FROM tblToiletServiceServiceHistory a ");
		bufQuery.append("LEFT JOIN tblLocation l ");
		bufQuery.append(" ON l.locationid = a.buildingId ");
		bufQuery.append(" LEFT JOIN tblZone z ");
		bufQuery.append(" ON l.zoneId= z.zoneId");
		bufQuery.append(" LEFT JOIN tblarea ar ");
		bufQuery.append(" ON ar.areaId= z.areaId ");
		bufQuery.append(" LEFT JOIN tblLevel lv ");
		bufQuery.append(" ON lv.levelId= ar.levelId   ");
		bufQuery.append("LEFT JOIN tblToiletChecklist cl ");
		bufQuery.append("ON cl.templateID = a.ChecklistTemplateID ");
		bufQuery.append("WHERE 1=1 ");
		// bufQuery.append("and  month(a.RevisedStart)=month(getdate()) and year(a.RevisedStart)=year(getdate()) "
		// );

		if (isNotNullOrEmpty(barcode))
			bufQuery.append("AND  l.locationCode  = ? ");

		if (isCheckDateRequired) {
			// LLM 23022012 : TRC list to show yesterday items up to until 1 am
			Date d1 = new Date();
			if (d1.getHours() == 0) {
				bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) <= 1   )");
			} else {
				bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) = 0   )");
			}
			// LLM 23022012 : TRC list to show yesterday items up to until 1 am

		}
		switch (taskStatus) {
		case 1:
			bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
			break;
		case 2:
			bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
			break;
		case 4:
			bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
			break;
		case 6:
			bufQuery.append("AND a.StatusID = 3  ");
			break;

		}

		bufQuery.append("ORDER BY a.RevisedStart,lv.levelDesc ASC, a.StatusID DESC ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int indx = 1;
			if (isNotNullOrEmpty(barcode))
				ps.set(indx++, barcode);

			if (isCheckDateRequired)
				ps.set(indx++, revisedDate);

			rs = ps.executeQuery();

			while (rs.next()) {
				int cidx = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(cidx++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(cidx++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(cidx++)));
				schduleListVo.setBuildingId(rs.getInt(cidx++));
				schduleListVo.setServiceDescription(rs.getString(cidx++));
				schduleListVo.setChecklistResultId(rs.getInt(cidx++));
				schduleListVo.setChecklistTemplateId(rs.getInt(cidx++));
				schduleListVo.setChecklistTemplateVersion(rs.getInt(cidx++));
				schduleListVo.setStatusId(rs.getInt(cidx++));
				schduleListVo.setPersonInChargeId(rs.getString(cidx++));
				schduleListVo.setLocation(rs.getString(cidx++));
				schduleListVo.setChecklistName(rs.getString(cidx++));
				// schduleListVo.setDropdowngroupid(rs.getInt(cidx++));
				schduleListVo.setLocationname(rs.getString(cidx++));
				ScheduleList.add(schduleListVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return ScheduleList;
	}

	public ScheduleListVO searchToiletScheduleByChecklistResultId(int checklistResultId) throws ULjException {
		ScheduleListVO schduleListVo = null;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.FrequencyId, a.statusId, a.remarks ");
		bufQuery.append("FROM tblToiletServiceServiceHistory a ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			ps.set(ind++, checklistResultId);

			rs = ps.executeQuery();
			if (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setFrequencyId(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setRemarks(rs.getString(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);

		}

		return schduleListVo;
	}

	// TenantNotice llm 20/11/2011
	public ArrayList<ScheduleListVO> searchTenantNoticeScheduleByCriteria(int taskStatus, Date revisedDate, boolean isCheckDateRequired, String barcode)
			throws ULjException {
		ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
//		ScheduleListVO schduleListVo;
//		Connection conn = DbUtil.getConnection();
//		DateUtil dUtil = new DateUtil();
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		StringBuffer bufQuery = new StringBuffer();
//
//		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, ");
//		bufQuery.append("l.locationid, a.ServiceDescription, a.ChecklistResultID, ");
//		bufQuery.append(" a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, ");
//		bufQuery.append("a.PersonInChargeID, l.locationcode , tn.terminalid, tn.noticeid, ");
//		bufQuery.append("tn.noticestatusid, tn.NoticeRemarks ,tn.NoticeDate, tn.NoticeInspOfficerUserinfoID, tn.noticeno, tn.tenantid, u.userid, tn.lastmodifieddate, ");
//		bufQuery.append("tn.followupdate, tn.reminderdate ");
//		bufQuery.append("FROM tbltenantnotice tn ");
//		bufQuery.append("LEFT JOIN tblLocation l ON l.locationid = tn.LocationID ");
//		bufQuery.append("left join tblServiceServiceHistory a on a.ChecklistResultID = tn.ChecklistResultID ");
//		bufQuery.append("left join tbluserinfo u on tn.NoticeInspOfficerUserinfoID = u.userinfoid ");
//		bufQuery.append("WHERE 1=1 ");
//
//		if (isNotNullOrEmpty(barcode))
//			bufQuery.append("AND  l.locationCode  = ? ");
//
//		if (isCheckDateRequired)
//
//		{
//			bufQuery.append("and ((a.ServiceMaintProgID = 5 and  Datediff(dd,a.RevisedStart,?) = 0) ");
//			bufQuery.append("or ( a.ServiceMaintProgID is null  and a.RevisedStart is null and  Datediff(dd,tn.NoticeDate, ?)=0)) ");
//		}
//
//		if (taskStatus != 0)
//			bufQuery.append("AND tn.noticestatusid =" + Integer.toString(taskStatus));
//
//
//		bufQuery.append(" order by tn.lastmodifieddate desc ");
//
//		try {
//			ps = conn.prepareStatement(bufQuery.toString());
//			int ind = 1;
//			if (isNotNullOrEmpty(barcode)) {
//				ps.set(ind++, barcode);
//			}
//
//			if (isCheckDateRequired) {
//				ps.set(ind++, revisedDate);
//				ps.set(ind++, revisedDate);
//			}
//
//			rs = ps.executeQuery();
//			while (rs.next()) {
//				int columnIndex = 1;
//				schduleListVo = new ScheduleListVO();
//				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
//				schduleListVo.setServiceDescription(rs.getString(columnIndex++));
//				schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
//				schduleListVo.setChecklistTemplateId(rs.getInt(columnIndex++));
//				schduleListVo.setChecklistTemplateVersion(rs.getInt(columnIndex++));
//				schduleListVo.setStatusId(rs.getInt(columnIndex++));
//				schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
//				schduleListVo.setLocation(rs.getString(columnIndex++));
//				// TenantNotice llm 20/11/2011
//				schduleListVo.setChecklistName(schduleListVo.getServiceDescription());
//				schduleListVo.setTerminalid(rs.getInt(columnIndex++));
//				schduleListVo.setNoticeid(rs.getInt(columnIndex++));
//				schduleListVo.setNoticeStatus(rs.getInt(columnIndex++));
//				schduleListVo.setRemarks(rs.getString(columnIndex++));
//				schduleListVo.setNoticedate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				schduleListVo.setNoticeinspid(rs.getInt(columnIndex++));
//				schduleListVo.setNoticeNo(rs.getInt(columnIndex++));
//				schduleListVo.setTenantId(rs.getInt(columnIndex++));
//				schduleListVo.setUserid(rs.getString(columnIndex++));
//				schduleListVo.setFollowupdate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				schduleListVo.setReminderdate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				ScheduleList.add(schduleListVo);
//			}
//		} catch (ULjException e) {
//			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
//			throw e;
//		} finally {
//			closeResultSet(rs);
//			closePreparedStatement(ps);
//		}

		return ScheduleList;
	}

	
	public ArrayList<ScheduleListVO> searchMonthlyMeterScheduleByCriteria(int taskStatus, Date revisedDate, boolean isCheckDateRequired, String barcode)
			throws ULjException {
		ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
		ScheduleListVO schduleListVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 03/05/2013 new table for monthly meter delay sync
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart,  ");
		bufQuery.append("l.locationid, a.ServiceDescription, a.ChecklistResultID,   ");
		bufQuery.append("a.StatusID,  a.PersonInChargeID, l.locationcode ");
		//bufQuery.append("a.StatusID,  a.PersonInChargeID, l.locationcode,tn.tenantname,  sm.SyncStatus  ");
		bufQuery.append(" FROM   ");
		bufQuery.append(" tblEngServiceServiceHistory a ");
		bufQuery.append("LEFT JOIN tblEngLocation l ON l.locationid = a.BuildingID  ");
		bufQuery.append("left join tbluserinfo u on a.PersonInChargeID =  u.userid  ");
		//bufQuery.append("left join tblTenantLocation tl on a.BuildingID =  tl.locationid ");
		//bufQuery.append("left join tblTenant tn on tl.tenantid = tn.tenantid ");
		//bufQuery.append("left join tblSyncedMonthlyMeterService sm on sm.ServiceMaintProgId=a.ServiceID and sm.ServiceResultId=a.ChecklistResultID ");
		bufQuery.append("WHERE 1=1 and a.ServiceMaintProgID = 10");


		if (isNotNullOrEmpty(barcode))
			bufQuery.append(" AND  l.locationCode  = ? ");

		if (isCheckDateRequired)
		{
			bufQuery.append(" and   Datediff(dd,a.RevisedStart,?) = 0 ");
		}

		switch (taskStatus) {
		case 1:
			bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
			break;
		case 2:
			bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
			break;
		case 4:
			bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
			break;
		case 6:
			bufQuery.append("AND a.StatusID = 3  ");
			break;

		}
		
		bufQuery.append(" order by a.RevisedStart DESC, a.StatusID DESC, l.locationcode ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (isNotNullOrEmpty(barcode)) {
				ps.set(ind++, barcode);
			}

			if (isCheckDateRequired) {
				ps.set(ind++, revisedDate);
				//ps.set(ind++, revisedDate);
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setServiceDescription(rs.getString(columnIndex++));
				schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
				schduleListVo.setLocation(rs.getString(columnIndex++));//+"-"+rs.getString(columnIndex++)
				//schduleListVo.setTenantId(rs.getInt(columnIndex++));
				//schduleListVo.setTenantNo(rs.getInt(columnIndex++));
				schduleListVo.setChecklistName(schduleListVo.getServiceDescription());
				//schduleListVo.setMonthlymetersyncstatus(rs.getInt(columnIndex++)); //llm 03/05/2013 new table for monthly meter delay sync
				ScheduleList.add(schduleListVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return ScheduleList;
	}
	
	
	public ArrayList<ScheduleListVO> searchMonthlyMeterScheduleByCriteria2(int taskStatus, Date revisedDate, boolean isCheckDateRequired, LocationVO lvo)
	throws ULjException {
	ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
	ScheduleListVO schduleListVo;
	Connection conn = DbUtil.getConnection();
	DateUtil dUtil = new DateUtil();
	PreparedStatement ps = null;
	ResultSet rs = null;
	StringBuffer bufQuery = new StringBuffer();
	
	bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart,  ");
	bufQuery.append("l.locationid, a.ServiceDescription, a.ChecklistResultID,   ");
	bufQuery.append("a.StatusID,  a.PersonInChargeID, l.locationcode , z.Zoneid, ar.AreaID, lv.LevelID, t.TerminalId ");//llm 03/05/2013 new table for monthly meter delay sync
//	bufQuery.append("a.StatusID,  a.PersonInChargeID, l.locationcode ,tn.tenantName,sm.SyncStatus, z.Zoneid, ar.AreaID, lv.LevelID, t.TerminalId ");//llm 03/05/2013 new table for monthly meter delay sync
	bufQuery.append(" FROM   ");
	bufQuery.append(" tblEngServiceServiceHistory a ");
	bufQuery.append("LEFT JOIN tblEngLocation l ON l.locationid = a.BuildingID  ");
	bufQuery.append("left join tbluserinfo u on a.PersonInChargeID =  u.userid  ");
	bufQuery.append("left join tblzone z on z.ZoneID= l.ZoneID  ");
	bufQuery.append("left join tblarea ar on ar.AreaID=z.AreaID ");
	bufQuery.append("left join tbllevel lv on lv.LevelID= ar.LevelID ");
	bufQuery.append("left join tblTerminal t on t.TerminalId=lv.TerminalId ");
	//bufQuery.append("left join tblTenantLocation tl on a.BuildingID =  tl.locationid ");
	//bufQuery.append("left join tblTenant tn on tl.tenantid = tn.tenantid ");
	//bufQuery.append("left join tblSyncedMonthlyMeterService sm on sm.ServiceMaintProgId=a.ServiceID and sm.ServiceResultId=a.ChecklistResultID ");
	bufQuery.append("WHERE 1=1 and a.ServiceMaintProgID = 10");
	
	if (lvo.getLocationId()!=null)
	{
		if (Integer.parseInt(lvo.getLocationId())>0)
			bufQuery.append(" and  l.locationid= ? ");	
	}
	
	if (lvo.getZoneId()!=null)
	{
		if (Integer.parseInt(lvo.getZoneId())>0)
			bufQuery.append(" and  z.zoneid= ? ");	
	}
	
	if (lvo.getAreaId()!=null)
	{
		if (Integer.parseInt(lvo.getAreaId())>0)
			bufQuery.append(" and  ar.areaid= ? ");	
	}
	
	if (lvo.getLevelId()!=null)
	{
		if (Integer.parseInt(lvo.getLevelId())>0)
			bufQuery.append(" and  lv.levelid= ? ");	
	}
	
	if (lvo.getTerminalId()>0)
	{
		bufQuery.append(" and  t.terminalid= ? ");	
	}
	
	if (isCheckDateRequired)
	{
		bufQuery.append(" and   Datediff(dd,a.RevisedStart,?) = 0 ");
	}
	
	switch (taskStatus) {
	case 1:
		bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
		break;
	case 2:
		bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
		break;
	case 4:
		bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
		break;
	case 6:
		bufQuery.append("AND a.StatusID = 3  ");
		break;
	
	}
	
	bufQuery.append(" order by a.RevisedStart DESC, a.StatusID DESC, l.locationcode ");
	
	try {
		ps = conn.prepareStatement(bufQuery.toString());
		int ind = 1;
		
		if (lvo.getLocationId()!=null)
		{
			if (Integer.parseInt(lvo.getLocationId())>0)
				ps.set(ind++, lvo.getLocationId());
		}
		
		if (lvo.getZoneId()!=null)
		{
			if (Integer.parseInt(lvo.getZoneId())>0)
				ps.set(ind++, lvo.getZoneId());
		}
		
		if (lvo.getAreaId()!=null)
		{
			if (Integer.parseInt(lvo.getAreaId())>0)
				ps.set(ind++, lvo.getAreaId());
		}
		
		if (lvo.getLevelId()!=null)
		{
			if (Integer.parseInt(lvo.getLevelId())>0)
				ps.set(ind++, lvo.getLevelId());
		}
		
		if (lvo.getTerminalId()>0)
		{
			ps.set(ind++, lvo.getTerminalId());
		}
		
		if (isCheckDateRequired) {
			ps.set(ind++, revisedDate);
			//ps.set(ind++, revisedDate);
		}
	
		rs = ps.executeQuery();
		while (rs.next()) {
			int columnIndex = 1;
			schduleListVo = new ScheduleListVO();
			schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			schduleListVo.setBuildingId(rs.getInt(columnIndex++));
			schduleListVo.setServiceDescription(rs.getString(columnIndex++));
			schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
			schduleListVo.setStatusId(rs.getInt(columnIndex++));
			schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
			schduleListVo.setLocation(rs.getString(columnIndex++));//+"-"+rs.getString(columnIndex++)
			//schduleListVo.setTenantId(rs.getInt(columnIndex++));
			//schduleListVo.setTenantNo(rs.getInt(columnIndex++));
			schduleListVo.setChecklistName(schduleListVo.getServiceDescription());
			//schduleListVo.setMonthlymetersyncstatus(rs.getInt(columnIndex++)); //llm 03/05/2013 new table for monthly meter delay sync
			
			ScheduleList.add(schduleListVo);
		}
	} catch (ULjException e) {
		Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		throw e;
	} finally {
		closeResultSet(rs);
		closePreparedStatement(ps);
	}
	
	return ScheduleList;
}
	
	
	public ArrayList<ScheduleListVO> searchScheduleByCriteria2(int taskStatus, Date revisedDate, boolean isCheckDateRequired, LocationVO lvo)
	throws ULjException {
	ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
	ScheduleListVO schduleListVo;
	Connection conn = DbUtil.getConnection();
	DateUtil dUtil = new DateUtil();
	PreparedStatement ps = null;
	ResultSet rs = null;
	StringBuffer bufQuery = new StringBuffer();
	
	bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
	bufQuery.append("a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, a.PersonInChargeID, l.locationcode, cl.TemplateDescription, a.ServiceMaintProgID,  f.frequencyDesc  "); //commercial
	bufQuery.append("FROM tblServiceServiceHistory a ");
	bufQuery.append("LEFT JOIN tblLocation l ");
	bufQuery.append("ON l.locationid = a.buildingId ");
	bufQuery.append("LEFT JOIN tblChecklist cl ");
	bufQuery.append("ON cl.templateID = a.ChecklistTemplateID ");
	bufQuery.append("left join tblzone z on z.ZoneID= l.ZoneID   ");
	bufQuery.append("left join tblarea ar on ar.AreaID=z.AreaID ");
	bufQuery.append("left join tbllevel lv on lv.LevelID= ar.LevelID ");
	bufQuery.append("left join tblTerminal t on t.TerminalId=lv.TerminalId ");
	bufQuery.append("LEFT JOIN tblMstFrequency f  ");
	bufQuery.append("ON f.frequencyId =  a.FrequencyID ");
	bufQuery.append("WHERE 1=1 ");
	bufQuery.append("AND A.serviceMaintProgID in (1,20,21,22,23,6) ");

	;// IS22: Filter out
													// tenant notice items
													// from the list
	
	if (lvo.getLocationId()!=null)
	{
		if (Integer.parseInt(lvo.getLocationId())>0)
			bufQuery.append(" and  l.locationid= ? ");	
	}
	
	if (lvo.getZoneId()!=null)
	{
		if (Integer.parseInt(lvo.getZoneId())>0)
			bufQuery.append(" and  z.zoneid= ? ");	
	}
	
	if (lvo.getAreaId()!=null)
	{
		if (Integer.parseInt(lvo.getAreaId())>0)
			bufQuery.append(" and  ar.areaid= ? ");	
	}
	
	if (lvo.getLevelId()!=null)
	{
		if (Integer.parseInt(lvo.getLevelId())>0)
			bufQuery.append(" and  lv.levelid= ? ");	
	}
	
	if (lvo.getTerminalId()>0)
	{
		bufQuery.append(" and  t.terminalid= ? ");	
	}
	
	if (isCheckDateRequired)
		bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) = 0   )");
	
	switch (taskStatus) {
	case 1:
		bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
		break;
	case 2:
		bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
		break;
	case 4:
		bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
		break;
	case 3:
		bufQuery.append("AND a.StatusID = 3  ");
		break;
	
	}
	
	bufQuery.append("ORDER BY a.RevisedStart DESC, a.StatusID DESC ");
	
	try {
		ps = conn.prepareStatement(bufQuery.toString());
		int ind = 1;
		if (lvo.getLocationId()!=null)
		{
			if (Integer.parseInt(lvo.getLocationId())>0)
				ps.set(ind++, lvo.getLocationId());
		}
		
		if (lvo.getZoneId()!=null)
		{
			if (Integer.parseInt(lvo.getZoneId())>0)
				ps.set(ind++, lvo.getZoneId());
		}
		
		if (lvo.getAreaId()!=null)
		{
			if (Integer.parseInt(lvo.getAreaId())>0)
				ps.set(ind++, lvo.getAreaId());
		}
		
		if (lvo.getLevelId()!=null)
		{
			if (Integer.parseInt(lvo.getLevelId())>0)
				ps.set(ind++, lvo.getLevelId());
		}
		
		if (lvo.getTerminalId()>0)
		{
			ps.set(ind++, lvo.getTerminalId());
		}
		if (isCheckDateRequired)
			ps.set(ind++, revisedDate);
	
		rs = ps.executeQuery();
		while (rs.next()) {
			int columnIndex = 1;
			schduleListVo = new ScheduleListVO();
			schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			schduleListVo.setBuildingId(rs.getInt(columnIndex++));
			schduleListVo.setServiceDescription(rs.getString(columnIndex++));
			schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
			schduleListVo.setChecklistTemplateId(rs.getInt(columnIndex++));
			schduleListVo.setChecklistTemplateVersion(rs.getInt(columnIndex++));
			schduleListVo.setStatusId(rs.getInt(columnIndex++));
			schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
			schduleListVo.setLocation(rs.getString(columnIndex++));
			schduleListVo.setChecklistName(rs.getString(columnIndex++));
	
	    	//commercial-terminal checklist
	    	schduleListVo.setServiceId(rs.getInt(columnIndex++));
	   	 	//override building ID to terminal for terminal check
	    	if(schduleListVo.getServiceId()==AppsConstant.TERMINAL_CHECKLIST_SERVICE_ID)
	    		schduleListVo.setLocation("Terminal " + schduleListVo.getBuildingId());
//	            	schduleListVo.setLocation(rs.getString(columnIndex++));
	
	    	schduleListVo.setFrequency(rs.getString(columnIndex++));

			ScheduleList.add(schduleListVo);
		}
	} catch (ULjException e) {
		Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		throw e;
	} finally {
		closeResultSet(rs);
		closePreparedStatement(ps);
	}
	
	return ScheduleList;
	}
	
	
	public ArrayList<ScheduleListVO> searchToiletScheduleByCriteria2(int taskStatus, Date revisedDate, boolean isCheckDateRequired,LocationVO lvo)
	throws ULjException {
	ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
	ScheduleListVO schduleListVo;
	Connection conn = DbUtil.getConnection();
	DateUtil dUtil = new DateUtil();
	PreparedStatement ps = null;
	ResultSet rs = null;
	StringBuffer bufQuery = new StringBuffer();
	// change for demo llm 22/08/2011
	bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.ServiceDescription, a.ChecklistResultID, ");
	bufQuery.append("a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, a.PersonInChargeID, l.locationcode, cl.TemplateDescription,  l.location,lv.levelDesc ");// cl.DropDownGroupId,
	bufQuery.append("FROM tblToiletServiceServiceHistory a ");
	bufQuery.append("LEFT JOIN tblLocation l ");
	bufQuery.append(" ON l.locationid = a.buildingId ");
	bufQuery.append("left join tblzone z on z.ZoneID= l.ZoneID  ");
	bufQuery.append("left join tblarea ar on ar.AreaID=z.AreaID ");
	bufQuery.append("left join tbllevel lv on lv.LevelID= ar.LevelID ");
	bufQuery.append("left join tblTerminal t on t.TerminalId=lv.TerminalId ");
	bufQuery.append("LEFT JOIN tblToiletChecklist cl ");
	bufQuery.append("ON cl.templateID = a.ChecklistTemplateID ");
	bufQuery.append("WHERE 1=1 ");
	
	if (lvo.getLocationId()!=null)
	{
		if (Integer.parseInt(lvo.getLocationId())>0)
			bufQuery.append(" and  l.locationid= ? ");	
	}
	
	if (lvo.getZoneId()!=null)
	{
		if (Integer.parseInt(lvo.getZoneId())>0)
			bufQuery.append(" and  z.zoneid= ? ");	
	}
	
	if (lvo.getAreaId()!=null)
	{
		if (Integer.parseInt(lvo.getAreaId())>0)
			bufQuery.append(" and  ar.areaid= ? ");	
	}
	
	if (lvo.getLevelId()!=null)
	{
		if (Integer.parseInt(lvo.getLevelId())>0)
			bufQuery.append(" and  lv.levelid= ? ");	
	}
	
	if (lvo.getTerminalId()>0)
	{
		bufQuery.append(" and  t.terminalid= ? ");	
	}
	if (isCheckDateRequired) {
		// LLM 23022012 : TRC list to show yesterday items up to until 1 am
		Date d1 = new Date();
		if (d1.getHours() == 0) {
			bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) <= 1   )");
		} else {
			bufQuery.append("AND ( Datediff(dd,a.RevisedStart, ?) = 0   )");
		}
		// LLM 23022012 : TRC list to show yesterday items up to until 1 am
	
	}
	switch (taskStatus) {
	case 1:
		bufQuery.append("AND a.ActualEnd IS NOT NULL AND a.StatusID != 3  ");
		break;
	case 2:
		bufQuery.append("AND a.ActualStart IS NULL AND a.StatusID != 3 ");
		break;
	case 4:
		bufQuery.append("AND a.ActualStart IS NOT NULL AND a.ActualEnd IS NULL AND a.StatusID != 3 ");
		break;
	case 6:
		bufQuery.append("AND a.StatusID = 3  ");
		break;
	
	}
	
	bufQuery.append("ORDER BY a.RevisedStart,lv.levelDesc ASC, a.StatusID DESC ");
	
	try {
		ps = conn.prepareStatement(bufQuery.toString());
		int indx = 1;
		if (lvo.getLocationId()!=null)
		{
			if (Integer.parseInt(lvo.getLocationId())>0)
				ps.set(indx++, lvo.getLocationId());
		}
		
		if (lvo.getZoneId()!=null)
		{
			if (Integer.parseInt(lvo.getZoneId())>0)
				ps.set(indx++, lvo.getZoneId());
		}
		
		if (lvo.getAreaId()!=null)
		{
			if (Integer.parseInt(lvo.getAreaId())>0)
				ps.set(indx++, lvo.getAreaId());
		}
		
		if (lvo.getLevelId()!=null)
		{
			if (Integer.parseInt(lvo.getLevelId())>0)
				ps.set(indx++, lvo.getLevelId());
		}
		
		if (lvo.getTerminalId()>0)
		{
			ps.set(indx++, lvo.getTerminalId());
		}
		if (isCheckDateRequired)
			ps.set(indx++, revisedDate);
	
		rs = ps.executeQuery();
	
		while (rs.next()) {
			int cidx = 1;
			schduleListVo = new ScheduleListVO();
			schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(cidx++)));
			schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(cidx++)));
			schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(cidx++)));
			schduleListVo.setBuildingId(rs.getInt(cidx++));
			schduleListVo.setServiceDescription(rs.getString(cidx++));
			schduleListVo.setChecklistResultId(rs.getInt(cidx++));
			schduleListVo.setChecklistTemplateId(rs.getInt(cidx++));
			schduleListVo.setChecklistTemplateVersion(rs.getInt(cidx++));
			schduleListVo.setStatusId(rs.getInt(cidx++));
			schduleListVo.setPersonInChargeId(rs.getString(cidx++));
			schduleListVo.setLocation(rs.getString(cidx++));
			schduleListVo.setChecklistName(rs.getString(cidx++));
			// schduleListVo.setDropdowngroupid(rs.getInt(cidx++));
			schduleListVo.setLocationname(rs.getString(cidx++));
			ScheduleList.add(schduleListVo);
		}
	} catch (ULjException e) {
		Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		throw e;
	} finally {
		closeResultSet(rs);
		closePreparedStatement(ps);
	}
	
	return ScheduleList;
	}

	public ArrayList<ScheduleListVO> searchTenantNoticeScheduleByCriteria2(int taskStatus, Date revisedDate, boolean isCheckDateRequired, LocationVO lvo)
	throws ULjException {
	ArrayList<ScheduleListVO> ScheduleList = new ArrayList();
//	ScheduleListVO schduleListVo;
//	Connection conn = DbUtil.getConnection();
//	DateUtil dUtil = new DateUtil();
//	PreparedStatement ps = null;
//	ResultSet rs = null;
//	StringBuffer bufQuery = new StringBuffer();
//
//	bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, ");
//	bufQuery.append("l.locationid, a.ServiceDescription, a.ChecklistResultID, ");
//	bufQuery.append(" a.ChecklistTemplateID, a.ChecklistTemplateVersion, a.StatusID, ");
//	bufQuery.append("a.PersonInChargeID, l.locationcode , tn.terminalid, tn.noticeid, ");
//	bufQuery.append("tn.noticestatusid, tn.NoticeRemarks ,tn.NoticeDate, tn.NoticeInspOfficerUserinfoID, tn.noticeno, tn.tenantid, u.userid, tn.lastmodifieddate, ");
//	bufQuery.append("tn.followupdate, tn.reminderdate ");
//	bufQuery.append("FROM tbltenantnotice tn ");
//	bufQuery.append("LEFT JOIN tblLocation l ON l.locationid = tn.LocationID ");
//	bufQuery.append("left join tblServiceServiceHistory a on a.ChecklistResultID = tn.ChecklistResultID ");
//	bufQuery.append("left join tbluserinfo u on tn.NoticeInspOfficerUserinfoID = u.userinfoid ");
//	bufQuery.append("left join tblzone z on z.ZoneID= l.ZoneID  ");
//	bufQuery.append("left join tblarea ar on ar.AreaID=z.AreaID ");
//	bufQuery.append("left join tbllevel lv on lv.LevelID= ar.LevelID ");
//	bufQuery.append("left join tblTerminal t on t.TerminalId=lv.TerminalId ");
//	bufQuery.append("WHERE 1=1 ");
//	if (lvo.getLocationId()!=null)
//	{
//		if (Integer.parseInt(lvo.getLocationId())>0)
//			bufQuery.append(" and  l.locationid= ? ");
//	}
//
//	if (lvo.getZoneId()!=null)
//	{
//		if (Integer.parseInt(lvo.getZoneId())>0)
//			bufQuery.append(" and  z.zoneid= ? ");
//	}
//
//	if (lvo.getAreaId()!=null)
//	{
//		if (Integer.parseInt(lvo.getAreaId())>0)
//			bufQuery.append(" and  ar.areaid= ? ");
//	}
//
//	if (lvo.getLevelId()!=null)
//	{
//		if (Integer.parseInt(lvo.getLevelId())>0)
//			bufQuery.append(" and  lv.levelid= ? ");
//	}
//
//	if (lvo.getImageVO()>0)
//	{
//		bufQuery.append(" and  t.terminalid= ? ");
//	}
//
//	if (isCheckDateRequired)
//
//	{
//		bufQuery.append("and ((a.ServiceMaintProgID = 5 and  Datediff(dd,a.RevisedStart,?) = 0) ");
//		bufQuery.append("or ( a.ServiceMaintProgID is null  and a.RevisedStart is null and  Datediff(dd,tn.NoticeDate, ?)=0)) ");
//	}
//
//	if (taskStatus != 0)
//		bufQuery.append("AND tn.noticestatusid =" + Integer.toString(taskStatus));
//
//
//	bufQuery.append(" order by tn.lastmodifieddate desc ");
//
//	try {
//		ps = conn.prepareStatement(bufQuery.toString());
//		int ind = 1;
//		if (lvo.getLocationId()!=null)
//		{
//			if (Integer.parseInt(lvo.getLocationId())>0)
//				ps.set(ind++, lvo.getLocationId());
//		}
//
//		if (lvo.getZoneId()!=null)
//		{
//			if (Integer.parseInt(lvo.getZoneId())>0)
//				ps.set(ind++, lvo.getZoneId());
//		}
//
//		if (lvo.getAreaId()!=null)
//		{
//			if (Integer.parseInt(lvo.getAreaId())>0)
//				ps.set(ind++, lvo.getAreaId());
//		}
//
//		if (lvo.getLevelId()!=null)
//		{
//			if (Integer.parseInt(lvo.getLevelId())>0)
//				ps.set(ind++, lvo.getLevelId());
//		}
//
//		if (lvo.getImageVO()>0)
//		{
//			ps.set(ind++, lvo.getImageVO());
//		}
//		if (isCheckDateRequired) {
//			ps.set(ind++, revisedDate);
//			ps.set(ind++, revisedDate);
//		}
//
//		rs = ps.executeQuery();
//		while (rs.next()) {
//			int columnIndex = 1;
//			schduleListVo = new ScheduleListVO();
//			schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//			schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//			schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//			schduleListVo.setBuildingId(rs.getInt(columnIndex++));
//			schduleListVo.setServiceDescription(rs.getString(columnIndex++));
//			schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
//			schduleListVo.setChecklistTemplateId(rs.getInt(columnIndex++));
//			schduleListVo.setChecklistTemplateVersion(rs.getInt(columnIndex++));
//			schduleListVo.setStatusId(rs.getInt(columnIndex++));
//			schduleListVo.setPersonInChargeId(rs.getString(columnIndex++));
//			schduleListVo.setLocation(rs.getString(columnIndex++));
//			// TenantNotice llm 20/11/2011
//			schduleListVo.setChecklistName(schduleListVo.getServiceDescription());
//			schduleListVo.setTerminalid(rs.getInt(columnIndex++));
//			schduleListVo.setNoticeid(rs.getInt(columnIndex++));
//			schduleListVo.setNoticeStatus(rs.getInt(columnIndex++));
//			schduleListVo.setRemarks(rs.getString(columnIndex++));
//			schduleListVo.setNoticedate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//			schduleListVo.setNoticeinspid(rs.getInt(columnIndex++));
//			schduleListVo.setNoticeNo(rs.getInt(columnIndex++));
//			schduleListVo.setTenantId(rs.getInt(columnIndex++));
//			schduleListVo.setUserid(rs.getString(columnIndex++));
//			schduleListVo.setFollowupdate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//			schduleListVo.setReminderdate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//			ScheduleList.add(schduleListVo);
//		}
//	} catch (ULjException e) {
//		Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
//		throw e;
//	} finally {
//		closeResultSet(rs);
//		closePreparedStatement(ps);
//	}
	
	return ScheduleList;
	}

	//Ryan-20120712 : additional check on inspection start time
	public ArrayList<TimeCheckVO> searchAllowedTime(int serviceMaintProgId)
	throws ULjException {
		ArrayList<TimeCheckVO> timeList = new ArrayList();
		TimeCheckVO vo ;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
	
	bufQuery.append("SELECT distinct  timestart, timeend ");
	bufQuery.append("FROM tblMaintProgServiceTime  ");
	bufQuery.append("WHERE serviceMaintProgId=?  ");
	
	try {
		ps = conn.prepareStatement(bufQuery.toString());
		int ind = 1;
		 
		ps.set(ind++, serviceMaintProgId);
	
		rs = ps.executeQuery(); 
		while (rs.next()) {
			int columnIndex = 1;
			vo = new TimeCheckVO();
			vo.setTimeStart( rs.getDate(columnIndex++));
			vo.setTimeEnd( rs.getDate(columnIndex++));
 
			timeList.add(vo);
		}
	} catch (ULjException e) {
		Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		throw e;
	} finally {
		closeResultSet(rs);
		closePreparedStatement(ps);
	}
	
	return timeList;
	}

	// llm 2/1/13 to add ssh record from handheld for commercial user
	public ArrayList<DropdownItemVO> getCommercialChecListName() throws ULjException {
		ArrayList<DropdownItemVO> statusList = new ArrayList();
		DropdownItemVO Vo;
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		bufQuery.append("SELECT TemplateID,TemplateDescription FROM tblCommercialChecklistName ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());

			rs = ps.executeQuery();
			Vo = new DropdownItemVO();
			Vo.setItemId("0");
			Vo.setItemDescription("Select Checklist");
			statusList.add(Vo);

			while (rs.next()) {
				int columnIndex = 1;
				Vo = new DropdownItemVO();
				Vo.setItemId(rs.getString(columnIndex++));
				Vo.setItemDescription(rs.getString(columnIndex++));
				statusList.add(Vo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return statusList;
	}
	
	// llm 04/01/2013 get templateversion
	public int getTemplateVersionByTemplateID (int templateid) throws ULjException {
		String query = "select max(templateversion) from" +
				" tblchecklist where templateid="+Integer.toString(templateid);
		
		DbUtil dUtil = new DbUtil();
		int cnt=1;
		
		cnt = dUtil.executeSingleIntValueQuery(query);
			
		return cnt;
		
	}
	
	//llm 06/03/3013 for trc cancel task
	public void updateToiletScheduleTime(ScheduleListVO vo, int updateTarget) throws ULjException {
		StringBuffer bufQuery = new StringBuffer();
		PreparedStatement ps = null;
		Connection conn = DbUtil.getConnection();

		bufQuery.append("UPDATE ");
		bufQuery.append("tblToiletServiceServiceHistory a ");
		bufQuery.append("SET ");
		if (updateTarget == 1)
			bufQuery.append("ActualStart =?, ");
		if (updateTarget == 2)
			bufQuery.append("ActualEnd=?, ");
		bufQuery.append("StatusID=?, ");
		bufQuery.append("remarks=?, ");
		bufQuery.append("LastModified=?  ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_STARTTIME)
				ps.set(ind++, vo.getActualStart());
			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_ENDTIME)
				ps.set(ind++, vo.getActualEnd());
			ps.set(ind++, vo.getStatusId());
			ps.set(ind++, vo.getRemarks());
			ps.set(ind++, vo.getLastModifiedDate());
			ps.set(ind++, vo.getChecklistResultId());

			ps.execute();
			conn.commit();

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}

	}
	
	
}
