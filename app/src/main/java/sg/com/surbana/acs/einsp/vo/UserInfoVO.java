package sg.com.surbana.acs.einsp.vo;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Nov 11, 2011		Ryan			Created
 *
 */
public class UserInfoVO {
	private final String TAG = "UserInfoVO.java";
	String userId;
	String userName;
	String userDesignation;
	String userHandphoneNo;
	String userOfficeNo;
	int userRoleId;
	int userInfoId;
	String ContractorId;
	
	int userGroupTypeId;

	//commercial-outlet
	int userGroupId;

	
	public int getUserGroupTypeId() {
		return userGroupTypeId;
	}
	public void setUserGroupTypeId(int userGroupTypeId) {
		this.userGroupTypeId = userGroupTypeId;
	}
	public int getUserGroupId() {
		return userGroupId;
	}
	public void setUserGroupId(int userTypeId) {
		this.userGroupId = userTypeId;
	}
	public int getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(int userRoleId) {
		this.userRoleId = userRoleId;
	}

	public int getUserInfoId() {
		return userInfoId;
	}

	public void setUserInfoId(int userInfoId) {
		this.userInfoId = userInfoId;
	}

	public String getContractorId() {
		return ContractorId;
	}

	public void setContractorId(String contractorId) {
		ContractorId = contractorId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserDesignation() {
		return userDesignation;
	}

	public void setUserDesignation(String userDesignation) {
		this.userDesignation = userDesignation;
	}

	public String getUserHandphoneNo() {
		return userHandphoneNo;
	}

	public void setUserHandphoneNo(String userHandphoneNo) {
		this.userHandphoneNo = userHandphoneNo;
	}

	public String getUserOfficeNo() {
		return userOfficeNo;
	}

	public void setUserOfficeNo(String userOfficeNo) {
		this.userOfficeNo = userOfficeNo;
	}

}
