package sg.com.surbana.acs.einsp.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.util.ColorPickerDialog;
import sg.com.surbana.acs.einsp.util.WrapMotionEvent;
import sg.com.surbana.acs.einsp.view.annotateshapes.BaseShape;
import sg.com.surbana.acs.einsp.view.annotateshapes.ShapeCircle;
import sg.com.surbana.acs.einsp.view.annotateshapes.ShapeLeftDownArrow;
import sg.com.surbana.acs.einsp.view.annotateshapes.ShapeLeftUpArrow;
import sg.com.surbana.acs.einsp.view.annotateshapes.ShapeRectangle;
import sg.com.surbana.acs.einsp.view.annotateshapes.ShapeRightDownArrow;
import sg.com.surbana.acs.einsp.view.annotateshapes.ShapeRightUpArrow;
import sg.com.surbana.acs.einsp.view.CommonImageView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jul 19, 2011		Ryan			Created
 *20 jul 2011		Ryan		    Annotate image integration
 */
public class AnnotateImage extends GraphicsActivity implements ColorPickerDialog.OnColorChangedListener {

	/** Called when the activity is first created. */
	private Bitmap mutatedImage;
	public static Bitmap origImage;
	private MyView drawView;
	private Canvas canv;
	private Paint mPaint;
	private MaskFilter mEmboss;
	private MaskFilter mBlur;
	FrameLayout main;
	private String frPage = "";

	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	float oldXDist = 1f;
	float oldYDist = 1f;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(0xFF0000F0);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(8);
		ImageView im = new ImageView(this);

		mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 }, 0.4f, 6, 3.5f);

		mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);

		// juehua 20120229 for merge chin hua's tablet xml to leeming's code
		if (Build.MODEL.equalsIgnoreCase("GT-P7500")) {
			setContentView(R.layout.tablet_annotate_img);
		}
		
		else {
			setContentView(R.layout.annotate_img);
		}

		Intent myLocalIntent = getIntent();
		Bundle myBundle = myLocalIntent.getExtras();
		if (myBundle != null) {
			try {
				frPage = (String) myBundle.getString("frpage");
				// origImage= FaultNewView.;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		// To control scaling
		int width = origImage.getWidth();
		int height = origImage.getHeight();

		mutatedImage = scaleBitmap(origImage);

		canv = new Canvas(mutatedImage);
		main = (FrameLayout) findViewById(R.id.imageContainer);
		im.setImageBitmap(mutatedImage);
		main.addView(im);
	}

	public class MyView extends View {
		public Bitmap mBitmap;
		private Canvas mCanvas;
		private Path mPath;
		public Bitmap tempBitmap;
		BaseShape bs;
		boolean isFreeDraw;
		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 4;

		public MyView(Context c, BaseShape bs) {
			super(c);
			LayoutParams params = new LayoutParams(mutatedImage.getWidth(), mutatedImage.getHeight());
			this.setLayoutParams(params);

			// To control scaling
			int width = mutatedImage.getWidth();
			int height = mutatedImage.getHeight();
			int newWidth = 250;
			int newHeight = 200;

			float fFactor = (float) height / (float) width;

			if (width <= height) {
				newWidth = 400;
				newHeight = (int) (newWidth * fFactor);
			} else {
				// newWidth = 600;
				// newHeight =450;

				newWidth = 700;
				newHeight = (int) (newWidth * fFactor);
			}

			// calculate the scale - in this case = 0.4f
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;

			// create matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);
			// rotate the Bitmap
			// matrix.postRotate(45);

			// mBitmap = Bitmap.createBitmap(kangoo.getWidth(),
			// kangoo.getHeight(), Bitmap.Config.RGB_565);
			// mBitmap = Bitmap.createBitmap(mutatedImage, 0, 0, width, height,
			// matrix,
			// true);
			// tempBitmap = Bitmap.createBitmap(width, height,
			// mutatedImage.getConfig());
			//
			mBitmap = Bitmap.createBitmap(newWidth, newHeight, mutatedImage.getConfig());

			//
			// Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0,
			// iWidth,
			// iHeight, matrix, true);
			//

			mCanvas = new Canvas(mBitmap);
			// mCanvas.drawBitmap(kangoo, 0,0, null);
			mPath = new Path();

			// tempCanvas= new Canvas(tempBitmap);

			this.bs = bs;
			if (bs != null) {
				isFreeDraw = false;
				mX = width / 2;
				mY = height / 2;
				bs.setPaint(mPaint);
			} else
				isFreeDraw = true;

		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);
		}

		@Override
		protected void onDraw(Canvas canvas) {
			if (isFreeDraw)
				canvas.drawPath(mPath, mPaint);
			else
				bs.drawMe(canvas, mX, mY);
		}

		float startX, startY;

		private void touch_start(float x, float y) {

			if (isFreeDraw) {
				mPath.moveTo(x, y);
				mX = x;
				mY = y;
			} else {
				// uncomment the code below to use touch point to move the
				// object
				mPath.moveTo(bs.x, bs.y);
				mX = bs.x;
				mY = bs.y;
				startX = x;
				startY = y;

				// uncomment the code below to use touch point to move the
				// object
				// mPath.moveTo( x, y);
				// mX = x;
				// mY = y;
			}
		}

		private void touch_move(float x, float y) {
			System.out.println("calling on move " + x + ":" + y);
			float dx = Math.abs(x - mX);
			float dy = Math.abs(y - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
				// uncomment the code below to use touch point to move the
				// object
				// mX = x;
				// mY = y;
				// uncomment the code below to use the existing object point to
				// move the object
				if (isFreeDraw) {
					mX = x;
					mY = y;
				} else {
					mX = (x - startX) + bs.x;
					mY = (y - startY) + bs.y;
				}
				startX = x;
				startY = y;
			}
		}

		public void drawMeOnCanvas(Canvas can) {
			if (isFreeDraw)
				can.drawPath(mPath, mPaint);
			else
				bs.drawMe(can, mX, mY);
		}

		public void rotate() {
			bs.rotate(40);
		}

		private void touch_up() {
			if (isFreeDraw) {
				mPath.lineTo(mX, mY);
				mCanvas.drawPath(mPath, mPaint);
			} else {
				mCanvas.drawPath(bs.getPath(), mPaint);
			}
		}

		private String mode = "drag";

		@Override
		public boolean onTouchEvent(MotionEvent rawEvent) {
			WrapMotionEvent event = WrapMotionEvent.wrap(rawEvent);

			float x = event.getX();
			float y = event.getY();

			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				invalidate();
				mode = "drag";
				break;
			case MotionEvent.ACTION_MOVE:
				if (mode.equals("drag")) {
					touch_move(x, y);
					invalidate();
				} else if (mode.equals("zoom") && !isFreeDraw) {
					float newDist = spacing(event);
					float newXDist = widthSpacing(event);
					float newYDist = heightSpacing(event);
					if (newDist > 10f) {
						float scale = newDist / oldDist;
						// Log.d("", "Trying to do zoomin here newDist=" +
						// scale);
						bs.setRadiusScale(scale);
						bs.setWidthAndHeightScale(newXDist / oldXDist, newYDist / oldYDist);

						invalidate();
						oldDist = newDist;
						oldXDist = newXDist;
						oldYDist = newYDist;

					}
				}
			case MotionEvent.ACTION_UP:
				touch_up();
				invalidate();
				break;
			case MotionEvent.ACTION_POINTER_UP:
				mode = "none";
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				oldDist = spacing(event);
				oldXDist = widthSpacing(event);
				oldYDist = heightSpacing(event);

				if (oldDist > 10f) {
					mode = "zoom";
				}
			}
			return true;
		}
	}

	/** Determine the space between the first two fingers */
	private float spacing(WrapMotionEvent event) {
		// ...
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	/** Determine the space between the first two fingers */
	private float widthSpacing(WrapMotionEvent event) {
		// ...
		float x = event.getX(0) - event.getX(1);
		return x;
	}

	/** Determine the space between the first two fingers */
	private float heightSpacing(WrapMotionEvent event) {
		// ...
		float y = event.getY(0) - event.getY(1);
		return y;
	}

	public void doSaveImage(View v) {
		try {

			drawAllToCanvas(canv);
			
			if (frPage.equals("common")) {
//				CommonImageView.imgViewTemp.setImageBitmap(mutatedImage);
				
				Intent i = new Intent();
				setResult(Activity.RESULT_OK, i);
				CommonImageView.capturedImage2 =  scaleBitmap2(mutatedImage);
				// i.putExtra("data", scaleBitmap2(mutatedImage));
				finish();
			}
			
			else if (frPage.equals("Monthly Meter")) {
				ReplaceMonthlyMeterImage.imgViewTemp.setImageBitmap(mutatedImage);
			} 
		
			Intent i = new Intent();
			setResult(Activity.RESULT_OK, i);

			finish();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void doUndo(View v) {
		if (main.getChildCount() > 1) {
			main.removeViewAt(main.getChildCount() - 1);
		}

	}

	public void doCancel(View v) {
		Intent i = new Intent();

 

		setResult(Activity.RESULT_CANCELED, i);
		
		finish();
	}

	public void doChangeColor(View v) {
		new ColorPickerDialog(this, this, mPaint.getColor()).show();
	}

	public void doErase(View v) {
		mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
	}

	private Bitmap scaleBitmap(Bitmap bitmap) {
		int iWidth = bitmap.getWidth();
		int iHeight = bitmap.getHeight();
		int newWidth = 0, newHeight = 0;
		float fFactor = (float) iHeight / (float) iWidth;

		if (iWidth <= iHeight) {
			newWidth = 400;
			newHeight = (int) (newWidth * fFactor);// 370;
		} else {
			newWidth = 700;
			newHeight = (int) (newWidth * fFactor);// 370;
		}
		float fScaleWidth = ((float) newWidth) / iWidth;
		float fScaleHeight = ((float) newHeight) / iHeight;

		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth, iHeight, matrix, true);

		FrameLayout main = (FrameLayout) findViewById(R.id.imageContainer);
		main.getLayoutParams().height = resizedBitmappreview.getHeight();
		main.getLayoutParams().width = resizedBitmappreview.getWidth();

		return resizedBitmappreview;
	}

	private Bitmap scaleBitmap2(Bitmap bitmap) {
		int iWidth = bitmap.getWidth();
		int iHeight = bitmap.getHeight();
		int newWidth = 0, newHeight = 0;
		float fFactor = (float) iHeight / (float) iWidth;

		if (iWidth <= iHeight) {
			newWidth = 300;
			newHeight = (int) (newWidth * fFactor);
		} else {
			// newWidth = 600;
			// newHeight =450;

			newWidth = 400;
			newHeight = (int) (newWidth * fFactor);
		}
		float fScaleWidth = ((float) newWidth) / iWidth;
		float fScaleHeight = ((float) newHeight) / iHeight;

		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth, iHeight, matrix, true);

		return resizedBitmappreview;
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {

			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void colorChanged(int color) {
		mPaint.setColor(color);

	}

	public void doAddArrowRU(View v) {
		drawView = new MyView(this, new ShapeRightUpArrow());
		main.addView(drawView);
	}

	public void doAddArrowRD(View v) {
		drawView = new MyView(this, new ShapeRightDownArrow());
		main.addView(drawView);
	}

	public void doAddArrowLU(View v) {
		drawView = new MyView(this, new ShapeLeftUpArrow());
		main.addView(drawView);
	}

	public void doAddArrowLD(View v) {
		drawView = new MyView(this, new ShapeLeftDownArrow());
		main.addView(drawView);
	}

	public void doAddCircle(View v) {
		drawView = new MyView(this, new ShapeCircle());
		main.addView(drawView);
	}

	public void doAddRect(View v) {
		drawView = new MyView(this, new ShapeRectangle());
		main.addView(drawView);
	}

	public void doAddFreeDraw(View v) {
		drawView = new MyView(this, null);
		main.addView(drawView);
	}

	private void drawAllToCanvas(Canvas canvas) {
		try {
			MyView currView;
			System.out.println("Number of shape is " + main.getChildCount());
			if (main.getChildCount() > 1) {
				for (int i = 1; i < main.getChildCount(); i++) {
					System.out.println("Adding shape");
					currView = (MyView) main.getChildAt(i);
					currView.drawMeOnCanvas(canvas);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void generateFiile(View v) {
		try {
			String path = Environment.getExternalStorageDirectory().toString();
			OutputStream fOut = null;
			File file = new File(path, "/testing2.jpg");
			fOut = new FileOutputStream(file);
			MyView currView;
			System.out.println("Number of shape is " + main.getChildCount());
			if (main.getChildCount() > 1) {
				for (int i = 1; i < main.getChildCount(); i++) {
					System.out.println("Adding shape");
					currView = (MyView) main.getChildAt(i);
					currView.drawMeOnCanvas(canv);
				}
			}
			mutatedImage.compress(Bitmap.CompressFormat.JPEG, 85, fOut);

			fOut.flush();
			fOut.close();

			MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	 
}