package sg.com.surbana.acs.einsp.vo;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 9, 2011		Ryan			Created
 *
 */
public class FaultListVO extends FaultVO {
	private final String TAG = "FaultListVO.java";

	private String userName;
	private String locationCode;

	private String personInCharge;

	private String faultNatureCode;
	private String tenantName;

	private Integer dropDownItemId;

	private String dropDownItemDesc;
	private String dropDownItemShortDesc;
	
	private int contrStaffUserinfoId;//llm 02/10/2013 eng fault

	public int getContrStaffUserinfoId() {
		return contrStaffUserinfoId;
	}

	public void setContrStaffUserinfoId(int contrStaffUserinfoId) {
		this.contrStaffUserinfoId = contrStaffUserinfoId;
	}

	public String getDropDownItemDesc() {
		return dropDownItemDesc;
	}

	public void setDropDownItemDesc(String dropDownItemDesc) {
		this.dropDownItemDesc = dropDownItemDesc;
	}

	public String getDropDownItemShortDesc() {
		return dropDownItemShortDesc;
	}

	public void setDropDownItemShortDesc(String dropDownItemShortDesc) {
		this.dropDownItemShortDesc = dropDownItemShortDesc;
	}

	public Integer getDropDownItemId() {
		return dropDownItemId;
	}

	public void setDropDownItemId(Integer dropDownItemId) {
		this.dropDownItemId = dropDownItemId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPersonInCharge() {
		return personInCharge;
	}

	public void setPersonInCharge(String personInCharge) {
		this.personInCharge = personInCharge;
	}

	public String getFaultNatureCode() {
		return faultNatureCode;
	}

	public void setFaultNatureCode(String faultNatureCode) {
		this.faultNatureCode = faultNatureCode;
	}

}
