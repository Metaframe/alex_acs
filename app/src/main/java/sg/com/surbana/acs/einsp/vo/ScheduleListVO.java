package sg.com.surbana.acs.einsp.vo;

import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 13, 2011		Ryan			Created
 *
 */
public class ScheduleListVO extends ScheduleVO {
	private final String TAG = "ScheduleListVO.java";
	String location;
	String checklistName;
	String tenantName;
	Integer dropdowngroupid;
	Integer noticeid;
	Integer terminalid;
	Integer noticeStatus;
	Integer groupId;
	Integer noticeinspid;
	Timestamp noticedate;
	Integer noticeNo;
	Integer tenantId;
	String userid;
	String locationname;
	Timestamp followupdate;
	Timestamp reminderdate;
	Integer tenantNo;
	String frequency;
	String refId;
	Integer monthlymetersyncstatus;//llm 03/05/2013 new table for monthly meter delay sync
	String equipmentDesc;
	String equipmentCode;
	Integer equipmentID;
	Timestamp ackdate;
	Integer scanLocationStart;
	Integer scanLocationEnd;
	String completionLocationCode;
	Integer defaultequipmentid;
	
	
	
	public Integer getDefaultequipmentid() {
		return defaultequipmentid;
	}

	public void setDefaultequipmentid(Integer defaultequipmentid) {
		this.defaultequipmentid = defaultequipmentid;
	}

	public String getCompletionLocationCode() {
		return completionLocationCode;
	}

	public void setCompletionLocationCode(String completionLocationCode) {
		this.completionLocationCode = completionLocationCode;
	}

	
	
	public Integer getScanLocationStart() {
		return scanLocationStart;
	}

	public void setScanLocationStart(Integer scanLocationStart) {
		this.scanLocationStart = scanLocationStart;
	}

	public Integer getScanLocationEnd() {
		return scanLocationEnd;
	}

	public void setScanLocationEnd(Integer scanLocationEnd) {
		this.scanLocationEnd = scanLocationEnd;
	}

	public Timestamp getAckdate() {
		return ackdate;
	}

	public void setAckdate(Timestamp ackdate) {
		this.ackdate = ackdate;
	}

	public Integer getEquipmentID() {
		return equipmentID;
	}

	public void setEquipmentID(Integer equipmentID) {
		this.equipmentID = equipmentID;
	}

	public String getEquipmentDesc() {
		return equipmentDesc;
	}

	public void setEquipmentDesc(String equipmentDesc) {
		this.equipmentDesc = equipmentDesc;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipemntCode) {
		this.equipmentCode = equipemntCode;
	}

	public Integer getMonthlymetersyncstatus() {
		return monthlymetersyncstatus;
	}

	public void setMonthlymetersyncstatus(Integer monthlymetersyncstatus) {
		this.monthlymetersyncstatus = monthlymetersyncstatus;
	}

	public Integer getDropdowngroupid() {
		return dropdowngroupid;
	}

	public void setDropdowngroupid(Integer dropdowngroupid) {
		this.dropdowngroupid = dropdowngroupid;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getChecklistName() {
		return checklistName;
	}

	public void setChecklistName(String checklistName) {
		this.checklistName = checklistName;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getNoticeStatus() {
		return noticeStatus;
	}

	public void setNoticeStatus(Integer noticeStatus) {
		this.noticeStatus = noticeStatus;
	}

	public Integer getNoticeid() {
		return noticeid;
	}

	public void setNoticeid(Integer noticeid) {
		this.noticeid = noticeid;
	}

	public Integer getTerminalid() {
		return terminalid;
	}

	public void setTerminalid(Integer terminalid) {
		this.terminalid = terminalid;
	}

	public Integer getNoticeinspid() {
		return noticeinspid;
	}

	public void setNoticeinspid(Integer noticeinspid) {
		this.noticeinspid = noticeinspid;
	}

	public Timestamp getNoticedate() {
		return noticedate;
	}

	public void setNoticedate(Timestamp noticedate) {
		this.noticedate = noticedate;
	}

	public Integer getNoticeNo() {
		return noticeNo;
	}

	public void setNoticeNo(Integer noticeNo) {
		this.noticeNo = noticeNo;
	}

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getLocationname() {
		return locationname;
	}

	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}

	public Timestamp getFollowupdate() {
		return followupdate;
	}

	public void setFollowupdate(Timestamp followupdate) {
		this.followupdate = followupdate;
	}

	public Timestamp getReminderdate() { 
		return reminderdate;
	}

	public void setReminderdate(Timestamp reminderdate) {
		this.reminderdate = reminderdate;
	}
	public Integer getTenantNo() {
		return tenantNo;
	}

	public void setTenantNo(Integer tenantNo) {
		this.tenantNo = tenantNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

}
 