/*
 * Copyright (C) 2011 Daniel Berndt - Codeus Ltd  -  DateSlider
 * 
 * Class for setting up the dialog and initialsing the underlying
 * ScrollLayouts
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;
import java.util.Calendar;

import sg.com.surbana.acs.einsp.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zebra.android.devdemo.connectivity.SignatureArea;

public class SignatureDialog extends Dialog {

	// private static String TAG = "DATESLIDER";
	private ArrayList<Integer> wheelsId = new ArrayList();

	private OnSignatureSetListener onSignatureSetListener;
	private Calendar mTime;
	private TextView mTitleText;
	private LinearLayout mLayout;
	private View vw;
	private SignatureArea signature;

	public SignatureDialog(Context context, OnSignatureSetListener l) {
		super(context);
		this.onSignatureSetListener = l;
	}

	public SignatureDialog(Context context, OnSignatureSetListener l, View v) {
		super(context);
		this.onSignatureSetListener = l;
		vw = v;
	}

	/**
	 * Set up the dialog with all the views and their listeners
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		if (savedInstanceState != null) {
			long time = savedInstanceState.getLong("time", mTime.getTimeInMillis());
			mTime.setTimeInMillis(time);
		}

		this.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		
		double screenInches =0;
		Display disp = this.getWindow().getWindowManager().getDefaultDisplay(); 
		int width = disp.getWidth();
		int height = disp.getHeight();
		DisplayMetrics dm = new DisplayMetrics();
		this.getWindow().getWindowManager().getDefaultDisplay().getMetrics(dm);
	    double x = Math.pow(dm.widthPixels/dm.xdpi,2);
	    double y = Math.pow(dm.heightPixels/dm.ydpi,2);
	    screenInches = Math.sqrt(x+y);
	    if (screenInches<6) 
	    	setContentView(R.layout.dialog_signature_note3);
	    else
	    	setContentView(R.layout.dialog_signature);
	    
		this.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialogtitle);

		mTitleText = (TextView) this.findViewById(R.id.dateSliderTitleText);
		mLayout = (LinearLayout) findViewById(R.id.dateSliderMainLayout);
		signature = (SignatureArea) findViewById(R.id.sigArea);
		Button okButton = (Button) findViewById(R.id.dateSliderOkButton);
		okButton.setOnClickListener(okButtonClickListener);
		mTitleText.setText("Signature Area");
		Button cancelButton = (Button) findViewById(R.id.dateSliderCancelButton);
		cancelButton.setOnClickListener(cancelButtonClickListener);

	}

	private android.view.View.OnClickListener okButtonClickListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			// return value to correct view
			// set title value
			if (onSignatureSetListener != null)
				onSignatureSetListener.onSet(SignatureDialog.this, signature.getBitmap(), vw);
			dismiss();
		}
	};

	private android.view.View.OnClickListener cancelButtonClickListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			dismiss();
		}
	};

	@Override
	public Bundle onSaveInstanceState() {
		Bundle savedInstanceState = super.onSaveInstanceState();
		if (savedInstanceState == null)
			savedInstanceState = new Bundle();
		return savedInstanceState;
	}

	/**
	 * Defines the interface which defines the methods of the OnDateSetListener
	 */
	public interface OnSignatureSetListener {
		/**
		 * this method is called when a date was selected by the user
		 * 
		 * @param view
		 *            the caller of the method
		 * 
		 */
		public void onSet(SignatureDialog view, Bitmap signature, View v);
	}

}
