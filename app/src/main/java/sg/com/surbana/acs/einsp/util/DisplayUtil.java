package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;
import java.util.HashMap;

import sg.com.surbana.acs.einsp.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.Toast;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *May 25, 2011		Ryan			Created
 *
 */
public class DisplayUtil {
	private final String TAG = "DisplayUtil.java";

	public void displayException(Context context, String msg, Exception e) {
		Toast t = Toast.makeText(context, "Please contact administrator \n" + msg + "\n" + e.getMessage(), Toast.LENGTH_LONG);
		t.setGravity(Gravity.CENTER, 0, 0);
		t.show();
	}

	public void displayMessage(Context context, String msg) {
		Toast t = Toast.makeText(context, msg, Toast.LENGTH_LONG);
		t.setGravity(Gravity.CENTER, 0, 0);
		t.show();
	}

	public SpecialAdapter showNoRecordFound(Context con) {
		String[] noRecordFrom = new String[] { "desc" };
		int[] noRecordTo = new int[] { R.id.item1 };
		ArrayList<HashMap<String, Object>> fillMaps = new ArrayList<HashMap<String, Object>>();

		HashMap tempMap = new HashMap<String, Object>();
		tempMap.put("desc", AppsConstant.TEXT_NO_RECORDS_FOUNT);
		fillMaps.add(tempMap);
		SpecialAdapter adapter = new SpecialAdapter(con, fillMaps, R.layout.common_no_record_found, noRecordFrom, noRecordTo);
		adapter.notifyDataSetChanged();
		return adapter;
	}

	public void displayAlertDialog(Context con, String title, String message) {
		new AlertDialog.Builder(con).setTitle(title).setMessage(message).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		}).show();
	}
	/*
	 * public ArrayList<DropdownItemVO> getDropdownItemsForOutlet(){
	 * ArrayList<DropdownItemVO> items=null; CommonDAO comDAO = new CommonDAO();
	 * try { items =
	 * comDAO.getDropDownItemForField(AppsConstant.DROPDOWN_OUTLET_TABLE_NAME,
	 * AppsConstant.DROPDOWN_OUTLET_FIELD_KEY,
	 * AppsConstant.DROPDOWN_OUTLET_FIELD_DESCRIPTION); } catch (ULjException e)
	 * { // TODO Auto-generated catch block Log.e(TAG,
	 * "Error in getDropdownItemsForOutlet", e); } return items; }
	 * 
	 * public ArrayList<DropdownItemVO> getDropdownItemsForCaseDescription(){
	 * ArrayList<DropdownItemVO> items=null; CommonDAO comDAO = new CommonDAO();
	 * try { items =
	 * comDAO.getDropDownItemForField(AppsConstant.DROPDOWN_CASE_DESC_TABLE_NAME
	 * , AppsConstant.DROPDOWN_CASE_DESC_FIELD_KEY,
	 * AppsConstant.DROPDOWN_CASE_DESC_FIELD_DESCRIPTION); } catch (ULjException
	 * e) { // TODO Auto-generated catch block Log.e(TAG,
	 * "Error in getDropdownItemsForCaseDescription", e); } return items; }
	 * 
	 * public ArrayList<DropdownItemVO> getDropdownItemsForCaseClassification(){
	 * ArrayList<DropdownItemVO> items=null; CommonDAO comDAO = new CommonDAO();
	 * try { items = comDAO.getDropDownItemForField(AppsConstant.
	 * DROPDOWN_CASE_CLASSIFICTION_TABLE_NAME,
	 * AppsConstant.DROPDOWN_CASE_CLASSIFICTION_FIELD_KEY,
	 * AppsConstant.DROPDOWN_CASE_CLASSIFICTION_FIELD_DESCRIPTION); } catch
	 * (ULjException e) { // TODO Auto-generated catch block Log.e(TAG,
	 * "Error in getDropdownItemsForCaseClassification", e); } return items; }
	 * 
	 * public ArrayList<DropdownItemVO> getDropdownItemsForCaseCategory(){
	 * ArrayList<DropdownItemVO> items=null; CommonDAO comDAO = new CommonDAO();
	 * try { items =
	 * comDAO.getDropDownItemForField(AppsConstant.DROPDOWN_CASE_CATEGORY_TABLE_NAME
	 * , AppsConstant.DROPDOWN_CASE_CATEGORY_FIELD_KEY,
	 * AppsConstant.DROPDOWN_CASE_CATEGORY_FIELD_DESCRIPTION); } catch
	 * (ULjException e) { // TODO Auto-generated catch block Log.e(TAG,
	 * "Error in getDropdownItemsForCaseCategory", e); } return items; } public
	 * ArrayList<DropdownItemVO> getDropdownItemsForCasePriority(){
	 * ArrayList<DropdownItemVO> items=null; CommonDAO comDAO = new CommonDAO();
	 * try { items =
	 * comDAO.getDropDownItemForField(AppsConstant.DROPDOWN_CASE_PRIORITY_TABLE_NAME
	 * , AppsConstant.DROPDOWN_CASE_PRIORITY_FIELD_KEY,
	 * AppsConstant.DROPDOWN_CASE_PRIORITY_FIELD_DESCRIPTION); } catch
	 * (ULjException e) { // TODO Auto-generated catch block Log.e(TAG,
	 * "Error in getDropdownItemsForCasePriority", e); } return items; } public
	 * ArrayList<DropdownItemVO> getDropdownItemsForCaseStatus(){
	 * ArrayList<DropdownItemVO> items=null; CommonDAO comDAO = new CommonDAO();
	 * try { items =
	 * comDAO.getDropDownItemForField(AppsConstant.DROPDOWN_CASE_STATUS_TABLE_NAME
	 * , AppsConstant.DROPDOWN_CASE_STATUS_FIELD_KEY,
	 * AppsConstant.DROPDOWN_CASE_STATUS_FIELD_DESCRIPTION); } catch
	 * (ULjException e) { // TODO Auto-generated catch block Log.e(TAG,
	 * "Error in getDropdownItemsForCaseStatus", e); } return items; }
	 */
}
