package sg.com.surbana.acs.einsp.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ALRED on 3/27/2017.
 */

@DatabaseTable(tableName = "Image2VO")
public class Image2VO implements Parcelable{
    @DatabaseField
    @SerializedName("terminalid")
    @Expose
    private String terminalid;
    @DatabaseField
    @SerializedName("monthlymeterno")
    @Expose
    private String monthlymeterno;
    @DatabaseField
    @SerializedName("readingno")
    @Expose
    private String readingno;
    @DatabaseField
    @SerializedName("ImageName")
    @Expose
    private String imageName;
    @DatabaseField
    @SerializedName("LastModifiedDate")
    @Expose
    private String lastModifiedDate;
    @DatabaseField
    @SerializedName("LastUpdatedBy")
    @Expose
    private String lastUpdatedBy;
    @DatabaseField
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @DatabaseField
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @DatabaseField
    @SerializedName("ImageCaption")
    @Expose
    private String imageCaption;
    @DatabaseField
    @SerializedName("ImageData")
    @Expose
    private String imageData;

    public String getTerminalid() {
        return terminalid;
    }

    public void setTerminalid(String terminalid) {
        this.terminalid = terminalid;
    }

    public String getMonthlymeterno() {
        return monthlymeterno;
    }

    public void setMonthlymeterno(String monthlymeterno) {
        this.monthlymeterno = monthlymeterno;
    }

    public String getReadingno() {
        return readingno;
    }

    public void setReadingno(String readingno) {
        this.readingno = readingno;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImageCaption() {
        return imageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.imageCaption = imageCaption;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public static final Parcelable.Creator<Image2VO> CREATOR  = new Creator<Image2VO>() {
        @Override
        public Image2VO createFromParcel(Parcel source) {
            Image2VO image2VO = new Image2VO();
            image2VO.terminalid = source.readString();
            image2VO.monthlymeterno = source.readString();
            image2VO.readingno = source.readString();
            image2VO.imageName = source.readString();
            image2VO.lastModifiedDate = source.readString();
            image2VO.lastUpdatedBy = source.readString();
            image2VO.latitude = source.readString();
            image2VO.longitude = source.readString();
            image2VO.imageCaption = source.readString();
            image2VO.imageData = source.readString();
            return image2VO;
        }

        @Override
        public Image2VO[] newArray(int size) {
            return new Image2VO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(terminalid);
        parcel.writeString(monthlymeterno);
        parcel.writeString(readingno);
        parcel.writeString(imageName);
        parcel.writeString(lastModifiedDate);
        parcel.writeString(lastUpdatedBy);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(imageCaption);
        parcel.writeString(imageData);
    }
}
