package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import sg.com.surbana.acs.einsp.R;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

public class SpecialAdapterForSchedule extends SimpleAdapter {
	private final String TAG = "SpecialAdapter.class";
	private Context con;
	private ImageView statusImage;
	private int[] colors = new int[] { R.drawable.list_item_selector_white, R.drawable.list_item_selector_purple };
	private int[] statusImages;
	private SparseBooleanArray checkedBoxes;
	private boolean isDefaultCheck;
	private boolean isContractor;
	public SpecialAdapterForSchedule(Context context, ArrayList<HashMap<String, Object>> list, int resource, String[] from, int[] to) {
		super(context, list, resource, from, to);
		con = context;
		statusImages = new CommonFunction().getScheduleStatusImageArray();
	}

//	public SpecialAdapterForSchedule(Context context, ArrayList<HashMap<String, Object>> list, int resource, String[] from, int[] to, boolean isContractor,
//			boolean isDefaultCheck) {
//		super(context, list, resource, from, to);
//		con = context;
//		checkedBoxes = new SparseBooleanArray();
//		this.isContractor = isContractor;
//		this.isDefaultCheck = isDefaultCheck;
//	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		int statusId = 0;
		Date revisedDate = null;
		Date actualStart = null;
		Date actualEnd = null;
		//llm 03/05/2013 new table for monthly meter delay sync
		int monthlymetersyncstatus=0;
		String frbutton ="";
		
		HashMap map = (HashMap) getItem(position);
		if (map.get(AppsConstant.HASHMAP_KEY_STATUS_ID) != null) {
			statusId = (Integer) map.get(AppsConstant.HASHMAP_KEY_STATUS_ID);
			revisedDate = (Date) map.get(AppsConstant.HASHMAP_KEY_REVISED_DATE);// use
																				// date
																				// as
																				// it
																				// is
																				// the
																				// same
																				// value
			actualStart = (Date) map.get(AppsConstant.HASHMAP_KEY_ACTUAL_START);
			actualEnd = (Date) map.get(AppsConstant.HASHMAP_KEY_ACTUAL_END);
		
			try {
				frbutton=(String) map.get("frButton");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		statusImage = (ImageView) view.findViewById(R.id.lstStatusCode);
		if (statusImage != null) {


			int statusIndex = processStatusImageCode(statusId, revisedDate, actualStart, actualEnd);
			statusImage.setBackgroundResource(statusImages[statusIndex]);
		}

		int colorPos = position % colors.length;
		view.setBackgroundResource(colors[colorPos]);
		return view;
	}

	private int processStatusImageCode(int statusId, Date revisedDate, Date actualStart, Date actualEnd) {
		int statusIndex = 4;
		Date currentDate = new Date();
		DateUtil dUtil = new DateUtil();

		// 3 Cancelled
		// 1 Completed
		// 4 In Progress
		// 2 To Be Done

		if (statusId == 3)
			statusIndex = 3;
		else if ((actualStart != null) && (actualEnd != null)) {
			statusIndex = 0;
		} else if (actualStart != null && actualEnd == null) {
			statusIndex = 1;
		} else if ((currentDate.after(revisedDate) || (currentDate.getDate() == revisedDate.getDate() && currentDate.getMonth() == revisedDate.getMonth() && currentDate
				.getYear() == revisedDate.getYear())) && (actualStart == null || actualStart.equals(""))) {
			statusIndex = 2;
		}


		return statusIndex;

	}
}
