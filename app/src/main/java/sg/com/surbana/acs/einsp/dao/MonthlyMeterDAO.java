package sg.com.surbana.acs.einsp.dao;

import java.util.ArrayList;
import java.util.Vector;

import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterImagesVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterSignOffVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import sg.com.surbana.acs.einsp.vo.ScheduleVO;

import android.util.Log;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;


/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Mar 14, 2012		Ryan			Created
 *
 */
public class MonthlyMeterDAO  extends BaseDAO {
	private final String TAG = "MonthlyMeterDAO.java";
	
	public ArrayList<DropdownItemVO> getMaintOfficer(Integer terminalid) throws ULjException {
		ArrayList<DropdownItemVO> List = new ArrayList();
		DropdownItemVO vo;
		Connection conn = DbUtil.getConnection();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		bufQuery.append("SELECT userid,username FROM tblMaintenanceOfficer  WHERE terminalid=? order by username  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int index = 1;
			ps.set(index++, terminalid);
			
			rs = ps.executeQuery();
			vo = new DropdownItemVO();
			vo.setItemId("0");
			vo.setItemDescription("Select Maintenance Officer");
			List.add(vo);

			while (rs.next()) {
				int columnIndex = 1;
				vo = new DropdownItemVO();
				vo.setItemId(rs.getString(columnIndex++));
				vo.setItemDescription(rs.getString(columnIndex++));
				List.add(vo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return List;
	}
	
	
	public ArrayList<MonthlyMeterVO> searchMetersbyLocationId(int locationid) throws ULjException {
		Log.d("MonthlyMeterDAO.java", "Searching monthly meters --" + locationid);

		ArrayList<MonthlyMeterVO> meterList = new ArrayList();
		
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("SELECT MeterNo,SerialNo,MeterType,InstallationDate,RemovalDate, ");
		bufQuery.append("IntialReading,LastReading,MultiplyFactor,UnitID,MeterCategory,MeterClass, ");
		bufQuery.append("MeterStatus,PUCNo,sublocation,locationid");
		bufQuery.append(" FROM tblMstUtilityMeterList" );
		bufQuery.append(" WHERE locationid = ? order by serialno");
		
//		bufQuery.append("SELECT mst.MeterNo,mst.SerialNo,mst.MeterType,InstallationDate,RemovalDate, ");
//		bufQuery.append("IntialReading,LastReading,r.MultiplyFactor,UnitID,MeterCategory,MeterClass ");
//		bufQuery.append("MeterStatus,PUCNo,r.locationid ");
//		bufQuery.append("FROM     tblMstUtilityMeterList mst left OUTER JOIN ");
//		bufQuery.append("tblUtilityMeterReadingHistory r ON mst.MeterNo = r.MeterNo ");
//		bufQuery.append("where ReadingNo in    ");        
//		bufQuery.append("( ");
//		bufQuery.append(" select MAX(readingno) as readingno from tblUtilityMeterReadingHistory ");
//		bufQuery.append(" group by meterno "); //--tenantid, tenantno
//		bufQuery.append("  )");
//		// and RemovalDate is null

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, locationid);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				MonthlyMeterVO mVO = new MonthlyMeterVO();
				mVO.setMeterNo(rs.getInt(columnIndex++));
				mVO.setSerialNo(rs.getString(columnIndex++));
				mVO.setMeterType(rs.getString(columnIndex++));
//				mVO.setInstallationDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				mVO.setRemovalDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				
				mVO.setInitialReading(rs.getDouble(columnIndex++));
				mVO.setLastReading(rs.getDouble(columnIndex++));
				mVO.setMultiplyFactor(rs.getFloat(columnIndex++));
				
				mVO.setUnitID(rs.getString(columnIndex++));
				mVO.setMeterCategory(rs.getString(columnIndex++));
				mVO.setMeterClass(rs.getString(columnIndex++));
				mVO.setMeterStatus(rs.getString(columnIndex++));
				
				mVO.setPUCNo(rs.getString(columnIndex++));
				mVO.setSubLocation(rs.getString(columnIndex++));
				mVO.setLocationID(rs.getInt(columnIndex++));
				meterList.add(mVO);
			}
		} catch (ULjException e) {
			Log.e("MonthlyMeterDAO.java", "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return meterList;
	}
	
	public MonthlyMeterVO searchMetersReadingHistory(int meterno, int checklistid) throws ULjException {
		Log.d("MonthlyMeterDAO.java", "Searching monthly meters --" + meterno);

		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		MonthlyMeterVO mVO = new MonthlyMeterVO();
		
		bufQuery.append("SELECT r.monthlymeterno, r.readingno, mst.MeterNo,mst.SerialNo,mst.MeterType,InstallationDate,RemovalDate, ");
		bufQuery.append("IntialReading,LastReading,mst.MultiplyFactor,UnitID,MeterCategory,MeterClass, ");
		bufQuery.append("MeterStatus,PUCNo,r.locationid ,r.ChecklistResultID,r.PrevConsumption, " +
				"r.PrevReading, r.CurrReading, r.Consumption ,r.remarks, mst.sublocation, r.meterround," +
				"r.unitno, r.customername, r.tenantname, r.tenantemail, r.signfilename, r.signimage ");
		bufQuery.append("FROM tblMstUtilityMeterList mst left OUTER JOIN ");
		bufQuery.append("tblUtilityMeterReadingHistory r ON mst.MeterNo = r.MeterNo  ");
//		bufQuery.append("where ReadingNo in       ");
//		bufQuery.append("( ");
//		bufQuery.append(" select MAX(readingno) as readingno from tblUtilityMeterReadingHistory ") ;
//		bufQuery.append(" group by meterno ) ");
		bufQuery.append("  where  r.meterno= ? and r.ChecklistResultID=?");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, meterno);
			ps.set(ind++, checklistid);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				mVO.setMonthlyMeterNo(rs.getInt(columnIndex++));
				mVO.setReadingNo(rs.getInt(columnIndex++));
				mVO.setMeterNo(rs.getInt(columnIndex++));
				mVO.setSerialNo(rs.getString(columnIndex++));
				mVO.setMeterType(rs.getString(columnIndex++));
//				mVO.setInstallationDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				mVO.setRemovalDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				
				mVO.setInitialReading(rs.getDouble(columnIndex++));
				mVO.setLastReading(rs.getDouble(columnIndex++));
				mVO.setMultiplyFactor(rs.getFloat(columnIndex++));
				
				mVO.setUnitID(rs.getString(columnIndex++));
				mVO.setMeterCategory(rs.getString(columnIndex++));
				mVO.setMeterClass(rs.getString(columnIndex++));
				mVO.setMeterStatus(rs.getString(columnIndex++));
				
				mVO.setPUCNo(rs.getString(columnIndex++));
				mVO.setLocationID(rs.getInt(columnIndex++));
				mVO.setChecklistNo(rs.getInt(columnIndex++));
				
				mVO.setPrevConsumption(rs.getFloat(columnIndex++));
				mVO.setPrevReading(rs.getString(columnIndex++));
				mVO.setCurrReading(rs.getString(columnIndex++));
				mVO.setConsumption(rs.getFloat(columnIndex++));
				
				mVO.setRemarks(rs.getString(columnIndex++));
				mVO.setSubLocation(rs.getString(columnIndex++));
				mVO.setMeterRound(rs.getBoolean(columnIndex++));
				
				mVO.setUnitNo(rs.getString(columnIndex++));
				mVO.setCustomerName(rs.getString(columnIndex++));
				mVO.setTenantName(rs.getString(columnIndex++));
				mVO.setTenantEmail(rs.getString(columnIndex++));
				mVO.setAckBySigFilename(rs.getString(columnIndex++));
				mVO.setaSignature(rs.getBytes(columnIndex++));
			
			}
		} catch (ULjException e) {
			Log.e("MonthlyMeterDAO.java", "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return mVO;
	}
	
	public MonthlyMeterVO searchPrevMetersReadingHistory(int meterno) throws ULjException {
		Log.d("MonthlyMeterDAO.java", "Searching monthly meters --" + meterno);

		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		MonthlyMeterVO mVO = new MonthlyMeterVO();
		
		bufQuery.append("SELECT top 1 r.monthlymeterno, r.readingno, mst.MeterNo,mst.SerialNo,mst.MeterType,InstallationDate,RemovalDate, ");
		bufQuery.append("IntialReading,LastReading,mst.MultiplyFactor,UnitID,MeterCategory,MeterClass, ");
		bufQuery.append("MeterStatus,PUCNo,r.locationid ,r.ChecklistResultID,r.PrevConsumption, " +
				"r.PrevReading, r.CurrReading, r.Consumption ,r.remarks, mst.sublocation, r.meterround," +
				"r.unitno, r.customername, r.tenantname, r.tenantemail, r.signfilename, r.signimage ");
		bufQuery.append("FROM tblMstUtilityMeterList mst left OUTER JOIN ");
		bufQuery.append("tblUtilityMeterReadingHistory r ON mst.MeterNo = r.MeterNo  ");
		bufQuery.append("  where  r.meterno= ? and r.unitno is not null and length(UnitNo)>0  order by lastupdatedDate desc ");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, meterno);
		
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				mVO.setMonthlyMeterNo(rs.getInt(columnIndex++));
				mVO.setReadingNo(rs.getInt(columnIndex++));
				mVO.setMeterNo(rs.getInt(columnIndex++));
				mVO.setSerialNo(rs.getString(columnIndex++));
				mVO.setMeterType(rs.getString(columnIndex++));
//				mVO.setInstallationDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				mVO.setRemovalDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				
				mVO.setInitialReading(rs.getDouble(columnIndex++));
				mVO.setLastReading(rs.getDouble(columnIndex++));
				mVO.setMultiplyFactor(rs.getFloat(columnIndex++));
				
				mVO.setUnitID(rs.getString(columnIndex++));
				mVO.setMeterCategory(rs.getString(columnIndex++));
				mVO.setMeterClass(rs.getString(columnIndex++));
				mVO.setMeterStatus(rs.getString(columnIndex++));
				
				mVO.setPUCNo(rs.getString(columnIndex++));
				mVO.setLocationID(rs.getInt(columnIndex++));
				mVO.setChecklistNo(rs.getInt(columnIndex++));
				
				mVO.setPrevConsumption(rs.getFloat(columnIndex++));
				mVO.setPrevReading(rs.getString(columnIndex++));
				mVO.setCurrReading(rs.getString(columnIndex++));
				mVO.setConsumption(rs.getFloat(columnIndex++));
				
				mVO.setRemarks(rs.getString(columnIndex++));
				mVO.setSubLocation(rs.getString(columnIndex++));
				mVO.setMeterRound(rs.getBoolean(columnIndex++));
				
				mVO.setUnitNo(rs.getString(columnIndex++));
				mVO.setCustomerName(rs.getString(columnIndex++));
				mVO.setTenantName(rs.getString(columnIndex++));
				mVO.setTenantEmail(rs.getString(columnIndex++));
				mVO.setAckBySigFilename(rs.getString(columnIndex++));
				mVO.setaSignature(rs.getBytes(columnIndex++));
			
			}
		} catch (ULjException e) {
			Log.e("MonthlyMeterDAO.java", "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return mVO;
	}
	
	public ScheduleListVO searchMonthlyMeterScheduleByChecklistResultId(int checklistResultId) throws ULjException {
		ScheduleListVO schduleListVo = null;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 03/05/2013 new field for monthly meter delay sync
		bufQuery.append("SELECT distinct a.ActualStart, a.ActualEnd, a.RevisedStart, a.BuildingID, a.FrequencyId, a.statusId, a.remarks, a.ChecklistResultID, a.serviceid ");
		bufQuery.append("FROM tblEngServiceServiceHistory a ");
		bufQuery.append("WHERE 1=1 and serviceMaintProgID=10 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			ps.set(ind++, checklistResultId);

			rs = ps.executeQuery();
			if (rs.next()) {
				int columnIndex = 1;
				schduleListVo = new ScheduleListVO();
				schduleListVo.setActualStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setActualEnd(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setRevisedStart(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				schduleListVo.setBuildingId(rs.getInt(columnIndex++));
				schduleListVo.setFrequencyId(rs.getInt(columnIndex++));
				schduleListVo.setStatusId(rs.getInt(columnIndex++));
				schduleListVo.setRemarks(rs.getString(columnIndex++));
				schduleListVo.setChecklistResultId(rs.getInt(columnIndex++));
				schduleListVo.setServiceMainProgId(rs.getInt(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);

		}

		return schduleListVo;
	}

	public void deleteImages(MonthlyMeterImagesVO vo) throws ULjException {

		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		// ResultSet rs=null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("Delete FROM tblMonthlyMeterImages ");
		bufQuery.append(" where terminalid =? and monthlymeterno = ?  ");
		if ( vo.getReadingNo()>0)
			{bufQuery.append("  and readingno=?");}

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, vo.getTerminalId());
			ps.set(ind++, vo.getMonthlyMeterNo());
			
			if ( vo.getReadingNo()>0)
			{ps.set(ind++, vo.getReadingNo());}

			ps.execute();
			conn.commit();
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {

			closePreparedStatement(ps);
		}

	}
	
	public void insertPhoto(MonthlyMeterImagesVO vo) throws ULjException {
		Log.v(TAG, "Save monthly meter Images");

		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Insert into tblMonthlyMeterImages  ");
		query.append("(terminalid, monthlymeterno, readingno ,ImageName,lastmodifieddate,lastupdatedby,Longitude, Latitude, ImageCaption, ImageData) ");
		query.append("values (?,?,?,?,?,?,?,?,?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, vo.getTerminalId());
			ps.set(index++, vo.getMonthlyMeterNo());
			ps.set(index++, vo.getReadingNo());
			ps.set(index++, vo.getImageFileName());
			ps.set(index++, vo.getLastUpdateDate());
			ps.set(index++, vo.getLastUpdatedBy());
			ps.set(index++, vo.getLongitude());
			ps.set(index++, vo.getLatitude());
			ps.set(index++, vo.getImageCaption());
			ps.set(index++, vo.getImageData());

			ps.execute();
			conn.commit();
			Log.d(TAG, "monthly meter Images inserted successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			// conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}
	
	public Vector<MonthlyMeterImagesVO> getMonthlyMeterImages(MonthlyMeterImagesVO tvo) throws ULjException {
		Vector<MonthlyMeterImagesVO> ImagesList = new Vector<MonthlyMeterImagesVO>();
		
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("SELECT terminalid, monthlymeterno, readingno,imagename, Longitude, Latitude,ImageCaption, ImageData"
				+ " FROM tblMonthlyMeterImages ");
		bufQuery.append(" where monthlymeterno = ? and readingno =?");
		bufQuery.append(" order by ImageName ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			ps.set(ind++, tvo.getMonthlyMeterNo());
			ps.set(ind++, tvo.getReadingNo());

			rs = ps.executeQuery();

			while (rs.next()) {

				int cIndex = 1;
				MonthlyMeterImagesVO vo = new MonthlyMeterImagesVO();
				vo.setTerminalId(rs.getInt(cIndex++));
				vo.setMonthlyMeterNo(rs.getInt(cIndex++));
				vo.setReadingNo(rs.getInt(cIndex++));
				vo.setImageFileName(rs.getString(cIndex++));
				vo.setLongitude(rs.getFloat(cIndex++));
				vo.setLatitude(rs.getFloat(cIndex++));
				vo.setImageCaption(rs.getString(cIndex++));
				vo.setImageData(rs.getBytes(cIndex++));
				ImagesList.addElement(vo);

			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return ImagesList;
	}
	
	
		
	public void insertMonthlyMeter(MonthlyMeterVO vo) throws ULjException {
		Log.v(TAG, "insertMonthlyMeter");

		Connection conn = DbUtil.getConnection();

		StringBuffer query = new StringBuffer("Insert into tblUtilityMeterReadingHistory  ");
		query.append("(MonthlyMeterNo, ReadingNo,meterno, ");
		query.append("ChecklistResultID, ");
		query.append("PrevConsumption, ");
		query.append("PrevReading, ");
		query.append("CurrReading, ");
		query.append("Consumption, ");
		query.append("TakenByID, ");
//		query.append("TakenOn, ");
//		query.append("AckBy, ");
//		query.append("AckBySigFilename, ");
//		query.append("AckDate, ");
		query.append("TenantID, ");
		query.append("TenantNo, ");
		query.append("LocationID, ");
		//query.append("MultiplyFactor, ");
		query.append("Remarks,lastupdatedby, lastupdateddate,meterround, unitno, customername, " +
				"tenantname,tenantemail,signfilename, signimage ) ");
	//	query.append("AckBySig ) ");
		query.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, vo.getMonthlyMeterNo());
			ps.set(index++, vo.getReadingNo());
			ps.set(index++, vo.getMeterNo());
			ps.set(index++, vo.getChecklistNo());
			ps.set(index++, vo.getPrevConsumption());
			ps.set(index++, vo.getPrevReading());
			ps.set(index++, vo.getCurrReading());
			ps.set(index++, vo.getConsumption());
			ps.set(index++, vo.getTakenBy());
//			ps.set(index++, vo.getTakenOn());
//			ps.set(index++, vo.getAname());
//			ps.set(index++, vo.getAckBySigFilename());
//			ps.set(index++, vo.getAdate());
			ps.set(index++, vo.getTenantID());
			ps.set(index++, vo.getTenantNo() );
			ps.set(index++, vo.getLocationID());
			//ps.set(index++, vo.getMultiplyFactor());
			ps.set(index++, vo.getRemarks());
			ps.set(index++, vo.getLastupdatedBy());
			ps.set(index++, vo.getLastUpdatedDate());
//			ps.set(index++, vo.getaSignature());
			ps.set(index++, vo.getMeterRound());
			ps.set(index++, vo.getUnitNo());
			ps.set(index++, vo.getCustomerName());
			ps.set(index++, vo.getTenantName());
			ps.set(index++, vo.getTenantEmail());
			ps.set(index++, vo.getAckBySigFilename());
			ps.set(index++, vo.getaSignature());
			
			ps.execute();
			conn.commit();
			Log.d(TAG, "monthly meter inserted successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}
	
	
	public void saveSignoff(MonthlyMeterSignOffVO vo) throws ULjException {
		Log.v(TAG, "insert monthly meter signoff  - start");
		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("insert into  tblMonthlyMeterSignoff ( ");
		query.append("monthlymeterno,");
		query.append("TakenByID, ");
		query.append("TakenOn,  ");
		query.append("A_Name, ");
		query.append("A_Date, ");
		query.append("A_SignatureFileName, ");
		query.append("maintofficerid, ");
		query.append("A_Designation, ");
		query.append("A_Telephone, ");
		query.append("A_Signature ) values (?,?,?,?,?,?,?,?,?,?)");
		
	
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(query.toString());
			int index = 1;

			ps.set(index++, vo.getMonthlyMeterNo());
			ps.set(index++, vo.getTakenById());
			ps.set(index++, vo.getTakenOn());
			ps.set(index++, vo.getaName());
			ps.set(index++, vo.getaDate());
			ps.set(index++, vo.getaSignatureFileName());
			ps.set(index++, vo.getMaintOfficerId());
			ps.set(index++, vo.getaDesignation());
			ps.set(index++, vo.getaTelelphone());
			ps.set(index++, vo.getaSignature());
			

			ps.execute();
			conn.commit();
		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			// conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}

		Log.v(TAG, "insert monthly meter signoff - end");

	}
	
	
	public void updateScheduleTime(ScheduleListVO vo, int updateTarget) throws ULjException {
		StringBuffer bufQuery = new StringBuffer();
		PreparedStatement ps = null;
		Connection conn = DbUtil.getConnection();

		bufQuery.append("UPDATE ");
		bufQuery.append("tblEngServiceServiceHistory a ");
		bufQuery.append("SET ");
		if (updateTarget == 1)
			bufQuery.append("ActualStart =?, ");
		if (updateTarget == 2)
			bufQuery.append("ActualEnd=?, ");
		bufQuery.append("StatusID=?, ");
		bufQuery.append("remarks=?, ");
		bufQuery.append("LastModified=?  ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND a.ChecklistResultID=?");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_STARTTIME)
				ps.set(ind++, vo.getActualStart());
			if (updateTarget == AppsConstant.CHECKLIST_UPDATE_ENDTIME)
				ps.set(ind++, vo.getActualEnd());
			ps.set(ind++, vo.getStatusId());
			ps.set(ind++, vo.getRemarks());
			ps.set(ind++, vo.getLastModifiedDate());
			ps.set(ind++, vo.getChecklistResultId());

			ps.execute();
			conn.commit();

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}

	}
	
	public MonthlyMeterSignOffVO SelectAckInfo(int monthlymeterno) {

		Connection conn = DbUtil.getConnection();
		MonthlyMeterSignOffVO vo = new MonthlyMeterSignOffVO();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  A_Name, A_Date, maintofficerid ,a_designation, a_telephone");
		bufQuery.append(" from   tblmonthlymetersignoff ");
		bufQuery.append(" where monthlymeterno=?");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, monthlymeterno);
			DateUtil dUtil = new DateUtil();
			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				vo.setaName(rs.getString(columnIndex++));
				vo.setaDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				vo.setMaintOfficerId(rs.getInt(columnIndex++));
				vo.setaDesignation(rs.getString(columnIndex++));
				vo.setaTelelphone(rs.getString(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return vo;
	}
	
	public Boolean ExistReadingno(MonthlyMeterVO mvo) {

		Connection conn = DbUtil.getConnection();
		
		Boolean bExist=false; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  monthlymeterno");
		bufQuery.append(" from   tblutilitymeterreadinghistory ");
		bufQuery.append(" where monthlymeterno=? and readingno=? and meterno=?");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, mvo.getMonthlyMeterNo());
			ps.set(ind++, mvo.getReadingNo());
			ps.set(ind++, mvo.getMeterNo());
			rs = ps.executeQuery();
			while (rs.next()) {
				
				bExist=true;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return bExist;
	}
	
	public Boolean ExistMonthlyMeterno(int monthlymeterno) {

		Connection conn = DbUtil.getConnection();
		
		Boolean bExist=false; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  monthlymeterno");
		bufQuery.append(" from   tblutilitymeterreadinghistory ");
		bufQuery.append(" where monthlymeterno=? ");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, monthlymeterno);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				
				bExist=true;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return bExist;
	}
	
	
	public void updateMonthlyMeter(MonthlyMeterVO vo) throws ULjException {
		Log.v(TAG, "update tblUtilityMeterReadingHistory");

		Connection conn = DbUtil.getConnection();

		StringBuffer query = new StringBuffer("update tblUtilityMeterReadingHistory  ");
//		query.append("meterno, ");
//		query.append("ChecklistResultID, ");
//		query.append("PrevConsumption, ");
//		query.append("PrevReading, ");
		query.append(" set CurrReading=?, ");
		query.append(" Consumption=?, ");
//		query.append("TakenByID, ");
//		query.append("TenantID, ");
//		query.append("TenantNo, ");
//		query.append("LocationID, ");
		query.append(" Remarks=?, lastupdatedby=?,  lastupdateddate=? , meterround=?" +
				", unitno=?, customername =?, tenantname=? , tenantemail=?" );
		
		if (vo.getAckBySigFilename()!=null && vo.getAckBySigFilename().length()>0)
			query.append(", signfilename=?, signimage=? "); 
				
		query.append(" where monthlymeterno=? and readingno=? and meterno=? ");
		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			
//			ps.set(index++, vo.getChecklistNo());
//			ps.set(index++, vo.getPrevConsumption());
//			ps.set(index++, vo.getPrevReading());
			ps.set(index++, vo.getCurrReading());
			ps.set(index++, vo.getConsumption());
//			ps.set(index++, vo.getTakenBy());
//			ps.set(index++, vo.getTenantID());
//			ps.set(index++, 0 );//vo.getTenantNo()
//			ps.set(index++, vo.getLocationID());
			ps.set(index++, vo.getRemarks());
			ps.set(index++, vo.getLastupdatedBy());
			ps.set(index++, vo.getLastUpdatedDate());
			ps.set(index++, vo.getMeterRound());
			ps.set(index++, vo.getUnitNo());
			ps.set(index++, vo.getCustomerName());
			ps.set(index++, vo.getTenantName());
			ps.set(index++, vo.getTenantEmail());
			
			if (vo.getAckBySigFilename()!=null && vo.getAckBySigFilename().length()>0)
			{
				ps.set(index++, vo.getAckBySigFilename());
				ps.set(index++, vo.getaSignature());
			}
			
			ps.set(index++, vo.getMonthlyMeterNo());
			ps.set(index++, vo.getReadingNo());
			ps.set(index++, vo.getMeterNo());
			
			
			ps.execute();
			conn.commit();
			Log.d(TAG, "monthly meter updated successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}

	
	public Boolean ExistSignOff(int monthlymeterno) {

		Connection conn = DbUtil.getConnection();
		
		Boolean bExist=false; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  monthlymeterno");
		bufQuery.append(" from   tblMonthlyMeterSignoff ");
		bufQuery.append(" where monthlymeterno=?");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, monthlymeterno);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				bExist=true;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return bExist;
	}
	
	
	public void updateSignoff(MonthlyMeterSignOffVO vo) throws ULjException {
		Log.v(TAG, "update monthly meter signoff  - start");
		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("update  tblMonthlyMeterSignoff ");
		query.append("set A_Name =?, ");
		query.append(" A_Date =?, ");
		query.append(" A_SignatureFileName= ? , ");
		query.append(" maintofficerid = ?, ");
		query.append(" A_Designation= ?, ");
		query.append(" A_Telephone =? , ");
		query.append(" A_Signature =? ");
		query.append(" where monthlymeterno = ?");
		
	
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(query.toString());
			int index = 1;

			ps.set(index++, vo.getaName());
			ps.set(index++, vo.getaDate());
			ps.set(index++, vo.getaSignatureFileName());
			ps.set(index++, vo.getMaintOfficerId());
			ps.set(index++, vo.getaDesignation());
			ps.set(index++, vo.getaTelelphone());
			ps.set(index++, vo.getaSignature());
			ps.set(index++, vo.getMonthlyMeterNo());
			

			ps.execute();
			conn.commit();
		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			// conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}

		Log.v(TAG, "update monthly meter signoff - end");

	}
	
	
	public Integer GetImagesCount(MonthlyMeterVO mvo) {

		Connection conn = DbUtil.getConnection();
		
		Integer cntimg =0; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  readingno ");
		bufQuery.append(" from   tblmonthlymeterimages where monthlymeterno=?  and readingno = ?");
	
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, mvo.getMonthlyMeterNo());
			ps.set(ind++, mvo.getReadingNo());
			rs = ps.executeQuery();
			while (rs.next()) {
				
				cntimg=cntimg+1;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return cntimg;
	}
	
	
	public void deleteReading(MonthlyMeterVO vo) throws ULjException {

		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		// ResultSet rs=null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("Delete FROM tblUtilityMeterReadingHistory ");
		bufQuery.append(" where monthlymeterno = ?  and readingno=? ");
		

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, vo.getMonthlyMeterNo());
			ps.set(ind++, vo.getReadingNo());
		
			ps.execute();
			conn.commit();
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {

			closePreparedStatement(ps);
		}

	}
	
	
	public String GetUserMeterType(int userinfoid) {

		Connection conn = DbUtil.getConnection();
		
		String strmetertype =""; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  metertype ");
		bufQuery.append(" from   tblUserMeterType where userinfoid=? ");
	
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, userinfoid);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				strmetertype =rs.getString(columnIndex++);
				
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return strmetertype;
	}
	
	public MonthlyMeterVO searchPreviousMetersReadingHistory(int meterno) throws ULjException {
		Log.d("MonthlyMeterDAO.java", "Searching monthly meters --" + meterno);

		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		MonthlyMeterVO mVO = new MonthlyMeterVO();
		
		bufQuery.append("SELECT top 1 r.monthlymeterno, r.readingno, mst.MeterNo,mst.SerialNo,mst.MeterType,InstallationDate,RemovalDate, ");
		bufQuery.append("IntialReading,LastReading,mst.MultiplyFactor,UnitID,MeterCategory,MeterClass, ");
		bufQuery.append("MeterStatus,PUCNo,r.locationid ,r.ChecklistResultID,r.PrevConsumption, r.PrevReading, r.CurrReading, r.Consumption ," +
				"r.remarks, mst.sublocation, r.meterround, r.unitno, r.customername, r.tenantname, r.tenantemail, r.signfilename, r.signimage ");
		bufQuery.append("FROM tblMstUtilityMeterList mst left OUTER JOIN ");
		bufQuery.append("tblUtilityMeterReadingHistory r ON mst.MeterNo = r.MeterNo  ");
		bufQuery.append("  where  r.meterno= ? order by r.lastupdateddate desc ");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, meterno);
		
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				mVO.setMonthlyMeterNo(rs.getInt(columnIndex++));
				mVO.setReadingNo(rs.getInt(columnIndex++));
				mVO.setMeterNo(rs.getInt(columnIndex++));
				mVO.setSerialNo(rs.getString(columnIndex++));
				mVO.setMeterType(rs.getString(columnIndex++));
//				mVO.setInstallationDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
//				mVO.setRemovalDate(dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
				
				mVO.setInitialReading(rs.getDouble(columnIndex++));
				mVO.setLastReading(rs.getDouble(columnIndex++));
				mVO.setMultiplyFactor(rs.getFloat(columnIndex++));
				
				mVO.setUnitID(rs.getString(columnIndex++));
				mVO.setMeterCategory(rs.getString(columnIndex++));
				mVO.setMeterClass(rs.getString(columnIndex++));
				mVO.setMeterStatus(rs.getString(columnIndex++));
				
				mVO.setPUCNo(rs.getString(columnIndex++));
				mVO.setLocationID(rs.getInt(columnIndex++));
				mVO.setChecklistNo(rs.getInt(columnIndex++));
				
				mVO.setPrevConsumption(rs.getFloat(columnIndex++));
				mVO.setPrevReading(rs.getString(columnIndex++));
				mVO.setCurrReading(rs.getString(columnIndex++));
				mVO.setConsumption(rs.getFloat(columnIndex++));
				
				mVO.setRemarks(rs.getString(columnIndex++));
				mVO.setSubLocation(rs.getString(columnIndex++));
				mVO.setMeterRound(rs.getBoolean(columnIndex++));
				
				mVO.setUnitNo(rs.getString(columnIndex++));
				mVO.setCustomerName(rs.getString(columnIndex++));
				mVO.setTenantName(rs.getString(columnIndex++));
				mVO.setTenantEmail(rs.getString(columnIndex++));
				mVO.setAckBySigFilename(rs.getString(columnIndex++));
				
				mVO.setaSignature(rs.getBytes(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e("MonthlyMeterDAO.java", "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return mVO;
	}
	// llm 10/12/12 
	public Integer GetMontlyMeterNoByChecklistResuiltID(int ChecklistResultID) {

		Connection conn = DbUtil.getConnection();
		
		Integer monthlymeterno =0; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select top 1  monthlymeterno ");
		bufQuery.append(" from   tblutilitymeterreadinghistory where ChecklistResultID=? ");
	
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, ChecklistResultID);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				monthlymeterno =rs.getInt(columnIndex++);
				
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return monthlymeterno;
	}
	//llm 03/05/2013 new table for monthly meter delay sync
	public void insertSyncedMonthlyMeter(ScheduleVO vo) throws ULjException {
		Log.v(TAG, "Save SyncedMonthlyMeter");
		DateUtil dateUtil = new DateUtil();
		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Insert into tblSyncedMonthlyMeterService  ");
		query.append("(ServiceMaintProgId,serviceresultid,syncdate,syncstatus) ");
		query.append("values (?,?,?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, vo.getServiceMainProgId());
			ps.set(index++, vo.getChecklistResultId());
			ps.set(index++, dateUtil.getCurrentTimeStamp());
			ps.set(index++, 0);

			ps.execute();
			conn.commit();
			Log.d(TAG, "SyncedMonthlyMeter inserted successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			// conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}
}
