package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *June 6, 2011		Ryan			Created
 *
 */
public abstract class CustomAdapterWithImage extends ArrayAdapter {
	private final String TAG = "CustomArrayAdapterNotDisplaySelection.java";
	protected int[] statusImages;

	public CustomAdapterWithImage(Context context, ArrayList vo) {
		super(context, android.R.layout.simple_spinner_item, vo);
		setDropDownViewResource(R.layout.cust_spinner_with_color_code);

		initializeStatusImageArray();
		// statusImages = new CommonFunction().getFaultStatusImageArray();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.cust_spinner_with_color_code, parent, false);
		}

		TextView desc = (TextView) convertView.findViewById(R.id.txtListValue);

		// Populate template
		DropdownItemVO data = (DropdownItemVO) getItem(position);
		desc.setText("");

		return convertView;
	}

	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.cust_spinner_with_color_code, parent, false);
		}

		LinearLayout l = (LinearLayout) convertView.findViewById(R.id.linearLayoutSpinnerWithImage);
		if (position == 0) {
			l.setBackgroundColor(android.graphics.Color.GRAY);
		} else {
			l.setBackgroundColor(android.graphics.Color.WHITE);
		}

		TextView desc = (TextView) convertView.findViewById(R.id.txtListValue);
		// Populate template
		DropdownItemVO data = (DropdownItemVO) getItem(position);

		ImageView statusImage = (ImageView) convertView.findViewById(R.id.lstStatusCode);
		if (statusImage != null) {
			int statusIndex = getStatusImageIndex(data.getItemId());
			if (statusIndex > -1)
				statusImage.setBackgroundResource(statusImages[statusIndex]);
			else
				statusImage.setBackgroundResource(android.R.color.transparent);
		}
		desc.setText(data.getItemDescription());
		return convertView;
	}

	// private int getStatusImageIndex(String itemNo){
	// int intItemNo = Integer.parseInt(itemNo);
	// int index=-1;
	// //hardcoded logic
	// switch (intItemNo){
	// case 1: index=0; break;
	// case 2: index=2; break;
	// case 3: index=3; break;
	// case 4: index=1; break;
	// case 5: index=4; break;
	//
	// }
	// return index;
	// }

	abstract void initializeStatusImageArray();

	abstract int getStatusImageIndex(String itemNo);

}
