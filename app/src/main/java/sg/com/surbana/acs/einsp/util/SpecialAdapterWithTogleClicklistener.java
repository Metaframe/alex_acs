package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;
import java.util.HashMap;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.ToggleButton;

public class SpecialAdapterWithTogleClicklistener extends SimpleAdapter {
	private Context con;
	private Activity act;

	public SpecialAdapterWithTogleClicklistener(Activity activity, Context context, ArrayList<HashMap<String, Object>> list, int resource, String[] from,
			int[] to) {
		super(context, list, resource, from, to);
		con = context;
		act = activity;
	}

	private final String TAG = "SpecialAdapter.class";

	private int[] colors = new int[] { main.CAG_RESOURCE.getColor(R.color.col_white), main.CAG_RESOURCE.getColor(R.color.col_light_purple) };

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		ViewHolderButton holder = (ViewHolderButton) view.getTag();
		if (holder == null) {
			holder = new ViewHolderButton(view);
			view.setTag(holder);

			holder.tButton.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					System.out.println(v.getTag());
					ToggleButton tb = (ToggleButton) v;

					HashMap map = (HashMap) getItem((Integer) v.getTag());

					if (tb.isChecked()) {
						map.put("condition", 1); // true
						act.showDialog(0);
					} else {
						map.put("condition", 2);// false
					}

				}
			});

		}

		holder.tButton.setTag(new Integer(position));

		HashMap map = (HashMap) getItem(position);

		// System.out.println("Position "+ position + "-" +
		// holder.tButton.isChecked()+ map.get("condition"));

		// ToggleButton tButton=(ToggleButton)
		// view.findViewById(R.id.toggleButton1);

		// if(map.get("condition")!=null && ((Integer)map.get("condition")==1 ||
		// (Integer)map.get("condition")==0))
		// tButton.setChecked(true);
		// else
		// tButton.setChecked(false);
		//

		view.setLongClickable(true);

		int colorPos = position % colors.length;
		view.setBackgroundColor(colors[colorPos]);
		return view;
	}

	private void showYesNoDialog(HashMap map, ToggleButton tb) {
		AlertDialog dialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(con);

		builder.setMessage("No selected, create new fault? ").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		dialog = builder.create();
		dialog.setOwnerActivity(act);
		dialog.show();
	}

}
