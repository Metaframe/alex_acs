package sg.com.surbana.acs.einsp.util;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import android.app.Notification;
import android.app.ProgressDialog;
import android.os.Handler;
import android.util.Log;

import com.ianywhere.ultralitejni12.SyncObserver;
import com.ianywhere.ultralitejni12.SyncResult;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *May 20, 2011		Ryan			Created
 *
 */
public class AppSyncObserver implements SyncObserver {
	private final String TAG = "AppSyncObserver.java";
	private final String COMMITTING_DOWNLOAD = "Committing Download";
	private final String CONNECTING = "Connecting";
	private final String DISCONNECTING = "Disconnecting";
	private final String DONE = "Synchronization Complete";
	private final String ERROR = "Error";
	private final String FINISHING_UPLOAD = "Uploading Completed";
	private final String STARTING = "Starting";
	private final String RECEIVING_DATA = "Receiving Data";
	private final String RECEIVING_TABLE = "Receiving Table";
	private final String RECEIVING_UPLOAD_ACK = "Receiving Upload Acknowledgement";
	private final String ROLLING_BACK_DOWNLOAD = "Rolling Back Download";
	private final String SENDING_DOWNLOAD_ACK = "Sending Download Acknowledgement";
	private final String SENDING_HEADER = "Sending Header";
	private final String SENDING_TABLE = "Sending table";
	private ProgressDialog progressDialog;
	private Handler mHandler = new Handler();
	private Notification notification;
	private int notificationID;
	int syncState;
	SyncResult syncResult;

	public boolean syncProgress(int state, SyncResult result) {
		syncState = state;
		syncResult = result;
		Log.v(TAG,
				state + " - syncProgress " + getStatus(syncState) + " bytes sent = " + syncResult.getSentByteCount() + "  bytes received = "
						+ result.getReceivedByteCount());
		if (progressDialog != null) {
			mHandler.post(new Runnable() {
				public void run() {
					// progressDialog.setMessage("syncProgress " +
					// getStatus(syncState) + " : Data sent = " +
					// syncResult.getSentByteCount() + "  Data received = " +
					// syncResult.getReceivedByteCount());
					// commented updating since only static message will be
					// shown
					// progressDialog.setMessage("syncProgress " +
					// getStatus(syncState) + " : \n Tables Synced = " +
					// syncResult.getSyncedTableCount()+ "  of  " +
					// syncResult.getTotalTableCount());

				}
			});
		}

		if (notification != null) {
			mHandler.post(new Runnable() {
				public void run() {
					// notification.contentView.setTextViewText(R.id.text,
					// notificationID + "syncProgress " + getStatus(syncState) +
					// " bytes sent = " + syncResult.getSentByteCount() +
					// "  bytes received = " +
					// syncResult.getReceivedByteCount());

					notification.contentView.setTextViewText(
							R.id.text,
							"Progress : " + getStatus(syncState) + " \nTables Synced : " + syncResult.getSyncedTableCount() + "  of  "
									+ syncResult.getTotalTableCount());
					main.NOTIFICATION_MANAGER.notify(notificationID, notification);

				}
			});

		}

		return false; // Always continue synchronization.
	}

	public String getStatus(int state) {
		switch (state) {
		case SyncObserver.States.COMMITTING_DOWNLOAD:
			return COMMITTING_DOWNLOAD;
		case SyncObserver.States.CONNECTING:
			return CONNECTING;
		case SyncObserver.States.DISCONNECTING:
			return DISCONNECTING;
		case SyncObserver.States.DONE:
			return DONE;
		case SyncObserver.States.ERROR:
			return ERROR;
		case SyncObserver.States.FINISHING_UPLOAD:
			return FINISHING_UPLOAD;
		case SyncObserver.States.STARTING:
			return STARTING;
		case SyncObserver.States.RECEIVING_DATA:
			return RECEIVING_DATA;
		case SyncObserver.States.RECEIVING_TABLE:
			return RECEIVING_TABLE;
		case SyncObserver.States.RECEIVING_UPLOAD_ACK:
			return RECEIVING_UPLOAD_ACK;
		case SyncObserver.States.ROLLING_BACK_DOWNLOAD:
			return ROLLING_BACK_DOWNLOAD;
		case SyncObserver.States.SENDING_DOWNLOAD_ACK:
			return SENDING_DOWNLOAD_ACK;
		case SyncObserver.States.SENDING_HEADER:
			return SENDING_HEADER;
		case SyncObserver.States.SENDING_TABLE:
			return SENDING_TABLE;
		}
		return "Unknown States";
	}

	public AppSyncObserver(ProgressDialog pd) {
		progressDialog = pd;
	} // The default constructor.

	public AppSyncObserver(Notification not, int notificationID) {
		notification = not;
		this.notificationID = notificationID;
	} // The default constructor.

}
