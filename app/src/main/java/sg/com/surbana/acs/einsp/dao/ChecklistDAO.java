package sg.com.surbana.acs.einsp.dao;

import java.util.ArrayList;
import java.util.Date;

import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.vo.ChecklistListVO;
import sg.com.surbana.acs.einsp.vo.ChecklistResultVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleVO;
import android.util.Log;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 14, 2011		Ryan			Created
 * 29 Jun 2011		Ryan			Subquery to retrieve the number fault id created for the checklist
 * 13 Sep 2011		Ryan			Insert record to service result table and sycn to sql server *
 */
public class ChecklistDAO extends BaseDAO {
	private final String TAG = "ChecklistDAO.java";

	public void insertChecklistResult(ChecklistResultVO vo) throws ULjException {
		Log.d(TAG, "Save checklist result " + vo.getChecklistItemId() + "-" + vo.getServiceDescription());

		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Insert into tblServiceChecklistResultAll_nosync  ");
		query.append("(ResultID, ChecklistItemID, GroupID, Status, Remarks, Servicedescription)");
		query.append("values ( ?,?,?,?,?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, vo.getResultId());
			ps.set(index++, vo.getChecklistItemId());
			ps.set(index++, vo.getGroupId());
			ps.set(index++, vo.getStatus());
			ps.set(index++, vo.getRemarks());
			ps.set(index++, vo.getServiceDescription());

			ps.execute();
			conn.commit();
			Log.d(TAG, "checklist result created successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}

	/**
	 * Retrieve list of checklsit item information with fault
	 * 
	 * @param resultId
	 * @return
	 */
	public ArrayList<ChecklistResultVO> selectChecklistResultWithFault(int resultId) {
		ArrayList<ChecklistResultVO> list = new ArrayList();

		Connection conn = DbUtil.getConnection();
		ChecklistResultVO vo;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  distinct tbf.ServiceChecklistResultId, tbf.ChecklistItemId,  tbf.ChecklistGroupId ");
		bufQuery.append(" 	 from   tblFaultDetails tbf ");
		bufQuery.append("	 where  tbf.ServiceChecklistResultId=?");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, resultId);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				vo = new ChecklistResultVO();
				vo.setResultId(rs.getInt(columnIndex++));
				vo.setChecklistItemId(rs.getInt(columnIndex++));
				vo.setGroupId(rs.getInt(columnIndex++));
				list.add(vo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return list;
	}

	/**
	 * Create record for each and every of the checklist item with fault
	 * 
	 * @param vo
	 * @throws ULjException
	 */
	public void insertFaultyChecklistResult(ChecklistResultVO vo) throws ULjException {
		Log.d(TAG, "Save checklist result " + vo.getChecklistItemId() + "-" + vo.getServiceDescription());

		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Insert into tblServiceChecklistResult ");
		query.append("(ResultID, ChecklistItemID, checklistGroupID,  Remarks )");
		query.append("values (  ?,?,?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, vo.getResultId());
			ps.set(index++, vo.getChecklistItemId());
			ps.set(index++, vo.getGroupId());
			ps.set(index++, " ");

			ps.execute();
			conn.commit();
			Log.d(TAG, "checklist result created successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}

	public void updateChecklistItemResult(int resultId, int itemId, int groupId, int status) throws ULjException {
		Log.d(TAG, "update checklist result " + resultId + "-" + itemId + "-" + groupId);

		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Update  tblServiceChecklistResultAll_nosync  ");
		query.append("set status = ? ");
		query.append("where ResultID = ? ");
		query.append("and ChecklistItemID = ?  ");
		query.append("and  GroupID=? ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, status);
			ps.set(index++, resultId);
			ps.set(index++, itemId);
			ps.set(index++, groupId);

			ps.execute();
			conn.commit();
			Log.d(TAG, "checklist result updated successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}

	public ArrayList<ChecklistListVO> searchChecklistByCriteria(int templateId, int versionId, int resultId) throws ULjException {
		Log.d(TAG, "Searching checklist templateId/versionId/resultId--" + templateId + " / " + versionId + " / " + resultId);

		ArrayList<ChecklistListVO> checklistList = new ArrayList<ChecklistListVO>();
		ChecklistListVO checklistVO;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("SELECT  cl.itemId, cl.itemDescription, cl.groupId, cl.groupDescription, ifnull(chr.status,0,chr.status),");// return
																																	// status=0
																																	// if
																																	// no
																																	// record

		// Ryan-20110629-Subquery to retrieve the number fault id created for
		// the checklist
//		bufQuery.append(" 	(select  count(tbf.FaultID)");
//		bufQuery.append(" 	 from tblFaultDetails tbf");
//		bufQuery.append("	 where tbf.ServiceChecklistResultId=?");
//		bufQuery.append("	 and tb.ChecklistItemId=cl.itemId");
//		bufQuery.append("	and tbf.ChecklistGroupId=cl.groupId) ");
	//llm 07/10/2013 count plus commercial fault, for commercial fault bug
		bufQuery.append("	(select sum(cnt) from  ");
		bufQuery.append("	( ");
		bufQuery.append("	select  count(a.FaultID) as cnt  from tblFaultDetails a	 where a.ServiceChecklistResultId=?	 ");
		bufQuery.append("	and a.ChecklistItemId=cl.itemId and a.ChecklistGroupId=cl.groupId union  ");
		bufQuery.append("	select  count(b.FaultID) as cnt	 from tblcommercialFaultDetails b where b.ServiceChecklistResultId=? ");	
		bufQuery.append("	and b.ChecklistItemId=cl.itemId and b.ChecklistGroupId=cl.groupId ) as t (cnt) )");
	//llm 07/10/2013 count plus commercial fault, for commercial fault bug
		bufQuery.append("FROM tblChecklist cl ");
		bufQuery.append("LEFT JOIN tblServiceChecklistResultAll_nosync chr ");
		bufQuery.append("ON cl.itemId =chr.checklistItemId ");
		bufQuery.append("AND cl.groupId =chr.groupId ");
		bufQuery.append("AND chr.resultId=?  ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND cl.templateId=?  ");
		bufQuery.append("AND cl.templateversion=?  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, resultId);
			ps.set(ind++, resultId);
			ps.set(ind++, resultId);
			ps.set(ind++, templateId);
			ps.set(ind++, versionId);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				checklistVO = new ChecklistListVO();
				checklistVO.setItemId(rs.getInt(columnIndex++));
				checklistVO.setItemDescription(rs.getString(columnIndex++));
				checklistVO.setGroupId(rs.getInt(columnIndex++));
				checklistVO.setGroupDescription(rs.getString(columnIndex++));
				checklistVO.setStatus(rs.getInt(columnIndex++));
				checklistVO.setNumOfFault(rs.getInt(columnIndex++));
				checklistList.add(checklistVO);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return checklistList;
	}

public Date searchServiceRevisedStart(int resultId) {
		Date revisedStart = null;
		Connection conn = DbUtil.getConnection();
		String query = " Select RevisedStart from tblServiceServiceHistory where  ChecklistResultID=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			ps.set(1, resultId);
			rs = ps.executeQuery();
			if (rs.next()) {
				revisedStart = rs.getDate(1);
			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		//
		return revisedStart;

	}

	public ArrayList<ChecklistListVO> searchTenantChecklistByCriteria(int templateId, int versionId, int resultId) throws ULjException {
		Log.d(TAG, "Searching checklist templateId/versionId/resultId--" + templateId + " / " + versionId + " / " + resultId);

		ArrayList<ChecklistListVO> checklistList = new ArrayList<ChecklistListVO>();
		ChecklistListVO checklistVO;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("SELECT  cl.itemId, cl.itemDescription, cl.groupId, cl.groupDescription, ifnull(chr.status,0,chr.status),");// return
																																	// status=0
																																	// if
																																	// no
																																	// record

		// Ryan-20110629-Subquery to retrieve the number fault id created for
		// the checklist
		bufQuery.append(" 	(select  count(tbf.FaultID)");
		bufQuery.append(" 	 from tblFaultDetails tbf");
		bufQuery.append("	 where tbf.ServiceChecklistResultId=?");
		bufQuery.append("	 and tbf.ChecklistItemId=cl.itemId");
		bufQuery.append("	and tbf.ChecklistGroupId=cl.groupId) ");

		bufQuery.append("FROM tblTenantChecklist cl ");
		bufQuery.append("LEFT JOIN tblServiceChecklistResultAll_nosync chr ");
		bufQuery.append("ON cl.itemId =chr.checklistItemId ");
		bufQuery.append("AND cl.groupId =chr.groupId ");
		bufQuery.append("AND chr.resultId=?  ");
		bufQuery.append("WHERE 1=1 ");
		bufQuery.append("AND cl.templateId=?  ");
		bufQuery.append("AND cl.templateversion=?  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, resultId);
			ps.set(ind++, resultId);
			ps.set(ind++, templateId);
			ps.set(ind++, versionId);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				checklistVO = new ChecklistListVO();
				checklistVO.setItemId(rs.getInt(columnIndex++));
				checklistVO.setItemDescription(rs.getString(columnIndex++));
				checklistVO.setGroupId(rs.getInt(columnIndex++));
				checklistVO.setGroupDescription(rs.getString(columnIndex++));
				checklistVO.setStatus(rs.getInt(columnIndex++));
				checklistVO.setNumOfFault(rs.getInt(columnIndex++));
				checklistList.add(checklistVO);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return checklistList;
	}

	public int getNewChecklistResultID() throws ULjException {
		String newId;
		DbUtil db = new DbUtil();
		
		String query = "Select max(checklistresultid) from tblNewChecklistResultID ";
		try {
			newId = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}
		int checklistresultid = 0;
		checklistresultid=Integer.parseInt(newId);
		return checklistresultid;
	}
	
	public void insertSSH(ScheduleVO vo) throws ULjException {
		Log.v(TAG, "Save Case");

		Connection conn = DbUtil.getConnection();

		StringBuffer query = new StringBuffer("Insert into tblserviceservicehistory  ");
		query.append("(ServiceMaintProgID,serviceID,ScheduledStart,RevisedStart,");
		query.append("ActualStart,ActualEnd,PersonInChargeID,Remarks,");
		query.append("ChecklistTemplateID,ChecklistTemplateVersion,");
		query.append("ChecklistResultID,BuildingID,ServiceDescription,");
		query.append("StatusID,LastModified,FrequencyID ) ");
		query.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, vo.getServiceMainProgId());
			ps.set(index++, vo.getServiceId());
			ps.set(index++, vo.getScheduledStart());
			ps.set(index++, vo.getRevisedStart());
			ps.set(index++, vo.getActualStart());
			ps.set(index++, vo.getActualEnd());
			ps.set(index++, vo.getPersonInChargeId());
			ps.set(index++, vo.getRemarks());
			ps.set(index++, vo.getChecklistTemplateId());
			ps.set(index++, vo.getChecklistTemplateVersion());
			ps.set(index++, vo.getChecklistResultId());
			ps.set(index++, vo.getBuildingId());
			ps.set(index++, vo.getServiceDescription());
			ps.set(index++, vo.getStatusId());
			ps.set(index++, vo.getLastModifiedDate());
			ps.set(index++, vo.getFrequencyId());

			ps.execute();
			conn.commit();
			Log.d(TAG, "Fault created successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}
		// llm 2/1/13 to add ssh record from handheld for commercial user
	
	// llm 21/1/13 to add ssh record from handheld for commercial user
	public void insertAdhocChecklistResult(int resultid,int locationid) throws ULjException {
		

		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Insert into tbladhocserviceservicehistory ");
		query.append(" (ChecklistResultID,locationid) values (?,?) ");

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++,resultid);
			ps.set(index++,locationid);

			ps.execute();
			conn.commit();
			Log.d(TAG, "adhoc checklist result created successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}
	// llm 21/1/13 to add ssh record from handheld for commercial user
	
	
	public int searchAdhocSSHLocationID(int resultId) {
		int locationid = 0;
		Connection conn = DbUtil.getConnection();
		String query = " Select locationid from tbladhocServiceServiceHistory where  ChecklistResultID=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			ps.set(1, resultId);
			rs = ps.executeQuery();
			if (rs.next()) {
				locationid = rs.getInt(1);
			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		//
		return locationid;

	}
	
	public void updateAdhocChecklistLocation(int resultId, int locationid) throws ULjException {
	
		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Update  tbladhocServiceservicehistory  ");
		query.append("set locationid = ? ");
		query.append("where checklistresultid = ? ");
	

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, locationid);
			ps.set(index++, resultId);
		
			ps.execute();
			conn.commit();
			Log.d(TAG, "checklist result location updated successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}
	
	public Boolean ExistAdhocInsp(int checklistresultid) {

		Connection conn = DbUtil.getConnection();
		
		Boolean bExist=false; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append(" select  checklistresultid");
		bufQuery.append(" from   tbladhocserviceservicehistory ");
		bufQuery.append(" where checklistresultid=?");
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, checklistresultid);
		
			rs = ps.executeQuery();
			while (rs.next()) {
				
				bExist=true;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}
		return bExist;
	}
	
	public void updateAdhocSSHMaintProgId(int resultId, int servicemaintprogid) throws ULjException {
		
		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Update  tblServiceservicehistory  ");
		query.append("set serviceid = ? ");
		query.append("where checklistresultid = ? ");
	

		PreparedStatement ps = conn.prepareStatement(query.toString());

		try {
			int index = 1;
			ps.set(index++, servicemaintprogid);
			ps.set(index++, resultId);
		
			ps.execute();
			conn.commit();
			Log.d(TAG, "checklist result location updated successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}


}

