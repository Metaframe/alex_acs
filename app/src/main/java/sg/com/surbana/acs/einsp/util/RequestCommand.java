package sg.com.surbana.acs.einsp.util;

import android.app.Activity;

import com.android.volley.Request;

import java.util.ArrayList;

/**
 * Created by Paul Alfred on 5/4/2017.
 */

public class RequestCommand<T> {

    private ArrayList<Request<T>> requestList = new ArrayList<Request<T>>();
    private Activity activity;

    public RequestCommand(Activity activity) {
        this.activity = activity;
    }
    public void add(Request<T> request){
        requestList.add(request);
    }

    public void close(Request<T> request) {
        requestList.add(request);
    }

    public void execute() {
        for(Request<T> request : requestList) {
            AddRequestSingleton.getInstance(activity).addToRequestQueue(request);
        }
    }
}
