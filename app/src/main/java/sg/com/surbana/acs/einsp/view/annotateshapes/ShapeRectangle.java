package sg.com.surbana.acs.einsp.view.annotateshapes;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Aug 16, 2011		Ryan			Created
 *
 */
public class ShapeRectangle extends BaseShape {
	private final String TAG = "AbstractShape.java";
	RectF roundedRect;
	Matrix mat = new Matrix();
	int DEFAULT_REQ_DIMENSION = 50;
	int DEFAULT_SCALE = 10;
	float x1, x2, y1, y2;

	public ShapeRectangle(Canvas can, Paint p) {
		super(can, p);
		xscale = DEFAULT_SCALE;
		yscale = DEFAULT_SCALE;
	}

	public ShapeRectangle() {
		super();
		xscale = DEFAULT_SCALE;
		yscale = DEFAULT_SCALE;
	}

	public void drawMe(Canvas can, float touchX, float touchY) {
		x = touchX;
		y = touchY;
		shpCanvas = can;
		drawMe(can);
	}

	public void drawMe() {
		shpPath.reset();
		shpPath.moveTo(x, y);
		roundedRect = new RectF(x - 10, y + 10, x + 10, y - 10);
		shpCanvas.drawRoundRect(roundedRect, 6, 6, shpPaint);
	}

	public void drawMe(Canvas can) {
		shpPath.reset();
		shpPath.moveTo(x, y);

		x1 = ((x) + DEFAULT_REQ_DIMENSION / 10) - ((DEFAULT_REQ_DIMENSION / 10) * xscale);
		x2 = ((x) - DEFAULT_REQ_DIMENSION / 10) + ((DEFAULT_REQ_DIMENSION / 10) * xscale);
		y1 = ((y) + DEFAULT_REQ_DIMENSION / 10) - ((DEFAULT_REQ_DIMENSION / 10) * yscale);
		y2 = ((y) - DEFAULT_REQ_DIMENSION / 10) + ((DEFAULT_REQ_DIMENSION / 10) * yscale);

		roundedRect = new RectF(x1, y1, x2, y2);
		// can.drawRoundRect(roundedRect, 12,13, shpPaint);
		System.out.println(" x and y is " + x + "/" + y);
		can.drawRect(roundedRect, shpPaint);

	}

}
