package sg.com.surbana.acs.einsp.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.ByteString;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sg.com.surbana.acs.einsp.vo.Image2VO;
import sg.com.surbana.acs.einsp.vo.ImageMapVO;
import sg.com.surbana.acs.einsp.vo.ImageSendDataVO;
import sg.com.surbana.acs.einsp.vo.MeterSendVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleList2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleListSendVO;

/**
 * Created by ALRED on 4/30/2017.
 */

public class ApiConnectionUtil {


    private DatabaseHelper databaseHelper = null;

    private Dao<MonthlyMeterVO, Integer> monthlyMeterDAO = null;
    private Dao<ScheduleListSendVO, Integer> scheduleListSendDAO = null;
    private Dao<ImageSendDataVO, Integer> imageSendDAO = null;
    private Dao<MeterSendVO, Integer> meterSendDAO = null;
    private Dao<ImageMapVO, Integer> imageMapDAO = null;
    private Dao<Image2VO, Integer> imageDAO = null;
    private Dao<ScheduleList2VO, Integer> scheduleListDAO = null;

    private Activity activity = null;

    private boolean isExistingResult = false;

    public ApiConnectionUtil(Activity activity){
        this.activity = activity;
        databaseHelper = DatabaseHelper.getHelper(activity);
        monthlyMeterDAO = databaseHelper.getMonthlyMeterVO();
        scheduleListSendDAO = databaseHelper.getScheduleListSendVO();
        imageSendDAO = databaseHelper.getImageSendDataVO();
        meterSendDAO = databaseHelper.getMeterSendVO();
        imageMapDAO = databaseHelper.getImageMapVO();
        scheduleListDAO = databaseHelper.getScheduleListDAO();
    }
    public void updateScheduleTime(final ScheduleListSendVO scheduleListSendVO) {
        buildRetrofit().saveServiceHistory(jsonObjectToString(scheduleListSendVO)).enqueue(new Callback<ScheduleListSendVO>() {
            @Override
            public void onResponse(Call<ScheduleListSendVO> call, Response<ScheduleListSendVO> response) {
                scheduleListSendDAO = getDatabaseHelper().getScheduleListSendVO();
                try {
                    UpdateBuilder<ScheduleListSendVO, Integer> scheduleUpdateBuilder = scheduleListSendDAO.updateBuilder();
                    scheduleUpdateBuilder.where().eq("intChecklistResultID",scheduleListSendVO.getIntChecklistResultID())
                            .and().eq("dtActualEnd",scheduleListSendVO.getDtActualEnd()).and().eq("isOffline",false);
                    scheduleUpdateBuilder.updateColumnValue("isOffline",true);
                    scheduleUpdateBuilder.update();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ScheduleListSendVO> call, Throwable t) {
                scheduleListSendDAO = getDatabaseHelper().getScheduleListSendVO();
                try {
                    scheduleListSendDAO.create(scheduleListSendVO);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void sendImageData(final File imageData) {
        RequestBody image = RequestBody.create(MediaType.parse("image/*"), imageData);
        MultipartBody.Part requestBody = MultipartBody.Part.createFormData("1", imageData.getName(), image);
        buildRetrofit(AppsConstant.PORT_URL).sendImageData(requestBody).enqueue(new Callback<ImageSendDataVO>() {
            @Override
            public void onResponse(Call<ImageSendDataVO> call, Response<ImageSendDataVO> response) {
//                imageSendDAO = getDatabaseHelper().getImageSendDataVO();
//                try {
//                    UpdateBuilder<ImageSendDataVO, Integer> imageSendUpdateBuilder = imageSendDAO.updateBuilder();
//                    imageSendUpdateBuilder.where().eq("MonthlyMeterNo",imageData.getMonthlyMeterNo()).and().eq("isOffline",false);
//                    imageSendUpdateBuilder.updateColumnValue("isOffline",true);
//                    imageSendUpdateBuilder.update();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            public void onFailure(Call<ImageSendDataVO> call, Throwable t) {
//                imageSendDAO = getDatabaseHelper().getImageSendDataVO();
//                try {
//                    if(imageSendDAO.queryForEq("monthlyMeterNo", imageData.getMonthlyMeterNo()) == null) {
//                        imageSendDAO.create(imageData);
//                    }
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
            }
        });
    }

    public boolean isExistingMeterNo(String monthlymeterno, String readingno, String meterno, String checkresultid){
        buildRetrofit().isExistingMonthlyMeterNo(monthlymeterno, readingno, meterno, checkresultid).enqueue(new Callback<MeterSendVO>() {
            @Override
            public void onResponse(Call<MeterSendVO> call, Response<MeterSendVO> response) {
                if(response.body() != null) {
                    Toast.makeText(activity, response.body().getResult(), Toast.LENGTH_SHORT);
                    isExistingResult = Boolean.parseBoolean(response.body().getResult());
                }
            }

            @Override
            public void onFailure(Call<MeterSendVO> call, Throwable t) {

            }
        });
        return isExistingResult;
    }

    public void updateReadingHistory(final MeterSendVO meterSendVO){
        String strMeterSendVO = gsonObjectToString(meterSendVO);
        buildRetrofit().updateReadingHistory(strMeterSendVO).enqueue(new Callback<MeterSendVO>() {
            @Override
            public void onResponse(Call<MeterSendVO> call, Response<MeterSendVO> response) {
                meterSendDAO = getDatabaseHelper().getMeterSendVO();
                try {
                    UpdateBuilder<MeterSendVO, Integer> meterUpdateBuilder = meterSendDAO.updateBuilder();
                    meterUpdateBuilder.where().eq("sMonthlyMeterNo",meterSendVO.getsMonthlyMeterNo()).and().eq("isOffline",false);
                    meterUpdateBuilder.updateColumnValue("isOffline",true);
                    meterUpdateBuilder.update();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MeterSendVO> call, Throwable t) {
                meterSendDAO = getDatabaseHelper().getMeterSendVO();
                try {
                    QueryBuilder queryBuilder = meterSendDAO.queryBuilder();
                    int querySize = queryBuilder.where().eq("sMonthlyMeterNo",meterSendVO.getsMonthlyMeterNo()).query().size();
                    if(querySize == 0) {
                        meterSendDAO.create(meterSendVO);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void insertReadingHistory(final MeterSendVO meterSendVO) {
        String strMeterSendVO = gsonObjectToString(meterSendVO);
        buildRetrofit().insertReadingHistory(strMeterSendVO).enqueue(new Callback<MeterSendVO>() {
            @Override
            public void onResponse(Call<MeterSendVO> call, Response<MeterSendVO> response) {
                meterSendDAO = getDatabaseHelper().getMeterSendVO();
                try {
                    UpdateBuilder<MeterSendVO, Integer> meterUpdateBuilder = meterSendDAO.updateBuilder();
                    meterUpdateBuilder.where().eq("sMonthlyMeterNo",meterSendVO.getsMonthlyMeterNo()).and().eq("isOffline",false);
                    meterUpdateBuilder.updateColumnValue("isOffline",true);
                    meterUpdateBuilder.update();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MeterSendVO> call, Throwable t) {
                meterSendDAO = getDatabaseHelper().getMeterSendVO();
                try {
                    QueryBuilder queryBuilder = meterSendDAO.queryBuilder();
                    int querySize = queryBuilder.where().eq("sMonthlyMeterNo",meterSendVO.getsMonthlyMeterNo()).query().size();
                    if(querySize == 0) {
                        meterSendDAO.create(meterSendVO);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public List<MonthlyMeterVO> findMeter(String locationID) {
        List<MonthlyMeterVO> meterList = null;
        monthlyMeterDAO = getDatabaseHelper().getMonthlyMeterVO();
        QueryBuilder<MonthlyMeterVO, Integer> queryBuilder = monthlyMeterDAO.queryBuilder();
        try {
            queryBuilder.where().eq(MonthlyMeterVO.LOCATIONID, locationID);
            meterList = queryBuilder.query();
            return meterList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return meterList;
    }

    public void storeMeters(Response<ArrayList<MonthlyMeterVO>> response) {
        monthlyMeterDAO = getDatabaseHelper().getMonthlyMeterVO();
        try {
            if(response.body() != null) {
                TableUtils.clearTable(getDatabaseHelper().getConnectionSource(), MonthlyMeterVO.class);
                monthlyMeterDAO.create(response.body());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Request<String> ImageUpload(String url, final Map<String, String> infoMap) {
        Log.d("ImageUpload","Body");
        StringRequest request = new StringRequest(Request.Method.POST, url,
            new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("ImageUpload","onResponse");
                    imageMapDAO = getDatabaseHelper().getImageMapVO();
                    try {
                        ImageMapVO imageMapVO = new ImageMapVO(infoMap);
                        UpdateBuilder<ImageMapVO, Integer> imageUpdateBuilder = imageMapDAO.updateBuilder();
                        imageUpdateBuilder.where().eq("fileName",imageMapVO.getFileName()).and().eq("isOffline",false);
                        imageUpdateBuilder.updateColumnValue("isOffline",true);
                        imageUpdateBuilder.update();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ImageUpload","onErrorResponse");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.d("ImageUpload","getParams");
                imageMapDAO = getDatabaseHelper().getImageMapVO();
                ImageMapVO imageMapVO = new ImageMapVO();
                imageMapVO.setFileName(infoMap.get("fileName"));
                imageMapVO.setMeterNo(infoMap.get("meterNo"));
                imageMapVO.setMonthlyMeterNo(infoMap.get("monthlyMeterNo"));
                imageMapVO.setReadingNo(infoMap.get("readingNo"));
                imageMapVO.setTerminalID(infoMap.get("terminalID"));
                imageMapVO.setFileContents(infoMap.get("fileContents"));
                imageMapVO.setCheckResultID(infoMap.get("checkResultID"));
                try {
                    imageMapDAO.create(imageMapVO);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return infoMap;
            }};
        request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    public void getImageVO(String id) {
        buildRetrofit().getImageReading(id).enqueue(new Callback<ArrayList<Image2VO>>() {
            @Override
            public void onResponse(Call<ArrayList<Image2VO>> call, Response<ArrayList<Image2VO>> response) {
                if(response.body() != null && response.body().size() > 0) {
                    imageDAO = getDatabaseHelper().getImage2VO();

                    try {
                        if(getDatabaseHelper().isOpen()) {
                            TableUtils.clearTable(imageDAO.getConnectionSource(), Image2VO.class);
                            imageDAO.create(response.body().get(0));
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Image2VO>> call, Throwable t) {
            }
        });
    }

    public Dao<ImageMapVO, Integer> getImageMapDAO() {
        return imageMapDAO;
    }

    public Dao<MeterSendVO, Integer> getMeterSendDAO() {
        return meterSendDAO;
    }

    public Dao<ImageSendDataVO, Integer> getImageSendDAO() {
        return imageSendDAO;
    }

    public Dao<ScheduleListSendVO, Integer> getScheduleListSendDAO() {
        return scheduleListSendDAO;
    }

    public Dao<MonthlyMeterVO, Integer> getMonthlyMeterDAO() {
        return monthlyMeterDAO;
    }

    public Dao<ScheduleList2VO, Integer> getScheduleListDAO(){return scheduleListDAO;}

    public WebServiceClient buildRetrofit(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppsConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();


        WebServiceClient webServiceClient = retrofit.create(WebServiceClient.class);

        return webServiceClient;
    }

    public WebServiceClient buildRetrofit(String url){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();


        WebServiceClient webServiceClient = retrofit.create(WebServiceClient.class);

        return webServiceClient;
    }

    public String jsonObjectToString(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        String string ="";
        try {
            string = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException jpe) {

        }
        return string;
    }


    public String gsonObjectToString(MeterSendVO meterSendVO) {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        return gson.toJson(meterSendVO);
    }

    private DatabaseHelper getDatabaseHelper() {
        if(databaseHelper == null) {
            databaseHelper = DatabaseHelper.getHelper(activity);
        }
        return databaseHelper;
    }

    public void closeDatabaseHelper(){
        if(databaseHelper != null) {
            databaseHelper.close();
            databaseHelper = null;
        }
    }
}
