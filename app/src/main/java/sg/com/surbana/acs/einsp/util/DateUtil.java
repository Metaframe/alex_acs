package sg.com.surbana.acs.einsp.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.util.Log;

public class DateUtil {
	private final String TAG = "DateUtil";
	public static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm:ss";
	public static final String DATEFORMAT_DD_MON_YYYY = "dd-MMM-yyyy";
	public static final String DATEFORMAT_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String DATEFORMAT_DD_MMM_YYYY = "dd MMMMM yyyy";

	public Timestamp dateString2Calendar(String s) throws ParseException {
		// Log.v(TAG, "Convert to timestamp " + s);
		Timestamp ts = null;
		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date d1;
		try {
			d1 = df.parse(s);
			ts = new Timestamp(d1.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in timestamp conversion " + s, e);
			throw e;
		}
		return ts;
	}

	public Timestamp dateString2Calendar(String s, String dateFormat) throws ParseException {
		// Log.v(TAG, "Convert to timestamp " + s);
		Timestamp ts = null;
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		Date d1;
		try {
			d1 = df.parse(s);
			ts = new Timestamp(d1.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in timestamp conversion " + s, e);
			throw e;
		}
		return ts;
	}

	public String timeStamp2dateString(Timestamp ts) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_DD_MON_YYYY);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}

	public String timeStamp2dateString(Timestamp ts, String format) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}

	public String timeStampLongValue2DateString(long tsValue) {
		String timeStampString = null;
		Date dt = new Date(tsValue);
		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		timeStampString = df.format(dt);
		return timeStampString;

	}

	public String timeStamp2dateString(Calendar c) {
		// Log.v(TAG, "Convert to timestamp " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_DD_MON_YYYY);
		Date d1;
		d1 = (Date) c.getTime();
		return df.format(d1);
	}

	public Timestamp getCurrentTimeStamp() {
		// Log.v(TAG, "get current timestamp");
		Timestamp ts = null;
		Date d1 = new Date();
		ts = new Timestamp(d1.getTime());
		return ts;

	}

	public Date getCurrentDate() {
		// Log.v(TAG, "get current Date");
		Timestamp ts = null;
		Date d1 = new Date();
		return d1;
	}

	public Timestamp ConvertDateToTimestamp(Date dt) {
		// Log.v(TAG, "get current timestamp");
		Timestamp ts = null;
		// if the dt is <1900, don't set the timestamp, somehow the rs.getDate
		// will still return value even if it is null in the DB
		if (dt != null) {
			if (dt.getYear() > 100)
				ts = new Timestamp(dt.getTime());
		}

		return ts;
	}

	public String timeStamp2dateString2(Timestamp ts) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}

	public String timeStamp2dateStringFullMonth(Timestamp ts) {
		// Log.v(TAG, "Convert to timestamp " + ts);
		SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_DD_MMM_YYYY);
		Date d1;
		d1 = (Date) ts.clone();
		return df.format(d1);
	}

	public Timestamp addDays(Timestamp ts, int intday) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Calendar c = Calendar.getInstance();
		c.setTime(ts);
		c.add(Calendar.DATE, intday); // number of days to add
		// String dt = sdf.format(); // dt is now the new date

		return ConvertDateToTimestamp(c.getTime());
	}

	// juehua 20120327 for airside module
	public String formatDate(Date date) {
		DateFormat formatter;
		formatter = new SimpleDateFormat("dd-MMM-yy");
		// try {
		// date = (Date) formatter.parse("11-June-07");
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		String s = null;
		if (date != null) {
			s = formatter.format(date);
		} else {
			s = "";
		}
		return s;
	}

	// llm for incident report
	public String formatDate(Date date, String dateformat) {
		DateFormat formatter;
		formatter = new SimpleDateFormat(dateformat);

		String s = null;
		if (date != null) {
			s = formatter.format(date);
		} else {
			s = "";
		}
		return s;
	}

	// juehua correct daysdiff
	public int daysDiff(Date laterDate, Date earlierDate) {
		if (earlierDate == null || laterDate == null)
			return 0;
		return (int) ((laterDate.getTime() / (1000 * 60 * 60 * 24)) - (earlierDate.getTime() / (1000 * 60 * 60 * 24)));
	}

	public long daysDiff2(Date start, Date end) {

		// reset all hours mins and secs to zero on start date
		Calendar startCal = GregorianCalendar.getInstance();
		startCal.setTime(start);
		startCal.set(Calendar.HOUR_OF_DAY, 0);
		startCal.set(Calendar.MINUTE, 0);
		startCal.set(Calendar.SECOND, 0);
		long startTime = startCal.getTimeInMillis();

		// reset all hours mins and secs to zero on end date
		Calendar endCal = GregorianCalendar.getInstance();
		endCal.setTime(end);
		endCal.set(Calendar.HOUR_OF_DAY, 0);
		endCal.set(Calendar.MINUTE, 0);
		endCal.set(Calendar.SECOND, 0);
		long endTime = endCal.getTimeInMillis();

		return (startTime) / (1000 * 60 * 60 * 24) - (endTime) / (1000 * 60 * 60 * 24);
	}
}
