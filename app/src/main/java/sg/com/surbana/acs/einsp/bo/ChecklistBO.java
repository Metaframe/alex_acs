package sg.com.surbana.acs.einsp.bo;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sg.com.surbana.acs.einsp.dao.ChecklistDAO;
import sg.com.surbana.acs.einsp.dao.ScheduleDAO;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.vo.ChecklistListVO;
import sg.com.surbana.acs.einsp.vo.ChecklistResultVO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import sg.com.surbana.acs.einsp.vo.TimeCheckVO;
import android.content.Context;
import android.util.Log;

import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 14, 2011		Ryan			Created
 *26 Jul 2011		Ryan			Added code to cater for M&E fault for temporary
 *26 Jul 2011		Ryan			Validate revised date before capturing start date
 *27 Jul 2011		Ryan			Added function to get schedule by result id
 *13 Sep 2011		Ryan			Insert record to service result table and sycn to sql server *
 */
public class ChecklistBO {
	private final String TAG = "ChecklistBO.java";
	private Context context;

public ChecklistBO(Context con) {
		context = con;
	}

public ScheduleListVO updateServiceHistoryForJobStart(int checklistResultId, Timestamp actualStart) throws ULjException {
		ScheduleDAO scheduleDao = new ScheduleDAO();
		ScheduleListVO scheduleVo = new ScheduleListVO();
		scheduleVo.setActualStart(actualStart);
		scheduleVo.setStatusId(AppsConstant.CHECKLIST_STATUS_IN_PROGRESS);
		scheduleVo.setChecklistResultId(checklistResultId);
		scheduleVo.setLastModifiedDate(actualStart); // since actual start is
														// based on current
														// sysdate, hence =
														// lastmodifieddate

		//scheduleDao.updateScheduleTime(scheduleVo, AppsConstant.CHECKLIST_UPDATE_STARTTIME);
		return scheduleVo;
	}

	//Ryan-20120712 : additional check on inspection start time
	public boolean validateInspectionTime(int serviceId){
		boolean canStartInspection=true;
		DisplayUtil dispUtil = new DisplayUtil();

		ScheduleDAO scheduleDao = new ScheduleDAO();
		DateUtil dUtil = new DateUtil();
		Date currentDate =dUtil.getCurrentDate();
		String alertMsg="";
		try {
			List <TimeCheckVO> timeList = scheduleDao.searchAllowedTime(serviceId);
			
			alertMsg="This inspection can only be performed at following time slot ";
			if (timeList.size()>0){
				canStartInspection=false;
				for(TimeCheckVO vo : timeList){
					System.out.println(vo.getTimeStart() + "-" + vo.getTimeEnd());
					alertMsg+= "\n - " + vo.getTimeStart().getHours()+":" + vo.getTimeStart().getMinutes() +"  to  " + vo.getTimeEnd().getHours()+":"  + vo.getTimeEnd().getMinutes() ;
					if(currentDate.after(vo.getTimeStart()) && currentDate.before(vo.getTimeEnd())){
						canStartInspection=true;
						break;
					}
				}
			}
			
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!canStartInspection){
			dispUtil.displayAlertDialog(context, AppsConstant.MESSAGE_VALIDATION, alertMsg);

		}
		
//		canStartInspection=true;
		return canStartInspection;
		
	}
	
	
	
	public void completeCheck(int resultId, Timestamp endTime, String remarks, Timestamp currentTime) throws ULjException {

		// update service history
		ScheduleDAO scheduleDao = new ScheduleDAO();
		ScheduleListVO scheduleVo = new ScheduleListVO();
		scheduleVo.setActualEnd(endTime);
		scheduleVo.setStatusId(AppsConstant.CHECKLIST_STATUS_COMPLETED);
		scheduleVo.setRemarks(remarks);
		scheduleVo.setChecklistResultId(resultId);
		scheduleVo.setLastModifiedDate(currentTime); // since actual start is
														// based on current
														// sysdate, hence =
														// lastmodifieddate

		scheduleDao.updateScheduleTime(scheduleVo, AppsConstant.CHECKLIST_UPDATE_ENDTIME);

		// TODO: Add code to update the result tables and sync to server
		// insert to tblServiceChecklistResult, based on the record in the
		// checklist records with fault
		// Ryan : 20110913 : Insert record to service result table and sycn to
		// sql server
		insertFaultyServiceResult(resultId);
	}

	public void completeCheckCancel(int resultId, Timestamp endTime, String remarks, Timestamp currentTime) throws ULjException {

		// update service history
		ScheduleDAO scheduleDao = new ScheduleDAO();
		ScheduleListVO scheduleVo = new ScheduleListVO();
		// scheduleVo.setActualEnd(endTime);
		scheduleVo.setStatusId(AppsConstant.CHECKLIST_RESULT_STATUS_CANCEL);
		scheduleVo.setRemarks(remarks);
		scheduleVo.setChecklistResultId(resultId);
		scheduleVo.setLastModifiedDate(currentTime); // since actual start is
														// based on current
														// sysdate, hence =
														// lastmodifieddate

		scheduleDao.updateScheduleTime(scheduleVo, AppsConstant.CHECKLIST_RESULT_STATUS_CANCEL);

		// TODO: Add code to update the result tables and sync to server
		// insert to tblServiceChecklistResult, based on the record in the
		// checklist records with fault
		// Ryan : 20110913 : Insert record to service result table and sycn to
		// sql server
		insertFaultyServiceResult(resultId);
	}

	private void insertFaultyServiceResult(int resultId) {
		// Ryan : 20110913 : Insert record to service result table and sycn to
		// sql server
		// Retrieve list of checklist item with fault
		// for each of those create empty record
		ChecklistDAO dao = new ChecklistDAO();
		ArrayList<ChecklistResultVO> faultResults = dao.selectChecklistResultWithFault(resultId);

		try {
			for (ChecklistResultVO vo : faultResults) {
				dao.insertFaultyChecklistResult(vo);
			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ScheduleListVO getScheduleByResultId(int resultId) throws ULjException, ParseException {
		ScheduleDAO scheduleDao = new ScheduleDAO();
		ScheduleListVO vo;

		try {
			vo = scheduleDao.searchScheduleByChecklistResultId(resultId);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in retrieving Schedule By Criteria", e);
			e.printStackTrace();
			throw e;
		}
		return vo;
	}

	
}
