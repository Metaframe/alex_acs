package sg.com.surbana.acs.einsp.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ALRED on 4/18/2017.
 */

public class ImageTestBodyVO {
    @SerializedName("docbinaryarray")
    @Expose
    private byte[] docbinaryarray;
    @SerializedName("docname")
    @Expose
    private String docname;

    public byte[] getDocbinaryarray() {
        return docbinaryarray;
    }

    public void setDocbinaryarray(byte[] docbinaryarray) {
        this.docbinaryarray = docbinaryarray;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }
}
