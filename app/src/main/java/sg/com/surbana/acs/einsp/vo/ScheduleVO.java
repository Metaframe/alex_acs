package sg.com.surbana.acs.einsp.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 13, 2011		Ryan			Created
 *
 */
public class ScheduleVO implements Serializable {
	private static final long serialVersionUID = 5079427102239183660L;

	private final String TAG = "ScheduleVO";

	int serviceMainProgId;
	int serviceId;
	Timestamp scheduledStart;
	Timestamp revisedStart;
	Timestamp actualStart;
	Timestamp actualEnd;
	String personInChargeId;
	String remarks;
	int checklistTemplateId;
	int checklistTemplateVersion;
	int checklistResultId;
	int buildingId;
	String serviceDescription;
	int statusId;
	Timestamp lastModifiedDate;
	int frequencyId;

	public int getServiceMainProgId() {
		return serviceMainProgId;
	}

	public void setServiceMainProgId(int serviceMainProgId) {
		this.serviceMainProgId = serviceMainProgId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public Timestamp getScheduledStart() {
		return scheduledStart;
	}

	public void setScheduledStart(Timestamp scheduledStart) {
		this.scheduledStart = scheduledStart;
	}

	public Timestamp getRevisedStart() {
		return revisedStart;
	}

	public void setRevisedStart(Timestamp revisedStart) {
		this.revisedStart = revisedStart;
	}

	public Timestamp getActualStart() {
		return actualStart;
	}

	public void setActualStart(Timestamp actualStart) {
		this.actualStart = actualStart;
	}

	public Timestamp getActualEnd() {
		return actualEnd;
	}

	public void setActualEnd(Timestamp actualEnd) {
		this.actualEnd = actualEnd;
	}

	public String getPersonInChargeId() {
		return personInChargeId;
	}

	public void setPersonInChargeId(String personInChargeId) {
		this.personInChargeId = personInChargeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getChecklistTemplateId() {
		return checklistTemplateId;
	}

	public void setChecklistTemplateId(int checklistTemplateId) {
		this.checklistTemplateId = checklistTemplateId;
	}

	public int getChecklistTemplateVersion() {
		return checklistTemplateVersion;
	}

	public void setChecklistTemplateVersion(int checklistTemplatVersion) {
		this.checklistTemplateVersion = checklistTemplatVersion;
	}

	public int getChecklistResultId() {
		return checklistResultId;
	}

	public void setChecklistResultId(int checklistResultId) {
		this.checklistResultId = checklistResultId;
	}

	public int getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getFrequencyId() {
		return frequencyId;
	}

	public void setFrequencyId(int frequencyId) {
		this.frequencyId = frequencyId;
	}

	public String getTAG() {
		return TAG;
	}

}
