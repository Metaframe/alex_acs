package sg.com.surbana.acs.einsp.vo;

import java.io.Serializable;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 *************************************************************** 
 *June 6, 2011		Ryan			Created
 *
 */
public class DropdownItemVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private final String TAG = "DropdownItemVO.java";

	private String itemDescription;
	private String itemId;

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public DropdownItemVO() {

	}

	public DropdownItemVO(String id, String desc) {
		itemDescription = desc;
		itemId = id;
	}

}
