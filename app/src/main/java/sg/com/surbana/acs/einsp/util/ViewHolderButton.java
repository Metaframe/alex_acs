package sg.com.surbana.acs.einsp.util;

import sg.com.surbana.acs.einsp.R;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 15, 2011		Ryan			Created
 *
 */
public class ViewHolderButton {
	private final String TAG = "ViewHolderToggleButton.java";
	public Button tButton = null;
	public Button counterButton = null;
	EditText txtRMK = null;
	Button btnCamera = null;
	public Button btnHistory = null;
	public int ref;

	public ViewHolderButton(View base) {
		this.tButton = (Button) base.findViewById(R.id.lstCondition);
		this.counterButton = (Button) base.findViewById(R.id.lstFaultCounter);
		this.txtRMK = (EditText) base.findViewById(R.id.lstRemarks);
//		this.btnCamera = (Button) base.findViewById(R.id.lstCamera);
//		this.btnHistory = (Button) base.findViewById(R.id.lstHistory);

	}
}
