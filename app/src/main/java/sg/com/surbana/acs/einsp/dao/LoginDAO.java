package sg.com.surbana.acs.einsp.dao;

import java.sql.Timestamp;

import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import android.util.Log;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

public class LoginDAO extends BaseDAO {
	private final String TAG = "LoginDAO.java";

	public void doInsertUser(String uid) throws ULjException {
		Connection conn = DbUtil.getConnection();

		StringBuffer query = new StringBuffer("Insert into ULIdentifyEmployee_nosync  ");
		query.append("(serialno, emp_id, lastsyncdate)");
		query.append(" values (?,?,?) ");
		PreparedStatement ps = null;
		DateUtil dutil = new DateUtil();
		try {
			ps = conn.prepareStatement(query.toString());

			int index = 1;
			ps.set(index++, 1);
			ps.set(index++, uid);
			ps.set(index++, dutil.getCurrentTimeStamp());
			ps.execute();
			conn.commit();
			Log.d("LoginDAO.java", "Fault created successfully");
		} catch (ULjException ejl) {
			Log.e("LoginDAO.java", "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}

	public void doUpdateUser(String uid) throws ULjException {
		Connection conn = DbUtil.getConnection();
		DateUtil dutil = new DateUtil();

		StringBuffer query = new StringBuffer("UPDATE ULIdentifyEmployee_nosync  set emp_id = ?, lastsyncdate = ? ");
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(query.toString());

			int index = 1;
			ps.set(index++, uid);
			ps.set(index++, dutil.getCurrentTimeStamp());

			ps.execute();
			conn.commit();
			Log.d("LoginDAO.java", "Updated User info successfully created successfully");
		} catch (ULjException ejl) {
			Log.e("LoginDAO.java", "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			closePreparedStatement(ps);
		}
	}

	public String doSelectUserId() throws ULjException {
		String query = "select upper(emp_id) from ULIdentifyEmployee_nosync";

		DbUtil dUtil = new DbUtil();
		String uId = dUtil.executeSingleValueQuery(query);

		return uId;
	}

	public String doSelectUserPassword(String uid) throws ULjException {
		String query = "select upper(Password) from tblUserInfo WHERE upper(userId)= upper('" + uid + "')";

		DbUtil dUtil = new DbUtil();
		String uPwd = dUtil.executeSingleValueQuery(query);

		return uPwd;
	}

	public Timestamp getSyncDate(String uid) throws ULjException {
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		Connection conn = DbUtil.getConnection();
		uid = uid.toUpperCase();
		bufQuery.append("select lastsyncdate from ULIdentifyEmployee_nosync  WHERE upper(emp_id)= ?");

		Timestamp syncDate = null;

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			ps.set(ind++, uid);

			rs = ps.executeQuery();
			while (rs.next()) {
				int columnIndex = 1;
				syncDate = (dUtil.ConvertDateToTimestamp(rs.getDate(columnIndex++)));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return syncDate;
	}

	// juehua for auth imei
	public String doSelectRegistedIMEI(String strIMEI) throws ULjException {
		String query = "select upper(deviceid) from tblRegisteredIMEI WHERE upper(imei)= upper('" + strIMEI + "')";

		DbUtil dUtil = new DbUtil();
		String deviceId = dUtil.executeSingleValueQuery(query);

		return deviceId;
	}

}