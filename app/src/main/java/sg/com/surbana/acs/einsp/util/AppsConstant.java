package sg.com.surbana.acs.einsp.util;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 8, 2011		Ryan			Created
 *05 Jul 2011		Ryan			Merge with LM's update
 *20 jul 2011		Ryan		    annotate picture integration
 */
public class AppsConstant {
	private final String TAG = "AppsConstant.java";

	public static String VERS_NEXT_FAULT_ID = "getNextFaultID 1.0";
	public static String VERS_NEXT_INCIDENT_ID = "getNextIncidentID 1.0";
	
	public static String VERS_NEXT_FAULT_ID1 = "getNextFaultID 1.1";
	public static String VERS_NEXT_FAULT_ID2 = "getNextFaultID 2.0";
	public static String VERS_NEXT_FAULT_ID3 = "getNextFaultID 3.0";
	public static String VERS_NEXT_FAULT_ID4 = "getNextFaultID 4.0";

	public static String VERS_NEXT_INCIDENT_ID1 = "getNextIncidentID 1.1";
	public static String VERS_NEXT_INCIDENT_ID2 = "getNextIncidentID 2.0";
	public static String VERS_NEXT_INCIDENT_ID3 = "getNextIncidentID 3.0";
	public static String VERS_NEXT_INCIDENT_ID4 = "getNextIncidentID 4.0";

	public static String VERS_LOGIN = "Login 1.2";

	public static String VERS_INIT = "Init Eng 1.2";//llm 05/05/2014
	public static String VERS_CHECK_LOGIN = "CheckLogin 1.2";
	public static String VERS_DOWNLOAD_Faults = "Download Eng Faults 1.2"; //2.0 for 23042014db llm 27/01/2014 1.7 for 26 aug db; 1.9 for stg db
	public static String VERS_DOWNLOAD_Checks = "Download Checks 1.3";
	public static String VERS_DOWNLOAD_IFS = "Download IFS 1.2";
	public static String VERS_UPDATE = "Update Eng Faults 1.2"; //2.0 for 23042014db llm 27/01/2014 1.7 for 26 aug db; 1.9 for stg db
	public static String VERS_UPDATE_HANDOVER = "update handover 1.1";
	public static String VERS_DOWNLOAD_HANDOVER = "downloadHandover 1.1";

	public static String VERS_UPDATE_IFS = "Update IFS 1.2";

	public static String VERS_UPDATE_TAKEOVER = "update takeover 1.0";
	public static String VERS_DOWNLOAD_TAKEOVER = "downloadTakeover 1.0";

	public static String VERS_UPDATE_TOILET = "update toilet 1.0";
	public static String VERS_DOWNLOAD_TOILET = "downloadToilet 1.0";

	public static String SUB_VERS_WATER_ON_OFF_UPLOAD = "pubWaterMeterOnOffUL";

	public static String SUB_VERS_FEEDBACK_UPLOAD = "pubFeedbackUL";
	public static String SUB_VERS_NEW_FAULT = "pubNewFault";
	public static String SUB_VERS_CHECKLIST_UPLOAD = "pubChecklistUL";
	public static String SUB_VERS_FAULT_UPLOAD = "pubFaultUL";
	public static String SUB_VERS_ATTENDANCE_UPLOAD = "pubAttendanceUL";

	public static String SUB_VERS_FAULT_DOWNLOAD = "pubFaultDL";
	public static String SUB_VERS_CHECKLIST_DOWNLOAD = "pubChecklistDL";
	public static String SUB_VERS_FEEDBACK_DOWNLOAD = "pubFeedbackDL";
	public static String SUB_VERS_INIT = "pubInit";
	public static String SUB_VERS_CHECKLOGIN = "pubCheckLogin";

	public static String SUB_VERS_HANDOVER_UPLOAD = "pubHandoverUpload";
	public static String SUB_VERS_HANDOVER_DOWNLOAD = "pubHandoverDownload";

	public static String SUB_VERS_TAKEOVER_UPLOAD = "pubTakeoverUpload";
	public static String SUB_VERS_TAKEOVER_DOWNLOAD = "pubTakeoverDownload";

	public static String SUB_VERS_TOILET_CHECKLIST_DOWNLOAD = "pubToiletDL";
	public static String SUB_VERS_TOILET_CHECKLIST_UPLOAD = "pubToiletUL";

	public static String SUB_VERS_NEW_INCIDENT = "pubNewIncident";

	public static String TEXT_NO_RECORDS_FOUNT = "NO RECORDS FOUND";

	public static String VERS_UPDATE_WATER_ON_OFF = "update watermeter 1.0";
	public static String VERS_UPDATE_COMPETENCY = "update competency 1.0";
	public static String SUB_VERS_COMPETENCY_UPLOAD = "pubCompetencyUL";

	// a.ShowChecklist(resultID, templateID, templateVersion, buildingID,
	// scannedBarcode)
	public static String INTENT_DATA_RESULT_ID = "resultId";
	public static String INTENT_DATA_TEMPLATE_ID = "templateId";
	public static String INTENT_DATA_TEMPLATE_VERS = "templateVersion";
	public static String INTENT_DATA_BUILDING_ID = "buildingId";
	public static String INTENT_DATA_HANDOVER_NO = "handoverNo";
	public static String INTENT_DATA_TAKEOVER_NO = "takeoverNo";
	public static String INTENT_DATA_SCANNED_BARCODE = "scannedBarcode";
	public static String INTENT_DATA_FAULT_ID = "faultId";
	public static String INTENT_DATA_LOCATION_ID = "locationId";
	public static String INTENT_DATA_IMAGE = "image";
	public static String INTENT_DATA_IS_TENANT = "tenant";
	public static String INTENT_DATA_SCHEDULE = "schedule";

	public static String INTENT_DATA_CHECKLISTNAME = "checkListName";

	public static String INTENT_DATA_GROUP_ID = "groupId";
	public static String INTENT_DATA_ITEM_ID = "itemId";
	public static String INTENT_DATA_SERVICE_RESULT_ID = "serviceResultId";
	public static String INTENT_DATA_DROPDOWNGROUP_ID = "dropDownGroupId";

	public static String HASHMAP_KEY_STATUS_ID = "statusId";
	public static String HASHMAP_KEY_FAULT_CONTR_STATUS_ID = "FaultContrstatusId";
	public static String HASHMAP_KEY_REVISED_DATE = "revicedDate";
	public static String HASHMAP_KEY_ACTUAL_START = "actualStart";
	public static String HASHMAP_KEY_ACTUAL_END = "actualEnd";
	public static String HASHMAP_KEY_DROPDOWNITEM_ID = "dropDownItemId";
	public static String HASHMAP_KEY_DROPDOWNITEM_DESC = "dropDownItemDesc";
	public static String HASHMAP_KEY_DROPDOWNITEM_SHORT_DESC = "dropDownItemShortDesc";
	public static String HASHMAP_KEY_REMARKS = "remarks";
	public static String HASHMAP_KEY_RESULT_ID = "resultId";
	public static String HASHMAP_KEY_GROUP_ID = "groupId";
	public static String HASHMAP_KEY_ITEM_ID = "itemId";
	public static String HASHMAP_KEY_FAULT_ID = "faultId";
	public static String HASHMAP_KEY_FAULT_IMAGE = "faultImg";

	public static String HASHMAP_KEY_TASK_ID = "taskId";
	public static String HASHMAP_KEY_TASK_ORDER = "taskOrder";

	public static String HASHMAP_KEY_NUMBER_OF_FAULT = "numOfFault";

	public static String HASHMAP_KEY_CHECKLIST_ITEM_CONDITION = "condition";

	public static int CHECKLIST_RESULT_STATUS_OK_Y = 1;
	public static int CHECKLIST_RESULT_STATUS_OK_N = 2;
	public static int CHECKLIST_RESULT_STATUS_NOT_EXISTS = 0;
	public static int CHECKLIST_RESULT_STATUS_CANCEL = 3;

	public static int CHECKLIST_STATUS_IN_PROGRESS = 4;
	public static int CHECKLIST_STATUS_OUTSTANDING = 2;
	public static int CHECKLIST_STATUS_COMPLETED = 1;
	public static int CHECKLIST_STATUS_TO_INSPECT= 5;
	public static int TOILET_CHECKLIST_STATUS_OUTSTANDING = 2;

	public static int CHECKLIST_UPDATE_STARTTIME = 1;
	public static int CHECKLIST_UPDATE_ENDTIME = 2;
	public static int CHECKLIST_UPDATE_CANCEL = 3;
	
	public static int CHECKLIST_UPDATE_WITNESSNACK = 4;

	public static String MESSAGE_VALIDATION = "Validation";
	public static String MESSAGE_JOB_START_DATE_REQUIRED = "Job has not been Started.";
	public static String MESSAGE_JOB_START_DATE_EXISTS = "Job Start has already been captured.";
	public static String MESSAGE_JOB_NOT_SCHEDULED_FOR_TODAY = "Check is not scheduled for today.";
	public static String MESSAGE_JOB_END_DATE_EXISTS = "Job End has already been captured.";
	public static String MESSAGE_JOB_IS_PAST_CHECK = "Past Check Cannot Be Done.";
	public static String MESSAGE_RECORD_SAVED_SUCCESSFULLY = "Record saved successfully, Initiating backend synchronization process";
	public static String MESSAGE_RECORD_PROCEED_TO_METER_ON_SCREEN = "Record saved successfully, Do you want to proceed with water meter turn on form";
	public static String MESSAGE_RECORD_PROCEED_TO_METER_OFF_SCREEN = "Record saved successfully, Do you want to proceed with water meter turn off form";
	public static String MESSAGE_RECORD_PROCEED_TO_CHECKLIST_VIEW_SCREEN = "Do you want to start the inspection now?"; // chin
																														// 2012)
	public static String HANDLER_KEY_ERROR = "error";
	public static String HANDLER_KEY_STATUS = "status";
	public static int HANDLER_VALUE_STATUS_OK = 1;
	public static int HANDLER_VALUE_STATUS_ERROR = -1;

	public static String INTENT_DATA_FAULT_TYPE = "faultType";
	public static String HASHMAP_KEY_CHECKLIST_VALUES = "checklistValues";
	public static String HASHMAP_KEY_CHECKLIST_STATUSES = "checklistStatuss";
	public static String HASHMAP_KEY_CHECKLIST_VALUES_POSITION = "checklistValuesPosition";
	public static String HASHMAP_KEY_CHECKLIST_STATUS_POSITION = "checklistStatusPosition";
	public static String HASHMAP_KEY_CHECKLIST_REMARKS = "checklistRemarks";
	public static String HASHMAP_KEY_CHECKLIST_QUANTITY = "checklistQuantity";
	public static String INTENT_DATA_METER_ON_OFF = "meterOnOff";
	public static String INTENT_DATA_LOCATION_NAME = "locationName";
	public static String INTENT_DATA_TENANT_ID = "tenantId";
	public static String INTENT_DATA_TENANT_NAME = "tenantName";
	public static String INTENT_DATA_UNIT_NO = "unitNo";
	public static int INTEGER_YES = 1;
	public static int INTEGER_NO = 0;

	public static int HANDOVER_TAKEOVER_STATUS_PENDING = 0;
	public static int HANDOVER_TAKEOVER_STATUS_COMPLETED = 1;

	public static int IFS_STATUS_OUTSTANDING = 1;
	public static int IFS_STATUS_ACKNOWLEDGE = 2;
	public static int IFS_STATUS_COMPLETED = 3;

	public static int COMPETENCY_RESULT_COMPETENT = 1;
	public static int COMPETENCY_RESULT_NOT_COMPETENT = 0;

	public static String COMPETENCY_SIGNATURE_ASSESSED_PREFIX = "ASS_";
	public static String COMPETENCY_SIGNATURE_ACKNOWLEDGED_PREFIX = "ACK_";

	public static String VERS_UPDATE_TENANT_NOTICE = "update tenant notice 1.1";
	public static String VERS_DOWNLOAD_TENANT_NOTICE = "downloadTenantNotice 1.1";
	public static String SUB_VERS_TENANT_NOTICE_UPLOAD = "pubTenantNoticeUL";
	public static String SUB_VERS_TENANT_NOTICE_DOWNLOAD = "pubTenantNoticeDL";

	public static String HASHMAP_KEY_TERMINAL_ID = "terminalId";
	public static String HASHMAP_KEY_NOTICE_ID = "noticeID";
	public static String INTENT_DATA_TERMINAL_ID = "terminalId";
	public static String INTENT_DATA_NOTICE_ID = "noticeID";
	public static String INTENT_DATA_REMARKS = "remarks";
	public static String INTENT_DATA_DESCRIPTION = "description";

	// public static String HASHMAP_KEY_IMG1= "img1";
	// public static String HASHMAP_KEY_IMG2= "img2";
	// public static String HASHMAP_KEY_IMG3= "img3";
	// public static String HASHMAP_KEY_IMG4= "img4";
	// public static String HASHMAP_KEY_IMG5= "img5";

	public static String HASHMAP_KEY_NOTICE_STATUS = "noticeStatus";
	public static String INTENT_DATA_NOTICE_STATUS = "noticeStatus";

	public static String SUB_VERS_NEW_TENANT_NOTICE = "pubNewTenantNotice";
	public static String VERS_NEXT_TENANT_NOTICE_ID = "getNextNoticeID 1.0";
	public static String VERS_NEXT_TENANT_NOTICE_ID_1 = "getNextNoticeID 1.0";
	public static String VERS_NEXT_TENANT_NOTICE_ID_2 = "getNextNoticeID 2.0";
	public static String VERS_NEXT_TENANT_NOTICE_ID_3 = "getNextNoticeID 3.0";
	public static String VERS_NEXT_TENANT_NOTICE_ID_4 = "getNextNoticeID 4.0";

	public static String INTENT_DATA_NOTICE_DATE = "noticeDate";
	public static String INTENT_DATA_NOTICE_INSP_ID = "noticeInspID";

	public static String INTENT_DATA_STATUS_ID = "statusID";
	public static String INTENT_DATA_FAULT_CONTR_STATUS_ID = "FaultContrstatusID";

	public static String MODULE_FAULT = "fault";
	public static String MODULE_COMPETENCY_CHECK = "competencycheck";
	public static String MODULE_HANDOVER = "handover";
	public static String MODULE_TAKEOVER = "takeover";
	public static String MODULE_WATER_METER = "watermeter";
	public static String MODULE_TENANT_NOTICE = "tenantnotice";
	public static String MODULE_MONTHLY_METER= "monthlymeter";

	public static String WEB_IMAGE_ROOT_URL = "http://58.145.229.141/Upload_Dir/";
	public static String WEB_URL_CONTEXT_PATH = "http://58.145.229.141/CAG_eIns";

	public static boolean DO_COMMIT_FALSE = false;
	public static boolean DO_COMMIT_TRUE = true;

	public static String INTENT_DATA_USERID = "userId";

	public static String CONTRACTOR_CAMPAIGN = "Campaign";
	public static String CONTRACTOR_SEKLOONG = "Sekloong";
	public static String CONTRACTOR_T2_BUILDING_CPG = "CPG";
	public static String CONTRACTOR_T1_BUILDING_CPGFM = "CPGFM";
	public static String CONTRACTOR_T1_CLEANING_CHYETHIAM = "Chye Thiam";
	public static String CONTRACTOR_T2_CLEANING_CHYETHIAM = "Chye Thiam";

	public static String CONFIG_IMAGE_WEB = "WEB_IMAGE_ROOT";
	public static String CONFIG_WEB_CONTEXT = "WEB_URL_CONTEXT_PATH";
	public static String CONFIG_APIES_WEBSERVICE_URL = "APIES_WEBSERVICE_URL";

	public static String CONFIG_UPDATE_APK_URL = "APP_UPDATE_APK_URL";
	public static String CONFIG_UPDATE_VERSION_URL = "APP_UPDATE_VERSION_URL";
	
	public static String ENG_CONFIG_UPDATE_APK_URL = "ENG_APP_UPDATE_APK_URL";
	public static String ENG_CONFIG_UPDATE_VERSION_URL = "ENG_APP_UPDATE_VERSION_URL";

	// public static String SUB_VERS_INCIDENT = "pubIncidentReportUL";
	// public static String VERS_UPDATE_INCIDENT = "update incident 1.0";

	public static String CONFIG_HANDOVER_HISTORY_URL = "TAKEOVER_HIST_URL";
	public static String CONFIG_TAKE_HISTORY_URL = "HANDOVER_HIST_URL";
	public static String CONFIG_EQUIPMENT_LIST_URL = "COM_CHECK_EQUIPMENT_LIST_IMG";
	public static String CONFIG_ASSOCIATE_ORGANIZATION = "ASSOCIATE_ORGANIZATION";

	public static String VERS_SYNC = "Sync 1.2";
	public static String SUB_VERS_SYNC = "pubSync";

	public static String CONFIG_TN_DAY = "TENANT_NOTICE_DAY_INTERVAL";

	public static int USER_GROUP_COMMERCIAL = 2;
	public static int USER_GROUP_AIRSIDE = 10;
	public static int USER_GROUP_ENGINEERING = 22;
	
	public static int FAULT_TYPE_COMMERCIAL = 3;

	public static int TERMINAL_CHECKLIST_SERVICE_ID = 6;
	public static String INTENT_DATA_SERVICEID = "serviceid";

	public static String VERS_UPDATE_PREFLIGHT = "update preflight 1.0";
	public static String VERS_DOWNLOAD_PREFLIGHT = "download preflight 1.0";
	public static String VERS_DOWNLOAD_PLB_CHECKLIST = "Download PLB Checks 1.0";
	public static String VERS_NEXT_PLB_FAULT_ID = "getNextPLBFaultID 1.0";

	public static String VERS_UPDATE_PLB_FAULT = "update plb fault 1.0";
	public static String VERS_DOWNLOAD_PLB_FAULT = "download plb fault 1.0";

	public static String SUB_VERS_DOWNLOAD_PLB_CHECKLIST = "pubPLBChecklistDL";
	public static String SUB_VERS_UPLOAD_PLB_CHECKLIST = "pubPLBChecklistUL";

	public static String SUB_VERS_DOWNLOAD_PREFLIGHT = "pubPreflightDL";
	public static String SUB_VERS_UPLOAD_PREFLIGHT = "pubPreflightUL";

	public static String SUB_VERS_NEW_PLB_FAULT = "pubNewPlbFault";

	public static String SUB_VERS_DOWNLOAD_PLB_FAULT = "pubPlbFaultDetailsDL";
	public static String SUB_VERS_UPLOAD_PLB_FAULT = "pubPlbFaultDetailsUL";

	public static String INTENT_DATA_PRECHECK_ID = "precheckId";
	public static String INTENT_DATA_PLB_ID = "plbId";

	public static int USER_GROUP_PLB = 21;
	//public static int PLB_FAULT_TYPE = 300010;
	//public static int PLB_ADGS_FAULT_TYPE = 300011;
	public static int PLB_LEAK_FAULTCAT = 9;

	public static String VERS_DOWNLOAD_MONTHLY_METER = "downloadMonthlyMeter 1.2";
	public static String VERS_UPDATE_MONTHLY_METER = "update monthly meter 1.2";
	public static String SUB_VERS_MONTHLY_METER_DOWNLOAD = "pubMonthlyMeterDL";
	public static String SUB_VERS_MONTHLY_METER_UPLOAD = "pubMonthlyMeterUL";

	public static String INTENT_DATA_TENANT_NO = "tenantNo";

	public static String VERS_NEXT_METER_READINGNO = "getNextMeterReadingNo 1.0";
	public static String INTENT_DATA_METER_NO = "meterNo";
	public static String INTENT_DATA_MONTHLY_METER_NO = "monthlymeterno";
	public static String VERS_NEXT_MONTHLY_METER_READINGNO = "getNextMonthlyMeterReadingNo 1.0";

	public static String SUB_VERS_NEW_MONTHLY_METER_READING = "pubNewMonthlyMeterReading";
	public static String INTENT_DATA_MONTHLY_METER_STATUS = "Status";
	public static String INTENT_DATA_MONTHLY_METER_READINGNO = "readingNo";

	public static String CONFIG_MM_DAY = "MONTHLY_METER_START_DATE";
	public static String CONFIG_MM_END_DAY = "MONTHLY_METER_END_DATE";
	
	public static String CONFIG_MM_CONSUMPTION_ALERT = "MONTHLY_METER_CONSUMPTION_ALERT";

	public static String VERS_DOWNLOAD_DASHBOARD = "downloadDashboard 4.0";
	public static String SUB_VERS_DASHBOARD_DOWNLOAD = "pubDashboardDL";

	public final static int USER_TYPE_CONTRACTOR_BUILDING = 3;
	public final static int USER_TYPE_CONTRACTOR_CLEANING = 4;
	public final static int USER_TYPE_CONTRACTOR_PEST_CONTROL = 5;
	public final static int USER_TYPE_CONTRACTOR_KOI_POND = 6;

	public final static int SCHEDULED_SERVICING_ID_CLEANING = 21;
	public final static int SCHEDULED_SERVICING_ID_KOIPOND = 22;

	public final static String FAULT_FOLLOWUP_TYPE_MINORWORK="MW";
	public static int USER_GROUP_EM = 1;
	

	public static String INTENT_DATA_ROUTINEWORK_ID = "routineWorkId";

	public final static int BARCODE_VERIFY_LOCATION_MATCHED_WITH_CORRECT_TAG=1;
	public final static int BARCODE_VERIFY_LOCATION_MATCHED_WITH_WRONG_TAG=2;
	public final static int BARCODE_VERIFY_LOCATION_MISMATCHED=0;
	public final static int BARCODE_VERIFY_UNKNOWN_LOCATION=-1;
	
	public static String VERS_UPDATE_ROUTINEWORK= "upload routinework 1.1";
	public static String VERS_DOWNLOAD_ROUTINEWORK = "download routinework 1.1";
	public static String SUB_VERS_ROUTINEWORK_UPLOAD = "pubRoutineWorkUL";
	public static String SUB_VERS_ROUTINEWORK_DOWNLOAD = "pubRoutineWorkDL";
	
	
	public static String MODULE_ROUTINEWORK= "routinework";
	public static String MODULE_ROUTINEWORK_RESULT= "routineworkresult";
	
	public static String VERS_DOWNLOAD_POWORKS = "downloadPOWorks 1.1";
	public static String SUB_VERS_POWORKS_DOWNLOAD = "pubPOWorksDL";
	public static String VERS_UPDATE_POWORKS = "update poworks 1.1";
	public static String SUB_VERS_POWORKS_UPLOAD = "pubPOWorksUL";

	public static String INTENT_DATA_LOCATIONDESC="locationdesc";
	public static String INTENT_DATA_POWORK_ID = "POWorkId";
	public static String INTENT_DATA_POWORK_RESULT_ID = "POWorkResultId";
	
	public static String MODULE_POWORK= "powork";
	public static String MODULE_POWORK_RESULT= "poworkresult";
	
	//llm 23/11/12 insert access log
	public static String VERS_UPLOAD_ACCESS_LOG = "update access log 1.0";
	public static String SUB_VERS_ACCESS_LOG_UPLOAD = "pubAccessLogUL";

	public static String INTENT_DATA_COMMERCIAL_SSH="CommercialSSH";
	public static String  INTENT_DATA_SERVICE_MAINT_PROG_DESC="ServiceMaintProgDesc";
	public static String  VERS_NEXT_CHECKLIST_ID="getNextChecklistID 1.1";
	public static String  SUB_VERS_NEW_CHECKLIST_RESULT_ID="pubNewChecklistResultID";
	

	public static int USER_GROUP_AES = 3;
	public static int USER_ROLE_AES_INSPECTOR = 46;
	public static int USER_ROLE_AES_IPPT_CONDUCTING_OFFICER = 49;
	public static int MODULE_AES_PLANNEDINSPECTION = 75;
	public static int MODULE_AES_HYDRANT = 82;
	public static int MODULE_AES_GATE = 83;
	public static int MODULE_AES_HOTWORKS = 84;
	public static int MODULE_AES_JOINTINSPECTION = 85;
	public static int AESCHECKLIST_RESULT_STATUS_NOT_EXISTS = 0;
	public static int AESCHECKLIST_RESULT_STATUS_C = 1;
	public static int AESCHECKLIST_RESULT_STATUS_NC = 2;
	public static int AESCHECKLIST_RESULT_STATUS_NA = 3;
	public static String HASHMAP_KEY_CHECKLIST_ITEM_REMARKS = "remarks";
	public static String HASHMAP_KEY_CHECKLIST_ITEMTYPE = "itemType";
	
	//public static String VERS_UPDATE_AES = "updateAes 1.0";

	public static String VERS_DOWNLOAD_OBS = "downloadAes obs 1.0";
	
	public static String VERS_UPDATE_OBS = "updateAes obs 1.0";
	public static String SUB_VERS_OBS = "pubAesObs";
	
	public static String SUB_VERS_OBS_UPLOAD = "pubAesObsUL";
	public static String SUB_VERS_OBS_DOWNLOAD = "pubAesObsDL";
	
	public static String VERS_UPDATE_HYDRANT = "updateAes hydrant 1.0";
	public static String VERS_DOWNLOAD_HYDRANT = "downloadAes hydrant 1.0";
	public static String SUB_VERS_HYDRANT_DOWNLOAD = "pubAesHydrantDL";

	public static String VERS_UPDATE_GATE = "updateAes gate 1.0";
	public static String VERS_DOWNLOAD_GATE = "downloadAes gate 1.0";
	public static String SUB_VERS_GATE_DOWNLOAD = "pubAesGateDL";	
	
	public static String VERS_UPDATE_HOTWORKS = "updateAes hotworks 1.0";
	public static String SUB_VERS_HOTWORKS = "pubAesHotworks";
	
	//public static String VERS_UPDATE_JOINTINSPECTION = "updateAes jointinspection 1.0";
	public static String VERS_UPDATE_JOINTINSPECTION = "updateAes jointinspection 1.1";
	public static String SUB_VERS_JOINTINSPECTION = "pubAesJointInspection";	
	
	public static String VERS_UPDATE_INCIDENTREPORT = "updateAes incidentreport 1.0";
	public static String SUB_VERS_INCIDENTREPORT = "pubAesIncidentReport";	
		
	public static String MODULE_FIRE_SAFETY= "firesafety";
	public static String VERS_UPDATE_SURVEYPATROL = "updateAes surveyPatrol 1.0";
	public static String VERS_DOWNLOAD_SURVEYPATROL = "downloadAes surveyPatrol 1.0";
	public static String SUB_VERS_SURVEYPATROL_UPLOAD = "pubAesPatrolSurveyUL";
	public static String SUB_VERS_SURVEYPATROL_DOWNLOAD = "pubAesPatrolSurveyDL";
	
	//IPPT
	public static String VERS_DOWNLOAD_IPPT = "DownloadAESIPPT 1.0"; //IPPT
	public static String VERS_DOWNLOAD_IPPT_1_1 = "DownloadAESIPPT 1.1"; //IPPT
	public static String VERS_UPLOAD_IPPT = "UploadAESIPPT 1.0"; //IPPT
	public static String VERS_UPLOAD_IPPT_1_1 = "UploadAESIPPT 1.1"; //IPPT
	public static String SUB_VERS_DOWNLOAD_IPPT_TESTDATES = "pubAesIpptSession"; //IPPT
	public static String SUB_VERS_DOWNLOAD_IPPT_INIT = "pubAesIpptInit"; //IPPT
	public static String SUB_VERS_DOWNLOAD_IPPT_GET_SESSIONID = "pubAesIPPTGetSessionID"; //IPPT
	public static String SUB_VERS_UPLOAD_IPPT_SESSION = "pubAesIPPTUploadSession"; //IPPT
	public static String SUB_VERS_UPLOAD_IPPT_ONGOING_SESSION = "pubAesIpptOngoingSession"; //IPPT
	public static String SUB_VERS_DOWNLOAD_IPPT_PARTICIPANT_PREV_RESULT = "pubAesIpptParticipantPrevResult"; //IPPT
	public static String SUB_VERS_DOWNLOAD_IPPT_OVERALL_PREV_RESULT = "pubAesIpptOverallPrevResult"; //IPPT
	
	public static String DIALOG_MESSAGE_CONFIRM_START_IPPT = "Confirm start a new IPPT session?"; //IPPT
	public static String DIALOG_MESSAGE_CONFIRM_COMPLETE_IPPT_2DOT4_RUN = "Confirm completion of 2.4Km run?"; //IPPT
	public static String DIALOG_MESSAGE_CANNOT_START_IPPT = "Current IPPT session is still on-going. Proceed with current session instead?"; //IPPT
	public static String DIALOG_MESSAGE_CANCEL_IPPT = "Confirm cancellation of IPPT session? All data will be lost"; //IPPT
	public static String DIALOG_MESSAGE_END_IPPT = "Confirm completion of IPPT session? All data will be uploaded to server and removed from handset"; //IPPT
	public static String DIALOG_MESSAGE_END_INCOMPLETE_IPPT = "Confirm completion of IPPT session when NOT ALL scores have been entered? All data will be uploaded to server and removed from handset"; //IPPT
	public static String DIALOG_MESSAGE_REMOVE_PARTICIPANT = "Confirm removal of participant from IPPT session? All data will be lost"; //IPPT
	public static String DIALOG_MESSAGE_RESTART_RUN = "Are you sure you want to restart/continue the Run?"; //IPPT
	public static String DIALOG_MESSAGE_RESET_RUN = "Are you sure you want to reset the Run?"; //IPPT
	public static String IPPT_SESSION_STATUS = "sessionstatus";
	public static String IPPT_SESSION_TESTDATE = "testdate";
	public static String IPPT_SESSION_TESTTIME = "testtime";
	public static String IPPT_SESSION_TESTLOCATION = "testlocation";
	public static String IPPT_NOT_AVAILABLE = "N.A";
	public static String IPPT_EXEMPTED = "EXEMPTED";
	public static String IPPT_NON_STATIONERY_DNS = "DNS";
	public static String IPPT_NON_STATIONERY_DNF = "DNF";
	public static String IPPT_BLANK = " ";
	public static int IPPT_TAG_VALUE_1 = 1;
	public static int IPPT_TAG_VALUE_2 = 2;
	public static int IPPT_TAG_VALUE_0 = 0;
	public static String IPPT_VALUE_TYPE_INT = "int";
	public static String IPPT_VALUE_TYPE_FLOAT = "float";
	public static String IPPT_SBJ_SCORE = "SBJScore";
	public static String IPPT_SITUP_SCORE = "SitUpScore";
	public static String IPPT_CHINUP_SCORE = "ChinUpScore";
	public static String IPPT_SHUTTLERUN_SCORE = "ShuttleRunScore";
	public static String IPPT_NON_STATIONERY_SCORE = "NonStationeryScore";
	public static int IPPT_EXEMPTED_SITUP = 1;
	public static int IPPT_RETEST_SITUP = 1;
	public static int IPPT_EXEMPTED_SBJ = 2;
	public static int IPPT_RETEST_SBJ = 2;
	public static int IPPT_EXEMPTED_CHINUP = 3;
	public static int IPPT_RETEST_CHINUP = 3;
	public static int IPPT_EXEMPTED_SHUTTLERUN = 4;
	public static int IPPT_RETEST_SHUTTLERUN = 4;
	public static int IPPT_EXEMPTED_RUN = 5;
	public static int IPPT_EXEMPTED_CYCLING = 6;
	public static int IPPT_EXEMPTED_WALK = 7;
	public static int IPPT_STATUS_FIT = 1;
	public static int IPPT_STATUS_UNFIT = 2;
	public static int IPPT_STATUS_CYCLING = 3;
	public static int IPPT_STATUS_WALK = 4;
	public static int IPPT_STATUS_RUN = 5;
	public static int IPPT_INITIAL_SCORE = -1;
	public static int IPPT_RETEST_SCORE = -99;
	public static int IPPT_NON_STATIONERY_TYPE_RUN = 1;
	public static int IPPT_NON_STATIONERY_TYPE_CYCLING = 2;
	public static int IPPT_NON_STATIONERY_TYPE_WALK = 3;
	public static int IPPT_NON_STATIONERY_SCORE_DNS = -77;
	public static int IPPT_NON_STATIONERY_SCORE_DNF = -88;
	public static String IPPT_WALK_GRADE_A = "K";
	public static String IPPT_WALK_GRADE_B = "L";
	public static String IPPT_WALK_GRADE_C = "M";
	public static String IPPT_WALK_GRADE_D = "N";
	public static String IPPT_WALK_GRADE_E = "O";
	public static int IPPT_AWARD_FAIL = 1;
	public static int IPPT_AWARD_PASS = 2;
	public static int IPPT_AWARD_SILVER = 3;
	public static int IPPT_AWARD_GOLD = 4;
	public static int IPPT_AWARD_MONETARY = 5;
	public static int IPPT_AWARD_RETEST = 6;
	public static String IPPT_GRADE_FAIL = "F";
	public static int IPPT_STATION_COMPLETED = 1;
	public static int IPPT_STATION_NA = 2;
	public static int IPPT_STATION_PENDING = 3;
	
	public static int IPPT_AWARD_WALK_NEED_IMPROVEMENT = 11;
	public static int IPPT_AWARD_WALK_FAIR = 12;
	public static int IPPT_AWARD_WALK_GOOD = 13;
	public static int IPPT_AWARD_WALK_VERY_GOOD = 14;
	public static int IPPT_AWARD_WALK_EXCELLENT = 15;
	
	public static String MESSAGE_TENANT_INFO_REQUIRED= "Please input tenant detail before completing the inspection.";

	//llm 12/04/2013 download commercial faults
	public static String VERS_DOWNLOAD_Commercial_Faults = "Download Commercial Faults 1.1";
	public static String SUB_VERS_COMMERCIAL_FAULT_DOWNLOAD = "pubCommercialFaultDL";
	public static String INTENT_DATA_IS_COMMERCIAL_FAULT = "isCommercialFault";
	
	//llm 03/05/2013 new table for monthly meter delay sync
	public static String HASHMAP_KEY_MONTHLY_METER_SYNCSTATUS = "MonthlyMeterSyncStatus";
	//llm 19/06/2013
	public static String INTENT_DATA_POWORKTYPEID = "poworktypeid";
	
	public static String VERS_DOWNLOAD_ENG_Checks = "Download Eng Checks 1.0";
	public static String VERS_UPLOAD_ENG_Checks = "Update Eng Checks 1.0";
	public static String SUB_VERS_ENG_CHECKLIST_DOWNLOAD = "pubEngCheckDL";
	public static String SUB_VERS_ENG_CHECKLIST_UPLOAD = "pubEngCheckUL";
	
	public static int ACMV_CHECKLIST_SERVICE_ID = 2200001;
	public static int ELECT_LCEC_CHECKLIST_SERVICE_ID = 2200002;
	public static int ELECT_UPS_CHECKLIST_SERVICE_ID = 2200003;
	public static int ELECT_BATTERY_CHECKLIST_SERVICE_ID = 2200004;
	public static int IBMS_CHECKLIST_SERVICE_ID = 2200005;
	public static int LIGHTING_CHECKLIST_SERVICE_ID = 2200006;
	public static int BAY_INDICATOR_CHECKLIST_SERVICE_ID = 2200007;
	public static int ENERGY_SAVING_CHECKLIST_SERVICE_ID = 2200008;
	public static int SMOKE_CURTAIN_CHECKLIST_SERVICE_ID = 2200009;
	public static int WATER_COOLER_CHECKLIST_SERVICE_ID = 2200010;
	//public static int FIRE_HYDRANT_PUMPING_CHECKLIST_SERVICE_ID = 2200011;
	public static int GEN1_CHECKLIST_SERVICE_ID = 2200013;
	public static int HT_BATTERY_CHECKLIST_SERVICE_ID = 2200016;
	public static int HT_GIS_PANEL3_HT_CHECKLIST_SERVICE_ID = 2200020;
	public static int HT_GIS_PANEL3_LT_CHECKLIST_SERVICE_ID = 2200021;
	public static int HT_GIS_PANEL3_GEN_CHECKLIST_SERVICE_ID = 2200022;
	public static int ENGINEERING_LOCATIONID_START =2200001 ;
	public static int PLB_ADGS_DAILY_CHECKLIST_SERVICE_ID = 2200025;
	public static int PLB_DAILY_CHECKLIST_SERVICE_ID = 2200026;
	public static int PLB_PARMETER_WEEKLY_CHECKLIST_SERVICE_ID = 2200027;
	public static int ELECT_CLOCK_CHECKLIST_SERVICE_ID = 2200028;
	public static int IBMS_EQ_CHECKLIST_SERVICE_ID = 2200029;
	public static int START_EQ_END_EQ_CHECKLIST_SERVICE_ID = 2200030;
	public static String INTENT_DATA_PLB_ADHOC_SSH="PLBADHOCSSH";
	
	public static String VERS_NEXT_CHECKLIST_RESULTID = "getNextPLBChecklistResultID 1.0";
	public static String SUB_VERS_NEW_CHECKLIST_RESULTID = "pubNewPLBChecklistResultID";

	public static final String BASE_URL = "http://213.136.66.251/";
	public static final String PORT_URL = "http://213.136.66.251:8028";
	public static final String URL_UPDATE_SIGN_IMAGE = "http://213.136.66.251:81/saveimage/UploadSignImage_tblUtilityMeterReadingHistory_upload_update.php";
	public static final String URL_INSERT_SIGN_IMAGE = "http://213.136.66.251:81/saveimage/UploadSignImage_tblMonthlyMeterImages_upload_insert_UtilityMeterReadingHistory.php";
	public static final String URL_METER_IMAGE = "http://213.136.66.251:81/saveimage/UploadImage_tblUtilityMeterReadingImages_upload_insert.php";

}
