package sg.com.surbana.acs.einsp.vo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ALRED on 5/1/2017.
 */

@DatabaseTable(tableName = "ImageMapVO")
public class ImageMapVO implements Serializable {
    @DatabaseField(id = true)
    private String fileName;
    @DatabaseField
    private String meterNo;
    @DatabaseField
    private String monthlyMeterNo;
    @DatabaseField
    private String readingNo;
    @DatabaseField
    private String terminalID;
    @DatabaseField
    private String fileContents;
    @DatabaseField
    private String checkResultID;

    @DatabaseField
    private boolean isOffline;

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    public ImageMapVO() {
        //donothing
    }

    public ImageMapVO(Map<String, String> map){
        this.fileName = map.get("fileName");
        this.meterNo = map.get("meterNo");
        this.monthlyMeterNo = map.get("monthlyMeterNo");
        this.readingNo = map.get("readingNo");
        this.terminalID = map.get("terminalID");
        this.fileContents = map.get("fileContents");
        this.checkResultID = map.get("checkResultID");
        this.isOffline = false;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public String getMonthlyMeterNo() {
        return monthlyMeterNo;
    }

    public void setMonthlyMeterNo(String monthlyMeterNo) {
        this.monthlyMeterNo = monthlyMeterNo;
    }

    public String getReadingNo() {
        return readingNo;
    }

    public void setReadingNo(String readingNo) {
        this.readingNo = readingNo;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getFileContents() {
        return fileContents;
    }

    public void setFileContents(String fileContents) {
        this.fileContents = fileContents;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getCheckResultID() {
        return checkResultID;
    }

    public void setCheckResultID(String checkResultID) {
        this.checkResultID = checkResultID;
    }

    public Map<String, String> imageToMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("fileName", fileName);
        map.put("meterNo", meterNo);
        map.put("monthlyMeterNo", monthlyMeterNo);
        map.put("readingNo", readingNo);
        map.put("terminalID", terminalID);
        map.put("fileContents", fileContents);
        map.put("checkResultID", checkResultID);
        return map;
    }


}
