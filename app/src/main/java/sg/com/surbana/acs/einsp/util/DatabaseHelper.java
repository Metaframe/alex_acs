package sg.com.surbana.acs.einsp.util;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import sg.com.surbana.acs.einsp.vo.Image2VO;
import sg.com.surbana.acs.einsp.vo.ImageMapVO;
import sg.com.surbana.acs.einsp.vo.ImageSendDataVO;
import sg.com.surbana.acs.einsp.vo.MeterReading2VO;
import sg.com.surbana.acs.einsp.vo.MeterSendVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleList2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleListSendVO;

/**
 * Created by ALRED on 4/12/2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "ACS_DB";
    private static final int DATABASE_VERSION = 1;

    private static DatabaseHelper helper = null;

    private Dao<ScheduleList2VO, Integer> scheduleListDAO = null;
    private Dao<MonthlyMeterVO, Integer> monthlyMeterDAO = null;
    private Dao<ScheduleListSendVO, Integer> scheduleListSendDAO = null;
    private Dao<Image2VO, Integer> imageDAO = null;
    private Dao<ImageSendDataVO, Integer> imageSendDAO = null;
    private Dao<MeterReading2VO, Integer> meterReadingDAO = null;
    private Dao<MeterSendVO, Integer> meterSendDAO = null;
    private Dao<ImageMapVO, Integer> imageMapDAO = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getHelper(Context context) {
        if (helper == null) {
            helper = new DatabaseHelper(context);
        }
        return helper;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try{
            TableUtils.createTableIfNotExists(connectionSource, ScheduleList2VO.class);
            TableUtils.createTableIfNotExists(connectionSource, MonthlyMeterVO.class);
            TableUtils.createTableIfNotExists(connectionSource, ScheduleListSendVO.class);
            TableUtils.createTableIfNotExists(connectionSource, Image2VO.class);
            TableUtils.createTableIfNotExists(connectionSource, ImageSendDataVO.class);
            TableUtils.createTableIfNotExists(connectionSource, MeterReading2VO.class);
            TableUtils.createTableIfNotExists(connectionSource, MeterSendVO.class);
            TableUtils.createTableIfNotExists(connectionSource, ImageMapVO.class);
        }catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public ConnectionSource getConnectionSource() {
        return super.getConnectionSource();
    }

    public Dao<ScheduleList2VO, Integer> getScheduleListDAO() {
        if (scheduleListDAO == null) {
            try {
                scheduleListDAO = getDao(ScheduleList2VO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return scheduleListDAO;
    }

    public Dao<MonthlyMeterVO, Integer> getMonthlyMeterVO() {
        if (monthlyMeterDAO == null) {
            try {
                monthlyMeterDAO = getDao(MonthlyMeterVO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return monthlyMeterDAO;
    }

    public Dao<ScheduleListSendVO, Integer> getScheduleListSendVO() {
        if (scheduleListSendDAO == null) {
            try {
                scheduleListSendDAO = getDao(ScheduleListSendVO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return scheduleListSendDAO;
    }

    public Dao<Image2VO, Integer> getImage2VO() {
        if (imageDAO == null) {
            try {
                imageDAO = getDao(Image2VO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return imageDAO;
    }

    public Dao<ImageSendDataVO, Integer> getImageSendDataVO() {
        if (imageSendDAO == null) {
            try {
                imageSendDAO = getDao(ImageSendDataVO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return imageSendDAO;
    }

    public Dao<MeterReading2VO, Integer> getMeterReading2VO() {
        if (meterReadingDAO == null) {
            try {
                meterReadingDAO = getDao(MeterReading2VO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return meterReadingDAO;
    }

    public Dao<MeterSendVO, Integer> getMeterSendVO() {
        if (meterSendDAO == null) {
            try {
                meterSendDAO = getDao(MeterSendVO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return meterSendDAO;
    }

    public Dao<ImageMapVO, Integer> getImageMapVO() {
        if (imageMapDAO == null) {
            try {
                imageMapDAO = getDao(ImageMapVO.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return imageMapDAO;
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            TableUtils.dropTable(connectionSource, ScheduleList2VO.class, true);
            TableUtils.dropTable(connectionSource, MonthlyMeterVO.class, true);
            TableUtils.dropTable(connectionSource, ScheduleListSendVO.class, true);
            TableUtils.dropTable(connectionSource, Image2VO.class, true);
            TableUtils.dropTable(connectionSource, ImageSendDataVO.class, true);
            TableUtils.dropTable(connectionSource, MeterReading2VO.class, true);
            TableUtils.dropTable(connectionSource, MeterSendVO.class, true);
            TableUtils.dropTable(connectionSource, ImageMapVO.class, true);
            onCreate(database, connectionSource);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        super.close();
        scheduleListDAO = null;
        monthlyMeterDAO = null;
        scheduleListSendDAO = null;
        imageDAO = null;
        imageSendDAO = null;
        meterReadingDAO = null;
        meterSendDAO = null;
        imageMapDAO = null;
        helper = null;
    }
}
