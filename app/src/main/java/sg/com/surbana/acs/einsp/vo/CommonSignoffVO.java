package sg.com.surbana.acs.einsp.vo;

import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Sep 7, 2011		Ryan			Created
 *
 */
public class CommonSignoffVO {
	private final String TAG = "CommonSignoffVO";
	String signoffNo;
	// party 1
	String partyOneName;
	String partyOneDesignation;
	String partyOneTelephone;
	String partyOneOrganization;
	Timestamp partyOneDate;
	byte[] partyOneSignature;

	String partyTwoName;
	String partyTwoDesignation;
	String partyTwoTelephone;
	String partyTwoOrganization;
	Timestamp partyTwoDate;
	byte[] partyTwoSignature;

	String refNo;
	String partyOneSignatureFileName;
	String partyTwoSignatureFileName;

	Timestamp lastUpdatedDate;
	int lastUpdatedBy;

	public String getSignoffNo() {
		return signoffNo;
	}

	public void setSignoffNo(String signoffNo) {
		this.signoffNo = signoffNo;
	}

	public String getPartyOneName() {
		return partyOneName;
	}

	public void setPartyOneName(String partyOneName) {
		this.partyOneName = partyOneName;
	}

	public String getPartyOneDesignation() {
		return partyOneDesignation;
	}

	public void setPartyOneDesignation(String partyOneDesignation) {
		this.partyOneDesignation = partyOneDesignation;
	}

	public String getPartyOneTelephone() {
		return partyOneTelephone;
	}

	public void setPartyOneTelephone(String partyOneTelephone) {
		this.partyOneTelephone = partyOneTelephone;
	}

	public String getPartyOneOrganization() {
		return partyOneOrganization;
	}

	public void setPartyOneOrganization(String partyOneOrganization) {
		this.partyOneOrganization = partyOneOrganization;
	}

	public Timestamp getPartyOneDate() {
		return partyOneDate;
	}

	public void setPartyOneDate(Timestamp partyOneDate) {
		this.partyOneDate = partyOneDate;
	}

	public byte[] getPartyOneSignature() {
		return partyOneSignature;
	}

	public void setPartyOneSignature(byte[] partyOneSignature) {
		this.partyOneSignature = partyOneSignature;
	}

	public String getPartyTwoName() {
		return partyTwoName;
	}

	public void setPartyTwoName(String partyTwoName) {
		this.partyTwoName = partyTwoName;
	}

	public String getPartyTwoDesignation() {
		return partyTwoDesignation;
	}

	public void setPartyTwoDesignation(String partyTwoDesignation) {
		this.partyTwoDesignation = partyTwoDesignation;
	}

	public String getPartyTwoTelephone() {
		return partyTwoTelephone;
	}

	public void setPartyTwoTelephone(String partyTwoTelephone) {
		this.partyTwoTelephone = partyTwoTelephone;
	}

	public String getPartyTwoOrganization() {
		return partyTwoOrganization;
	}

	public void setPartyTwoOrganization(String partyTwoOrganization) {
		this.partyTwoOrganization = partyTwoOrganization;
	}

	public Timestamp getPartyTwoDate() {
		return partyTwoDate;
	}

	public void setPartyTwoDate(Timestamp partyTwoDate) {
		this.partyTwoDate = partyTwoDate;
	}

	public byte[] getPartyTwoSignature() {
		return partyTwoSignature;
	}

	public void setPartyTwoSignature(byte[] partyTwoSignature) {
		this.partyTwoSignature = partyTwoSignature;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getPartyOneSignatureFileName() {
		return partyOneSignatureFileName;
	}

	public void setPartyOneSignatureFileName(String partyOneSignatureFileName) {
		this.partyOneSignatureFileName = partyOneSignatureFileName;
	}

	public String getPartyTwoSignatureFileName() {
		return partyTwoSignatureFileName;
	}

	public void setPartyTwoSignatureFileName(String partyTwoSignatureFileName) {
		this.partyTwoSignatureFileName = partyTwoSignatureFileName;
	}

	public Timestamp getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

}
