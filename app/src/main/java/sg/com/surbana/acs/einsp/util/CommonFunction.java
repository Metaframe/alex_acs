package sg.com.surbana.acs.einsp.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.dao.FaultDAO;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import sg.com.surbana.acs.einsp.vo.ServerIPVO;
import sg.com.surbana.acs.einsp.vo.UserInfoVO;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 16, 2011		Ryan			Created
 *
 */
public class CommonFunction {
	private final String TAG = "CommonFunction.java";

	public String getLocationCodeById(int locationId) throws ULjException {

		String locationCode;
		DbUtil db = new DbUtil();
		String query = "Select LocationCode from tblEngLocation where locationID= " + locationId;
		try {
			locationCode = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return locationCode;
	}

	public int[] getFaultStatusImageArray() {
		int[] statusImages = new int[] { 
				R.drawable.status_green_square, 	
				R.drawable.status_yellow_square,    
				R.drawable.status_red_square,		
				R.drawable.status_green_round, 		
				R.drawable.status_yellow_round, 
				R.drawable.status_green_half, 
				R.drawable.status_yellow_half, 
				R.drawable.status_red_green_square,//llm 02/10/2013 eng fault
				R.drawable.status_purple_square
				};

		return statusImages;
		//R.drawable.status_green_square_repeat//llm 02/10/2013 eng fault

	}

	public int[] getHTOverStatusArray() {
		int[] statusImages = new int[] { R.drawable.status_green_square, R.drawable.status_red_square, };

		return statusImages;
	}

	public int[] getFeedbackStatusImageArray() {
		int[] statusImages = new int[] { R.drawable.status_red_square, R.drawable.status_yellow_square, R.drawable.status_green_square

		};

		return statusImages;
	}

	public int[] getScheduleStatusImageArray() {
		int[] statusImages = new int[] { R.drawable.status_green_square, 
				R.drawable.status_yellow_square, R.drawable.status_red_square,
				R.drawable.status_purple_square,R.drawable.status_green_square_ack,
				R.drawable.status_yellow_square_ack,R.drawable.transparentbox};
		
//		int[] statusImages = new int[] { R.drawable.status_green_square, 
//				R.drawable.status_yellow_square, R.drawable.status_red_square,
//				R.drawable.status_purple_square,R.drawable.transparentbox};
				
		// R.drawable.status_purple_square
		return statusImages;
	}

	public int[] getPOScheduleStatusImageArray() {
		int[] statusImages = new int[] { R.drawable.status_green_square, R.drawable.status_yellow_square, R.drawable.status_red_square,
				R.drawable.status_purple_square, R.drawable.status_green_round, R.drawable.transparentbox };

		return statusImages;
	}

	public int getUserinfoID(String userId) throws ULjException {
		int id = 0;

		DbUtil db = new DbUtil();
		String query = "Select UserinfoID from tblUserInfo where UserID= '" + userId + "'";
		try {
			id = db.executeSingleIntValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return id;
	}
	
	public String getUserIDbyUserinfoid (int Userinfoid) throws ULjException {
		String id = "";

		DbUtil db = new DbUtil();
		String query = "Select userid from tblUserInfo where userinfoid= " + Userinfoid ;
		try {
			id = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return id;
	}


	public UserInfoVO getUserInfoById(String userId) throws ULjException {
		int id = 0;
		UserInfoVO userVO;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		// bufQuery.append("SELECT userId, userName, userRoleId, UserinfoId, ContractorId, staffHandphoneNo, staffOfficeNo, staffDesignation from tblUserInfo where UserID= '"+
		// userId +"'");

		bufQuery
				.append("SELECT userId, userName, userRoleId, a.UserinfoId, ContractorId, staffHandphoneNo, staffOfficeNo, staffDesignation,b.userTypeId ,c.userInfoTypeId from tblUserInfo a");
		bufQuery.append("	left join tblusergroup b ");
		bufQuery.append("	on a.userinfoid=b.userinfoId ");
		bufQuery.append("	left join tbluserinfotype c ");
		bufQuery.append("	on a.userinfoid=c.userinfoId ");
		bufQuery.append("	where UserID= '" + userId + "'");

		//
		// bufQuery.append("SELECT userId, userName, userRoleId, a.UserinfoId, ContractorId, staffHandphoneNo, staffOfficeNo, staffDesignation,b.userTypeId from tblUserInfo a"
		// );
		// bufQuery.append("	left join tblusergroup b " ) ;
		// bufQuery.append("	on a.userinfoid=b.userinfoId " ) ;
		// bufQuery.append("	where UserID= '"+ userId +"'" );

		try {
			ps = conn.prepareStatement(bufQuery.toString());

			rs = ps.executeQuery();
			userVO = new UserInfoVO();
			if (rs.next()) {
				userVO = new UserInfoVO();
				int columnIndex = 1;
				userVO.setUserId(rs.getString(columnIndex++));
				userVO.setUserName(rs.getString(columnIndex++));
				userVO.setUserRoleId(rs.getInt(columnIndex++));
				userVO.setUserInfoId(rs.getInt(columnIndex++));
				userVO.setContractorId(rs.getString(columnIndex++));
				userVO.setUserHandphoneNo(rs.getString(columnIndex++));
				userVO.setUserOfficeNo(rs.getString(columnIndex++));
				userVO.setUserDesignation(rs.getString(columnIndex++));
				userVO.setUserGroupId(rs.getInt(columnIndex++));
				userVO.setUserGroupTypeId(rs.getInt(columnIndex++));

				return userVO;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}
		return null;
	}

	public byte[] convertBitmapToByteArray(Bitmap bmp) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		if (bmp != null) {
			bmp.compress(Bitmap.CompressFormat.JPEG, 50, bos);
			byte[] bitmapdata = bos.toByteArray();
			return bitmapdata;
		} else
			return null;
	}

	public String getUniqueTimeStampWithDeviceId() {
		String sUniqueId;
		DateUtil dUtil = new DateUtil();
		sUniqueId = dUtil.getCurrentTimeStamp().toString().replace("-", "").replace(".", "").replace(":", "").replace(" ", "").trim() + main.strDeviceID;

		dUtil = null;

		return sUniqueId;
	}

	public LocationVO getLocIdbyLocCode(String locationcode) throws ULjException {

		LocationVO vo = new LocationVO();
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery
				.append("SELECT tbllevel.levelid,tblarea.areaid,tblzone.zoneid,tblEngLocation.locationid," +
						"tblEngLocation.location ,tblTerminal.Terminal, tblterminal.terminalid, tblEngLocation.locationcode  "
						+ " FROM tblEngLocation, tblArea, tblLevel ,tblZone, tblTerminal "
						+ " where "
						+ " tblEngLocation.LocationCode = ? "
						+ " and tblZone.ZoneID = tblEngLocation.ZoneID "
						+ " and tblZone.AreaID = tblArea.AreaID "
						+ " and tblLevel.TerminalId = tblTerminal.TerminalId " + " and tblArea.LevelID = tblLevel.LevelID ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			System.out.println(bufQuery.toString());
			System.out.println(locationcode);
			ps.set(ind++, locationcode);

			rs = ps.executeQuery();

			if (rs.next()) {
				int columnIndex = 1;
				vo.setLevelId(rs.getString(columnIndex++));
				vo.setAreaId(rs.getString(columnIndex++));
				vo.setZoneId(rs.getString(columnIndex++));
				vo.setLocationId(rs.getString(columnIndex++));
				vo.setLocation(rs.getString(columnIndex++));
				vo.setTerminal(rs.getString(columnIndex++));
				vo.setTerminalId(rs.getInt(columnIndex++));
				vo.setLocationcode(rs.getString(columnIndex++));
				System.out.println(vo.getLocation());
				System.out.println(vo.getTerminal());

			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		return vo;
	}

	public LocationVO getLocationVoByLocId(String locationId) throws ULjException {

		LocationVO vo = new LocationVO();
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery
				.append("SELECT tbllevel.levelid,tblarea.areaid,tblzone.zoneid,tblEngLocation.locationid,tblEngLocation.location ,tblTerminal.Terminal, tblTerminal.terminalid "
						+ " FROM tblEngLocation, tblArea, tblLevel ,tblZone, tblTerminal "
						+ " where "
						+ " tblEngLocation.LocationId= ? "
						+ " and tblZone.ZoneID = tblEngLocation.ZoneID "
						+ " and tblZone.AreaID = tblArea.AreaID "
						+ " and tblLevel.TerminalId = tblTerminal.TerminalId " + " and tblArea.LevelID = tblLevel.LevelID ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			System.out.println(bufQuery.toString());
			System.out.println(locationId);
			ps.set(ind++, locationId);

			rs = ps.executeQuery();

			if (rs.next()) {
				int columnIndex = 1;
				vo.setLevelId(rs.getString(columnIndex++));
				vo.setAreaId(rs.getString(columnIndex++));
				vo.setZoneId(rs.getString(columnIndex++));
				vo.setLocationId(rs.getString(columnIndex++));
				vo.setLocation(rs.getString(columnIndex++));
				vo.setTerminal(rs.getString(columnIndex++));
				vo.setTerminalId(rs.getInt(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		return vo;
	}

	public ArrayList<DropdownItemVO> getTenantListList() throws ULjException {
		ArrayList<DropdownItemVO> voList = new ArrayList();
		DropdownItemVO vo = new DropdownItemVO();
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("SELECT tenantId, tenantName  FROM tblTenant ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			rs = ps.executeQuery();

			vo = new DropdownItemVO();
			vo.setItemId("-1");
			vo.setItemDescription("");

			voList.add(vo);
			int cnt = 0;
			while (rs.next()) {
				vo = new DropdownItemVO();
				int columnIndex = 1;
				vo.setItemId(rs.getString(columnIndex++));
				vo.setItemDescription(rs.getString(columnIndex++));

				voList.add(vo);
				cnt = cnt + 1;
				Log.e(TAG, "LOADING TENATN LIST  count records : " + Integer.toString(cnt));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		return voList;
	}

	public void captureAttendanceResult(String id, int userInfoId, int locationId, Timestamp ts) throws ULjException {
		Log.v(TAG, "insert AttendanceResult - start");
		Connection conn = DbUtil.getConnection();
		StringBuffer query = new StringBuffer("Insert into tblAttendance ");
		query.append("(internalId, userInfoId, locationId, arrivalTime) ");
		query.append("values (?,?,?,? ) ");
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(query.toString());
			int index = 1;
			ps.set(index++, id);
			ps.set(index++, userInfoId);
			ps.set(index++, locationId);
			ps.set(index++, ts);
			ps.execute();
			conn.commit();
			Log.d(TAG, "sign off inserted successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			if (ps != null)
				ps.close();
		}

		Log.v(TAG, "AttendanceResult - end");
	}

	public int generateUniqueNumberBasedOnDateTime() {
		int val = 0;
		String fullString;
		DateUtil dUtil = new DateUtil();
		int y, m, d;
		String h, mn, s;
		int dt;
		String tm;
		fullString = dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp(), "yyyymmddHHmmss");
		y = Integer.parseInt(fullString.substring(0, 4));
		m = Integer.parseInt(fullString.substring(4, 6));
		d = Integer.parseInt(fullString.substring(6, 8));

		h = fullString.substring(8, 10);
		mn = fullString.substring(10, 12);
		s = fullString.substring(12, 14);

		dt = y + (100 + m) + d;
		tm = h + mn + s;

		val = dt * 1000000 + Integer.parseInt(tm);
		if (val <= 0)
			val *= -1;

		return val;
	}

	// llm 29/11/2011
	public int[] getTNScheduleStatusImageArray() {
		int[] statusImages = new int[] { R.drawable.transparentbox, R.drawable.status_red_square, R.drawable.status_green_square,
				R.drawable.status_yellow_square, R.drawable.status_green_square, R.drawable.status_yellow_round, R.drawable.status_green_square,
				R.drawable.status_green_round, R.drawable.status_purple_square

		};

		// R.drawable.status_green_square,
		// R.drawable.status_yellow_square,
		// R.drawable.status_red_square,
		// R.drawable.status_purple_square,
		// R.drawable.transparentbox

		// 1 New
		// 2 All OK
		// 3 Notice Issued
		// 4 Defect(s) done
		// 5 Defects not done, Reminder Issued
		// 6 Defects done with Reminder Issued
		// 7 Defects not done, Routed to CD
		// 8 Cancelled

		return statusImages;
	}

	public UserInfoVO getUserInfoByUserinfoId(int userinfoid) throws ULjException {
		int id = 0;
		UserInfoVO userVO;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		bufQuery
				.append("SELECT userId, userName, userRoleId, UserinfoId, ContractorId, staffHandphoneNo, staffOfficeNo, staffDesignation from tblUserInfo where userinfoid= "
						+ Integer.toString(userinfoid));

		try {
			ps = conn.prepareStatement(bufQuery.toString());

			rs = ps.executeQuery();
			userVO = new UserInfoVO();
			if (rs.next()) {
				userVO = new UserInfoVO();
				int columnIndex = 1;
				userVO.setUserId(rs.getString(columnIndex++));
				userVO.setUserName(rs.getString(columnIndex++));
				userVO.setUserRoleId(rs.getInt(columnIndex++));
				userVO.setUserInfoId(rs.getInt(columnIndex++));
				userVO.setContractorId(rs.getString(columnIndex++));
				userVO.setUserHandphoneNo(rs.getString(columnIndex++));
				userVO.setUserOfficeNo(rs.getString(columnIndex++));
				userVO.setUserDesignation(rs.getString(columnIndex++));
				return userVO;
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}
		return null;
	}

	public String getHandheldConfig(String type) throws ULjException {
		String configValue;
		DbUtil db = new DbUtil();
		String query = "Select ConfigValue from tblHandheldConfiguration where ConfigType = '" + type + "'";
		try {
			configValue = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return configValue;
	}

	public String getTenantIDbyLocation(int locationId) throws ULjException {

		String tenantid;
		DbUtil db = new DbUtil();
		String query = "Select tenantid from tblTenantLocation where locationID= " + locationId;
		try {
			tenantid = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}
		if (tenantid == null) {
			tenantid = "0";
		}

		return tenantid;
	}

	public String getImageWebPath(String id, String module, String type) throws ULjException {
		String path = "";
		// <terminalid> / <module> / <id> / <type>

		DbUtil db = new DbUtil();
		String query = "Select dbo.RetrieveImageSavedPath('" + id + "','" + module + "' )";

		StringBuffer buffQuery = new StringBuffer();

		buffQuery.append("SELECT tblterminal.terminalid  " + " FROM tblEngLocation, tblArea, tblLevel ,tblZone, tblTerminal ");

		if (module.equals(AppsConstant.MODULE_COMPETENCY_CHECK)) {
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select  cc.locationid from tblCompetencyCheck cc where cc.CompetencyCheckId ='" + id + "') ");
		}

		if (module.equals(AppsConstant.MODULE_FAULT)) {
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select LocationID from tblFaultDetails where faultId ='" + id + "')");

		}
		//llm 07/10/2013 commercial fault get image bug,for commercial fault bug
		if (module.equals("commercialfault")) {
		
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select LocationID from tblCommercialFaultDetails where faultId ='" + id + "')");

		}
		//llm 07/10/2013 commercial fault get image bug,for commercial fault bug
		if (module.equals(AppsConstant.MODULE_HANDOVER)) {
			// buffQuery.append("			, tblHandoverList ho ");
			// buffQuery.append(" 			WHERE tblLocation.LocationID = ho.LocationID ");
			// buffQuery.append("	AND ho.HandoverNo='" + id + "'");
			// juehua query change
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select ho.LocationID  from tblHandoverList ho where  ho.HandoverNo ='" + id + "')");

		}

		if (module.equals(AppsConstant.MODULE_TAKEOVER)) {
			
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select ho.LocationID  from tbltakeoverList ho where  ho.takeoverNo ='" + id + "')");

		}
		if (module.equals(AppsConstant.MODULE_ROUTINEWORK)) {
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select rw.LocationID  from tblroutinework rw  where  rw.routineworkid ='" + id + "')");
		}

		if (module.equals(AppsConstant.MODULE_POWORK)) {
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select po.LocationID  from tblpowork po  where  po.poworkid ='" + id + "')");
		}

		if (module.equals(AppsConstant.MODULE_POWORK_RESULT)) {
			buffQuery.append("	WHERE  tblEngLocation.LocationID = (select po.LocationID  from tblpowork po  where  po.poworkid ='" + id + "')");
		}

		buffQuery.append(" and tblZone.ZoneID = tblEngLocation.ZoneID " + " and tblZone.AreaID = tblArea.AreaID "
				+ " and tblLevel.TerminalId = tblTerminal.TerminalId " + " and tblArea.LevelID = tblLevel.LevelID ");

		try {
			path = db.executeSingleValueQuery(buffQuery.toString());
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		path = path + "/" + module + "/" + id + "/" + type;

		System.out.println("Path constructed is " + path + "/" + type);

		return path;
	}

	public void clearAccountTables() throws ULjException {
		List tablesToClear = new ArrayList();

		tablesToClear.add("delete  from tblCompetencyChecklistResult");
		tablesToClear.add("delete  from tblCompetencyCheckAssessment");
		tablesToClear.add("delete  from tblCompetencyCheck");

		tablesToClear.add("delete  from tblFaultContrImages");
		tablesToClear.add("delete from tblFaultContrDetails");
		tablesToClear.add("delete from tblFaultDetailsImages");
		tablesToClear.add("delete from tblFaultDetails");

		tablesToClear.add("delete from tblHandoverChecklistResult");
		tablesToClear.add("delete  from tblHandoverImages");
		tablesToClear.add("delete  from tblHandoverLockKey");
		tablesToClear.add("delete  from tblHandoverSignoff");
		tablesToClear.add("delete  from tblHandoverList");

		tablesToClear.add("delete  from tblIFSFeedbackDetails");
		tablesToClear.add("delete  from tblIFSFeedbackRating");

		tablesToClear.add("delete  from tblServiceChecklistResultAll_nosync");
		tablesToClear.add("delete  from tblServiceChecklistResult");
		tablesToClear.add("delete  from tblServiceServiceHistory");

		tablesToClear.add("delete  from tblTakeoverImages");
		tablesToClear.add("delete  from tblTakeoverLockKey");
		tablesToClear.add("delete  from tblTakeoverSignoff");
		tablesToClear.add("delete  from tblTakeoverList");

		tablesToClear.add("delete  from tblTenantNoticeImages");
		tablesToClear.add("delete  from tblTenantNoticeChecklistResult");
		tablesToClear.add("delete  from tblTenantNotice");
		tablesToClear.add("delete  from tblTenantServiceServiceHistory");

		tablesToClear.add("delete  from tblToiletRandomResult");
		tablesToClear.add("delete  from tblToiletServiceChecklistResult");
		tablesToClear.add("delete  from tblToiletServiceServiceHistory");

		tablesToClear.add("delete  from tblWaterMeterOnOffAppliance");
		tablesToClear.add("delete  from tblWaterMeterOnOffImages");
		tablesToClear.add("delete  from tblWaterMeterOnOffSignoff");
		tablesToClear.add("delete  from tblWaterMeterOnOff");

		tablesToClear.add("delete from tblMonthlyMeterImages");
		tablesToClear.add("delete from tblMstUtilityMeterList");
		tablesToClear.add("delete from tblUserMeterType");
		tablesToClear.add("delete from tblUtilityMeterReadingHistory");
		// //incident report
		tablesToClear.add("delete from tblIncidentReport");
		tablesToClear.add("delete from tblIncidentReportDriver");
		tablesToClear.add("delete from tblIncidentReportPersonel");
		tablesToClear.add("delete from tblIncidentReportingMajorAccident");
		tablesToClear.add("delete from tblIncidentReportingPest");
		tablesToClear.add("delete from tblIncidentReportingPestType");
		tablesToClear.add("delete from tblIncidentReportingLeakage");
		tablesToClear.add("delete from tblIncidentReportingGlass");
		tablesToClear.add("delete from tblIncidentReportingGeneral");
		tablesToClear.add("delete from tblIncidentReportImages");
		tablesToClear.add("delete from tblMstIncidentReportingAccident");
		tablesToClear.add("delete from tblMstIncidentReportingGlassType");
		tablesToClear.add("delete from tblMstIncidentReportingGlassTypeIncident");
		tablesToClear.add("delete from tblMstIncidentReportingInfestation");
		tablesToClear.add("delete from tblMstIncidentReportingPest");
		tablesToClear.add("delete from tblMstIncidentReportingSpot");
		tablesToClear.add("delete from tblMstIncidentReportingWeather");
		tablesToClear.add("delete from tblMstIncidentType");

		// //powork
		tablesToClear.add("delete from tblPOWork");
		tablesToClear.add("delete from tblPOWorkImages");
		tablesToClear.add("delete from tblPOWorkResult");
		tablesToClear.add("delete from tblPOWorkResultImages");
		tablesToClear.add("delete from tblAesServiceChecklistResult");
		tablesToClear.add("delete from tblAesServiceServiceHistory");

		tablesToClear.add("delete from tblAesHydrant");
		tablesToClear.add("delete from tblAesHydrantAssignment");
		tablesToClear.add("delete from tblAesHydrantInspectHistory");

		tablesToClear.add("delete from tblAesGate");
		tablesToClear.add("delete from tblAesGateAssignment");
		tablesToClear.add("delete from tblAesGateInspectHistory");

		DbUtil db = new DbUtil();
		for (int i = 0; i < tablesToClear.size(); i++) {
			System.out.println("Clearing table " + (String) tablesToClear.get(i));
			if (i == tablesToClear.size() - 1)// if last table in the list ->
				// commit
				db.executeStatement(AppsConstant.DO_COMMIT_TRUE, (String) tablesToClear.get(i));
			else
				db.executeStatement(AppsConstant.DO_COMMIT_FALSE, (String) tablesToClear.get(i));
		}

	}

	// comemercial-outlet
	public String getTenantNamebyLocation(int locationId) throws ULjException {

		String tenantName;
		DbUtil db = new DbUtil();

		String query = "Select b.TenantName  from  tblTenantLocation  a, tblTenant b where a.tenantId = b.tenantId and  locationID= " + locationId;
		try {
			tenantName = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return tenantName;
	}

	// comemercial-outlet
	public String getTenantNamebyFaultId(String faultId) throws ULjException {

		String tenantName;
		DbUtil db = new DbUtil();

		String query = "Select b.TenantName  from  tblFaultTenant  a, tblTenant b where a.tenantId = b.tenantId and  faultId= '" + faultId + "'";
		try {
			tenantName = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return tenantName;
	}

	public String getTerminalById(int terminalId) throws ULjException {

		String locationCode;
		DbUtil db = new DbUtil();
		String query = "Select terminal from tblTerminal where terminalID= " + terminalId;
		try {
			locationCode = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return locationCode;
	}

	public String getTenantNamebyTenantId(int tenantid) throws ULjException {

		String tenantname;
		DbUtil db = new DbUtil();
		String query = "Select tenantname from tblTenant where tenantid= " + tenantid;
		try {
			tenantname = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}
		if (tenantname == null) {
			tenantname = "";
		}

		return tenantname;
	}

	public String getTenantNobyLocation(int locationId) throws ULjException {

		String tenantid;
		DbUtil db = new DbUtil();
		String query = "Select tenantno from tblTenantLocation where locationID= " + locationId;
		try {
			tenantid = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}
		if (tenantid == null) {
			tenantid = "0";
		}

		return tenantid;
	}

	public String getLocationCodeByZoneId(int zoneid) throws ULjException {
		LocationVO vo = new LocationVO();
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		String locationCode = "";

		bufQuery.append("select * " + "from tblzone, tblarea, tbllevel, tblTerminal " + "where tblzone.AreaID= tblarea.AreaID "
				+ "and tblarea.LevelID= tbllevel.LevelID " + "and tbllevel.TerminalId= tblTerminal.TerminalId " + "and tblzone.ZoneID = ? ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			System.out.println(bufQuery.toString());
			System.out.println(zoneid);
			ps.set(ind++, zoneid);

			rs = ps.executeQuery();

			if (rs.next()) {
				int columnIndex = 1;
				vo.setZoneId(rs.getString(columnIndex++));
				vo.setZone(rs.getString(columnIndex++));
				vo.setAreaId(rs.getString(columnIndex++));
				vo.setAreaId(rs.getString(columnIndex++));
				vo.setArea(rs.getString(columnIndex++));
				vo.setLevelId(rs.getString(columnIndex++));
				vo.setLevelId(rs.getString(columnIndex++));
				vo.setLevel(rs.getString(columnIndex++));
				vo.setTerminalId(rs.getInt(columnIndex++));
				vo.setTerminalId(rs.getInt(columnIndex++));
				vo.setTerminal(rs.getString(columnIndex++));

			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		locationCode = vo.getTerminal() + "/" + vo.getLevel() + "/" + vo.getArea() + "/" + vo.getZone() + "/";

		return locationCode;
	}

	public String getLocationCodeByAreaId(int areaid) throws ULjException {
		String locationCode;
		DbUtil db = new DbUtil();
		String query = "select terminal+'/'+LevelDesc+'/'+area+'/' " + "from  tblarea, tbllevel, tblTerminal  " + "where  "
				+ "tblarea.LevelID= tbllevel.LevelID  " + "and tbllevel.TerminalId= tblTerminal.TerminalId and areaid=  " + areaid;
		try {
			locationCode = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return locationCode;

	}

	public String getLocationCodeByLevelId(int levelid) throws ULjException {
		String locationCode;
		DbUtil db = new DbUtil();
		String query = "select terminal+'/'+LevelDesc+'/' " + "from  tbllevel, tblTerminal  " + "where  "
				+ " tbllevel.TerminalId= tblTerminal.TerminalId and levelid=  " + levelid;
		try {
			locationCode = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return locationCode;

	}

	public String AddLeadingZeros(int digit, String num, int decimalpt) {
		String sNumber;
		int dNumber = 0;
		String pNumber = "";

		if (num.equals(""))
			num = "0";

		sNumber = num; // (num + "").split(".");

		if (decimalpt > 0) {
			dNumber = Integer.parseInt(sNumber);
			pNumber = ".000";
		} else {
			dNumber = Integer.parseInt(num);
		}

		String stringNumber = String.format("%0" + digit + "d", dNumber);

		stringNumber = stringNumber + pNumber;

		return stringNumber;

	}

	public double round3Decimals(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.###");
		return Double.valueOf(twoDForm.format(d));
	}

	public boolean isSupervisor(String userid) throws ULjException {
		String query = "select count(*) from tblusersupervisor where supervisorid = '" + userid + "'";
		DbUtil dUtil = new DbUtil();
		int cnt = 0;
		cnt = dUtil.executeSingleIntValueQuery(query);

		if (cnt > 0)
			return true;
		else
			return false;

	}

	public int verifyBarcodeLocationByLocationId(int locationId, String barcodeString) throws ULjException {

		FaultDAO faultDao = new FaultDAO();
		LocationVO lvo = new LocationVO();

		// String locationcode="";
		// String barcodeversion="";
		// String sy =";";
		// getlatest barcode version here
		//
		// if (barcodeString.contains(sy))
		// {
		// String[] splitBarcode = barcodeString.split(sy);
		//
		// locationcode = splitBarcode[0];
		// barcodeversion = splitBarcode[1];
		// barcodeString=locationcode;
		// }
		//
		lvo = faultDao.getLocIdbyLocCode(barcodeString);
		if (lvo == null)
			return AppsConstant.BARCODE_VERIFY_UNKNOWN_LOCATION;
		// llm 18/12/2012
		if (lvo.getLocationId() == null)
			return AppsConstant.BARCODE_VERIFY_UNKNOWN_LOCATION;

		Integer intlocationid = Integer.parseInt(lvo.getLocationId());
		if (intlocationid == locationId) {

			// if (barcodeString.contains(sy))
			// {
			// //insert into temp table
			// //insertBarcodeVersionTracking(barcode1, barcode2);
			// }
			//

			return AppsConstant.BARCODE_VERIFY_LOCATION_MATCHED_WITH_CORRECT_TAG;
		} else
			return AppsConstant.BARCODE_VERIFY_LOCATION_MISMATCHED;

	}

	public int[] getRoutineWorkScheduleStatusImageArray() {
		int[] statusImages = new int[] { R.drawable.status_green_square, R.drawable.status_yellow_square, R.drawable.status_red_square,
				R.drawable.status_purple_square, R.drawable.transparentbox, R.drawable.status_green_round };
		// R.drawable.status_purple_square
		return statusImages;
	}

	public Bitmap LoadImage(String URL, BitmapFactory.Options options, Handler handler) {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			in.close();
		} catch (Exception e1) {
			e1.printStackTrace();
			handler.sendEmptyMessage(0);
		}
		handler.sendEmptyMessage(1);
		return bitmap;
	}

	private InputStream OpenHttpConnection(String strURL) throws Exception {
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inputStream = httpConn.getInputStream();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return inputStream;
	}

	public int[] getPOWorkScheduleStatusImageArray() {
		int[] statusImages = new int[] { R.drawable.status_green_square, R.drawable.status_yellow_square, R.drawable.status_red_square,
				R.drawable.status_purple_square, R.drawable.transparentbox, R.drawable.status_green_round };
		// R.drawable.status_purple_square
		return statusImages;
	}

	public void clearIFSnFaultTables() throws ULjException {
		List tablesToClear = new ArrayList();

		tablesToClear.add("delete  from tblIFSFeedbackDetails");
		tablesToClear.add("delete  from tblIFSFeedbackRating");
		// to clear fault tables llm 07/12/12
		tablesToClear.add("delete  from tblFaultDetails");
		tablesToClear.add("delete  from tblFaultDetailsImages");
		tablesToClear.add("delete  from tblFaultContrDetails");
		tablesToClear.add("delete  from tblFaultContrImages");
		tablesToClear.add("delete  from tblFaultSubCategory");

		DbUtil db = new DbUtil();
		for (int i = 0; i < tablesToClear.size(); i++) {
			System.out.println("Clearing table " + (String) tablesToClear.get(i));
			if (i == tablesToClear.size() - 1)// if last table in the list ->
				// commit
				db.executeStatement(AppsConstant.DO_COMMIT_TRUE, (String) tablesToClear.get(i));
			else
				db.executeStatement(AppsConstant.DO_COMMIT_FALSE, (String) tablesToClear.get(i));
		}

	}

	public String getFaultCategoryDesc(int faultcategoryid) throws ULjException {

		String faultcategorydesc;
		DbUtil db = new DbUtil();
		String query = "Select FaultCategory from tblMstFaultCategory where FaultCategoryID= " + faultcategoryid;
		try {
			faultcategorydesc = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return faultcategorydesc;
	}

	public boolean isOnline(Context context) {
		boolean value = false;
		@SuppressWarnings("static-access")
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.getApplicationContext().CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();
		if (info != null && info.isAvailable())
			value = true;

		return value;
	}

	public boolean isAirplaneModeOn(Context context) {

		return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;

	}

	public void insertAccessLog(int appsver, String action) throws ULjException {
		Log.v(TAG, "insert access log - start");

		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		StringBuffer query = new StringBuffer("Insert into tblAccessLog ");
		query.append("(Type,UserId,SessionId,AppsVersion,");
		query.append("IMEINo,Action,ActionDate) ");
		query.append("values (?,?,?,?,?,?,? ) ");
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(query.toString());
			int index = 1;
			ps.set(index++, "HandHeld");
			ps.set(index++, main.strCurrentUserId);
			ps.set(index++, main.SessionId);
			ps.set(index++, appsver);
			ps.set(index++, main.strDeviceID);
			ps.set(index++, action);
			ps.set(index++, dUtil.getCurrentTimeStamp());
			ps.execute();
			conn.commit();
			Log.d(TAG, "access log inserted successfully");

		} catch (ULjException ejl) {
			Log.e(TAG, "Error in executing query " + query.toString(), ejl);
			conn.rollback();
			throw ejl;
		} finally {
			if (ps != null)
				ps.close();
		}

		Log.v(TAG, "insert access log - end");
	}

	public void clearAccessLogTable() throws ULjException {
		List tablesToClear = new ArrayList();

		tablesToClear.add("delete  from tblAccessLog");

		DbUtil db = new DbUtil();
		for (int i = 0; i < tablesToClear.size(); i++) {
			System.out.println("Clearing table " + (String) tablesToClear.get(i));
			if (i == tablesToClear.size() - 1)// if last table in the list ->
				// commit
				db.executeStatement(AppsConstant.DO_COMMIT_TRUE, (String) tablesToClear.get(i));
			else
				db.executeStatement(AppsConstant.DO_COMMIT_FALSE, (String) tablesToClear.get(i));
		}

	}

	public boolean isValidRole(int roleID) throws ULjException {
		String query = "select count(*) from tbluserrole where userinfoid = " + main.iCurrentUserInfoId + " and roleid = " + roleID;
		DbUtil dUtil = new DbUtil();
		int cnt = 0;
		cnt = dUtil.executeSingleIntValueQuery(query);

		if (cnt > 0)
			return true;
		else
			return false;
	}

	public HashMap<String, Object> getCurrentVersion(Context context) {
		String pkg = context.getPackageName();
		HashMap<String, Object> map = new HashMap<String, Object>();
		String currentAppVersionName = null;
		int currentAppVersionNumber = 0;
		try {
			currentAppVersionName = context.getPackageManager().getPackageInfo(pkg, 0).versionName;
			currentAppVersionNumber = context.getPackageManager().getPackageInfo(pkg, 0).versionCode;
		} catch (NameNotFoundException e) {
			Log.e(TAG, "", e);
		}
		System.out.println("Current Version Name is " + currentAppVersionName);
		System.out.println("Current Version Code is " + currentAppVersionNumber);
		map.put("currentVersionName", currentAppVersionName);
		map.put("currentVersionNumber", currentAppVersionNumber);
		return map;
	}

	public HashMap<String, Object> GetVersionFromServer(String versionUrl) {
		// versionUrl : this is the file you want to download from the remote server
		// version.txt contain Version Code = 2; \n Version name = 2.1;
		System.out.println("===============Getting version info from Web  " + versionUrl);
		URL u;
		HashMap<String, Object> map = new HashMap<String, Object>();
		String newAppVersionName = null;
		int newAppVersionNumber = 0;
		try {
			u = new URL(versionUrl.toString());
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			// c.setDoOutput(true);
			c.connect();
			InputStream in = c.getInputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = in.read(buffer)) != -1) {
				baos.write(buffer, 0, len1); // Write Into ByteArrayOutputStream Buffer.
			}
			String temp = "";
			String s = baos.toString();// baos.toString(); contain Version Code = 2; \n Version name = 2.1;
			for (int i = 0; i < s.length(); i++) {
				i = s.indexOf("=") + 1;
				while (s.charAt(i) == ' ') // Skip Spaces
				{
					i++; // Move to Next.
				}
				while (s.charAt(i) != ';' && (s.charAt(i) >= '0' && s.charAt(i) <= '9' || s.charAt(i) == '.')) {
					temp = temp.toString().concat(Character.toString(s.charAt(i)));
					i++;
				}
				//
				s = s.substring(i); // Move to Next to Process.!
				temp = temp + " "; // Separate w.r.t Space Version Code and Version Name.
			}
			String[] fields = temp.split(" ");// Make Array for Version Code and Version Name.
			newAppVersionNumber = Integer.parseInt(fields[0].toString());// .ToString() Return String Value.
			newAppVersionName = fields[1].toString();
			System.out.println("New Version Code is " + newAppVersionNumber);
			System.out.println("New Version Name is " + newAppVersionName);
			baos.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		map.put("newVersionName", newAppVersionName);
		map.put("newVersionNumber", newAppVersionNumber);
		return map;
	}

	// llm 21/05/2013 get userid when faultreportbyuserinfoid =0
	public String getLoggedInUserId() throws ULjException {

		String userid = "";
		DbUtil db = new DbUtil();
		String query = "SELECT emp_id FROM ULIdentifyEmployee_nosync ";
		try {
			userid = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return userid;
	}
	
	public ServerIPVO GetIPFromServer (String versionUrl) //llm 18/07/2013
	{
		// versionUrl : this is the file you want to download from the remote server
		// version.txt contain Version Code = 2; \n Version name = 2.1;
		System.out.println("===============Getting IP info from Web  " + versionUrl);

		URL u;
		ServerIPVO svo = new ServerIPVO();
		
		try {
			u = new URL(versionUrl.toString());
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			// c.setDoOutput(true);
			c.connect();
			InputStream in = c.getInputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = in.read(buffer)) != -1) {
				baos.write(buffer, 0, len1); // Write Into ByteArrayOutputStream Buffer.
			}
			String temp = "";
			String s = baos.toString();// baos.toString(); contain Version Code = 2; \n Version name = 2.1;
			for (int i = 0; i < s.length(); i++) {
				i = s.indexOf("=") + 1;
				while (s.charAt(i) == ' ') // Skip Spaces
				{
					i++; // Move to Next.
				}
				while (s.charAt(i) != ';' ) // llm 06/08/2013 to cater for url with characters other than numbers
				{
					temp = temp.toString().concat(Character.toString(s.charAt(i)));
					i++;
				}
				//
				s = s.substring(i); // Move to Next to Process.!
				temp = temp + " "; // Separate w.r.t Space Version Code and Version Name.
			}
			String[] fields = temp.split(" ");// Make Array for Version Code and Version Name.

			if (fields.length>=1)
			try {svo.setIP1(fields[0].toString());} catch (Exception e) {e.printStackTrace();}
			
			if (fields.length>=2)
			try {svo.setIP2(fields[1].toString());} catch (Exception e) {e.printStackTrace();}
			
			if (fields.length>=3)
			try {svo.setEXPIRY_DATE_1(fields[2].toString());} catch (Exception e) {e.printStackTrace();}
			
			if (fields.length>=4)
			try {svo.setEXPIRY_DATE_2(fields[3].toString());} catch (Exception e) {e.printStackTrace();}
			
			if (fields.length>=5)
			try {svo.setPORT1(Integer.parseInt(fields[4].toString()));} catch (Exception e) {e.printStackTrace();}
			
			if (fields.length>=6)
			try {svo.setPORT2(Integer.parseInt(fields[5].toString()));} catch (Exception e) {e.printStackTrace();}
			
			if (fields.length>=7)
			try {svo.setVERSION(Integer.parseInt(fields[6].toString()));} catch (Exception e) {e.printStackTrace();} //llm 23/07/2013 add version

			baos.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 return svo;
	}

	//llm 23/07/2013 add version
	public ServerIPVO ReadfromLocalTXT()
	{
		File root = android.os.Environment.getExternalStorageDirectory();  
		File sdcard =  new File (root.getAbsolutePath() + "/txt");
		File file = new File(sdcard,"IP.txt");

		//Read text from file
		String line;
		String line2="";
		ServerIPVO svo = new ServerIPVO();
		
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    

		    while ((line = br.readLine()) != null) {
		       
		        line2 = line2+line+"\r\n";
		    }
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
		}
		
		String temp = "";
		String s = line2;// baos.toString(); contain Version Code = 2; \n Version name = 2.1;
		for (int i = 0; i < s.length(); i++) {
			i = s.indexOf("=") + 1;
			while (s.charAt(i) == ' ') // Skip Spaces
			{
				i++; // Move to Next.
			}
			while (s.charAt(i) != ';' && (s.charAt(i) >= '0' && s.charAt(i) <= '9' || s.charAt(i) == '.' || s.charAt(i) == '-' )) {
				temp = temp.toString().concat(Character.toString(s.charAt(i)));
				i++;
			}
			//
			s = s.substring(i); // Move to Next to Process.!
			temp = temp + " "; // Separate w.r.t Space Version Code and Version Name.
		}
		String[] fields = temp.split(" ");// Make Array for Version Code and Version Name.

		if (fields.length>=1)
		try {svo.setIP1(fields[0].toString());} catch (Exception e) {e.printStackTrace();}
		
		if (fields.length>=2)
		try {svo.setIP2(fields[1].toString());} catch (Exception e) {e.printStackTrace();}
		
		if (fields.length>=3)
		try {svo.setEXPIRY_DATE_1(fields[2].toString());} catch (Exception e) {e.printStackTrace();}
		
		if (fields.length>=4)
		try {svo.setEXPIRY_DATE_2(fields[3].toString());} catch (Exception e) {e.printStackTrace();}
		
		if (fields.length>=5)
		try {svo.setPORT1(Integer.parseInt(fields[4].toString()));} catch (Exception e) {e.printStackTrace();}
		
		if (fields.length>=6)
		try {svo.setPORT2(Integer.parseInt(fields[5].toString()));} catch (Exception e) {e.printStackTrace();}
		
		if (fields.length>=7)
		try {svo.setVERSION(Integer.parseInt(fields[6].toString()));} catch (Exception e) {e.printStackTrace();}

		return svo;
		
	}
	
	
	public Integer getEquipmentIDdbyEquipmentCode(String EquipmentCode) throws ULjException {

		LocationVO vo = new LocationVO();
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		int equipid=0;
		
		bufQuery
				.append("SELECT equipmentid  "
						+ " FROM tblEngEquipment "
						+ " where "
						+ " equipmentCode = ? ");
				
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
		
			ps.set(ind++, EquipmentCode);

			rs = ps.executeQuery();

			if (rs.next()) {
				int columnIndex = 1;
			
				equipid=rs.getInt(columnIndex++);
			

			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		return equipid;
	}
	

	
	public String getEquipmentCodedbyEquipmentId(Integer EquipmentId) throws ULjException {

		
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		String equipcode="";
		
		bufQuery
				.append("SELECT equipmentcode  "
						+ " FROM tblEngEquipment "
						+ " where "
						+ " equipmentid = ? ");
				
		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
		
			ps.set(ind++, EquipmentId);

			rs = ps.executeQuery();

			if (rs.next()) {
				int columnIndex = 1;
			
				equipcode=rs.getString(columnIndex++);
			

			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		return equipcode;
	}
	
	public String getIBMSFMCLocationCode(int terminalid) throws ULjException {
		String locationcode;
		DbUtil db = new DbUtil();
		String query = "Select fmclocationcode from tblIBMSFMCLocationCode where terminalid = " + Integer.toString(terminalid) ;
		try {
			locationcode = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return locationcode;
	}
	
	
	public String getLocationNameByLocationCode(String loccode) throws ULjException {
		
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		String locationname = "";

		bufQuery.append("select location from tblEnglocation where locationcode = ? ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			
			ps.set(ind++, loccode);

			rs = ps.executeQuery();

			if (rs.next()) {
				int columnIndex = 1;
				locationname =rs.getString(columnIndex++);
				
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}

		return locationname;
	}

	
	public int getPLBLocIDbyLocCode(String loccode) throws ULjException {
		int id = 0;

		DbUtil db = new DbUtil();
		String query = "Select locationid from tblplblocation where locationcode= '" + loccode +"'" ;
		try {
			id = db.executeSingleIntValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return id;
	}
	//llm  06/03/2015 added to take level from db
	public String getLevelbyLocationId(String locationid) throws ULjException {
		String id = "";

		DbUtil db = new DbUtil();
		String query = "select leveldesc from tblenglocation l left join tblzone  z "+
		" on l.ZoneID=z.ZoneID "+ 
		" left join tblarea a on a.AreaID=z.AreaID "+
		" left join tbllevel lv on lv.LevelID = a.LevelID "+
		" where locationid =  "+locationid;
		try {
			id = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return id;
	}
	
	public String getTerminalAbbrByTerminalID(int terminalid) throws ULjException {

		String terminalabbr="";
		DbUtil db = new DbUtil();
		String query = "Select terminalabbr from tblterminal where terminalid= " + terminalid;
		try {
			terminalabbr = db.executeSingleValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return terminalabbr;
	}


	public int getTerminalByLocationId(int locationid) throws ULjException {

		int terminalid;
		DbUtil db = new DbUtil();
		String query = "select t.TerminalID  from tblenglocation l "+
" left join tblZone z on l.ZoneID = z.ZoneID   "+
" left join tblArea a on z.AreaID =a.AreaID  "+
" left join tblLevel lv on a.LevelID =lv.LevelID  "+
" left join tblTerminal t on lv.TerminalID =t.TerminalID  "+
" where l.locationid =  "+ Integer.toString(locationid);

		try {
			terminalid = db.executeSingleIntValueQuery(query);
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + query.toString(), e);
			throw e;
		}

		return terminalid;
	}

	public boolean isValidEmail(String email)
	{
		boolean isValidEmail = false;

		String emailExpression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(emailExpression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
		{
			isValidEmail = true;
		}
		return isValidEmail;
	}
	
}
