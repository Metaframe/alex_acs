package sg.com.surbana.acs.einsp.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.FaultImagesVO;
import sg.com.surbana.acs.einsp.vo.FaultListVO;
import sg.com.surbana.acs.einsp.vo.FaultVO;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import android.util.Log;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 9, 2011		Ryan			Created
 *02 Aug 2011		Ryan			Fixed wrong data retrieval sequence error
 *
 */

public class FaultDAO extends BaseDAO {
	private final String TAG = "FaultDAO.java";

	public LocationVO getLocIdbyLocCode(String locationcode) throws ULjException {

		LocationVO vo = new LocationVO();
		Connection conn = DbUtil.getConnection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();

		bufQuery.append("SELECT tblLevel.LevelID, tblarea.areaid, tblzone.zoneid,tblEngLocation.locationid,tblEngLocation.location ,tblTerminal.Terminal, tblterminal.terminalid "
				+ "FROM tblEngLocation, tblArea, tblLevel ,tblZone, tblTerminal "
				+ "where "
				+ "tblEngLocation.LocationCode = ? "
				+ "and tblZone.ZoneID = tblEngLocation.ZoneID "
				+ "and tblZone.AreaID = tblArea.AreaID "
				+ "and tblLevel.TerminalId = tblTerminal.TerminalId "
				+ "and tblArea.LevelID = tblLevel.LevelID " +
				" and tblEngLocation.locationid < "+AppsConstant.ENGINEERING_LOCATIONID_START);//2200001

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			ps.set(ind++, locationcode);

			rs = ps.executeQuery();

			while (rs.next()) {
				int columnIndex = 1;

				vo.setLevelId(rs.getString(columnIndex++));
				vo.setAreaId(rs.getString(columnIndex++));
				vo.setZoneId(rs.getString(columnIndex++));
				vo.setLocationId(rs.getString(columnIndex++));
				vo.setLocation(rs.getString(columnIndex++));
				vo.setTerminal(rs.getString(columnIndex++));
				vo.setTerminalId(rs.getInt(columnIndex++));
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return vo;
	}

	public ArrayList<DropdownItemVO> getLevel(int terminalId) throws ULjException {
		ArrayList<DropdownItemVO> list = new ArrayList();
		DropdownItemVO Vo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			bufQuery.append("select distinct lv.LevelID,LevelDesc ");
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
			//bufQuery.append("inner join tblTenantLocation tl on tl.LocationID=l.LocationID  ");

			if (terminalId > 0) {
			bufQuery.append(" where t.terminalid = ? ");
			}
		}
		else
		{	
			bufQuery.append("SELECT levelid, leveldesc FROM tbllevel ");
		
			if (terminalId > 0) {
				bufQuery.append(" where terminalid = ? ");
			}
		}
		bufQuery.append(" order by leveldesc  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (terminalId > 0)
				ps.set(ind++, terminalId);

			rs = ps.executeQuery();
			Vo = new DropdownItemVO();
			Vo.setItemId("0");
			Vo.setItemDescription("Select Level");
			list.add(Vo);

			while (rs.next()) {
				int columnIndex = 1;
				Vo = new DropdownItemVO();
				Vo.setItemId(rs.getString(columnIndex++));
				Vo.setItemDescription(rs.getString(columnIndex++));
				list.add(Vo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return list;
	}
	
	public ArrayList<DropdownItemVO> getArea(int levelId) throws ULjException {
		ArrayList<DropdownItemVO> areaList = new ArrayList();
		DropdownItemVO areaVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			bufQuery.append("select distinct a.AreaID, a.Area ");
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
			//bufQuery.append("inner join tblTenantLocation tl on tl.LocationID=l.LocationID  ");

			if (levelId > 0) {
				bufQuery.append(" where lv.LevelID = ? ");
			}
			
		}
		else
		{	
			bufQuery.append("SELECT AreaID, Area FROM tblArea ");
			if (levelId > 0) {
				bufQuery.append(" where LevelID = ? ");
			}
		}
		bufQuery.append(" order by Area  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (levelId > 0)
				ps.set(ind++, levelId);

			rs = ps.executeQuery();
			areaVo = new DropdownItemVO();
			areaVo.setItemId("0");
			areaVo.setItemDescription("Select Area");
			areaList.add(areaVo);

			while (rs.next()) {
				int columnIndex = 1;
				areaVo = new DropdownItemVO();
				areaVo.setItemId(rs.getString(columnIndex++));
				areaVo.setItemDescription(rs.getString(columnIndex++));
				areaList.add(areaVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return areaList;
	}
	
	public ArrayList<DropdownItemVO> getFaultArea(int levelId) throws ULjException {
		ArrayList<DropdownItemVO> areaList = new ArrayList();
		DropdownItemVO areaVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			bufQuery.append("select distinct a.AreaID, a.Area ");
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
		

			if (levelId > 0) {
				bufQuery.append(" where lv.LevelID = ? ");
			}
			
		}
		else
		{	
			bufQuery.append("SELECT AreaID, Area FROM tblArea ");
			if (levelId > 0) {
				bufQuery.append(" where LevelID = ? ");
			}
		}
		bufQuery.append(" order by Area  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (levelId > 0)
				ps.set(ind++, levelId);

			rs = ps.executeQuery();
			areaVo = new DropdownItemVO();
			areaVo.setItemId("0");
			areaVo.setItemDescription("Select Area");
			areaList.add(areaVo);

			while (rs.next()) {
				int columnIndex = 1;
				areaVo = new DropdownItemVO();
				areaVo.setItemId(rs.getString(columnIndex++));
				areaVo.setItemDescription(rs.getString(columnIndex++));
				areaList.add(areaVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return areaList;
	}

	public ArrayList<DropdownItemVO> getZone(int areaId) throws ULjException {
		ArrayList<DropdownItemVO> zoneList = new ArrayList();
		DropdownItemVO zoneVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		String x ="\"zone\"";
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			
			bufQuery.append("select distinct z.zoneid,");
			bufQuery.append(x);
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
			//bufQuery.append("inner join tblTenantLocation tl on tl.LocationID=l.LocationID  ");

			if (areaId > 0) {
				bufQuery.append(" where a.AreaID = ? ");
			}
			
		}
		else
		{
			bufQuery.append("SELECT * FROM tblZone ");
			if (areaId > 0) {
				bufQuery.append(" where AreaID = ? ");
			}
		}
		bufQuery.append(" order by  "+x);

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (areaId > 0)
				ps.set(ind++, areaId);

			rs = ps.executeQuery();
			zoneVo = new DropdownItemVO();
			zoneVo.setItemId("0");
			zoneVo.setItemDescription("Select Zone");
			zoneList.add(zoneVo);

			while (rs.next()) {
				int columnIndex = 1;
				zoneVo = new DropdownItemVO();
				zoneVo.setItemId(rs.getString(columnIndex++));
				zoneVo.setItemDescription(rs.getString(columnIndex++));
				zoneList.add(zoneVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return zoneList;
	}

	public ArrayList<DropdownItemVO> getFaultZone(int areaId) throws ULjException {
		ArrayList<DropdownItemVO> zoneList = new ArrayList();
		DropdownItemVO zoneVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		String x ="\"zone\"";
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			
			bufQuery.append("select distinct z.zoneid,");
			bufQuery.append(x);
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
		

			if (areaId > 0) {
				bufQuery.append(" where a.AreaID = ? ");
			}
			
		}
		else
		{
			bufQuery.append("SELECT * FROM tblZone ");
			if (areaId > 0) {
				bufQuery.append(" where AreaID = ? ");
			}
		}
		bufQuery.append(" order by  "+x);

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (areaId > 0)
				ps.set(ind++, areaId);

			rs = ps.executeQuery();
			zoneVo = new DropdownItemVO();
			zoneVo.setItemId("0");
			zoneVo.setItemDescription("Select Zone");
			zoneList.add(zoneVo);

			while (rs.next()) {
				int columnIndex = 1;
				zoneVo = new DropdownItemVO();
				zoneVo.setItemId(rs.getString(columnIndex++));
				zoneVo.setItemDescription(rs.getString(columnIndex++));
				zoneList.add(zoneVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return zoneList;
	}

	public ArrayList<DropdownItemVO> getLocation(int zoneId) throws ULjException {
		ArrayList<DropdownItemVO> locationList = new ArrayList();
		DropdownItemVO locationVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			bufQuery.append("select distinct l.LocationID, l.Location ");
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
			//bufQuery.append("inner join tblTenantLocation tl on tl.LocationID=l.LocationID  ");
			if (zoneId > 0) {
				bufQuery.append(" where z.ZoneID = ? ");
			}
			
		}
		else
		{
			bufQuery.append("SELECT LocationID, Location FROM tblEngLocation ");
			if (zoneId > 0) {
				bufQuery.append(" where ZoneID = ? and locationid < "+AppsConstant.ENGINEERING_LOCATIONID_START);//2200001
			}
		}
		bufQuery.append(" order by Location  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (zoneId > 0)
				ps.set(ind++, zoneId);

			rs = ps.executeQuery();
			locationVo = new DropdownItemVO();
			locationVo.setItemId("0");
			locationVo.setItemDescription("Select Location");
			locationList.add(locationVo);

			while (rs.next()) {
			// fixed t2 location dropdown error
				int lcIdx = 1;
				locationVo = new DropdownItemVO();
				locationVo.setItemId(rs.getString(lcIdx++));
				locationVo.setItemDescription(rs.getString(lcIdx++));
				locationList.add(locationVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return locationList;
	}
	
	public ArrayList<DropdownItemVO> getFaultLocation(int zoneId) throws ULjException {
		ArrayList<DropdownItemVO> locationList = new ArrayList();
		DropdownItemVO locationVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			bufQuery.append("select distinct l.LocationID, l.Location ");
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
		
			if (zoneId > 0) {
				bufQuery.append(" where z.ZoneID = ? ");
			}
			
		}
		else
		{
			bufQuery.append("SELECT LocationID, Location FROM tblEngLocation ");
			if (zoneId > 0) {
				bufQuery.append(" where ZoneID = ? and locationid < "+AppsConstant.ENGINEERING_LOCATIONID_START);//2200001
			}
		}
		bufQuery.append(" order by Location  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (zoneId > 0)
				ps.set(ind++, zoneId);

			rs = ps.executeQuery();
			locationVo = new DropdownItemVO();
			locationVo.setItemId("0");
			locationVo.setItemDescription("Select Location");
			locationList.add(locationVo);

			while (rs.next()) {
			// fixed t2 location dropdown error
				int lcIdx = 1;
				locationVo = new DropdownItemVO();
				locationVo.setItemId(rs.getString(lcIdx++));
				locationVo.setItemDescription(rs.getString(lcIdx++));
				locationList.add(locationVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return locationList;
	}
	
	public ArrayList<DropdownItemVO> getLocationWithTenantName(int zoneId) throws ULjException {
		ArrayList<DropdownItemVO> locationList = new ArrayList();
		DropdownItemVO locationVo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
		//llm 14/03/2013 to show only tenant location for commercial user
		if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)
		{
			bufQuery.append("select distinct l.LocationID, l.Location, tl.TenantID, tn.TenantName ");
			bufQuery.append(" from tblTerminal t left join tblLevel lv ");
			bufQuery.append("on t.TerminalID =lv.TerminalID  ");
			bufQuery.append("left join tblArea a on lv.LevelID =a.LevelID  ");
			bufQuery.append("left join tblZone z on a.Areaid = z.AreaID ");
			bufQuery.append("left join tblEngLocation l on z.ZoneID = l.ZoneID  ");
			bufQuery.append("inner join tblTenantLocation tl on tl.LocationID=l.LocationID  ");
			bufQuery.append("left join tbltenant tn on tl.tenantid = tn.tenantid ");

			if (zoneId > 0) {
				bufQuery.append(" where z.ZoneID = ? ");
			}
			
		}
		else
		{
			bufQuery.append("SELECT LocationID, Location FROM tblEngLocation ");
			if (zoneId > 0) {
				bufQuery.append(" where ZoneID = ? ");
			}
		}
		bufQuery.append(" order by Location  ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;
			if (zoneId > 0)
				ps.set(ind++, zoneId);

			rs = ps.executeQuery();
			locationVo = new DropdownItemVO();
			locationVo.setItemId("0");
			locationVo.setItemDescription("Select Location");
			locationList.add(locationVo);
			String tmp1="";
			String tmp2="";
			while (rs.next()) {
			// fixed t2 location dropdown error
				int lcIdx = 1;
				locationVo = new DropdownItemVO();
				locationVo.setItemId(rs.getString(lcIdx++));
				tmp1=rs.getString(lcIdx++);
				try
				{
				if (rs.getString(lcIdx++)!=null)
				{
					tmp2=rs.getString(lcIdx++);
				}
				}
				catch (ULjException e){}
				if (tmp2.equals(""))
				locationVo.setItemDescription(tmp1);
				else
					locationVo.setItemDescription(tmp1+" - "+tmp2);
				locationList.add(locationVo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return locationList;
	}
	
	public ArrayList<DropdownItemVO> getTerminal() throws ULjException {
		ArrayList<DropdownItemVO> terminalList = new ArrayList();
		DropdownItemVO Vo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
//		bufQuery.append("SELECT terminalid, terminal FROM tblterminal ");
//		bufQuery.append(" order by terminal  ");
		bufQuery.append("select distinct t.TerminalId, t.Terminal  from tblenglocation loc  ");
		bufQuery.append("left join tblzone z on loc.zoneid = z.ZoneID ");
		bufQuery.append("left join tblarea a on z.AreaID=a.AreaID ");
		bufQuery.append("left join tbllevel lv on lv.LevelID= a.LevelID  ");
		bufQuery.append("left join tblTerminal t on lv.TerminalId=t.TerminalId ");
		bufQuery.append("inner join tblEngServiceServiceHistory ssh on ssh.buildingid = loc.locationid");
		//bufQuery.append("where locationid in ");
		//bufQuery.append("(select distinct buildingid from ) ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			rs = ps.executeQuery();
			Vo = new DropdownItemVO();
			Vo.setItemId("0");
			Vo.setItemDescription("Select Building");
			terminalList.add(Vo);

			while (rs.next()) {
				int columnIndex = 1;
				Vo = new DropdownItemVO();
				Vo.setItemId(rs.getString(columnIndex++));
				Vo.setItemDescription(rs.getString(columnIndex++));
				terminalList.add(Vo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return terminalList;
	}
	
	public ArrayList<DropdownItemVO> getFaultTerminal() throws ULjException {
		ArrayList<DropdownItemVO> terminalList = new ArrayList();
		DropdownItemVO Vo;
		Connection conn = DbUtil.getConnection();
		DateUtil dUtil = new DateUtil();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer bufQuery = new StringBuffer();
//		bufQuery.append("SELECT terminalid, terminal FROM tblterminal ");
//		bufQuery.append(" order by terminal  ");
		bufQuery.append("select distinct t.TerminalId, t.Terminal  from tblenglocation loc  ");
		bufQuery.append("left join tblzone z on loc.zoneid = z.ZoneID ");
		bufQuery.append("left join tblarea a on z.AreaID=a.AreaID ");
		bufQuery.append("left join tbllevel lv on lv.LevelID= a.LevelID  ");
		bufQuery.append("left join tblTerminal t on lv.TerminalId=t.TerminalId where t.terminalid is not null ");
	
		//bufQuery.append("where locationid in ");
		//bufQuery.append("(select distinct buildingid from ) ");

		try {
			ps = conn.prepareStatement(bufQuery.toString());
			int ind = 1;

			rs = ps.executeQuery();
			Vo = new DropdownItemVO();
			Vo.setItemId("0");
			Vo.setItemDescription("Select Building");
			terminalList.add(Vo);

			while (rs.next()) {
				int columnIndex = 1;
				Vo = new DropdownItemVO();
				Vo.setItemId(rs.getString(columnIndex++));
				Vo.setItemDescription(rs.getString(columnIndex++));
				terminalList.add(Vo);
			}
		} catch (ULjException e) {
			Log.e(TAG, "Error in executing query " + bufQuery.toString(), e);
			throw e;
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
		}

		return terminalList;
	}
	
}
