package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;

import android.content.Context;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *June 24, 2011		Ryan			Created
 *
 */
public class ScheduleStatusAdapter extends CustomAdapterWithImage {
	private final String TAG = "ScheduleStatusAdapter";

	public ScheduleStatusAdapter(Context context, ArrayList vo) {
		super(context, vo);
	}

	@Override
	int getStatusImageIndex(String itemNo) {
		int intItemNo = Integer.parseInt(itemNo);
		int index = -1;
		// hardcoded logic                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
		switch (intItemNo) {
		case 1:
			index = 0;
			break;
		case 2:
			index = 2;
			break;
		case 3:
			index = 3;
			break;
		case 4:
			index = 1;
			break;
		case 5:
			index = 4;
			break;
		case 6: 
			index = 5;
	
		}
		return index;
	}

	@Override
	void initializeStatusImageArray() {
		statusImages = new CommonFunction().getScheduleStatusImageArray();
	}

}
