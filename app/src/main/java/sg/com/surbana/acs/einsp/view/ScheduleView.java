package sg.com.surbana.acs.einsp.view;

import java.security.Timestamp;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.bo.ScheduleBO;
import sg.com.surbana.acs.einsp.dao.ChecklistDAO;
import sg.com.surbana.acs.einsp.dao.ScheduleDAO;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.CommonFunction;
import sg.com.surbana.acs.einsp.util.CustomAdapterWithImage;
import sg.com.surbana.acs.einsp.util.CustomArrayAdapterWithText;
import sg.com.surbana.acs.einsp.util.DatabaseHelper;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.util.ScheduleStatusAdapter;
import sg.com.surbana.acs.einsp.util.SpecialAdapterForSchedule;
import sg.com.surbana.acs.einsp.util.SynchronizationUtils;
import sg.com.surbana.acs.einsp.util.WebServiceClient;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.Image2VO;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import sg.com.surbana.acs.einsp.vo.MeterReading2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleList2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ianywhere.ultralitejni12.ULjException;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import com.surbana.common.dateslider.DateSlider;
import com.surbana.common.dateslider.DefaultDateSlider;
import com.surbana.common.dateslider.SliderConstant;

public class ScheduleView extends BaseView {
	/** Called when the activity is first created. */
	private final String TAG = "ScheduleView.java";


	TextView txtDate;
	EditText txtLocation;
	EditText txtStatus;
	TextView txtNumOfRecords;
	Spinner btnStatus;
	ListView lstSchedule;
	ArrayList scheduleList;
	String scannedBarcode;
	int previousItemIndex = 0;// This is to prevent onSelectedListener to get
								// called unnecessarily.
	 double screenInches=0;
	private static final int CAMERA_PIC_REQUEST = 1337;
	TextView lblSchDate, lblSchStatus, lblSchLocation,lblLocation;

	Button btnLogout;
	Button btnInformation;
	Button btnHome;

	int passedInStatus = 0; // added for demo 20/08/2011 llm
	String frButton = "";
	Button btnStatusOutstanding, btnStatusInProgress, btnCompleted;
	LinearLayout LinearLayoutAddTN, LinearLayoutAddSSH,linearLayoutSyncMM,linearLayoutFaultCount;
	// llm 2/1/13 to add ssh record from handheld for commercial user
	Spinner spnCommercialChecklistName;

	// flag has been added: chin hua (09 feb 2012)
	String oneRecFlag = "F";
	String scanBarFlag = "F";
	String locMoreRecFlag = "F";

	boolean isContractor = true;
	boolean isDefaultChecked=false;

	private boolean useDateFilter = false;
	private int selectedStatusId = 0;
	private DropdownItemVO selectedStatus;
	private String testDate;
	private String location;
	private LocationVO lvo;

	private ArrayList<HashMap<String, Object>> fillMaps;

	private MeterReading2VO meterReading2VO;

	private DatabaseHelper databaseHelper = null;
	private Dao<ScheduleList2VO, Integer> scheduleListDao = null;
	private Dao<MeterReading2VO, Integer> meterReadingDAO = null;

	/*
	 * Change History
	 * **************************************************************Date
	 * Updated By Remarks
	 * **************************************************************27 jul 2011
	 * Ryan Move loadlist to onResume to ensure 'backbutton' will reflect the
	 * updated status
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		// added for demo 20/08/2011 llm
		Intent myLocalIntent = getIntent();
		Bundle myBundle = myLocalIntent.getExtras();
		if (myBundle != null) {

			try {
				passedInStatus = myBundle.getInt("status");
			} catch (Exception e) {
			}
			try {
				frButton = myBundle.getString("frButton");
			} catch (Exception e) {
			}

		}


		Display disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		int height = disp.getHeight();
		DisplayMetrics dm = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(dm);
	    double x = Math.pow(dm.widthPixels/dm.xdpi,2);
	    double y = Math.pow(dm.heightPixels/dm.ydpi,2);
	    screenInches = Math.sqrt(x+y);

			if (screenInches>=6)
				setContentView(R.layout.schedule);
			else
				setContentView(R.layout.schedulenote3);

		lblSchDate = (TextView) findViewById(R.id.lblSchDate);
		lblSchStatus = (TextView) findViewById(R.id.lblSchStatus);
		lblSchLocation = (TextView) findViewById(R.id.lblSchLocation);
		lblLocation = (TextView) findViewById(R.id.lblLocation);

		lblLocation.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
	        	if (!lblLocation.getText().toString().equals("Location \n(Filter)"))
	        	{	lblSchLocation.setText("Location"); }
	    		filterList();

	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){

	        }
	    });

		lblSchDate.setOnClickListener(new DateReset());
		lblSchStatus.setOnClickListener(new StatusReset());
		lblSchLocation.setOnClickListener(new LocationReset());
		lblLocation.setOnClickListener(new LocationReset());

		txtDate = (TextView) findViewById(R.id.txtDate);
		//txtLocation = (EditText) findViewById(R.id.txtLocation);
		//txtStatus = (EditText) findViewById(R.id.txtStatus);
		btnStatus = (Spinner) findViewById(R.id.btnStatusFilter);
		// llm 1/12/2011
		btnStatusOutstanding = (Button) findViewById(R.id.btnStatusOutstanding);
		btnStatusInProgress = (Button) findViewById(R.id.btnStatusInProgress);
		btnCompleted = (Button) findViewById(R.id.btnCompleted);
		// llm
		lstSchedule = (ListView) findViewById(R.id.lstSchedule);

	    LayoutParams lp = (LayoutParams) lstSchedule.getLayoutParams();

		if (screenInches>=6)
		{
			 lp.height = 700;
			 lstSchedule.setLayoutParams(lp);
		}
		if (Build.MODEL.equalsIgnoreCase("SM-P355")) //tab a 8.0
		{	 lp.height = 660;
			 lstSchedule.setLayoutParams(lp);
		}


		txtNumOfRecords = (TextView) findViewById(R.id.txtNumRecordsFound);
		LinearLayoutAddTN = (LinearLayout) findViewById(R.id.linearLayoutAddTN);
		LinearLayoutAddTN.setVisibility(View.GONE);
			// llm 2/1/13 to add ssh record from handheld for commercial user
		try {
			LinearLayoutAddSSH = (LinearLayout) findViewById(R.id.linearLayoutAddSSH);
			LinearLayoutAddSSH.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//llm 03/05/2013 new table for monthly meter delay sync
		try {
			linearLayoutSyncMM = (LinearLayout) findViewById(R.id.linearLayoutSyncMM);
			linearLayoutSyncMM.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		linearLayoutFaultCount = (LinearLayout) findViewById(R.id.linearLayoutFaultCount);
		spnCommercialChecklistName= (Spinner)findViewById(R.id.spnCommercialChecklistName);


		if (frButton.equals("MonthlyMeter"))
		{
			//linearLayoutSyncMM.setVisibility(View.VISIBLE);
			linearLayoutFaultCount.setVisibility(View.GONE);
		}

		if (frButton.equals("TenantNotice")) {

			btnCompleted.setVisibility(View.GONE);
			this.btnStatusOutstanding.setVisibility(View.GONE);
			this.btnStatusInProgress.setVisibility(View.GONE);
			LinearLayoutAddTN.setVisibility(View.VISIBLE);
		}
		// llm 2/1/13 to add ssh record from handheld for commercial user
		// llm 12/03/2013 uncomment this when adhoc go live		
				if (main.iCurrentUserGroupId==AppsConstant.USER_GROUP_COMMERCIAL)//commercial
				{

					ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> commercialchknameListing;
					commercialchknameListing = getCommercialCheckListName();
					CustomArrayAdapterWithText commAdapter = new CustomArrayAdapterWithText(getApplicationContext(), commercialchknameListing);
					this.spnCommercialChecklistName.setAdapter(commAdapter);

					LinearLayoutAddSSH.setVisibility(View.VISIBLE);

				}
		// llm 12/03/2013 uncomment this when adhoc go live	


		// added for demo 20/08/2011 llm
		ArrayList<sg.com.surbana.acs.einsp.vo.DropdownItemVO> statusListing;

			statusListing = getStatusList();
			CustomAdapterWithImage statusAdapter = new ScheduleStatusAdapter(getApplicationContext(), statusListing);
			btnStatus.setAdapter(statusAdapter);

		btnStatus.setOnItemSelectedListener(new MyOnItemSelectedListener());

		lstSchedule.setOnItemClickListener(new ScheduleListItemOnItemClickListener());

		//rowItems for the ListView
		fillMaps = new ArrayList<HashMap<String, Object>>();

		gotoToday();
	}

	private ArrayList<DropdownItemVO> getStatusList() {
		DropdownItemVO vo;
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		vo = new DropdownItemVO("0", "All Status");
		list.add(vo);

		vo = new DropdownItemVO("2", "Outstanding");
		list.add(vo);
		vo = new DropdownItemVO("4", "In progress");
		list.add(vo);
		vo = new DropdownItemVO("1", "Completed");
		list.add(vo);
		vo = new DropdownItemVO("3", "Cancelled");
		list.add(vo);
		return list;
	}

	public void showDateSlider(View v) {
		showDialog(SliderConstant.DEFAULTDATESELECTOR_ID);
	}

	public void doBarcodeScan(View v) {
		// Intent cameraIntent = new
		// Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		// startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
		Intent intent = new Intent(this, CaptureActivity.class);
		startActivityForResult(intent, 0); // Must use startActivityForResult

		//
		// Flashlight fl = new FroyoLight();
		// fl.setOn(true, this);
		this.scanBarFlag = "T"; // chin hua (09 feb 2012)
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (Activity.RESULT_OK == resultCode) {
			Bundle extras = data.getExtras();
			scannedBarcode = (String) extras.get(CaptureActivity.INTENT_RESULT_LOCATION_CODE);
			lblSchLocation.setText(scannedBarcode);
			lblLocation.setText("Location \n(Filter)");
			filterList();
			populateListFromDB();
		}
	}

	public void doFilterOutstanding(View v) {
		btnStatus.setSelection(1);
		filterList();
		populateListFromDB();
	}

	public void doFilterInProgress(View v) {
		btnStatus.setSelection(2);
		filterList();
		populateListFromDB();
	}

	public void doFilterCompleted(View v) {
		btnStatus.setSelection(3);
		filterList();
		populateListFromDB();
	}

	public void doFilterToday(View v) {
		gotoToday();
	}

	private void gotoToday() {
		DateUtil dUtil = new DateUtil();
		lblSchDate.setText(dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp()));

		if (frButton.equals("MonthlyMeter"))
		lblSchDate.setText("Date");


		filterList();
		populateListFromDB();
	}

	public void doResetStatusAndRefreshList(View v) {
		btnStatus.setSelection(0);
		filterList();
	}

	public void doClearDateAndRefreshList(View v) {
		txtDate.setText("");
		filterList();
	}

	public void doClearLocationAndRefreshList(View v) {
		txtLocation.setText("");
		filterList();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// this method is called after invoking 'showDialog' for the first time
		// here we initiate the corresponding DateSlideSelector and return the
		// dialog to its caller

		// get todays date and the time
		final Calendar c = Calendar.getInstance();
		switch (id) {
		case SliderConstant.DEFAULTDATESELECTOR_ID:
			return new DefaultDateSlider(this, mDateSetListener, c);
		}
		return null;
	}

	// define the listener which is called once a user selected the date.
	private com.surbana.common.dateslider.DateSlider.OnDateSetListener mDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			DateUtil dUtil = new DateUtil();
			lblSchDate.setText(dUtil.timeStamp2dateString(selectedDate));
			filterList();
			populateListFromDB();
		}
	};

	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			if (previousItemIndex != pos) {
				previousItemIndex = pos;
				Object obj = parent.getItemAtPosition(pos);
				DropdownItemVO vo;
				if (obj != null) {
					vo = (DropdownItemVO) obj;
					lblSchStatus.setText(vo.getItemDescription());
					filterList();
					populateListFromDB();
				}
			}

		}

		@Override
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
			lblSchStatus.setText("");
		}
	}

	// startInspectionDialog has been added: chin hua (09 feb 2012)
	private void startInspectionDialog() {
		AlertDialog dialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(AppsConstant.MESSAGE_RECORD_PROCEED_TO_CHECKLIST_VIEW_SCREEN).setCancelable(true).setTitle("Mobile Inspection System")
				.setPositiveButton("Yes", new DialogOnClickListener(1)).setNegativeButton("No", new DialogOnClickListener(2));
		dialog = builder.create();
		dialog.setOwnerActivity(this);
		dialog.show();
	}

	private void populateList(ArrayList ar, String testDate, int selectedStatusId, String location, MeterReading2VO meterReading2VO ) {
		ScheduleList2VO vo = new ScheduleList2VO();

		String[] from = new String[] { "id", "date", "location",
				"PIC", "checklist","tenantname"};
		int[] to = new int[] { R.id.item1, R.id.lstDate,
				R.id.lstLocation,R.id.lstPIC, R.id.lstChecklistName, R.id.lstTenantName};
		int numberOfRowItems;
		java.sql.Timestamp ts;


		// prepare the list of all records
		HashMap<String, Object> tempMap;

		DateUtil dUtil = new DateUtil();
		int statusId=0;
		int tndays = 7;
		CommonFunction comFun = new CommonFunction();
		try {
			tndays= Integer.parseInt(comFun.getHandheldConfig(AppsConstant.CONFIG_TN_DAY));
		} catch (ULjException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (Exception e1) {
		}

		for (int i = 0; i < ar.size(); i++) {
			vo = (ScheduleList2VO) ar.get(i);
			vo.setMeterReading2VO(meterReading2VO);
			String rowDate;
			try {
			switch (vo.getStatusInt()) {
				case 1:
				case 2:
				case 8:
					rowDate = dUtil.timeStamp2dateString(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"));
					break;
				case 3:
				case 5:
					rowDate =  dUtil.timeStamp2dateString(dUtil.addDays(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"), tndays));
					break;
				case 4:
					rowDate = dUtil.timeStamp2dateString(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"));
					break;
				case 6:
				case 7:
					rowDate = dUtil.timeStamp2dateString(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"));
					break;
				default:
					rowDate = "";
			}
				boolean isDateEqual = rowDate.equals(testDate) || testDate.equals("");
				boolean isSameStatudId =  selectedStatusId == vo.getStatusInt() || selectedStatusId == 0;
				boolean isSameLocation = vo.getServiceMaintProgDescription().contains(location) || location.equals("");
			if (!((isSameStatudId && isSameLocation && isDateEqual))) {
				continue;
			}
			tempMap = new HashMap<String, Object>();
				// tempMap.put("date",
				// dUtil.timeStamp2dateString(vo.getFaultDate()));
			tempMap.put("id", "-");
			if (frButton.equals("TenantNotice")) {
				String schtype = "";
				if (Integer.parseInt(vo.getChecklistResult()) > 0) {
					schtype = "SCH : ";
				} else {
					schtype = "ACH : ";
				}

				tempMap.put("id", schtype + vo.getStatusInt());
//llm taken out 03/03/2011
//				if (vo.getRevisedStart() == null) {
//					try {
//						tempMap.put("date", dUtil.timeStamp2dateString(vo.getNoticedate()));
//					} catch (Exception e) {
//					}
//					;
//				} else {
//					try {
//						tempMap.put("date", dUtil.timeStamp2dateString(vo.getRevisedStart()));
//					} catch (Exception e) {
//					}
//					;
//				}
//llm taken out 03/03/2011
				if (vo.getStatusInt() == 1 || vo.getStatusInt() == 2 || vo.getStatusInt() == 8) {
					ts = dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss");
					tempMap.put("date", dUtil.timeStamp2dateString(ts));
					//getNoticedate = getactualstart
				} else if (vo.getStatusInt() == 3) {
					//tempMap.put("date", dUtil.timeStamp2dateString(vo.getRevisedStart()));
					tempMap.put("date", dUtil.timeStamp2dateString(dUtil.addDays(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"), tndays)));
				} else if (vo.getStatusInt() == 4) {
					//getfollowupdate
					//tempMap.put("date", vo.getRevisedStart());
					ts = dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss");
					tempMap.put("date", dUtil.timeStamp2dateString(ts));
				} else if (vo.getStatusInt() == 5) {
					tempMap.put("date", dUtil.timeStamp2dateString(dUtil.addDays(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"), tndays)));
				} else if (vo.getStatusInt() == 6 || vo.getStatusInt() == 7) {
					tempMap.put("date", dUtil.timeStamp2dateString(dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss")));
				}

				if (vo.getPersonInChargeID() == null || vo.getPersonInChargeID().equals("")) {
					//getUserId
					tempMap.put("PIC", vo.getPersonInChargeID());
				} else {
					tempMap.put("PIC", vo.getPersonInChargeID());
				}

				// 1 New
				// 2 All OK
				// 3 Notice Issued
				// 4 Defect(s) done
				// 5 Defects not done, Reminder Issued
				// 6 Defects done with Reminder Issued
				// 7 Defects not done, Routed to CD
				// 8 Cancelled
				// if ( vo.getChecklistName()==null||
				// vo.getChecklistName().equals(""))
				// {
				tempMap.put("checklist", "Adhoc Tenant Notice");
				if (vo.getStatusInt() == 1)// >0 new
				{
					tempMap.put("checklist", "New"); // Scheduled Tenant
					// Notice
				} else if (vo.getStatusInt() == 2) {
					tempMap.put("checklist", "All OK");
				} else if (vo.getStatusInt() == 3) {
					tempMap.put("checklist", "Notice issued");
				} else if (vo.getStatusInt() == 4) {
					tempMap.put("checklist", "Defects done");
				} else if (vo.getStatusInt() == 5) {
					tempMap.put("checklist", "Defects not done with Reminder Issued");
				} else if (vo.getStatusInt() == 6) {
					tempMap.put("checklist", "Defects done with Reminder Issued");
				} else if (vo.getStatusInt() == 7) {
					tempMap.put("checklist", "Defects not done. Routed to CD");
				}

				// if (vo.getNoticeStatus()>=5)
				// {
				// tempMap.put("checklist", "Tenant Notice Reminder");
				// }
				else if (vo.getStatusInt() == 8) {
					tempMap.put("checklist", "Cancelled Tenant Notice");
				}
				;
				// }
				// else
				// {
				// try {tempMap.put("checklist", vo.getChecklistName());
				// if (vo.getNoticeStatus()>=5)
				// {
				// tempMap.put("checklist", "Tenant Notice Reminder");
				// }
				// } catch (Exception e) {};
				// if (vo.getNoticeStatus()==1)//>0 new
				// {
				// tempMap.put("checklist", "New");//Scheduled Tenant Notice
				// }
				//
				// }
				} else {
					//tempMap.put("date", vo.getRevisedStart());
					ts = dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss");
					tempMap.put("date", dUtil.timeStamp2dateString(ts));
					tempMap.put("PIC", vo.getPersonInChargeID());
					//Temporary measure to cater for cleaning and koi pond servicing to differenciate different shift based on the maintenance programme name
					if (AppsConstant.SCHEDULED_SERVICING_ID_CLEANING == Integer.parseInt(vo.getServiceMaintProgID())
							|| AppsConstant.SCHEDULED_SERVICING_ID_KOIPOND == Integer.parseInt(vo.getServiceMaintProgID()))
						tempMap.put("checklist", vo.getServiceMaintProgDescription());

					else
						tempMap.put("checklist", vo.getChecklistResult());
					;
					if (frButton.equals("MonthlyMeter"))
						tempMap.put("checklist", "Utilities Recording");

					statusId = vo.getStatusInt();
					//Commercial-Outlet : use service description field to store tenant name, due to the complication to create
					//additional field for service service history
					if (AppsConstant.USER_GROUP_COMMERCIAL == main.iCurrentUserGroupId)
						//tempMap.put("tenantname", vo.getServiceMaintProgDescription()) ;
						tempMap.put("tenantname", null);
					else
						//tempMap.put("tenantname", vo.getFrequencyID()); //use tenantname field to display frequency if it is not a commercial check
						//no FrequencyVariable
						tempMap.put("tenantname", null);
				}
				if (frButton.equals("ToiletRandom")) {
					tempMap.put("location", vo.getServiceMaintProgDescription());

				} else {
					//llm 28/01/2013 commercial adhoc fault
					if (AppsConstant.USER_GROUP_COMMERCIAL == main.iCurrentUserGroupId && vo.getServiceMaintProgDescription().length() <= 0) {

						ChecklistDAO cdao = new ChecklistDAO();
						CommonFunction comFunction = new CommonFunction();
						int locid = cdao.searchAdhocSSHLocationID(Integer.parseInt(vo.getChecklistResult()));
						if (locid == 0) {
							tempMap.put("location", "Adhoc");
							tempMap.put("tenantname", "");
						} else {
							tempMap.put("location", comFunction.getLocationCodeById(locid));
							tempMap.put("tenantname", comFunction.getTenantNamebyLocation(locid));
						}
					} else
						tempMap.put("location", vo.getServiceMaintProgDescription());
					//llm 28/01/2013 commercial adhoc fault
					;
				}
				tempMap.put(AppsConstant.HASHMAP_KEY_REVISED_DATE, dUtil.dateString2Calendar(vo.getRevisedStart(), "yyyy-MM-dd'T'HH:mm:ss"));
				tempMap.put(AppsConstant.HASHMAP_KEY_STATUS_ID, vo.getStatusInt());
				if (vo.getActualStart() == null || vo.getActualStart().equals("") || vo.getActualStart().length() == 0) {
					tempMap.put(AppsConstant.HASHMAP_KEY_ACTUAL_START, null);
				} else {
					tempMap.put(AppsConstant.HASHMAP_KEY_ACTUAL_START, dUtil.dateString2Calendar(vo.getActualStart(), "yyyy-MM-dd'T'HH:mm:ss"));
				}
				if (vo.getActualEnd() == null || vo.getActualEnd().equals("") || vo.getActualEnd().length() == 0) {
					tempMap.put(AppsConstant.HASHMAP_KEY_ACTUAL_END, null);
				} else {
					tempMap.put(AppsConstant.HASHMAP_KEY_ACTUAL_END, dUtil.dateString2Calendar(vo.getActualEnd(), "yyyy-MM-dd'T'HH:mm:ss"));
				}
				//tempMap.put(AppsConstant.HASHMAP_KEY_NOTICE_STATUS, vo.getStatusInt());
				tempMap.put(AppsConstant.HASHMAP_KEY_NOTICE_STATUS, null);
				//llm 03/05/2013 new table for monthly meter delay sync
				tempMap.put(AppsConstant.HASHMAP_KEY_MONTHLY_METER_SYNCSTATUS, null);
				tempMap.put("frButton", frButton);

				fillMaps.add(tempMap);

			} catch (ParseException p) {
				p.printStackTrace();
			} catch (ULjException ul) {
				ul.printStackTrace();
			}
		}

		// fill in the grid_item layout
		if (ar.size() > 0 && fillMaps.size() > 0) {
			// if after the barcode scanning record is > 1: chin hua (09 feb 2012)
			if (ar.size() > 1 && scanBarFlag == "T") {
				this.locMoreRecFlag = "T";
			}
			// will only prompt out the startInspectionDialog when record =
			// 1 and after the barcode scanning: chin hua (09 feb 2012)
			if (ar.size() == 1 && "T".equals(scanBarFlag) && "F".equals(locMoreRecFlag)
					&& (statusId == AppsConstant.CHECKLIST_STATUS_OUTSTANDING && !frButton.equals("ToiletRandom")
					|| statusId == AppsConstant.TOILET_CHECKLIST_STATUS_OUTSTANDING && frButton.equals("ToiletRandom"))) {
				this.oneRecFlag = "T";
				startInspectionDialog();
				scanBarFlag = "F";
			}
			SpecialAdapterForSchedule adapter;
			// juehua 20120229 for merge chin hua's tablet xml to leeming's
			// code
//				if (Build.MODEL.equalsIgnoreCase("GT-P7500")) {
//					adapter = new SpecialAdapterForSchedule(getApplicationContext(), fillMaps, R.layout.tablet_schedule_custom_list, from, to);
//				} else {
//					adapter = new SpecialAdapterForSchedule(getApplicationContext(), fillMaps, R.layout.schedule_custom_list, from, to);
//				}]

			if (screenInches >= 6) {
				adapter = new SpecialAdapterForSchedule(getApplicationContext(), fillMaps, R.layout.schedule_custom_list, from, to);
			} else {
				adapter = new SpecialAdapterForSchedule(getApplicationContext(), fillMaps, R.layout.schedule_custom_listnote3, from, to);

			}
			lstSchedule.setAdapter(adapter);


		} else {
			// else should show NO RECORD FOUND
			DisplayUtil du = new DisplayUtil();
			lstSchedule.setAdapter(du.showNoRecordFound(getApplicationContext()));
		}


		//display the number of items in the list
		numberOfRowItems = fillMaps.size() == 0 ? ar.size() : fillMaps.size();
		if (screenInches>=6) {
			txtNumOfRecords.setText("Number Of Records Found: " + String.valueOf(numberOfRowItems));
		} else {
			txtNumOfRecords.setText(String.valueOf(numberOfRowItems));
		}
	}


	private void filterList() {
		ScheduleBO bo = new ScheduleBO(ScheduleView.this);
		useDateFilter = false;
			// Date Filter
		testDate = lblSchDate.getText().toString();
		if (testDate.equals("Date"))
			testDate = "";

		if (!"".equals(testDate)) {
			System.out.println("Selected date is " + testDate);
			useDateFilter = true;
		}

		// Status Fiilter
		selectedStatus = (DropdownItemVO) btnStatus.getSelectedItem();
		System.out.println("Selected Item is " + selectedStatus.getItemId() + "-" + selectedStatus.getItemDescription());

		selectedStatusId = 0;
		// added for demo 20/08/2011 llm

		if (passedInStatus != 0) {
			if (passedInStatus == 99)// for open & in progress status
			{
				btnStatus.setSelection(0);
			} else {
				btnStatus.setSelection(passedInStatus);
			}
		}
		// added for demo 20/08/2011 llm
		if (selectedStatus != null)
			selectedStatusId = Integer.parseInt(selectedStatus.getItemId());

		location = "";
		location= lblSchLocation.getText().toString();


		if (location.equals("Location"))
		{
			location = "";
		}

		lvo= new LocationVO();
		lvo.setAreaId(Integer.toString(spnAreaid));
		lvo.setLevelId(Integer.toString(spnLevelid));
		lvo.setZoneId(Integer.toString(spnZoneid));
		lvo.setTerminalId(spnTerminalid);
		lvo.setLocationId(Integer.toString(spnLocationId));

//			try {
//			scheduleList = bo.getMonthlyMeterScheduleByCriteria(useDateFilter, testDate, selectedStatusId, location,lvo);
//			populateList2(scheduleList);


		//Retrofit Builder
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(AppsConstant.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		WebServiceClient webServiceClient = retrofit.create(WebServiceClient.class);
		Call<ArrayList<ScheduleList2VO>> callServiceHistory = webServiceClient.getServiceHistory(main.strCurrentUserId,"1");
		Call<ArrayList<MeterReading2VO>> callMeterReading = webServiceClient.getReadingHistory(main.strCurrentUserId);
		meterReading2VO = new MeterReading2VO();


		callMeterReading.enqueue(new Callback<ArrayList<MeterReading2VO>>() {
			@Override
			public void onResponse(Call<ArrayList<MeterReading2VO>> call, Response<ArrayList<MeterReading2VO>> response) {
				meterReading2VO = new MeterReading2VO();
				if(response.body() != null && response.body().size() > 0) {
					meterReading2VO = response.body().get(0);
				}
			}

			@Override
			public void onFailure(Call<ArrayList<MeterReading2VO>> call, Throwable t) {
				try {
					if(getDatabaseHelper().isOpen()) {
						meterReading2VO = new MeterReading2VO();
						meterReadingDAO = getDatabaseHelper().getMeterReading2VO();
						if(meterReadingDAO.countOf() > 0) {
							meterReading2VO = meterReadingDAO.queryForAll().get(0);
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (java.lang.IllegalStateException e) {
					e.printStackTrace();
				}
			}
		});


		callServiceHistory.enqueue(new Callback<ArrayList<ScheduleList2VO>>() {
			@Override
			public void onResponse(Call<ArrayList<ScheduleList2VO>> call, Response<ArrayList<ScheduleList2VO>> response) {
				try {
					if(response.body() != null) {
						QueryBuilder<ScheduleList2VO, Integer> scheduleListQueryBuilder = scheduleListDao.queryBuilder();
						List<ScheduleList2VO> apiList = response.body();
						for(ScheduleList2VO vo : apiList) {
							if(scheduleListQueryBuilder.where().eq("checklistResult", vo.getChecklistResult()).countOf() == 0) {
								scheduleListDao.create(vo);
							}
						}
						populateListFromDB();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (java.lang.IllegalStateException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<ArrayList<ScheduleList2VO>> call, Throwable t) {
			}
		});



			passedInStatus = 0;

//		} catch (ULjException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	private void populateListFromDB() {
		try {
			scheduleListDao = getDatabaseHelper().getScheduleListDAO();
            scheduleList = new ArrayList();

            scheduleList.addAll(scheduleListDao.queryForAll());
            fillMaps.clear();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
		populateList(scheduleList, testDate, selectedStatusId, location, meterReading2VO);
		promptProgressDialogWithDelay();
	}

	class ScheduleListItemOnItemClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView adapterView, View view, int position, long id) {
			// TODO Auto-generated method stub

			Log.d(TAG, "Item clicked " + position + "getting the schedule object");
			if (scheduleList != null && scheduleList.size() > 0) {
				ScheduleList2VO vo = (ScheduleList2VO) scheduleList.get(position);
				DateUtil dUtil = new DateUtil();
				if (vo != null) {
					Bundle myData = new Bundle();
					try {

						Log.d(TAG, "Location " + vo.getServiceMaintProgDescription() + "Person in charge " + vo.getPersonInChargeID());
						// a.ShowChecklist(resultID, templateID, templateVersion,
						// buildingID, scannedBarcode)

						myData.putInt(AppsConstant.INTENT_DATA_RESULT_ID, Integer.parseInt(vo.getChecklistResult()));
						myData.putInt(AppsConstant.INTENT_DATA_TEMPLATE_ID, Integer.parseInt(vo.getChecklistResult()));
						myData.putInt(AppsConstant.INTENT_DATA_BUILDING_ID, Integer.parseInt(vo.getLocationID()));
						myData.putInt(AppsConstant.INTENT_DATA_NOTICE_STATUS, vo.getStatusInt());
						myData.putString(AppsConstant.INTENT_DATA_REMARKS, vo.getRemarks());
						myData.putString(AppsConstant.INTENT_DATA_NOTICE_DATE, dUtil.timeStamp2dateString(dUtil.dateString2Calendar(vo.getScheduledStart(), "yyyy-MM-dd'T'HH:mm:ss")));
						myData.putString(AppsConstant.INTENT_DATA_SCANNED_BARCODE, scannedBarcode);
						myData.putInt(AppsConstant.INTENT_DATA_STATUS_ID, vo.getStatusInt());
						//commercial-outlet checklist

						myData.putString(AppsConstant.INTENT_DATA_TENANT_NAME, vo.getServiceMaintProgDescription());

						//commercial-terminal checklist
						myData.putInt(AppsConstant.INTENT_DATA_SERVICEID, Integer.parseInt(vo.getServiceID()));
						myData.putParcelable(AppsConstant.INTENT_DATA_SCHEDULE, vo);
					} catch (ParseException pe) {

					}
//					catch (NumberFormatException ne) {
//
//					}
					Intent scheduleIntent = new Intent();

					scheduleIntent = new Intent(ScheduleView.this, MonthlyMeterView.class);

					scheduleIntent.putExtras(myData);
					startActivity(scheduleIntent);
				}
			}
		}
	}

	private class DateReset implements OnClickListener {
		public void onClick(View v) {

			lblSchDate.setText("Date");
			filterList();
			populateListFromDB();

		}
	}

	private class StatusReset implements OnClickListener {
		public void onClick(View v) {

			lblSchStatus.setText("Status");
			btnStatus.setSelection(0);
			filterList();
			populateListFromDB();


		}
	}

	private class LocationReset implements OnClickListener {
		public void onClick(View v) {

			lblSchLocation.setText("Location");
			
			lblLocation.setText("Location \n(Filter)");
			spnTerminalid =0;
			spnLevelid=0;
			spnAreaid=0;
			spnZoneid=0;
			spnLocationId=0;
			
			filterList();
			populateListFromDB();


		}
	}

	private class dologout implements OnClickListener {
		public void onClick(View v) {
			try {
				Intent myIntent = new Intent(ScheduleView.this, main.class);
				startActivity(myIntent);
			} catch (Exception e) {
				Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
	}

	private class gotoHome implements OnClickListener {
		public void onClick(View v) {
			try {
				finish();

			} catch (Exception e) {
				Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
	}

	private class showinfo implements OnClickListener {
		public void onClick(View v) {
			try {
				AlertDialog diaBox = makeAndShowDialogBox("Mobile Inspection System", "Copyright 2011\n" + "All rights reserved\n" + "Surbana Technologies Pte Ltd\n"
						+ "168 Jln Bukit Merah\n" + "#01-01 Surbana One\n" + "Singapore 150168\n" + "Tel: +65 6248 1540");
				diaBox.show();

			} catch (Exception e) {
				Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
	}

	private AlertDialog makeAndShowDialogBox(String sTitle, String sMsg) {

		AlertDialog myQuittingDialogBox =

		new AlertDialog.Builder(this)
		// set message, title, and icon
				.setTitle(sTitle).setMessage(sMsg)

				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// whatever should be done when answering "NO" goes here
						// msg = "Cancel " + Integer.toString(whichButton);
						// txtMsg.setText(msg);
					}
				})

				.create();

		return myQuittingDialogBox;
	}

	@Override
	protected void onResume() {
		// ScheduleBO bo = new ScheduleBO(ScheduleView.this);
		// boolean useDateFilter = false;
		// try {
		// System.out.println((new Date()).toString());
		//
		// scheduleList = bo.getScheduleByCriteria(useDateFilter, "s", 0, null);
		// populateList(scheduleList );
		//
		// System.out.println((new Date()).toString());
		//
		// } catch (ULjException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		super.onResume();
		filterList();
		populateListFromDB();
	}

	
	private class DialogOnClickListener implements DialogInterface.OnClickListener {
		int command = 0; // 1-Yes , 2-No

		public DialogOnClickListener(int command) {
			this.command = command;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			// direct to ChecklistView.java if user click "yes": chin hua (09
			// feb 2012)
			if (command == 1) {

				int position = 0;

				Log.d(TAG, "Item clicked " + position + "getting the schedule object");
				if (scheduleList != null && scheduleList.size() > 0) {
					ScheduleListVO vo = (ScheduleListVO) scheduleList.get(position);
					DateUtil dUtil = new DateUtil();
					if (vo != null) {
						Log.d(TAG, "Location " + vo.getLocation() + "Person in charge " + vo.getPersonInChargeId());

						Bundle myData = new Bundle();
						try {
							myData.putInt(AppsConstant.INTENT_DATA_RESULT_ID, vo.getChecklistResultId());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_TEMPLATE_ID, vo.getChecklistTemplateId());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_TEMPLATE_VERS, vo.getChecklistTemplateVersion());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_BUILDING_ID, vo.getBuildingId());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_TERMINAL_ID, vo.getTerminalid());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_NOTICE_ID, vo.getNoticeid());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_NOTICE_STATUS, vo.getNoticeStatus());
						} catch (Exception e) {
						}
						try {
							myData.putString(AppsConstant.INTENT_DATA_REMARKS, vo.getRemarks());
						} catch (Exception e) {
						}
						try {
							myData.putString(AppsConstant.INTENT_DATA_NOTICE_DATE, dUtil.timeStamp2dateString(vo.getNoticedate()));
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_NOTICE_INSP_ID, vo.getNoticeinspid());
						} catch (Exception e) {
						}
						try {
							myData.putString(AppsConstant.INTENT_DATA_SCANNED_BARCODE, scannedBarcode);
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_STATUS_ID, vo.getStatusId());
						} catch (Exception e) {
						}
						try {
							myData.putInt(AppsConstant.INTENT_DATA_TENANT_ID, vo.getTenantId());
						} catch (Exception e) {
						}
							//commercial-outlet checklist
						try {myData.putString(AppsConstant.INTENT_DATA_TENANT_NAME,vo.getServiceDescription());} catch (Exception e) {}
						try {
							myData.putString("oneRecFlag", oneRecFlag);
						} catch (Exception e) {
						}

//						Intent scheduleIntent = new Intent();
////						
//						
//							
//						scheduleIntent = new Intent(ScheduleView.this, ChecklistView.class);
//							
//						scheduleIntent.putExtras(myData);
//						startActivity(scheduleIntent);
					}
				}
			} else
				dialog.cancel();
		}
	}
		// llm 2/1/13 to add ssh record from handheld for commercial user
	private ArrayList<DropdownItemVO> getCommercialCheckListName() {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		ScheduleDAO tDao = new ScheduleDAO();
		try {
			list = tDao.getCommercialChecListName();
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	

	//llm 03/05/2013 new table for monthly meter delay sync
	public void doSyncMM(View v)
	{
		SynchronizationUtils syncUtil = new SynchronizationUtils();
		try {
			syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_UPDATE_MONTHLY_METER, AppsConstant.SUB_VERS_MONTHLY_METER_UPLOAD,
					false, DbUtil.getConnection(),ScheduleView.this, processNewIdHandler);

		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}
	
	Handler processNewIdHandler = new Handler() {
		public void handleMessage(Message msg) {
			Bundle bun;
			bun = msg.getData();
			if (bun != null) {

				if (bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != 0 && bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != AppsConstant.HANDLER_VALUE_STATUS_ERROR) {
						
					gotoToday();
					
				}
			}
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		if(databaseHelper != null) {
			getDatabaseHelper().close();
			databaseHelper = null;
		}
	}


	private DatabaseHelper getDatabaseHelper() {
		if(databaseHelper == null) {
			databaseHelper = DatabaseHelper.getHelper(this);
		}
		return databaseHelper;
	}

	//llm 03/05/2013 new table for monthly meter delay sync
}