package sg.com.surbana.acs.einsp.util;

import android.net.Uri;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sg.com.surbana.acs.einsp.vo.Image2VO;
import sg.com.surbana.acs.einsp.vo.ImageSendDataVO;
import sg.com.surbana.acs.einsp.vo.ImageTestBodyVO;
import sg.com.surbana.acs.einsp.vo.MeterReading2VO;
import sg.com.surbana.acs.einsp.vo.MeterSendVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleList2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleListSendVO;

import static android.R.attr.name;

/**
 * Created by ALRED on 3/18/2017.
 */

public interface WebServiceClient {

    @GET("webapi/api/ASC/tblEngServiceServiceHistory_download_cursor")
    Call<ArrayList<ScheduleList2VO>> getServiceHistory(@Query("userID") String userID,
                                                       @Query("downloadType") String downloadType);

    @GET("webapi/api/ASC/tblUtilityMeterReadingImages_download_cursor")
    Call<ArrayList<Image2VO>> getImageReading(@Query("userID") String userID);

    @GET("webapi/api/ASC/tblUtilityMeterReadingHistory_download_cursor")
    Call<ArrayList<MeterReading2VO>> getReadingHistory(@Query("userID") String userID);

    @GET("webapi/api/ASC/tblEngServiceServiceHistory_upload_update")
    Call<ScheduleListSendVO> saveServiceHistory(@Query("jsonResponse") String scheduleListSend2VO);

    @GET("webapi/api/ASC/ValidateMontlyMeter")
    Call<MeterSendVO> isExistingMonthlyMeterNo(@Query("monthlymeterno") String monthlymeterno,
                                               @Query("readingno") String readingno,
                                               @Query("meterno") String meterno,
                                               @Query("checklistResultId") String checklistResultId);

    @GET("webapi/api/ASC/tblUtilityMeterReadingHistory_upload_update")
    Call<MeterSendVO> updateReadingHistory(@Query("jsonResponse") String meterReading2VO);

    @GET("/webapi/api/ASC/tblMstUtilityMeterList_download_cursor")
    Call<ArrayList<MonthlyMeterVO>> getMeterList(@Query("userID") String userId);

    @GET("webapi/api/ASC/tblMonthlyMeterImages_upload_insert_UtilityMeterReadingHistory")
    Call<MeterSendVO> insertReadingHistory(@Query("jsonResponse") String meterReading2VO);

    @Multipart
    @POST("api/VehicleInformationSummary/PostViolationImages")
    Call<ImageSendDataVO> sendImageData(@Part MultipartBody.Part image);

    @Multipart
    @POST("api/VehicleInformationSummary/PostViolationImages")
    Call<ImageSendDataVO> sendImageData(@Part(value = "1", encoding = "7-bit") RequestBody image);
}
