package sg.com.surbana.acs.einsp.bo;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import sg.com.surbana.acs.einsp.dao.MonthlyMeterDAO;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterImagesVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import android.content.Context;
import android.util.Log;
import com.ianywhere.ultralitejni12.ULjException;


/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Mar 15, 2012		Ryan			Created
 *
 */
public class MonthlyMeterBO {
	private final String TAG = "MonthlyMeterBO.java";
	private Context context;
	
	public ArrayList<MonthlyMeterVO> getMeters(int locationid) throws ULjException {
		MonthlyMeterDAO dao = new MonthlyMeterDAO();

		try {
			ArrayList<MonthlyMeterVO> meterlist = dao.searchMetersbyLocationId(locationid);

			return meterlist;
		} catch (ULjException e) {
			Log.e(TAG, "Error in retrieving master meters ", e);
			e.printStackTrace();
			throw e;
		}
	}
	
	public MonthlyMeterVO getMetersReadingHistory(int meterno, int checklistresultid) throws ULjException {
		MonthlyMeterDAO dao = new MonthlyMeterDAO();

		try {
			MonthlyMeterVO vo = dao.searchMetersReadingHistory(meterno, checklistresultid);

			return vo;
		} catch (ULjException e) {
			Log.e(TAG, "Error in retrieving master meters ", e);
			e.printStackTrace();
			throw e;
		}
	}
	
	
	public MonthlyMeterVO getPrevMetersReadingHistory(int meterno) throws ULjException {
		MonthlyMeterDAO dao = new MonthlyMeterDAO();

		try {
			MonthlyMeterVO vo = dao.searchPreviousMetersReadingHistory(meterno);

			return vo;
		} catch (ULjException e) {
			Log.e(TAG, "Error in retrieving master meters ", e);
			e.printStackTrace();
			throw e;
		}
	}
	
	public boolean canCheckBeDoneToday(Date revisedStart, int checkFrequency) throws ULjException {

		DisplayUtil dispUtil = new DisplayUtil();
		DateUtil dateUtil = new DateUtil();

		int dateDifference = -1;
		Date currentDate = dateUtil.getCurrentDate();

		if (revisedStart != null && currentDate != null) {
			dateDifference = (int) ((currentDate.getTime() - revisedStart.getTime()) / (1000 * 60 * 60 * 24));
		}

		// if (dateDifference <0){ //future date
		// dispUtil.displayAlertDialog(context, AppsConstant.MESSAGE_VALIDATION,
		// AppsConstant.MESSAGE_JOB_NOT_SCHEDULED_FOR_TODAY);
		// return false;
		// }else
		if ((checkFrequency == 1) && (dateDifference > 1)) { // daily check
			dispUtil.displayAlertDialog(context, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_IS_PAST_CHECK);
			return false;
		} else if ((checkFrequency == 2) && (dateDifference > 7)) //llm 25/07/2013 add 1 more day
		{ // weekly check
			dispUtil.displayAlertDialog(context, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_IS_PAST_CHECK);
			return false;
		} else if ((checkFrequency == 3) && (dateDifference > 14)) //llm 25/07/2013 add 1 more day
		{ // fortnightly check
			dispUtil.displayAlertDialog(context, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_IS_PAST_CHECK);
			return false;
		} else if ((checkFrequency == 4)
				&& (currentDate.getMonth() >= revisedStart.getMonth() && currentDate.getYear() == revisedStart.getYear() && revisedStart.getDate() < currentDate
						.getDate())) { // monthly check
			dispUtil.displayAlertDialog(context, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_IS_PAST_CHECK);
			return false;
		}
		
		
		
		return true;

	}
	
	public MonthlyMeterBO(Context con) {
		context = con;
	}
	public ScheduleListVO getScheduleByResultId(int resultId) throws ULjException, ParseException {
		MonthlyMeterDAO scheduleDao = new MonthlyMeterDAO();
		ScheduleListVO vo;

		try {
			vo = scheduleDao.searchMonthlyMeterScheduleByChecklistResultId(resultId);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in retrieving Schedule By Criteria", e);
			e.printStackTrace();
			throw e;
		}
		return vo;
	}
	
	public void createNewImage(MonthlyMeterImagesVO vo, int i) throws ULjException {
		MonthlyMeterDAO mDao = new MonthlyMeterDAO();

		Log.d(TAG, "create new fault image - start");
		String newFaultImgId;
		String newImgFileName;
		try {
			DateUtil dUtil = new DateUtil();
			// IMEI + _ + TERMINAL + _ +DATE(MMDDYYYY)+ _ +noticeid+_+
			// checklistitemid + _+i
			// newFaultImgId = main.strDeviceID
			// + "_"+vo.getImageVO()+"_"
			// + dUtil.getCurrentTimeStamp().toString().replace("-", "")
			// .replace(".", "").replace(":", "").replace(" ", "")
			// .trim().substring(0,8)+
			// "_"+vo.getNoticeId()+"_"+vo.getChecklistItemId()+"_"+Integer.toString(i);

			newFaultImgId = vo.getMonthlyMeterNo() + "_"+vo.getReadingNo()+"_" + 
			Integer.toString(i)+"_"+
			dUtil.getCurrentTimeStamp().toString().replace("-", "").replace(".", "").replace(":", "").replace(" ", "");
			Log.d(TAG, "New monthly meter Images ID = " + newFaultImgId);

			if (vo.getImageFileName() == "gotimage") {
				newImgFileName = newFaultImgId + ".JPG";
				Log.d(TAG, "New Image filename is " + newImgFileName);
				vo.setImageFileName(newImgFileName);
			} else {
				vo.setImageFileName(null);
			}

			mDao.insertPhoto(vo);

		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		Log.d(TAG, "create new fault image - end");
	}
	
	public void completeCheck(int resultId, Timestamp endTime, String remarks, Timestamp currentTime) throws ULjException {

		// update service history
		MonthlyMeterDAO mDao = new MonthlyMeterDAO();
		ScheduleListVO scheduleVo = new ScheduleListVO();
		scheduleVo.setActualEnd(endTime);
		scheduleVo.setStatusId(AppsConstant.CHECKLIST_STATUS_COMPLETED);
		scheduleVo.setRemarks("");
									
		scheduleVo.setChecklistResultId(resultId);
		scheduleVo.setLastModifiedDate(currentTime);

		mDao.updateScheduleTime(scheduleVo, AppsConstant.CHECKLIST_UPDATE_ENDTIME);
	}
}
