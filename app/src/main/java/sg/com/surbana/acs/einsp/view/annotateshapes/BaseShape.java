package sg.com.surbana.acs.einsp.view.annotateshapes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Aug 16, 2011		Ryan			Created
 *
 */
public abstract class BaseShape {
	private final String TAG = "AbstractShape.java";
	public float x;
	public float y;

	float xscale = 1;
	float yscale = 1;
	float rscale = 1;

	Canvas shpCanvas;
	Path shpPath;
	Paint shpPaint;
	float rotationAngle = 0;

	BaseShape() {
		shpPath = new Path();

	}

	BaseShape(Canvas can, Paint paint) {
		shpCanvas = can;
		shpPath = new Path();
	}

	public void setWidthAndHeightScale(float widthScale, float heightScale) {
		xscale = xscale * widthScale;
		yscale = yscale * heightScale;
	}

	public void setRadiusScale(float radiusScale) {
		rscale = rscale * radiusScale;
		// System.out.println("New scale is " + rscale);
	}

	public void rotate(float angle) {
		rotationAngle = angle;
		drawMe();
	}

	public void rotateCW() {
		rotationAngle -= 5;
		drawMe();
	}

	public void rotateCCW() {
		rotationAngle += 5;
		drawMe();
	}

	public Path getPath() {
		return shpPath;
	}

	public abstract void drawMe(Canvas can, float touchX, float touchY);

	public abstract void drawMe(Canvas can);

	public abstract void drawMe();

	public void setPaint(Paint p) {
		shpPaint = p;
	}
}
