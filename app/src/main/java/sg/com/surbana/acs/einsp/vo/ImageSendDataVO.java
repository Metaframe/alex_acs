package sg.com.surbana.acs.einsp.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ALRED on 3/29/2017.
 */

@DatabaseTable(tableName = "ImageSendDataVO")
public class ImageSendDataVO implements Serializable {
    @DatabaseField
    @SerializedName("TerminalID")
    @Expose
    private Integer terminalID;
    @DatabaseField(id = true)
    @SerializedName("MonthlyMeterNo")
    @Expose
    private Integer monthlyMeterNo;
    @DatabaseField
    @SerializedName("ReadingNo")
    @Expose
    private Integer readingNo;
    @DatabaseField
    private String sImageFileName;
    @DatabaseField
    @SerializedName("dateLastModifiedDate")
    @Expose
    private String dateLastModifiedDate;
    @DatabaseField
    @SerializedName("intLastModifiedByUserInfoID")
    @Expose
    private int intLastModifiedByUserInfoID;
    @DatabaseField
    @SerializedName("fAltitude")
    @Expose
    private Double fAltitude;
    @DatabaseField
    @SerializedName("fLongtitude")
    @Expose
    private Double fLongtitude;
    @DatabaseField
    private String sImageCaption;

    @DatabaseField
    private boolean isOffline;

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    public Integer getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(Integer terminalID) {
        this.terminalID = terminalID;
    }

    public Integer getMonthlyMeterNo() {
        return monthlyMeterNo;
    }

    public void setMonthlyMeterNo(Integer monthlyMeterNo) {
        this.monthlyMeterNo = monthlyMeterNo;
    }

    public Integer getReadingNo() {
        return readingNo;
    }

    public void setReadingNo(Integer readingNo) {
        this.readingNo = readingNo;
    }

    public String getSImageFileName() {
        return sImageFileName;
    }

    public void setSImageFileName(String sImageFileName) {
        this.sImageFileName = sImageFileName;
    }

    public String getDateLastModifiedDate() {
        return dateLastModifiedDate;
    }

    public void setDateLastModifiedDate(String dateLastModifiedDate) {
        this.dateLastModifiedDate = dateLastModifiedDate;
    }

    public int getIntLastModifiedByUserInfoID() {
        return intLastModifiedByUserInfoID;
    }

    public void setIntLastModifiedByUserInfoID(int intLastModifiedByUserInfoID) {
        this.intLastModifiedByUserInfoID = intLastModifiedByUserInfoID;
    }

    public Double getFAltitude() {
        return fAltitude;
    }

    public void setFAltitude(Double fAltitude) {
        this.fAltitude = fAltitude;
    }

    public Double getFLongtitude() {
        return fLongtitude;
    }

    public void setFLongtitude(Double fLongtitude) {
        this.fLongtitude = fLongtitude;
    }

    public String getSImageCaption() {
        return sImageCaption;
    }

    public void setSImageCaption(String sImageCaption) {
        this.sImageCaption = sImageCaption;
    }

}
