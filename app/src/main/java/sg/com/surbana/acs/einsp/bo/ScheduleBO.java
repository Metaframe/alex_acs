package sg.com.surbana.acs.einsp.bo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import sg.com.surbana.acs.einsp.dao.ScheduleDAO;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import android.content.Context;
import android.util.Log;

import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 13, 2011		Ryan			Created
 *
 */
public class ScheduleBO {
	private final String TAG = "ScheduleBO.java";
	private Context context;

	public ScheduleBO(Context con) {
		context = con;
	}

	public ArrayList<ScheduleListVO> getScheduleByCriteria(boolean useDateFilter, String strDate, int statusId, String buildingId, LocationVO lvo) throws ULjException,
			ParseException {

		ScheduleDAO scheduleDao = new ScheduleDAO();
		ArrayList<ScheduleListVO> scheduleList = null;
		;
		DateUtil dUtil = new DateUtil();

		Date date = null;
		if (useDateFilter)
			date = dUtil.dateString2Calendar(strDate, DateUtil.DATEFORMAT_DD_MON_YYYY);// String
																						// format
																						// e.g.
																						// 01-JUL-2011

		try {
			// added for demo llm 25/08/2011
			if (statusId == 99) {
				scheduleList = scheduleDao.searchScheduleExcludeStatus(statusId, date, useDateFilter, buildingId);
			} else
			// added for demo llm 25/08/2011
			{
				if (buildingId.length()>0)
				{
					//filter by scan location button
					scheduleList = scheduleDao.searchScheduleByCriteria(statusId, date, useDateFilter, buildingId);
				}
				else
				{
					//filter by drop down location button
					scheduleList = scheduleDao.searchScheduleByCriteria2(statusId, date, useDateFilter, lvo);
				}
			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in retrieving Schedule list", e);
			e.printStackTrace();
			throw e;
		}
		return scheduleList;
	}

	public ArrayList<ScheduleListVO> getMonthlyMeterScheduleByCriteria(boolean useDateFilter, String strDate, int statusId, String buildingId, LocationVO lvo)
	throws ULjException, ParseException {

	ScheduleDAO scheduleDao = new ScheduleDAO();
	ArrayList<ScheduleListVO> scheduleList = null;
	
	DateUtil dUtil = new DateUtil();
	
	Date date = null;
	if (useDateFilter)
		date = dUtil.dateString2Calendar(strDate, DateUtil.DATEFORMAT_DD_MON_YYYY);// String
																					// 01-JUL-2011
	
	try {
		if (buildingId.length()>0)
		{	//filter by scan location button
			scheduleList = scheduleDao.searchMonthlyMeterScheduleByCriteria(statusId, date, useDateFilter, buildingId);
		}
		else
		{
			//filter by drop down location button
			scheduleList = scheduleDao.searchMonthlyMeterScheduleByCriteria2(statusId, date, useDateFilter, lvo);
		}	
	} catch (ULjException e) {
		// TODO Auto-generated catch block
		Log.e(TAG, "Error in retrieving Schedule list for monthly meter", e);
		e.printStackTrace();
		throw e;
	}
	return scheduleList;
	}
}
