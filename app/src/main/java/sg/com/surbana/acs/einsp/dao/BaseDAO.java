package sg.com.surbana.acs.einsp.dao;

import java.util.Date;

import android.util.Log;

import com.ianywhere.ultralitejni12.PreparedStatement;
import com.ianywhere.ultralitejni12.ResultSet;
import com.ianywhere.ultralitejni12.ULjException;

public class BaseDAO {
	private final String TAG = "BaseDAO.java";

	public String formatString(String s) {
		if (s == null)
			return "";
		else
			return s;
	}

	public boolean isNotNullOrEmpty(String s) {
		if (s == null || "".equals(s))
			return false;
		else
			return true;

	}

	public boolean isNotNullOrEmpty(Date s) {
		if (s == null || "".equals(s))
			return false;
		else
			return true;

	}

	protected void closeResultSet(ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (ULjException e) {
				Log.e(TAG, "Error in closing result set", e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	protected void closePreparedStatement(PreparedStatement ps) {
		if (ps != null)
			try {
				ps.close();
			} catch (ULjException e) {
				Log.e(TAG, "Error in closing Prepared Statement", e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
