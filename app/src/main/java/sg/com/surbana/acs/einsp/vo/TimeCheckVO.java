package sg.com.surbana.acs.einsp.vo;

import java.util.Date;
 

public class TimeCheckVO {
	private Date timeStart;
	private Date timeEnd;
	public Date getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}
	public Date getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}
	 
}
