package sg.com.surbana.acs.einsp.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.view.HomeView;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.util.Log;
import android.widget.RemoteViews;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *05 May 2012		Ryan			adopt callback handler mechanism to allow caller function to decide 'post' sync process
 */
public class UpdateThread extends Thread {
	private final String TAG = "UpdateThread";

	public Handler mHandler;
	String updateUrl;
	String versionUrl;
	String downloadedFileName;
	NotificationManager notificationManager;
	Notification notification;
	int notificationId;
	Intent nextScreen;
	Context context;
	String filePath;
	int currentAppVersionNumber = 0;
	int newAppVersionNumber = 0;
	String currentAppVersionName;
	String newAppVersionName;
	// private static boolean IS_DOWNLOADING_UPDATE=false;
	private final int NOTIFICATIONID = 1002;

	public UpdateThread(Context appContext, String url, String versionUrlPath, String fileName, String targetfilePath, Handler hand) {

		context = appContext;
		updateUrl = url;
		versionUrl = versionUrlPath;
		downloadedFileName = fileName;
		filePath = targetfilePath;
		mHandler = hand;
	}

	private void getCurrentVersion() {
		String pkg = context.getPackageName();
		try {
			currentAppVersionName = context.getPackageManager().getPackageInfo(pkg, 0).versionName;
			currentAppVersionNumber = context.getPackageManager().getPackageInfo(pkg, 0).versionCode;
			System.out.println("Current Version Name is " + currentAppVersionName);
			System.out.println("Current Version Code is " + currentAppVersionNumber);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean checkForUpdate() {
		boolean updateFound = false;
		getCurrentVersion();
		GetVersionFromServer();
		// only download if it is not currently downloading and the new app version is greater than the old one
		if (newAppVersionNumber > currentAppVersionNumber) {
			updateFound = true;
		}
		return updateFound;
	}

	public void GetVersionFromServer() {
		// versionUrl : this is the file you want to download from the remote server
		// version.txt contain Version Code = 2; \n Version name = 2.1;
		System.out.println("===============Getting version info from Web  " + versionUrl);

		URL u;
		try {
			u = new URL(versionUrl.toString());
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			// c.setDoOutput(true);
			c.connect();
			InputStream in = c.getInputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = in.read(buffer)) != -1) {
				baos.write(buffer, 0, len1); // Write Into ByteArrayOutputStream Buffer.
			}
			String temp = "";
			String s = baos.toString();// baos.toString(); contain Version Code = 2; \n Version name = 2.1;
			for (int i = 0; i < s.length(); i++) {
				i = s.indexOf("=") + 1;
				while (s.charAt(i) == ' ') // Skip Spaces
				{
					i++; // Move to Next.
				}
				while (s.charAt(i) != ';' && (s.charAt(i) >= '0' && s.charAt(i) <= '9' || s.charAt(i) == '.')) {
					temp = temp.toString().concat(Character.toString(s.charAt(i)));
					i++;
				}
				//
				s = s.substring(i); // Move to Next to Process.!
				temp = temp + " "; // Separate w.r.t Space Version Code and Version Name.
			}
			String[] fields = temp.split(" ");// Make Array for Version Code and Version Name.

			newAppVersionNumber = Integer.parseInt(fields[0].toString());// .ToString() Return String Value.
			newAppVersionName = fields[1].toString();
			System.out.println("New Version Code is " + newAppVersionNumber);
			System.out.println("New Version Name is " + newAppVersionName);
			baos.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return true;
	}// Method End.

	public void run() {
		boolean isUpdateFound = checkForUpdate();
		if (isUpdateFound) {
			try {
				// IS_DOWNLOADING_UPDATE=true;
				// notification = notificationBuilder(context);
				// main.NOTIFICATION_MANAGER.notify(NOTIFICATIONID, notification);
				URL url = new URL(updateUrl);
				HttpURLConnection c = (HttpURLConnection) url.openConnection();
				c.setRequestMethod("GET");
				// c.setDoOutput(true);
				c.connect();
				File file = new File(filePath);
				file.mkdirs();
				downloadedFileName = downloadedFileName + newAppVersionNumber + ".tmp";
				File outputFile = new File(file, downloadedFileName);
				FileOutputStream fos = new FileOutputStream(outputFile);
				InputStream is = c.getInputStream();
				byte[] buffer = new byte[1024];
				int len1 = 0;
				System.out.println("Total Byte " + c.getContentLength());
				long count = 0;
				while ((len1 = is.read(buffer)) != -1) {
					count += len1;
					// System.out.println("Progress "+ c.getContentLength() + "- " +count) ;
					fos.write(buffer, 0, len1);
					System.out.println("Progress " + (int) (count * 100 / c.getContentLength()) + " %");
					if ((int) (count * 100 / c.getContentLength()) == 100) {
						outputFile.renameTo(new File(file, downloadedFileName.split(".tmp")[0] + ".apk"));
					}
				}
				fos.close();
				is.close();// till here, it works fine - .apk is download to my sdcard in download file
				// TODO install at other thread
				// Intent intent = new Intent(Intent.ACTION_VIEW);
				// intent.setDataAndType(Uri.fromFile(new File(filePath + "/" + downloadedFileName)), "application/vnd.android.package-archive");
				// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				// context.startActivity(intent);
			} catch (IOException e) {
				// Toast.makeText(getApplicationContext(), "Update error!", Toast.LENGTH_LONG).show();
				Log.e(TAG, "", e);
			} finally {
				// IS_DOWNLOADING_UPDATE=false;
				if (notification != null) {
					notification.tickerText = "Mobile Inspection System version download completed";
					main.NOTIFICATION_MANAGER.notify(NOTIFICATIONID, notification);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						Log.e(TAG, "", e);
					}// sleep 3 second before removing the notificaiton
					main.NOTIFICATION_MANAGER.cancel(NOTIFICATIONID);
					System.out.println("==============================Cancelling notification " + notificationId);
				}
			}
		}
	}

	private Notification notificationBuilder(Context cont) {

		int icon = R.drawable.caglogo;
		String tickerText = " Mobile Inspection System update in progress...";
		long when = System.currentTimeMillis();
		Notification notification = new Notification(icon, tickerText, when);

		RemoteViews contentView = new RemoteViews(cont.getPackageName(), R.layout.custom_notification);
		contentView.setTextViewText(R.id.text, "Update Found, Downloading New Version of Mobile Inspection System.....");
		notification.contentView = contentView;

		Intent i = new Intent(cont, HomeView.class).setAction(Intent.ACTION_MAIN).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(cont, 0, i, 0);

		notification.contentIntent = contentIntent;

		return notification;
	}

}
