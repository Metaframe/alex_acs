package sg.com.surbana.acs.einsp.util;

import java.util.ArrayList;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *June 27, 2011		Lee Ming			Created
 *
 */
public class CustomArrayAdapterWithText extends ArrayAdapter {
	private final String TAG = "CustomArrayAdapterWithText.java";

	public CustomArrayAdapterWithText(Context context, ArrayList vo) {
		super(context, android.R.layout.simple_spinner_item, vo);
		setDropDownViewResource(R.layout.cust_spinner);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.cust_spinner_selectedtext, parent, false);
		}

		TextView desc = (TextView) convertView.findViewById(R.id.txtListValue);

		// Populate template
		DropdownItemVO data = (DropdownItemVO) getItem(position);
		desc.setText(data.getItemDescription());
		// desc.setText("");
		return convertView;
	}

	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.cust_spinner, parent, false);
		}

		LinearLayout l = (LinearLayout) convertView.findViewById(R.id.linearLayoutSpinner);
		if (position == 0) {
			l.setBackgroundColor(R.color.bgcolor_lightpurple);
		} else if (position % 2 == 0) {
			l.setBackgroundColor(android.graphics.Color.WHITE);
		} else {
			l.setBackgroundColor(android.graphics.Color.WHITE);
		}

		TextView desc = (TextView) convertView.findViewById(R.id.txtListValue);

		// Populate template
		DropdownItemVO data = (DropdownItemVO) getItem(position);
		desc.setText(data.getItemDescription());
		return convertView;
	}
}
