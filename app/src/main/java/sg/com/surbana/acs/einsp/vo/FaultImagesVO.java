package sg.com.surbana.acs.einsp.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *July 6, 2011		Lee Ming			Created
 *
 */
public class FaultImagesVO implements Serializable {
	private static final long serialVersionUID = -6114205509027782382L;
	private final String TAG = "FaultImagesVO.java";

	// For Fault Search Criteria
	private String faultDetailsImageId;
	private String faultId;
	private String imageFileName;
	private byte[] faultImage;
	private String imageCaption;
	private Timestamp lastUpdateDate;
	private int lastUpdatedBy;
	private float latitude;
	private float longitude;
	private int contrId;

	public String getFaultDetailsImageId() {
		return faultDetailsImageId;
	}

	public void setFaultDetailsImageId(String faultDetailsImageId) {
		this.faultDetailsImageId = faultDetailsImageId;
	}

	public String getFaultId() {
		return faultId;
	}

	public void setFaultId(String faultId) {
		this.faultId = faultId;
	}

	public byte[] getFaultImage() {
		return faultImage;
	}

	public void setFaultImage(byte[] faultImage) {
		this.faultImage = faultImage;
	}

	public String getImageCaption() {
		return imageCaption;
	}

	public void setImageCaption(String imageCaption) {
		this.imageCaption = imageCaption;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public int getContrId() {
		return contrId;
	}

	public void setContrId(int contrId) {
		this.contrId = contrId;
	}

}
