package sg.com.surbana.acs.einsp;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.bo.LoginBO;
import sg.com.surbana.acs.einsp.dao.LoginDAO;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.CommonFunction;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DbUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.util.SynchronizationUtils;
import sg.com.surbana.acs.einsp.util.UpdateThread;
import sg.com.surbana.acs.einsp.view.HomeView;
import sg.com.surbana.acs.einsp.vo.UserInfoVO;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *25 Jul 2011		Ryan			Added check for password upon login
 *27 Jul 2011		Ryan			Pause thread by 2 second for new login to allow password to be downloaded
 *28 Jul 2011		Ryan			Use common Menu
 * */

public class main extends Activity {
	/** Called when the activity is first created. */
	public static NotificationManager NOTIFICATION_MANAGER;
	private final String TAG = "main.java";
	public static String strDeviceID;
	public static String strCurrentUserId;
	public static int iCurrentUserGroupId;
	public static int iCurrentUserInfoId;
	public static String SessionId;
	// public final static boolean NO_SYNC=true; //used for development purpose
	// : to control whether or not the mobilink sync function will get executed
	public final static boolean NO_SYNC = true; // used for development purpose
													// : to control whether or
													// not the mobilink sync
													// function will get
													// executed
	int testCount = 0;

	private EditText etLoginId, etLoginPassword;
	private Button btnLogin;
	private boolean isUserExist = false;
	public static Resources CAG_RESOURCE;
	public static String WEBSERVER_ROOT = null;
	public static String WEBSERVER_IMAGEPATH = null;
	private boolean needRefresh = false;
	public static int TOP_VERSION = 0;

	int versionNumber = 0;
	public static final String DOWNLOAD_FILE_NAME = "eInpsVersion";	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		android.provider.Settings.System.putInt(getContentResolver(), android.provider.Settings.System.AUTO_TIME, 1);

		// juehua 20120229 for merge chin hua's tablet xml to leeming's code
		if (Build.MODEL.equalsIgnoreCase("GT-P7500")) {
			setContentView(R.layout.tablet_main);
		} else {
			setContentView(R.layout.main);
		}

		PackageInfo pinfo;

		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNumber = pinfo.versionCode;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String serName = Context.NOTIFICATION_SERVICE;
		DbUtil dbUtil = new DbUtil(main.this);
		DisplayUtil dsUtil = new DisplayUtil();
		try {
			dbUtil.initialize();
			CAG_RESOURCE = getResources();

		} catch (ULjException e1) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error when initializing Db", e1);
			dsUtil.displayException(this, "Error when initializing Db", e1);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error when initializing Db", e1);
			dsUtil.displayException(this, "Error when initializing Db", e1);
		}

		NOTIFICATION_MANAGER = (NotificationManager) getSystemService(serName);
		CommonFunction comFunc = new CommonFunction();
		comFunc.generateUniqueNumberBasedOnDateTime();
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if (telephonyManager.getDeviceId() == null)
			strDeviceID = "12345678";
		else
			strDeviceID = telephonyManager.getDeviceId().toString();

		etLoginId = (EditText) findViewById(R.id.login_name);
		etLoginPassword = (EditText) findViewById(R.id.login_password);

		btnLogin = (Button)findViewById(R.id.button1);
		
		comFunc = null;
	

	}

	public void doLogin(View v) {
		SynchronizationUtils syncUtil = new SynchronizationUtils();
		String sUserId, sPassword;
		boolean isDownloadOnly = true;

		sUserId = etLoginId.getText().toString();
		sPassword = etLoginPassword.getText().toString();
		syncUtil.setMbUserDetails(sUserId, sPassword);

		if ("".equals(sUserId) || "".equals(sPassword))
			showMessageDialog("Please Enter User ID and Password");
		else {
			btnLogin.setVisibility(View.INVISIBLE);
			doProcessToMainScreen();
		}

	}

	// Define handler
	public Handler callbackHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// check sync status
			int status = msg.getData().getInt(AppsConstant.HANDLER_KEY_STATUS);
			if (status == AppsConstant.HANDLER_VALUE_STATUS_OK)
				doProcessToMainScreen();

		}
	};

	private void doProcessToMainScreen() {
		LoginBO bo = new LoginBO();
		SynchronizationUtils syncUtil = new SynchronizationUtils();
		String sUserId, sPassword;

		boolean isDownloadOnly = true;
		Log.d(TAG, "In call back method process to main screen");

		// For demo purpose, load All
		CommonFunction comFun = new CommonFunction();
		DisplayUtil dUtil = new DisplayUtil();

		sUserId = etLoginId.getText().toString();
		sPassword = etLoginPassword.getText().toString();

		// Intent intentToGoAfterSync = new Intent(main.this, HomeView.class);
		boolean checkIfSameUser = false;
		checkIfSameUser = isSameUser();

		// juehua for auth imei. sample 359616042423665
		LoginBO loginBO = new LoginBO();
		Log.v(TAG, "imei: " + strDeviceID);
		Boolean isIMEIExist = false;
		try {
			isIMEIExist = loginBO.authenticateIMEI(strDeviceID);
			Log.v(TAG, "isIMEIExist: " + isIMEIExist);
		} catch (ULjException e1) {
			Log.e(TAG, "", e1);
		}

		try {
			try {
				syncUtil.setSyncHost(); //llm 18/07/2013
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!isUserExist || !checkIfSameUser || needRefresh) {
				if (!checkIfSameUser || needRefresh) {
					System.out.println("REFRESHINGGG DB");
					// if different user re-fresh db
					DbUtil dbUtil = new DbUtil();
					dbUtil.refreshNewDB();
					isUserExist = false;
				}

				System.out.println("Calling Init Function");
				syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_INIT, AppsConstant.SUB_VERS_INIT, isDownloadOnly,
						DbUtil.getConnection(), main.this, initCallBack);
			}
			// juehua for auth imei
			else if (!isIMEIExist) {
				Log.v(TAG, "imei no record do sync: " + strDeviceID);
				syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_INIT, AppsConstant.SUB_VERS_INIT, isDownloadOnly,
						DbUtil.getConnection(), main.this, initCallBack);
			}
			// /////////////////////////
			else {
				checkUserAuthentication();
			}
		} catch (Exception e) {
			btnLogin.setVisibility(View.VISIBLE);
			//llm 2015/01/20 show dialog box when mobilink down
			showMessageDialog("Unable to perform synchronization, please contact adminitrator if problem persist");
			Log.e(TAG, "", e);
		} 

	
	}

	private void checkUserAuthentication() {
		LoginBO bo = new LoginBO();
		System.out.println("Calling Check user Authentication Function");
		SynchronizationUtils syncUtil = new SynchronizationUtils();
		try {
			String sUserId, sPassword;
			sUserId = etLoginId.getText().toString();
			sPassword = etLoginPassword.getText().toString();
			
			System.out.println("Checking login for the first time");
			if (bo.authenticateUser(sUserId, sPassword)) {
				// if no user used the device before, trigger init sync to
				// download required data

				refreshMasterIfNeeded(sUserId);
	
				CommonFunction comFun = new CommonFunction();
				
				bo.doProcessIfUserExist(isUserExist, sUserId);

				strCurrentUserId = sUserId;
				UserInfoVO localUserInfoVO = new UserInfoVO();
				try {
					localUserInfoVO = comFun.getUserInfoById(main.strCurrentUserId);
					iCurrentUserInfoId = localUserInfoVO.getUserInfoId();
					iCurrentUserGroupId = localUserInfoVO.getUserGroupId();
				} catch (ULjException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Intent intentToGoAfterSync = new Intent(main.this, HomeView.class);
//				if (iCurrentUserGroupId == 1) {
//					intentToGoAfterSync = new Intent(main.this, DashboardView.class);
//				}
				intentToGoAfterSync.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

				localUserInfoVO = null;
				WEBSERVER_IMAGEPATH = comFun.getHandheldConfig(AppsConstant.CONFIG_IMAGE_WEB);
				WEBSERVER_ROOT = comFun.getHandheldConfig(AppsConstant.CONFIG_WEB_CONTEXT);
				//UPDATE_APK_URL = comFun.getHandheldConfig(AppsConstant.ENG_CONFIG_UPDATE_APK_URL);
				//UPDATE_VERSION_URL = comFun.getHandheldConfig(AppsConstant.ENG_CONFIG_UPDATE_VERSION_URL);

				// insert access log llm 23/11/12

				try {
					SessionId = comFun.getUniqueTimeStampWithDeviceId();
					comFun.clearAccessLogTable();
					comFun.insertAccessLog(versionNumber, "Login");
					syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_UPLOAD_ACCESS_LOG, AppsConstant.SUB_VERS_ACCESS_LOG_UPLOAD,
							false, DbUtil.getConnection(), main.this, null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// insert access log llm 23/11/12

				comFun = null;
				startActivity(intentToGoAfterSync);
			} else {

				System.out.println("Check login function");

				// authenticate fail, can be due to change of password,
				// re-download uid
				syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_CHECK_LOGIN, AppsConstant.SUB_VERS_CHECKLOGIN, true,
						DbUtil.getConnection(), main.this, checkLoginCallBack);

			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			showExceptionDialog("Error while autenticating user", e);
		}
	}

	Handler initCallBack = new Handler() {
		public void handleMessage(Message msg) {
			Bundle bun;
			bun = msg.getData();
			if (bun != null) {
				if (bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != 0 && bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != AppsConstant.HANDLER_VALUE_STATUS_ERROR) {
					// juehua for auth imei
					LoginBO loginBO = new LoginBO();
					try {
						if (!loginBO.authenticateIMEI(strDeviceID)) {
							showMessageDialog("Your device is not authorized to use this system");
							//checkUserAuthentication();
						} else {
							checkUserAuthentication();
						}
					} catch (ULjException e) {
						// should not catch error again
					}
					// ///////////////////////////////

					// checkUserAuthentication();
				} else {
					showMessageDialog("Unable to perform Init synchronization, please contact adminitrator if problem persist");
					//llm 2015/01/20 show button when mobilink down
					btnLogin.setVisibility(View.VISIBLE);
				}
			}
		}
	};

	Handler checkLoginCallBack = new Handler() {
		public void handleMessage(Message msg) {
			Bundle bun;
			bun = msg.getData();
			if (bun != null) {
				if (bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != 0 && bun.getInt(AppsConstant.HANDLER_KEY_STATUS) != AppsConstant.HANDLER_VALUE_STATUS_ERROR) {
					LoginBO bo = new LoginBO();

					try {
						String sUserId, sPassword;
						sUserId = etLoginId.getText().toString();
						sPassword = etLoginPassword.getText().toString();
						if (bo.authenticateUser(sUserId, sPassword)) {
							// if no user used the device before, trigger init
							// sync to
							// download required data

							refreshMasterIfNeeded(sUserId);

							Intent intentToGoAfterSync = new Intent(main.this, HomeView.class);
//							if (iCurrentUserGroupId == 1) {
//								intentToGoAfterSync = new Intent(main.this, DashboardView.class);
//							}

							CommonFunction comFun = new CommonFunction();
							intentToGoAfterSync.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							bo.doProcessIfUserExist(isUserExist, sUserId);

							strCurrentUserId = sUserId;
							// Commercial-outlet
							UserInfoVO localUserInfoVO = new UserInfoVO();
							try {
								localUserInfoVO = comFun.getUserInfoById(main.strCurrentUserId);
								iCurrentUserInfoId = localUserInfoVO.getUserInfoId();
								iCurrentUserGroupId = localUserInfoVO.getUserGroupId();
							} catch (ULjException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							localUserInfoVO = null;
							// iCurrentUserInfoId =
							// comFun.getUserinfoID(strCurrentUserId);
							WEBSERVER_IMAGEPATH = comFun.getHandheldConfig(AppsConstant.CONFIG_IMAGE_WEB);
							WEBSERVER_ROOT = comFun.getHandheldConfig(AppsConstant.CONFIG_WEB_CONTEXT);
//							UPDATE_APK_URL = comFun.getHandheldConfig(AppsConstant.CONFIG_UPDATE_APK_URL);
//							UPDATE_VERSION_URL = comFun.getHandheldConfig(AppsConstant.CONFIG_UPDATE_VERSION_URL);
							SynchronizationUtils syncUtil = new SynchronizationUtils();
							// insert access log llm 23/11/12
							try {
								SessionId = comFun.getUniqueTimeStampWithDeviceId();
								comFun.clearAccessLogTable();
								comFun.insertAccessLog(versionNumber, "Login");
								syncUtil.synchronizedDoSyncForVersionWithoutScreenTransition(AppsConstant.VERS_UPLOAD_ACCESS_LOG, AppsConstant.SUB_VERS_ACCESS_LOG_UPLOAD,false,
										DbUtil.getConnection(), main.this, null);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							//insert access log llm 23/11/12
							comFun = null;
							startActivity(intentToGoAfterSync);
						} else {
							// authenticate fail, can be due to change of
							// password, re-download uid

							showMessageDialogWithConfirmation("Invalid ID / Password. \nPlease contact your administrator if you have forgotten your password");
						}
					} catch (ULjException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						showExceptionDialog("Error while autenticating user", e);
					}
				} else {
					showMessageDialog("Unable to perform CheckLogin synchronization, please contact adminitrator if problem persist");
				}
			}
		}
	};

	@Override
	protected void onResume() {
		// System.out.print("etLoginPassword: " + etLoginPassword);
		etLoginPassword.setText("");

		LoginBO bo = new LoginBO();
		try {
			String currentUserId = bo.doGetUserId();
			if (currentUserId != null && !"".equals(currentUserId))
				isUserExist = true;

			etLoginId.setText(currentUserId);
			
//			if (isUserExist==false && main.strCurrentUserId!=null ) 
//			{
//				etLoginId.setText(main.strCurrentUserId.toUpperCase());
//				
//			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			Log.w(TAG, "Error when retrieving current user id");
		}
		//llm 2015/01/20 show login button when mobilink down 
		btnLogin.setVisibility(View.VISIBLE);
		
		etLoginPassword.setText("12345678");
		etLoginId.setText("KUMAR");

		super.onResume();
	}

	private boolean isSameUser() {
		LoginBO bo = new LoginBO();
		try {
			String currentUserId = bo.doGetUserId();
			if (currentUserId != null && !"".equals(currentUserId)) {
				if (!currentUserId.equals(etLoginId.getText().toString()))
					return false;

			}
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			Log.w(TAG, "Error when retrieving current user id");
		}
		return true;
	}

	protected void showExceptionDialog(String errorMsg, Exception e) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayException(this, errorMsg, e);
		dUtil = null;
		btnLogin.setVisibility(View.VISIBLE);
	}

	protected void showMessageDialog(String messageToShow) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayMessage(this, messageToShow);
		dUtil = null;
		btnLogin.setVisibility(View.VISIBLE);
	}

	protected void showMessageDialogWithConfirmation(String messageToShow) {
		DisplayUtil dUtil = new DisplayUtil();
		dUtil.displayAlertDialog(this, "Confirmation", messageToShow);
		dUtil = null;
		btnLogin.setVisibility(View.VISIBLE);
	}



	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.loginmenu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			super.onOptionsItemSelected(item);
			int itemID = item.getItemId();
			// title to display is taken from current menu item
			String msg = item.getTitle().toString();
			if (msg.equals("Refresh Data"))
				doRefreshDB();

		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();

		}
		return false;
	}// onOptionsItemSelected

	public void doRefreshDB() {
		needRefresh = true;
		
		//llm 2015/01/20 delete udb upon press refresh button
		System.out.println("REFRESHINGGG DB");
		
		DbUtil dbUtil = new DbUtil();
		try {
			dbUtil.refreshNewDB();
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	// check if need refresh master tables
	// commercial-outlet
	private void refreshMasterIfNeeded(String sUserId) {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		DateUtil dateUtil = new DateUtil();
		int dateDifference = -1;
		Date currentDate = dateUtil.getCurrentDate();
		LoginDAO ldao = new LoginDAO();
		Timestamp syncdate;
		try {
			syncdate = ldao.getSyncDate(sUserId);
		} catch (Exception ex) {
			return;
			// this method should not prevent user from proceeding
		}

		if (syncdate != null && currentDate != null) {
			dateDifference = (int) ((currentDate.getTime() - syncdate.getTime()) / (1000 * 60 * 60 * 24));
		}

		if (day == 2 && dateDifference >= 1)// monday
		{
			Log.v(TAG, "First login of the week, need refresh");
			SynchronizationUtils syncUtil = new SynchronizationUtils();
			boolean isDownloadOnly = false;
			Log.d(TAG, "Do Sync");
			CommonFunction comFun = new CommonFunction();
			if (comFun.isOnline(this.getApplicationContext())) {
				try {

					CommonFunction comFunc = new CommonFunction();

					comFunc.clearIFSnFaultTables();

					syncUtil.synchronizedDoSyncForVersion(AppsConstant.VERS_SYNC, AppsConstant.SUB_VERS_SYNC, isDownloadOnly, DbUtil.getConnection(),
							main.this, null);
					ldao.doUpdateUser(sUserId);
				} catch (Exception e) {
					Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			} else {
				Toast toast = Toast.makeText(getBaseContext(), "Internet connection not detected.\nSynchronizing data to server cannot be done.",
						Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 40, 60);
				toast.show();

			}
		}

	}
}