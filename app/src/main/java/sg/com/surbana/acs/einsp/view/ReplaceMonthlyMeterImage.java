package sg.com.surbana.acs.einsp.view;

import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.WrapMotionEvent;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ReplaceMonthlyMeterImage extends Activity implements OnClickListener, LocationListener, OnTouchListener {
	private final String TAG = "ReplaceMonthlyMeterImage";

	public static ImageView imgViewTemp;
	protected ImageView imgView;
	public static Boolean flagSetImage = true;
	protected ImageButton imgBtnOk, imgBtnCancel, imgBtnRecapture, imgBtnLogout, imgbtnAnnotate;
	protected ImageButton imgBtnCalendar, imgBtnSearch, imgBtnSync, imgBtnBarcode;
	private static final int CAMERA_PIC_REQUEST = 1337;
	private static final int ANNOTATION_REQUEST = 8220;
	public static Bitmap[] bitmapReplace;
	public static String[] strCaption;
	public static String[] gpslocation;
	public static String[] imageDate;

	private Bitmap thumbnail;
	private EditText txtCaption, txtLongitude, txtLatitude;
	public static int iPosition;
	public static int iMode = 0;

	private LocationManager mLoc;
	private static final Integer MINIMUM_UPDATE_INTERVAL = 30000; // update
																	// every 5
																	// seconds
	private static final Integer MINIMUM_UPDATE_DISTANCE = 10; // update every
																// 10 meters

	// FOR ZOOMING -START
	Matrix matrix;
	Matrix savedMatrix;
	// We can be in one of these 3 states
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;

	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	private LinearLayout imageContainer, linearLayoutLongtitude, linearLayoutLatitude;

	// FOR ZOOMING -END

	/**
	 * Called when the activity is first created.
	 * 
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// juehua 20120229 for merge chin hua's tablet xml to leeming's code
		if (Build.MODEL.equalsIgnoreCase("GT-P7500")) {
			setContentView(R.layout.tablet_capture_img);
		} else {
			setContentView(R.layout.capture_img);
		}

		setupWidgets();

		// the location manager allows access to the current location and GPS
		// status
		mLoc = (LocationManager) getSystemService(LOCATION_SERVICE);
	}

	/**
	 * Set ups the UI
	 */
	private void setupWidgets() {
		try {

			// ok,cancel and recapture buttons
			imgBtnOk = (ImageButton) findViewById(R.id.imgbtnOK);
			imgBtnOk.setOnClickListener(this);

			imgBtnCancel = (ImageButton) findViewById(R.id.imgbtnCancel);
			imgBtnCancel.setOnClickListener(this);

			imgBtnRecapture = (ImageButton) findViewById(R.id.imgbtnRecapture);
			imgBtnRecapture.setOnClickListener(this);

			imgbtnAnnotate = (ImageButton) findViewById(R.id.imgbtnAnnotate);
			imgbtnAnnotate.setOnClickListener(this);

			txtCaption = (EditText) findViewById(R.id.txtCaptionCapture);
			txtLongitude = (EditText) findViewById(R.id.txtLongitude);
			txtLatitude = (EditText) findViewById(R.id.txtLatitude);
			linearLayoutLongtitude = (LinearLayout) findViewById(R.id.linearLayoutLongtitude);
			linearLayoutLatitude = (LinearLayout) findViewById(R.id.linearLayoutLatitude);

			imageContainer = (LinearLayout) findViewById(R.id.linearLayoutPreview);

			linearLayoutLongtitude.setVisibility(View.GONE);
			linearLayoutLatitude.setVisibility(View.GONE);

			// thumbnail image
			imgView = (ImageView) findViewById(R.id.imageViewPreview);
			Bitmap bitmap = ((BitmapDrawable) imgViewTemp.getDrawable()).getBitmap();
			if (bitmapReplace == null)
				bitmapReplace = new Bitmap[5];

			if (strCaption == null)
				strCaption = new String[5];

			if (gpslocation == null)
				gpslocation = new String[5];

			if (imageDate == null)
				imageDate = new String[5];

			// imgView.setImageBitmap(resizedBitmappreview);
			imgView.setImageDrawable(imgViewTemp.getDrawable());
			imgView.setOnTouchListener(this);
			// matrix.setTranslate(1f, 1f);
			imgView.setImageMatrix(matrix);
			resetImage();
			if (iMode != 0 && iMode != 1) {
				txtCaption.setText(strCaption[MonthlyMeterImagesView.iIndex].toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * handles click event of views
	 * 
	 * @param View
	 */
	public void onClick(View v) {
		try {
			if (v == imgBtnOk) // ok is clicked
			{

				try {

					DateUtil dUtil = new DateUtil();

					if (iMode == 0) // called from addnewtask for capture of
									// image
					{
						MonthlyMeterImagesView.iIndex++;
						bitmapReplace[MonthlyMeterImagesView.iIndex] = ((BitmapDrawable) (imgView.getDrawable())).getBitmap();
						strCaption[MonthlyMeterImagesView.iIndex] = txtCaption.getText().toString();
						gpslocation[MonthlyMeterImagesView.iIndex] = txtLongitude.getText().toString() + "," + txtLatitude.getText().toString();
						imageDate[MonthlyMeterImagesView.iIndex] = dUtil.timeStamp2dateString2(dUtil.getCurrentTimeStamp()).toString();

					}

					else if (iMode == 1) // called from edittask for capture of
											// image
					{

						MonthlyMeterImagesView.iIndex++;
						bitmapReplace[MonthlyMeterImagesView.iIndex] = ((BitmapDrawable) (imgView.getDrawable())).getBitmap();
						strCaption[MonthlyMeterImagesView.iIndex] = txtCaption.getText().toString();
						gpslocation[MonthlyMeterImagesView.iIndex] = txtLongitude.getText().toString() + "," + txtLatitude.getText().toString();
						imageDate[MonthlyMeterImagesView.iIndex] = dUtil.timeStamp2dateString2(dUtil.getCurrentTimeStamp()).toString();

					} else if (iMode == 3) // called from addnewtask for replace
											// of image
					{
						bitmapReplace[MonthlyMeterImagesView.iIndex] = ((BitmapDrawable) (imgView.getDrawable())).getBitmap();
						strCaption[MonthlyMeterImagesView.iIndex] = txtCaption.getText().toString();
						gpslocation[MonthlyMeterImagesView.iIndex] = txtLongitude.getText().toString() + "," + txtLatitude.getText().toString();
						imageDate[MonthlyMeterImagesView.iIndex] = dUtil.timeStamp2dateString2(dUtil.getCurrentTimeStamp()).toString();

					} else if (iMode == 4) // called from edittask for replace
											// of image
					{
						bitmapReplace[MonthlyMeterImagesView.iIndex] = ((BitmapDrawable) (imgView.getDrawable())).getBitmap();
						strCaption[MonthlyMeterImagesView.iIndex] = txtCaption.getText().toString();
						gpslocation[MonthlyMeterImagesView.iIndex] = txtLongitude.getText().toString() + "," + txtLatitude.getText().toString();
						imageDate[MonthlyMeterImagesView.iIndex] = dUtil.timeStamp2dateString2(dUtil.getCurrentTimeStamp()).toString();

					}
					Intent i = new Intent();
					setResult(Activity.RESULT_OK, i);
					finish();

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (v == imgbtnAnnotate) {
				// cannot use bundle pass image. image too big use static
				// variable

				Intent myIntent = new Intent(ReplaceMonthlyMeterImage.this, AnnotateImage.class);
				Bundle myData = new Bundle();
				myData.putString("frpage", "Monthly Meter");
				myIntent.putExtras(myData);

				Bitmap tmpImg;
				tmpImg = ((BitmapDrawable) imgViewTemp.getDrawable()).getBitmap();
				AnnotateImage.origImage = tmpImg;
				startActivityForResult(myIntent, ANNOTATION_REQUEST);

			} else if (v == imgBtnRecapture) // recapture is clicked
			{
				if (iMode == 4 || iMode == 3) {
					strCaption[MonthlyMeterImagesView.iIndex] = txtCaption.getText().toString().trim();

				}

				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
				flagSetImage = false;

			} else if (v == imgBtnCancel) // cancel is clicked
			{
				// Intent i=new Intent();
				// setResult(Activity.RESULT_OK,i);
				finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the captured image from camera
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
			try {
				// To get the captured image
				flagSetImage = false;
				thumbnail = (Bitmap) data.getExtras().get("data");

				Bitmap resizedBitmappreview = scaleBitmap(thumbnail);
				imgView.setImageBitmap(resizedBitmappreview);
				imgViewTemp.setImageBitmap(resizedBitmappreview);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == ANNOTATION_REQUEST && resultCode == RESULT_OK) {

			Bitmap resizedBitmappreview = ((BitmapDrawable) (imgViewTemp.getDrawable())).getBitmap();
			imgView.setImageBitmap(resizedBitmappreview);
			imgViewTemp.setImageBitmap(resizedBitmappreview);

		}
		resetImage();
	}

	/**
	 * creates the scaled bitmap for preview of the image
	 * 
	 * @param bitmap
	 * @return
	 */
	private Bitmap scaleBitmap(Bitmap bitmap) {
		int iWidth = bitmap.getWidth();
		int iHeight = bitmap.getHeight();
		int newWidth = 0, newHeight = 0;
		float fFactor = (float) iHeight / (float) iWidth;

		if (iWidth <= iHeight) {
			newWidth = 300;
			newHeight = (int) (newWidth * fFactor);
		} else {
			// newWidth = 600;
			// newHeight =450;

			newWidth = 400;
			newHeight = (int) (newWidth * fFactor);
		}
		float fScaleWidth = ((float) newWidth) / iWidth;
		float fScaleHeight = ((float) newHeight) / iHeight;

		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth, iHeight, matrix, true);

		return resizedBitmappreview;
	}

	// GPS start
	@Override
	/**
	 * onResume is is always called after onStart, even if the app hasn't been paused
	 */
	protected void onResume() {
		// add a location listener and request updates every 10000ms or 10m
		mLoc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MINIMUM_UPDATE_INTERVAL, MINIMUM_UPDATE_DISTANCE, this);
		// GPS_PROVIDER
		resetImage();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// GPS, as it turns out, consumes battery like crazy
		mLoc.removeUpdates(this);
		super.onPause();
	}

	@Override
	protected void onStop() {
		// may as well just finish since saving the state is not important for
		// this toy app
		// finish(); commented if not annotation will go back to faultnewview
		// instead of replaceimage
		super.onStop();
	}

	public void onLocationChanged(Location loc) {
		// display some information based on the current position
		StringBuilder sbLong = new StringBuilder("");
		StringBuilder sbLat = new StringBuilder("");

		sbLong.append(loc.getLongitude());
		txtLongitude.setText(sbLong.toString());

		sbLat.append(loc.getLatitude());
		txtLatitude.setText(sbLat.toString());

	}

	public void onProviderDisabled(String provider) {
		// called if/when the GPS is disabled in settings
		// Toast.makeText(this, "GPS disabled", Toast.LENGTH_LONG).show();
		//
		// // end program since we depend on GPS
		// AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		// alertbox.setMessage("This app requires GPS. Please activate it first!");
		// alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener()
		// {
		// public void onClick(DialogInterface arg0, int arg1) {
		// finish();
		// }
		// });
		// alertbox.show();
	}

	public void onProviderEnabled(String provider) {
		// Toast.makeText(this, "GPS enabled", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// called upon GPS status changes
		// switch (status) {
		// case LocationProvider.OUT_OF_SERVICE:
		// Toast.makeText(this, "Status changed: out of service",
		// Toast.LENGTH_LONG).show();
		// break;
		// case LocationProvider.TEMPORARILY_UNAVAILABLE:
		// Toast.makeText(this, "Status changed: temporarily unavailable",
		// Toast.LENGTH_LONG).show();
		// break;
		// case LocationProvider.AVAILABLE:
		// Toast
		// .makeText(this, "Status changed: available",
		// Toast.LENGTH_LONG).show();
		// break;
		// }
	}

	// GPS stop

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onTouch(View v, MotionEvent rawEvent) {
		// TODO Auto-generated method stub

		WrapMotionEvent event = WrapMotionEvent.wrap(rawEvent);
		System.out.println("getting the image dimension");
		System.out.println(event.getX());
		System.out.println(event.getY());
		// ...
		ImageView view = (ImageView) v;

		// Dump touch event to log
		dumpEvent(event);

		// Handle touch events here...
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			savedMatrix.set(matrix);
			start.set(event.getX(), event.getY());
			Log.d(TAG, "mode=DRAG");
			mode = DRAG;
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			oldDist = spacing(event);
			Log.d(TAG, "oldDist=" + oldDist);
			if (oldDist > 10f) {
				savedMatrix.set(matrix);
				midPoint(mid, event);
				mode = ZOOM;
				Log.d(TAG, "mode=ZOOM");
			}
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			mode = NONE;
			Log.d(TAG, "mode=NONE");
			break;
		case MotionEvent.ACTION_MOVE:
			if (mode == DRAG) {
				// ...
				matrix.set(savedMatrix);
				matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
			} else if (mode == ZOOM) {
				float newDist = spacing(event);
				Log.d(TAG, "newDist=" + newDist);
				if (newDist > 10f) {
					matrix.set(savedMatrix);
					float scale = newDist / oldDist;
					matrix.postScale(scale, scale, mid.x, mid.y);
				}
			}
			break;
		}

		view.setImageMatrix(matrix);
		return true; // indicate event was handled

	}

	/** Show an event in the LogCat view, for debugging */
	private void dumpEvent(WrapMotionEvent event) {
		// ...
		String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
		StringBuilder sb = new StringBuilder();
		int action = event.getAction();
		int actionCode = action & MotionEvent.ACTION_MASK;
		sb.append("event ACTION_").append(names[actionCode]);
		if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
			sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
			sb.append(")");
		}
		sb.append("[");
		for (int i = 0; i < event.getPointerCount(); i++) {
			sb.append("#").append(i);
			sb.append("(pid ").append(event.getPointerId(i));
			sb.append(")=").append((int) event.getX(i));
			sb.append(",").append((int) event.getY(i));
			if (i + 1 < event.getPointerCount())
				sb.append(";");
		}
		sb.append("]");
		Log.d(TAG, sb.toString());
	}

	/** Determine the space between the first two fingers */
	private float spacing(WrapMotionEvent event) {
		// ...
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	/** Calculate the mid point of the first two fingers */
	private void midPoint(PointF point, WrapMotionEvent event) {
		// ...
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	private void resetImage() {
		matrix = new Matrix();
		savedMatrix = new Matrix();
		System.out.println("getting the container dimension");
		System.out.println(imageContainer.getLayoutParams().height);
		System.out.println(imageContainer.getLayoutParams().width);
		int imgWidth = imgViewTemp.getDrawable().getIntrinsicWidth();
		int imgHeight = imgViewTemp.getDrawable().getIntrinsicHeight();

		int newWidth = 0, newHeight = 0;
		float fFactor = (float) imgHeight / (float) imgWidth;

		int newX = 0;
		int newY = 0;
		if (imgWidth <= imgHeight) {
			newHeight = imageContainer.getLayoutParams().height;
			newWidth = (int) (newHeight / fFactor);
			newX = imageContainer.getLayoutParams().width / 2 - newWidth / 2;
		} else {
			newWidth = imageContainer.getLayoutParams().width;
			newHeight = (int) (newWidth * fFactor);
			newY = imageContainer.getLayoutParams().height / 2 - newHeight / 2;
			// newY=newY/4;
		}
		float fScaleWidth = ((float) newWidth) / imgWidth;
		float fScaleHeight = ((float) newHeight) / imgHeight;

		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		matrix.postTranslate(newX, newY);
		imgView.setImageMatrix(matrix);

	}
	// FOR PINCH ZOOM -END

}
