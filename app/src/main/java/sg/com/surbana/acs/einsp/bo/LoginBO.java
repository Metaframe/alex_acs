package sg.com.surbana.acs.einsp.bo;

import sg.com.surbana.acs.einsp.dao.LoginDAO;
import sg.com.surbana.acs.einsp.util.CommonFunction;

import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jul 25, 2011		Ryan			Created
 *25 Jul 2011		Ryan			Added check for password upon login
 */
public class LoginBO {
	private final String TAG = "LoginBO.java";

	public void doSaveUserID(String userId) throws ULjException {
		LoginDAO loginDao = new LoginDAO();
		loginDao.doInsertUser(userId);
	}

	public void doUpdateUserID(String userId) throws ULjException {
		LoginDAO loginDao = new LoginDAO();
		loginDao.doUpdateUser(userId);
	}

	public void doProcessIfUserExist(boolean userExist, String uId) throws ULjException {
		// if already have existing user, no need to trigger Init, else, do Init
		// sync
		CommonFunction comFunc = new CommonFunction();

		// DbUtil dbUtil = new DbUtil();
//		if (userExist) {
//			// more code to be put here, refer to the current logic
//			String currentUserId = doGetUserId();
//			if (!uId.equals(currentUserId)) {
//				// Clear user related tables
//				// comFunc.clearAccountTables();
//				doUpdateUserID(currentUserId);
//			}
//		} else
//			doSaveUserID(uId);
	}

	public String doGetUserId() throws ULjException {
		LoginDAO loginDao = new LoginDAO();
		String uId = loginDao.doSelectUserId();
		return uId;

	}

	/**
	 * Check if user entered user id and password matches the one in the db
	 * 
	 * @param uid
	 * @param password
	 * @return
	 */
	public boolean authenticateUser(String uId, String password) {
		LoginDAO loginDao = new LoginDAO();
		String dbPassword = null;
		try {
			dbPassword = loginDao.doSelectUserPassword(uId);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (dbPassword != null && dbPassword.equalsIgnoreCase(password))
			return true;
		else
			//changed from false to true : Paul
			return true;

	}

	/**
	 * Check if handheld matches any record in the db tblRegistedIMEI
	 * juehua for auth imei
	 * 
	 * @param strIMEI
	 * @return
	 */
	public boolean authenticateIMEI(String strIMEI) throws ULjException {
		LoginDAO loginDao = new LoginDAO();

		String deviceId = loginDao.doSelectRegistedIMEI(strIMEI);
		if (deviceId != null) {
			return true;
		}
//changed from false to true : Paul
		return true;
	}
}
