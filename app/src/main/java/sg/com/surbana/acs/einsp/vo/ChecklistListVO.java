package sg.com.surbana.acs.einsp.vo;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 14, 2011		Ryan			Created
 *
 */
public class ChecklistListVO extends ChecklistVO {
	private final String TAG = "ChecklistListVO.java";
	private int status;
	private int numOfFault;
	private int dropDownGroupId;

	private String dropDownGroupShortDesc;
	private String remarks;

	public int getDropDownGroupId() {
		return dropDownGroupId;
	}

	public void setDropDownGroupId(int dropDownGroupId) {
		this.dropDownGroupId = dropDownGroupId;
	}

	public String getDropDownGroupShortDesc() {
		return dropDownGroupShortDesc;
	}

	public void setDropDownGroupShortDesc(String dropDownGroupShortDesc) {
		this.dropDownGroupShortDesc = dropDownGroupShortDesc;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getNumOfFault() {
		return numOfFault;
	}

	public void setNumOfFault(int numOfFault) {
		this.numOfFault = numOfFault;
	}

}
