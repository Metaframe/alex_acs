package sg.com.surbana.acs.einsp.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import okio.ByteString;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sg.com.surbana.acs.einsp.R;
import sg.com.surbana.acs.einsp.main;
import sg.com.surbana.acs.einsp.bo.ChecklistBO;
import sg.com.surbana.acs.einsp.bo.MonthlyMeterBO;
import sg.com.surbana.acs.einsp.dao.MonthlyMeterDAO;
import sg.com.surbana.acs.einsp.util.ApiConnectionUtil;
import sg.com.surbana.acs.einsp.util.AppsConstant;
import sg.com.surbana.acs.einsp.util.CommonFunction;
import sg.com.surbana.acs.einsp.util.DatabaseHelper;
import sg.com.surbana.acs.einsp.util.DateUtil;
import sg.com.surbana.acs.einsp.util.DisplayUtil;
import sg.com.surbana.acs.einsp.util.NumberSpinnerDialog2;
import sg.com.surbana.acs.einsp.util.RequestCommand;
import sg.com.surbana.acs.einsp.util.SignatureDialog;
import sg.com.surbana.acs.einsp.util.SoftKeyboardControlListener;
import sg.com.surbana.acs.einsp.util.SynchronizationUtils;
import sg.com.surbana.acs.einsp.util.NumberSpinnerDialog2.OnNumberSetListener;
import sg.com.surbana.acs.einsp.util.SignatureDialog.OnSignatureSetListener;
import sg.com.surbana.acs.einsp.vo.DropdownItemVO;
import sg.com.surbana.acs.einsp.vo.Image2VO;
import sg.com.surbana.acs.einsp.vo.ImageMapVO;
import sg.com.surbana.acs.einsp.vo.ImageSendDataVO;
import sg.com.surbana.acs.einsp.vo.LocationVO;
import sg.com.surbana.acs.einsp.vo.MeterSendVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterImagesVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterSignOffVO;
import sg.com.surbana.acs.einsp.vo.MonthlyMeterVO;
import sg.com.surbana.acs.einsp.vo.ScheduleList2VO;
import sg.com.surbana.acs.einsp.vo.ScheduleListSendVO;
import sg.com.surbana.acs.einsp.vo.ScheduleListVO;
import sg.com.surbana.acs.einsp.vo.UserInfoVO;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ianywhere.ultralitejni12.ULjException;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;


/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Mar 3, 2012		Lee Ming			Created
 *
 */
public class MonthlyMeterView extends BaseView implements SoftKeyboardControlListener.Listener, OnClickListener, OnLongClickListener,OnTouchListener {
	private final String TAG = "MonthlyMeterView.java";
	private static final int CAMERA_PIC_REQUEST = 1337;
	int imageCount=-1;
	int iPhoto;
	private Bitmap[] bitmapReplace;
	private ImageView[] imgViewThumbNail;
	private String[] captions;
	private String[] gpsLocation;
	private String[] imageDate;
	public static Bitmap[] bitmap = null;
	private Dialog dialogEdit;
	private static int tenantid=0;
	private static String tenantno="0";
	private static int spnLocationId,resultId,monthlymeterno=0;
	private ImageButton imgBtnDel, imgBtnNoDel, imgBtnClose, imgBtnPass, imgBtnRedo;
	EditText lblTenantName,lblUnitNo;
	//	Spinner spnMaintOfficerName;
	LinearLayout lnrMeter, lnrSave ,lnrCollapse, linearLayoutMeterRound, linearLayoutMeterC1,
			linearLayoutMeterC2,linearLayoutMeterPhoto;
	private OnClickListener onClick;
	private ImageButton imgBtnSave;
	TextView txMeterValue,txMeterCat,lblMeterLocation;
	Button btnCamera,btnIcon,btnReset,btnCamera0;
	private final int REQUEST_CODE_JOB_END = 1;
	private final int REQUEST_CODE_JOB_START = 0;
	//	EditText txtHname, txtHtelephone, txtHdate, txtHDesignation;
//	EditText txtAname,  txtAdate, txtAtelephone, txtADesignation;
	Bitmap  bmpAsignature;
	private Context context;
	ImageView imgViewSign;
	TextView lblConsumptionPercent,	txtPrevConsumption,	txtPrevReading, txtMeterNo, lblVariancePercent;
	TextView txtCurrReading, txtMeterId, txtSerialNo, txtMultiplyFactor,txtRemarks;
	TextView txtMeterReading,txtCaption;

	Button btnJobStart;
	Button btnJobEnd;
	EditText txtJobStart;
	EditText txtJobEnd,txtCustomerName,txtTenantName, txtTenantEmail;
	private ScheduleListVO scheduleVo;
	MonthlyMeterBO bo = new MonthlyMeterBO(this);
	Boolean bTemp = false;
	Button btnSignature2,btnSign;
	private LinearLayout linearLayoutPreview, linearLayoutThumb, linearLayoutCaption;
	private ImageView imgViewPreview;

	private double consumptionalert=0.1;
	CheckBox chkmeterround ;
	double maxMeterValue = 10000000.0;
	int spinnum=0;
	int decnum =0;
	String NumtoShowonSpn="";
	String UserMeterType ="";
	int maxlen=0;
	int lastindex=0;
	String strzeros="";
	String strmaxvalue="";
	CommonFunction comFunction = new CommonFunction();
	//SynchronizationUtils syncUtil = new SynchronizationUtils();
	CommonFunction comf = new CommonFunction();
	int iIndexTemp = -1;
	public static int iIndex = -1;
	// juehua for decimal limitation
	DecimalFormat df = new DecimalFormat("#.000");
	int singalStenths =0;
	int signallimit=99;
	int inttextsize=22;
	private ProgressDialog dialogWait;
	static Boolean flagPhoto = false;
	private int iPreviewInd = 0;

	private UserInfoVO uvo;
	private String imageFileName;
	private boolean isExistingResult;
	private ScheduleList2VO scheduleList2VO;
	private ScheduleListSendVO scheduleListSendVO;
	private ArrayList<MonthlyMeterVO> meters;

	private ImageSendDataVO imageSendDataVO;
	private MeterSendVO meterSendVO;

	private Map<String, String> map;

	private ApiConnectionUtil apiConnectionUtil;
	private RequestCommand requestCommand;

	private UpdateBuilder<ScheduleList2VO, Integer> updateScheduleItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (Build.MODEL.equalsIgnoreCase("GT-P7500")) {
			setContentView(R.layout.monthly_meter);
		} else {
			setContentView(R.layout.monthly_meter);
		}

		apiConnectionUtil = new ApiConnectionUtil(this);

		try {

			consumptionalert= Double.parseDouble(comFunction.getHandheldConfig(AppsConstant.CONFIG_MM_CONSUMPTION_ALERT))*100;

		} catch (ULjException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (Exception e1) {
		}

//		imageFileName = getIntent().getExtras().getString(AppsConstant.INTENT_DATA_RESULT_ID);
//		imageFileName += ".jpg";
//		File directory = new File(context.getFilesDir().getPath());
//		File[] files = directory.listFiles();

		lblUnitNo = (EditText) findViewById(R.id.txtUnitNo);
		lblTenantName = (EditText) findViewById(R.id.txtTenantName);
//		spnMaintOfficerName = (Spinner)findViewById(R.id.spnMaintOfficerName);
		lnrMeter = (LinearLayout) findViewById(R.id.linearLayoutMeter);
		lnrSave = (LinearLayout) findViewById(R.id.linearLayoutSave);
		imgBtnSave = (ImageButton) findViewById(R.id.imgbtnSave);
		imgBtnSave.setOnClickListener(this);
		btnCamera0 = (Button) findViewById(R.id.btnCamera1);
		btnCamera0.setOnClickListener(this);
		imgViewSign = (ImageView)findViewById(R.id.imgViewSign);
		imgViewSign.setOnClickListener(this);

		lblMeterLocation = (TextView)findViewById(R.id.lblMeterLocation);
		imgViewThumbNail = new ImageView[5];
		imgViewThumbNail[0] = (ImageView) findViewById(R.id.imageView1);
		imgViewThumbNail[1] = (ImageView) findViewById(R.id.imageView2);
		imgViewThumbNail[2] = (ImageView) findViewById(R.id.imageView3);
		imgViewThumbNail[3] = (ImageView) findViewById(R.id.imageView4);
		imgViewThumbNail[4] = (ImageView) findViewById(R.id.imageView5);
		captions=new  String[5] ;
		gpsLocation=new  String[5];
		imageDate=new  String[5];

		bitmapReplace = new Bitmap[5];
		linearLayoutPreview = (LinearLayout) findViewById(R.id.linearLayoutPreview);
		linearLayoutThumb = (LinearLayout) findViewById(R.id.linearLayoutThumb);
		linearLayoutCaption = (LinearLayout) findViewById(R.id.linearLayoutCaption);

		linearLayoutPreview.setVisibility(View.GONE);
		linearLayoutThumb.setVisibility(View.GONE);
		linearLayoutCaption.setVisibility(View.GONE);
		imgViewPreview = (ImageView) findViewById(R.id.imageViewPreview);
		imgViewPreview.setOnLongClickListener(this);
		txtCaption = (TextView) findViewById(R.id.txtCaption);


		for (int i = 0; i < 5; i++) {
			imgViewThumbNail[i].setOnLongClickListener(this);
			imgViewThumbNail[i].setOnClickListener(this);
		}

		linearLayoutMeterC1= (LinearLayout) findViewById(R.id.linearLayoutMeterC1);
		linearLayoutMeterC2= (LinearLayout) findViewById(R.id.linearLayoutMeterC2);
		linearLayoutMeterPhoto= (LinearLayout) findViewById(R.id.linearLayoutMeterPhoto);
		linearLayoutMeterPhoto.setVisibility(View.GONE);
		btnJobStart = (Button) findViewById(R.id.btnJobStart);
		btnJobEnd = (Button) findViewById(R.id.btnJobEnd);
		txtJobStart = (EditText) findViewById(R.id.txtJobStart);
		txtJobEnd = (EditText) findViewById(R.id.txtJobEnd);
		txtCustomerName = (EditText) findViewById(R.id.txtCustomerName);
		txtTenantName = (EditText) findViewById(R.id.txtTenantName);
		txtTenantEmail = (EditText) findViewById(R.id.txtTenantEmail);
		btnSignature2=(Button)findViewById(R.id.btnSignature2);
		btnSign=(Button)findViewById(R.id.btnSign);
		btnSign.setOnClickListener(this);
		if (onClick == null)
			onClick = MonthlyMeterView.this;
		if (context == null)
			context = MonthlyMeterView.this.getBaseContext();

		Bundle extras = getIntent().getExtras();
//		try {
			spnLocationId = extras.getInt(AppsConstant.INTENT_DATA_BUILDING_ID);
//		} catch (Exception e1) {
//		}
		try {
			resultId = extras.getInt(AppsConstant.INTENT_DATA_RESULT_ID);
		} catch (Exception e1) {
		}
		try {
			DateUtil dUtil = new DateUtil();
			//scheduleVo = bo.getScheduleByResultId(resultId);
			scheduleVo = new ScheduleListVO();
			ScheduleList2VO tempScheduleItem =getIntent().getParcelableExtra(AppsConstant.INTENT_DATA_SCHEDULE);
			scheduleList2VO = apiConnectionUtil.getScheduleListDAO().queryBuilder().where().eq("checklistResult",tempScheduleItem.getChecklistResult()).query().get(0);
			scheduleListSendVO = new ScheduleListSendVO();

			scheduleVo.setActualStart((dUtil.dateString2Calendar(scheduleList2VO.getRevisedStart(), DateUtil.DATE_TIME_FORMAT)));
			if(scheduleList2VO.getActualEnd().toString() != null && scheduleList2VO.getActualEnd().toString().length() != 0) {
				scheduleVo.setActualEnd((dUtil.dateString2Calendar(scheduleList2VO.getRevisedStart(), DateUtil.DATE_TIME_FORMAT)));
			}
			scheduleVo.setRevisedStart((dUtil.dateString2Calendar(scheduleList2VO.getRevisedStart(), DateUtil.DATE_TIME_FORMAT)));
			scheduleVo.setBuildingId(Integer.parseInt(scheduleList2VO.getLocationID()));
			scheduleVo.setFrequencyId(Integer.parseInt(scheduleList2VO.getFrequencyID()));
			scheduleVo.setStatusId(scheduleList2VO.getStatusInt());
			scheduleVo.setRemarks(scheduleList2VO.getRemarks());
			scheduleVo.setChecklistResultId(Integer.parseInt(scheduleList2VO.getChecklistResult()));
			scheduleVo.setServiceMainProgId(Integer.parseInt(scheduleList2VO.getServiceMaintProgID()));
		}
// catch (ULjException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
		catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		initiateComponents();



		LocationVO lvo = new LocationVO();
		try {
			lvo = comFunction.getLocationVoByLocId(Integer.toString(spnLocationId));
			this.lblMeterLocation.setText(scheduleList2VO.getServiceMaintProgDescription());
		} catch (ULjException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			tenantid = 0;

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		try {
		try {
			apiConnectionUtil.getMeterSendDAO().refresh(scheduleList2VO.getMeterSendVO());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(scheduleList2VO.getMeterSendVO() != null) {
			lblTenantName.setText((scheduleList2VO.getMeterSendVO().getSTenantName()));
			}
//		} catch (ULjException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		try {
			Dao<Image2VO, Integer> imageDAO = DatabaseHelper.getHelper(this).getImage2VO();
			spnTerminalid = Integer.parseInt(imageDAO.queryForAll().get(0).getTerminalid());
		} catch (SQLException e) {
			e.printStackTrace();
		}


//		uvo = new UserInfoVO();
//		try {
//			uvo = comFunction.getUserInfoById(main.strCurrentUserId);
//
//			MonthlyMeterDAO mmdao = new MonthlyMeterDAO();
//			UserMeterType= mmdao.GetUserMeterType(uvo.getUserInfoId());
//
//		} catch (ULjException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}

		map = new HashMap<String, String>();
		map.put("fileName","string");
		map.put("meterNo", "string");
		map.put("monthlyMeterNo", "string");
		map.put("readingNo", "string");
		map.put("terminalID", "string");
		map.put("fileContents","string");
		map.put("checkResultID", "string");

		requestCommand = new RequestCommand(this);
		getMetersList();

		try {

			// path=path +"/"+module+"/"+id + "/"+type;
			// Ryan-20111213

			String webImageSubURI = spnTerminalid + "/" + AppsConstant.MODULE_MONTHLY_METER + "/" + monthlymeterno + "/image";

			// search existing images
			Vector<ImageMapVO> listFaultImages = new Vector<ImageMapVO>();
			List<ImageMapVO> imageList = new ArrayList<ImageMapVO>();
			if(scheduleList2VO.getMeterImageMap() != null) {
				apiConnectionUtil.getImageMapDAO().refresh(scheduleList2VO.getMeterImageMap());
				imageList = apiConnectionUtil.getImageMapDAO().queryBuilder().where().eq("checkResultID", scheduleList2VO.getChecklistTemplateID()).query();

			};

			if (iIndex == -1 && listFaultImages.size() <= 0) {
				MonthlyMeterDAO tDAO = new MonthlyMeterDAO();
				MonthlyMeterImagesVO vo = new MonthlyMeterImagesVO();
				vo.setMonthlyMeterNo(monthlymeterno);
				vo.setReadingNo(1);
				vo.setMeterNo(1);
				vo.setTerminalId(spnTerminalid);
				for(int i = 0; i < imageList.size(); i++) {
					listFaultImages.add(imageList.get(i));
				}

				iIndex = listFaultImages.size() - 1;
				iIndexTemp = listFaultImages.size() - 1;
				imageCount=listFaultImages.size() - 1;

				if (listFaultImages.size() > 0) {
					linearLayoutPreview.setVisibility(View.VISIBLE);
					linearLayoutThumb.setVisibility(View.VISIBLE);
					linearLayoutCaption.setVisibility(View.VISIBLE);
					linearLayoutMeterPhoto.setVisibility(View.VISIBLE);

					if (bitmapReplace == null)
						bitmapReplace = new Bitmap[5];

					if (captions == null)
						captions = new String[5];

					if (gpsLocation == null)
						gpsLocation = new String[5];

					if (imageDate == null)
						imageDate = new String[5];

					Bitmap bitmapTemp = null;

					for (int i = 0; i < listFaultImages.size(); i++) {
							byte[] decodedByte = Base64.decode(listFaultImages.elementAt(i).getFileContents().getBytes(), Base64.DEFAULT);
							bitmapTemp = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
							Bitmap resizedBitmapThumbail = Bitmap.createScaledBitmap(bitmapTemp, 100, 100, true);
							bitmapReplace[i] = bitmapTemp;
							imgViewThumbNail[i].setImageBitmap(resizedBitmapThumbail);
							imgViewThumbNail[i].setTag(i);

							String tmpLoc = "";
							gpsLocation[i] = tmpLoc;
							DateUtil dUtil = new DateUtil();

							if (i == 0) {
								imgViewPreview.setTag(0);
								imgViewPreview.setImageBitmap(bitmapTemp);
								Bitmap resizedBitmappreview1 = scaleBitmap(bitmapTemp);
								imgViewPreview.setImageBitmap(resizedBitmappreview1);
							}

						}
					}// end for
					bitmap = bitmapReplace;

				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// showExceptionDialog("Unable to load image from web", e);

			if (dialogWait != null)
				dialogWait.dismiss();
			e.printStackTrace();
		}
		// }
		// search existing images

		if (bitmapReplace == null || bitmapReplace.length == 0) {
			linearLayoutMeterPhoto.setVisibility(View.GONE);
			linearLayoutPreview.setVisibility(View.GONE);
			linearLayoutThumb.setVisibility(View.GONE);
			linearLayoutCaption.setVisibility(View.GONE);

		} else {
			linearLayoutMeterPhoto.setVisibility(View.VISIBLE);
			linearLayoutPreview.setVisibility(View.VISIBLE);
			linearLayoutThumb.setVisibility(View.VISIBLE);
			linearLayoutCaption.setVisibility(View.VISIBLE);
			setImage(bitmapReplace);

		}
		// 1 =completed; 3 = cancelled
		if (scheduleVo.getStatusId()!=3 && scheduleVo.getStatusId()!=0 ) //completed
		{


			if (scheduleVo.getStatusId()==1)
			{
				btnSignature2.setVisibility(View.GONE);
				//btnSign.setVisibility(View.GONE);
				this.lnrSave.setVisibility(View.GONE);
				this.btnJobEnd.setEnabled(false);
				this.btnJobStart.setEnabled(false);
				this.btnCamera0.setEnabled(false);
			}

			if (txtJobStart.getText().toString().length()<=0)
			{
				btnSignature2.setVisibility(View.GONE);
				//btnSign.setVisibility(View.GONE);
				this.imgViewSign.setEnabled(false);
				this.lnrSave.setVisibility(View.GONE);
				this.btnJobEnd.setEnabled(false);
				this.btnJobStart.setEnabled(true);
				this.btnCamera0.setEnabled(true);
			}
			else
			{
				if (txtJobStart.getText().toString().length()>0 &&
						txtJobEnd.getText().toString().length()<=0)
				{
					btnSignature2.setVisibility(View.VISIBLE);
					//btnSign.setVisibility(View.VISIBLE);
					this.imgViewSign.setEnabled(true);
					this.lnrSave.setVisibility(View.VISIBLE);
					this.btnJobEnd.setEnabled(true);
					this.btnJobStart.setEnabled(false);
					this.btnCamera0.setEnabled(true);
				}
			}
		}
		updateScheduleItem = apiConnectionUtil.getScheduleListDAO().updateBuilder();
	}


	private ArrayList<DropdownItemVO> getMaintOfficerList(Integer terminalid) {
		ArrayList<DropdownItemVO> list = new ArrayList<DropdownItemVO>();
		list = null;
		MonthlyMeterDAO mDao = new MonthlyMeterDAO();

		try {
			list = mDao.getMaintOfficer(terminalid);
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}


	private void loadMasterMeters() {
		MonthlyMeterBO bo = new MonthlyMeterBO(this);
		Boolean enableControl = true;
		MonthlyMeterDAO dao = new MonthlyMeterDAO();




//		try {
			if (meters.size() > 0) {
				Integer i =0;
				MonthlyMeterVO hvo = new MonthlyMeterVO();
				monthlymeterno=0;
				//llm 10/12/12
				monthlymeterno=dao.GetMontlyMeterNoByChecklistResuiltID(scheduleVo.getChecklistResultId());

				for (MonthlyMeterVO vo : meters) {
					if (vo.getLocationID() > 0) {
						i=i+1;
//						hvo = bo.getMetersReadingHistory(vo.getMeterNo(),scheduleVo.getChecklistResultId());
//
//						if (hvo.getMeterNo()!=null)
//						{
////								if (monthlymeterno <=0)
////								{monthlymeterno = hvo.getMonthlyMeterNo();}
//							enableControl=true;
////								if (UserMeterType.length()>0)
////								{
////									if (UserMeterType.contains(vo.getMeterClass()))
////										enableControl=true;
////									else
////										enableControl=false;
////								}
////
//							if (scheduleVo.getStatusId()==1)
//								enableControl=false;
//
//							if (txtJobStart.getText().toString().length()<=0)
//								enableControl=false;
//							populateMeter(hvo,i,enableControl);
//						}
//						else
//						{
							//no reading load master
							if (monthlymeterno <=0)
							{
								CommonFunction comFunction= new CommonFunction();
								monthlymeterno =	comFunction.generateUniqueNumberBasedOnDateTime();
							}

							enableControl=true;
//								if (UserMeterType.length()>0)
//								{
//									if (UserMeterType.contains(vo.getMeterClass()))
//										enableControl=true;
//									else
//										enableControl=false;
//									}

							if (scheduleVo.getStatusId()==1)
								enableControl=false;
							if (txtJobStart.getText().toString().length()<=0)
								enableControl=false;

                        try {
                            populateMeter(vo,i,enableControl);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

//					}
				}

			}


			Log.d(TAG, "Number of meters retrieved " + meters.size());
//		}
//		catch (ULjException e) {
//			// TODO Auto-generated catch block
//			Log.e(TAG, "Error in loading checklist", e);
//			e.printStackTrace();
//		}

	}


	public void populateMeter(MonthlyMeterVO vo, final Integer cntbutton, Boolean enable) throws SQLException {

		lblUnitNo.setEnabled(enable);
		//lblUnitNo.setFocusable(enable);//this will cause edittext disable after reload when click start button
		lblUnitNo.requestFocus();
		txtCustomerName.setEnabled(enable);
		//txtCustomerName.setFocusable(enable);
		txtTenantName.setEnabled(enable);
		//txtTenantName.setFocusable(enable);
		txtTenantEmail.setEnabled(enable);
		//txtTenantEmail.setFocusable(enable);
		imgViewSign.setEnabled(enable);

		//if (enable==false)
		//btnSign.setVisibility(View.GONE);

		MonthlyMeterVO prevvo = new MonthlyMeterVO();
		if (vo==null || vo.getUnitNo()==null || vo.getUnitNo().length()<=0)
		{
			MonthlyMeterDAO dao = new MonthlyMeterDAO();

			//get previous info
			try {
				prevvo = dao.searchPrevMetersReadingHistory(vo.getMeterNo());
			} catch (ULjException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		MeterSendVO meterSendVO = scheduleList2VO.getMeterSendVO();
		apiConnectionUtil.getMeterSendDAO().refresh(meterSendVO);
		if(meterSendVO != null) {
			lblUnitNo.setText(meterSendVO.getSUnitNo());
			txtCustomerName.setText(meterSendVO.getSCustomerName());
			txtTenantName.setText(meterSendVO.getSTenantName());
			txtTenantEmail.setText(meterSendVO.getSTenantEmail());

//		if (vo!=null && vo.getUnitNo()!=null)
//			lblUnitNo.setText(vo.getUnitNo());
//		else
//		{
//			if (prevvo!=null && prevvo.getUnitNo()!=null && txtJobStart.getText().length()>0)
//				lblUnitNo.setText(prevvo.getUnitNo());
//		}
//
//		if (vo!=null && vo.getCustomerName()!=null)
//			txtCustomerName.setText(vo.getCustomerName());
//		else
//		{
//			if (prevvo!=null && prevvo.getCustomerName()!=null && txtJobStart.getText().length()>0)
//				txtCustomerName.setText(prevvo.getCustomerName());
//		}
//
//		if (vo!=null && vo.getTenantName()!=null)
//			txtTenantName.setText(vo.getTenantName());
//		else
//		{
//			if (prevvo!=null && prevvo.getTenantName()!=null && txtJobStart.getText().length()>0)
//				txtTenantName.setText(prevvo.getTenantName());
//		}
//
//
//		if (vo!=null && vo.getTenantEmail()!=null)
//			txtTenantEmail.setText(vo.getTenantEmail());
//		else
//		{
//			if (prevvo!=null && prevvo.getTenantEmail()!=null && txtJobStart.getText().length()>0)
//				txtTenantEmail.setText(prevvo.getTenantEmail());
//		}
		//Signature
		apiConnectionUtil.getImageMapDAO().refresh(scheduleList2VO.getSignatureImageMap());
		ImageMapVO signatureImageMapVO = scheduleList2VO.getSignatureImageMap();
		if(signatureImageMapVO != null) {
			byte[] decodedbyte = Base64.decode(signatureImageMapVO.getFileContents(), Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(decodedbyte, 0, decodedbyte.length);
			Bitmap resizedBitmapThumbail = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
			bmpAsignature = resizedBitmapThumbail;
			imgViewSign.setImageBitmap(resizedBitmapThumbail);
		}

//		if (vo!=null && vo.getAckBySigFilename()!=null &&  vo.getAckBySigFilename().length()>0) {
//			Bitmap bitmapTemp = null;
//			if (vo != null && vo.getaSignature().length <= 0) {
//				String webImageSubURI = spnTerminalid + "/" + AppsConstant.MODULE_MONTHLY_METER + "/" + monthlymeterno + "/image";
//
//				String strFileURL = main.WEBSERVER_IMAGEPATH + "/" + webImageSubURI + "/" + vo.getAckBySigFilename();
//
//				BitmapFactory.Options bmOptions;
//				bmOptions = new BitmapFactory.Options();
//				bmOptions.inSampleSize = 1;
//
//				System.out.println("Retrieving image from  " + strFileURL);
//				try {
//					bitmapTemp = LoadImage(strFileURL, bmOptions, dialogHandler);
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				if (bitmapTemp != null) {
//					Bitmap resizedBitmapThumbail = Bitmap.createScaledBitmap(bitmapTemp, 100, 100, true);
//
//					imgViewSign.setImageBitmap(resizedBitmapThumbail);
//				}
//			} else {
//				try {
//					bitmapTemp = BitmapFactory.decodeByteArray(vo.getaSignature(), 0, vo.getaSignature().length);
//					Bitmap resizedBitmapThumbail = Bitmap.createScaledBitmap(bitmapTemp, 100, 100, true);
//					imgViewSign.setImageBitmap(resizedBitmapThumbail);
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//		}
	}




		LinearLayout linearLayout = new LinearLayout(this);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		linearLayout.setLayoutParams(layoutParams);
		layoutParams.gravity = Gravity.LEFT;
		layoutParams.setMargins(0, 0, 0, 0);
		linearLayout.setTag("lnrMeter" + Integer.toString(cntbutton));
		//linearLayout.setBackgroundResource(R.color.bgcolor_lightpurple);

		//linearlayout1 controls start

		LinearLayout linearLayoutNonCollapse = new LinearLayout(this);

		LinearLayout.LayoutParams layoutParamsNonCollapse = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParamsNonCollapse.gravity = Gravity.LEFT;
		layoutParamsNonCollapse.setMargins(10, 5, 0, 0);

		linearLayoutNonCollapse.setLayoutParams(layoutParamsNonCollapse);
		linearLayoutNonCollapse.setTag("lnrNonCollapse" + Integer.toString(cntbutton));
		linearLayoutNonCollapse.setOrientation(LinearLayout.VERTICAL);

		LinearLayout.LayoutParams layoutParamsCollapse = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParamsCollapse.gravity = Gravity.LEFT;
		layoutParamsCollapse.setMargins(10, 0, 0, 0);

		LinearLayout linearLayoutCollapse = new LinearLayout(this);
		linearLayoutCollapse.setLayoutParams(layoutParamsCollapse);
		linearLayoutCollapse.setTag("lnrCollapse" + Integer.toString(cntbutton));
		linearLayoutCollapse.setOrientation(LinearLayout.VERTICAL);

//		Button btnIcon = new Button(this);
		LinearLayout.LayoutParams layoutIcons = new LinearLayout.LayoutParams(0, 60, 0.3f);
//
//		if (vo.getMeterClass() !=null)
//		{
//			if (vo.getMeterClass().equals("E"))
//				btnIcon.setBackgroundResource(R.drawable.btnelectric);
//			else
//				btnIcon.setBackgroundResource(R.drawable.btnwater);
//
//		}
//		btnIcon.setTag("btnIcon" + Integer.toString(cntbutton));
//		btnIcon.setOnClickListener(onClick);
//		btnIcon.setLayoutParams(layoutIcons);
		TextView lblSerialNo = new TextView(this);
		LinearLayout.LayoutParams layoutParamsCom = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.4f);
		layoutParamsCom.gravity = Gravity.LEFT;
		lblSerialNo.setLayoutParams(layoutParamsCom);
		lblSerialNo.setTextColor(Color.BLACK);
		lblSerialNo.setTextSize(inttextsize);
		lblSerialNo.setTag("lblSerialNo" + Integer.toString(cntbutton));
		lblSerialNo.setText("Serial No");

		TextView txtMeterId= new TextView(this);
		LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f);
		layoutParams1.gravity = Gravity.CENTER;
		txtMeterId.setLayoutParams(layoutParams1);
		txtMeterId.setTextColor(Color.BLACK);
		txtMeterId.setTextSize(inttextsize);
		txtMeterId.setTag("txtMeterId" + Integer.toString(cntbutton));
		//txtMeterId.setGravity(17);
		if (vo.getMeterNo() !=null)
			txtMeterId.setText(vo.getMeterNo().toString());
		txtMeterId.setVisibility(View.GONE);

		TextView txtMeterCat= new TextView(this);
		txtMeterCat.setLayoutParams(layoutParams1);
		txtMeterCat.setTextColor(Color.BLACK);
		txtMeterCat.setTextSize(inttextsize);
		txtMeterCat.setTag("txtMeterCat" + Integer.toString(cntbutton));

		if (vo.getMeterCategory() !=null)
		{
			txtMeterCat.setText(vo.getMeterCategory().toString());


			spinnum=0;
			if (vo.getMeterCategory().contains("."))
			{
				spinnum=Integer.parseInt( vo.getMeterCategory().toString().trim().substring(0,  vo.getMeterCategory().toString().indexOf(".")));
				maxlen=spinnum+4;
				decnum = 3;
			}
			else
			{
				spinnum=Integer.parseInt( vo.getMeterCategory().toString().trim());
				maxlen=spinnum+2;
				decnum = 0;
			}
			int n = 0;
			strzeros="";
			strmaxvalue="";
			for (n = 0; n <spinnum; n++) {
				strzeros = strzeros+"0";
			}



			strmaxvalue= "1"+strzeros;
			maxMeterValue = Integer.parseInt(strmaxvalue);



		}

		txtMeterCat.setVisibility(View.GONE);

		TextView txtMeter = new TextView(this);
		layoutParams1.gravity = Gravity.LEFT;

		txtMeter.setLayoutParams(layoutParams1);
		txtMeter.setPadding(5, 8, 0, 0);
		txtMeter.setTextColor(Color.BLACK);


		txtMeter.setTextSize(inttextsize);
		txtMeter.setTag("txtMeterNo" + Integer.toString(cntbutton));
		//txtMeter.setGravity(17);
		if (vo.getSerialNo() !=null)
			txtMeter.setText(vo.getSerialNo());

		txtMeter.setGravity(Gravity.LEFT);

		TextView txtMeterSubLocation = new TextView(this);
		LinearLayout.LayoutParams layoutParamsSubLocation = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT, 1.4f);
		layoutParamsSubLocation.gravity = Gravity.LEFT;
		txtMeterSubLocation.setLayoutParams(layoutParamsSubLocation);
		txtMeterSubLocation.setTextColor(Color.BLACK);
		txtMeterSubLocation.setTextSize(inttextsize);
		txtMeterSubLocation.setTag("txtMeterSubLocation" + Integer.toString(cntbutton));
		//txtMeterSubLocation.setGravity(17);
		if (vo.getSubLocation().length()>0 )
			//llm 03/12/2012 to set to unit number if sublocation = '-' or space
			if ((!vo.getSubLocation().trim().equals("-") ) && (vo.getSubLocation().trim().length()>0 ))
				txtMeterSubLocation.setText(vo.getSubLocation());
			else
				txtMeterSubLocation.setText(this.lblUnitNo.getText());
			//llm 03/12/2012 to set to unit number if sublocation = '-' or space
		else
			txtMeterSubLocation.setText(this.lblUnitNo.getText());

		txtMeterSubLocation.setPadding(5, 8, 0, 0);
		txtMeterSubLocation.setVisibility(View.INVISIBLE);// not used

		Button btnReset = new Button(this);

		btnReset.setBackgroundResource(R.drawable.reset);
		btnReset.setTag("btnReset" + Integer.toString(cntbutton));
		btnReset.setOnClickListener(onClick);
		btnReset.setLayoutParams(layoutIcons);
		btnReset.setEnabled(enable);
		//if (scheduleVo.getStatusId()==1 )
		btnReset.setVisibility(View.INVISIBLE);

		linearLayout.addView(lblSerialNo);
		linearLayout.addView(txtMeterId); //store meter id
		linearLayout.addView(txtMeterCat);//store meter category
		linearLayout.addView(txtMeter);
		linearLayout.addView(txtMeterSubLocation);
		//linearLayout.addView(btnReset);
		linearLayout.requestLayout();
		linearLayout.invalidate();

		//linearlayout1 controls ends

		//linearlayout for meter type start

		LinearLayout linearLayoutmetertype = new LinearLayout(this);
		linearLayoutmetertype.setLayoutParams(layoutParams);
		layoutParams.gravity = Gravity.LEFT;
		layoutParams.setMargins(0, 0, 0, 0);
		linearLayoutmetertype.setTag("lnrMeterType" + Integer.toString(cntbutton));

		TextView lblMeterType = new TextView(this);
		layoutParamsCom.gravity = Gravity.LEFT;
		lblMeterType.setLayoutParams(layoutParamsCom);
		lblMeterType.setTextColor(Color.BLACK);
		lblMeterType.setTextSize(inttextsize);
		lblMeterType.setTag("lblMeterType" + Integer.toString(cntbutton));
		lblMeterType.setText("Meter Type");

		LinearLayout.LayoutParams layoutParamsMT = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 2.3f);

		TextView txtMeterType = new TextView(this);
		layoutParamsMT.gravity = Gravity.LEFT;
		txtMeterType.setLayoutParams(layoutParamsMT);
		txtMeterType.setPadding(5, 8, 0, 0);
		txtMeterType.setTextColor(Color.BLACK);
		txtMeterType.setTextSize(inttextsize);
		txtMeterType.setTag("txtMeterType" + Integer.toString(cntbutton));
		if (vo.getMeterType() !=null)
			txtMeterType.setText(vo.getMeterType());

		//linearlayout for meter type end
		linearLayoutmetertype.addView(lblMeterType);
		linearLayoutmetertype.addView(txtMeterType);
		linearLayoutmetertype.requestLayout();
		linearLayoutmetertype.invalidate();

		//linearlayout for meter uom start

		LinearLayout linearLayoutmeteruom = new LinearLayout(this);
		linearLayoutmeteruom.setLayoutParams(layoutParams);
		layoutParams.gravity = Gravity.LEFT;
		layoutParams.setMargins(0, 0, 0, 0);
		linearLayoutmeteruom.setTag("lnrMeterUOM" + Integer.toString(cntbutton));

		TextView lblMeteruom = new TextView(this);
		layoutParamsCom.gravity = Gravity.LEFT;
		lblMeteruom.setLayoutParams(layoutParamsCom);
		lblMeteruom.setTextColor(Color.BLACK);
		lblMeteruom.setTextSize(inttextsize);
		lblMeteruom.setTag("lblMeteruom" + Integer.toString(cntbutton));
		lblMeteruom.setText("UOM");


		TextView txtMeterUOM = new TextView(this);
		layoutParamsMT.gravity = Gravity.LEFT;
		txtMeterUOM.setLayoutParams(layoutParamsMT);
		txtMeterUOM.setPadding(5, 8, 0, 0);
		txtMeterUOM.setTextColor(Color.BLACK);
		txtMeterUOM.setTextSize(inttextsize);
		txtMeterUOM.setTag("txtMeterUOM" + Integer.toString(cntbutton));
		if (vo.getMeterClass() !=null)
			txtMeterUOM.setText(vo.getMeterClass());

		//linearlayout for meter uom end
		linearLayoutmeteruom.addView(lblMeteruom);
		linearLayoutmeteruom.addView(txtMeterUOM);
		linearLayoutmeteruom.requestLayout();
		linearLayoutmeteruom.invalidate();


		//linearlayout2 controls start
		LinearLayout linearLayout2 = new LinearLayout(this);
		LinearLayout.LayoutParams layoutParamsReading = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		linearLayout2.setLayoutParams(layoutParamsReading);
		layoutParamsReading.gravity = Gravity.LEFT;
		layoutParamsReading.setMargins(0, 0, 0, 0);
		linearLayout2.setTag("lnrMeterReading" + Integer.toString(cntbutton));


		final EditText txtMeter2 = new EditText(this);
		txtMeter2.setHint("Meter Reading");
		txtMeter2.setHintTextColor(R.color.col_lightgrey);
		LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT, 1.8f);
		layoutParams2.gravity = Gravity.CENTER;
		txtMeter2.setLayoutParams(layoutParams2);
		txtMeter2.setTextColor(Color.BLACK);
		txtMeter2.setTextSize(inttextsize);
		txtMeter2.setTag("txtMeterRead" + Integer.toString(cntbutton));
		txtMeter2.setGravity(17);
		txtMeter2.setSelectAllOnFocus(true);

		if (meterSendVO != null && meterSendVO.getSCurrReading()!=null)
			txtMeter2.setText(meterSendVO.getSCurrReading());
		else
			txtMeter2.setText(comFunction.AddLeadingZeros(spinnum, "",decnum));


		int maxLength = maxlen;  //2 = 1 for period, 1
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(maxLength);
		txtMeter2.setFilters(FilterArray);
		txtMeter2.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);

		DigitsKeyListener MyDigitKeyListener =
				new DigitsKeyListener(false, true); // first true : is signed, second one : is decimal
		txtMeter2.setKeyListener( MyDigitKeyListener );
		txtMeter2.setEnabled(enable);
		txtMeter2.setFocusable(enable);

		txtMeter2.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus)
					//calVariance(txtMeter2.getText().toString(),false);
					if ((txtMeter2.getText().length()>0) )
						doCalculation(v,true);

				if (hasFocus)
					lastindex = cntbutton;
			}
		});

		txtMeter2.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s) {
				//if (txtMeter2.getText().length()>0)
				doCalculation(txtMeter2,false);


			}
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			public void onTextChanged(CharSequence s, int start, int before, int count){

			}
		});

//		Button btnMeter = new Button(this);
//		btnMeter.setTag("btnMeter" + Integer.toString(cntbutton));
//		LinearLayout.LayoutParams layoutParamsbtn = new LinearLayout.LayoutParams(65, 65);
//		layoutParamsbtn.gravity = Gravity.LEFT;
//		layoutParamsbtn.setMargins(2, 2, 2, 2);
//
//		btnMeter.setLayoutParams(layoutParamsbtn);
//		btnMeter.setText("");
//		btnMeter.setBackgroundResource(R.drawable.meter);
//		btnMeter.setPadding(3, 3, 3, 3);
//		btnMeter.setOnClickListener(onClick);
//		btnMeter.setEnabled(enable);

		TextView txtMultiplyFactor = new TextView(this);
		txtMultiplyFactor.setLayoutParams(layoutParams1);
		txtMultiplyFactor.setTextColor(Color.BLACK);
		txtMultiplyFactor.setTextSize(inttextsize);
		txtMultiplyFactor.setTag("txtMultiplyFactor" + Integer.toString(cntbutton));
		txtMultiplyFactor.setGravity(17);
		txtMultiplyFactor.setPadding(0, 0, 0, 0);
		if (vo.getMultiplyFactor()!=null)
			txtMultiplyFactor.setText(" X " +Float.toString(vo.getMultiplyFactor()));
		else
			txtMultiplyFactor.setText("");

		linearLayout2.addView(txtMeter2);
		//linearLayout2.addView(btnMeter);
		//linearLayout2.addView(txtMultiplyFactor);
		linearLayout2.requestLayout();
		linearLayout2.invalidate();
		//linearlayout2 controls ends


		//linearlayoutmeterround controls start
		LinearLayout linearLayoutMeterRound = new LinearLayout(this);
		linearLayoutMeterRound.setLayoutParams(layoutParamsReading);
		linearLayoutMeterRound.setTag("linearLayoutMeterRound" + Integer.toString(cntbutton));

		TextView lblMeterRound = new TextView(this);
		LinearLayout.LayoutParams layoutParamsMeterRound = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT, 3.5f);
		layoutParamsMeterRound.gravity = Gravity.CENTER;
		lblMeterRound.setLayoutParams(layoutParamsMeterRound);
		lblMeterRound.setTextColor(Color.BLACK);
		lblMeterRound.setTextSize(inttextsize);
		lblMeterRound.setTag("lblMeterRound" + Integer.toString(cntbutton));
		lblMeterRound.setText("Meter value has turned back to zero after exceed the maximal digit");
		lblMeterRound.setBackgroundResource(R.color.Red);
		lblMeterRound.setTypeface(null,Typeface.BOLD);
		lblMeterRound.setGravity(17);

		CheckBox chkMeterRound = new CheckBox(this);
		LinearLayout.LayoutParams layoutParamsMeterRound2 = new LinearLayout.LayoutParams(0,  LayoutParams.WRAP_CONTENT, 0.35f);
		chkMeterRound.setLayoutParams(layoutParamsMeterRound2);
		chkMeterRound.setTextColor(Color.BLACK);
		chkMeterRound.setTextSize(inttextsize);
		chkMeterRound.setTag("chkMeterRound" + Integer.toString(cntbutton));
		chkMeterRound.setGravity(17);
		chkMeterRound.setBackgroundResource(R.color.Red);
		chkMeterRound.setClickable(false);
		chkMeterRound.setVisibility(View.GONE);
		linearLayoutMeterRound.setVisibility(View.GONE);
		if (vo.getMeterRound()!=null)
		{
			if (vo.getMeterRound()==true)
				linearLayoutMeterRound.setVisibility(View.VISIBLE);

		}

//		if (vo.getMeterRound()!=null)
//		{
//			if (vo.getMeterRound()==true)
//			{
//				chkMeterRound.setChecked(vo.getMeterRound());
//				linearLayoutMeterRound.setVisibility(View.VISIBLE);
//			}
//
//		}


		linearLayoutMeterRound.addView(lblMeterRound);
		linearLayoutMeterRound.addView(chkMeterRound);
		linearLayoutMeterRound.requestLayout();
		linearLayoutMeterRound.invalidate();
		//linearlayoutmeterround controls end


		//linearlayout21 controls start
		LinearLayout linearLayout21 = new LinearLayout(this);
		linearLayout21.setLayoutParams(layoutParamsReading);
		linearLayout2.setTag("lnrConsumption" + Integer.toString(cntbutton));

		TextView lblConsumption = new TextView(this);
		layoutParamsCom.gravity = Gravity.LEFT;
		lblConsumption.setLayoutParams(layoutParamsCom);
		lblConsumption.setTextColor(Color.BLACK);
		lblConsumption.setTextSize(inttextsize);
		lblConsumption.setTag("lblConsumption" + Integer.toString(cntbutton));
		lblConsumption.setText("Consumption");

		TextView lblConsumptionPercent = new TextView(this);
		LinearLayout.LayoutParams layoutParamsCom2 = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.4f);
		lblConsumptionPercent.setLayoutParams(layoutParamsCom2);
		lblConsumptionPercent.setTextColor(Color.BLACK);
		lblConsumptionPercent.setTextSize(inttextsize);
		lblConsumptionPercent.setTag("lblConsumptionPercent" + Integer.toString(cntbutton));
		lblConsumptionPercent.setGravity(17);
		if (meterSendVO != null && meterSendVO.getSConsumption()!=null)
			lblConsumptionPercent.setText(Double.toString(comFunction.round3Decimals(Double.parseDouble(meterSendVO.getSConsumption()))));

		else
			lblConsumptionPercent.setText("0.000");

		//empty view
		TextView lblEmptyView1 = new TextView(this);
		LinearLayout.LayoutParams layoutParamsEmpty = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
		lblEmptyView1.setLayoutParams(layoutParamsEmpty);

		Button btnCamerax  = new Button(this);
		LinearLayout.LayoutParams layoutParamsCamera = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.4f);
		btnCamerax.setLayoutParams(layoutParamsCamera);
		layoutParamsCamera.gravity = Gravity.RIGHT ;
		layoutParamsCamera.setMargins(0, 0, 10, 0);
		btnCamerax.setPadding(50, 0, 0, 0);
		btnCamerax.setVisibility(View.INVISIBLE);//for alignment only

		linearLayout21.addView(lblConsumption);
		linearLayout21.addView(lblConsumptionPercent);
		linearLayout21.addView(lblEmptyView1);//empty view
		linearLayout21.addView(btnCamerax);//for alignment only
		linearLayout21.requestLayout();
		linearLayout21.invalidate();
		//linearlayout21 controls end


		//linearlayout22 controls start
		LinearLayout linearLayout22 = new LinearLayout(this);
		linearLayout21.setLayoutParams(layoutParamsReading);
		linearLayout2.setTag("lnrVariance" + Integer.toString(cntbutton));

		TextView lblVariance = new TextView(this);
		lblVariance.setLayoutParams(layoutParamsCom);
		lblVariance.setTextColor(Color.BLACK);
		lblVariance.setTextSize(inttextsize);
		lblVariance.setTag("lblVariance" + Integer.toString(cntbutton));
		lblVariance.setText("Variance (%)");

		TextView lblVariancePercent = new TextView(this);
		lblVariancePercent.setLayoutParams(layoutParamsCom2);
		lblVariancePercent.setTextColor(Color.BLACK);
		lblVariancePercent.setTextSize(inttextsize);
		lblVariancePercent.setTag("lblVariancePercent" + Integer.toString(cntbutton));
		lblVariancePercent.setGravity(17);
		lblVariancePercent.setText("0");
		//set to 0 first. calculation for variance start from L672

		//empty view
		TextView lblEmptyView2 = new TextView(this);
		lblEmptyView2.setLayoutParams(layoutParamsEmpty);

		//linearLayout22.addView(lblVariance);
		//linearLayout22.addView(lblVariancePercent);
		//linearLayout22.addView(lblEmptyView2);//empty view
		//linearLayout22.requestLayout();
		//linearLayout22.invalidate();
		//linearlayout21 controls end


		//linearlayout3 controls starts
		LinearLayout linearLayout3 = new LinearLayout(this);
		LinearLayout.LayoutParams layoutParamsPrev1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		linearLayout3.setLayoutParams(layoutParamsPrev1);
		layoutParamsPrev1.gravity = Gravity.LEFT;
		layoutParamsPrev1.setMargins(0, 0, 0, 0);
		linearLayout3.setTag("lnrMeterPrev1" + Integer.toString(cntbutton));

		TextView lblPrevConsumption = new TextView(this);
		LinearLayout.LayoutParams layoutParamsPrevC = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.4f);

		lblPrevConsumption.setLayoutParams(layoutParamsCom);
		lblPrevConsumption.setTextColor(Color.BLACK);
		lblPrevConsumption.setTextSize(inttextsize);
		lblPrevConsumption.setTag("lblPrevConsumption" + Integer.toString(cntbutton));
		lblPrevConsumption.setText("Previous Consumption");

		TextView txtPrevConsumption = new TextView(this);
		LinearLayout.LayoutParams layoutParamsPrevReading = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.4f);

		txtPrevConsumption.setLayoutParams(layoutParamsPrevReading);
		txtPrevConsumption.setTextColor(Color.BLACK);
		txtPrevConsumption.setTextSize(inttextsize);
		txtPrevConsumption.setTag("txtPrevConsumption" + Integer.toString(cntbutton));
		txtPrevConsumption.setGravity(17);


		MonthlyMeterVO pvo = new MonthlyMeterVO();
		try {
			pvo =bo.getPrevMetersReadingHistory(vo.getMeterNo());
		} catch (ULjException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		if (pvo!=null)
//		{
//			if (pvo.getConsumption()!=null)
//			txtPrevConsumption.setText(Double.toString(pvo.getConsumption()));
//		}
//		else
//		{
//			if (vo.getPrevConsumption()!=null)
//			{txtPrevConsumption.setText(Double.toString(vo.getPrevConsumption()));}
//			else
//			{
//				if (vo.getLastReading()!=null)
//				{
//					txtPrevConsumption.setText(Double.toString(vo.getLastReading()-vo.getInitialReading()));
//				}
//				else
//					txtPrevConsumption.setText("0");
//			}
//		}

		if (pvo!=null)
		{

			if (vo.getPrevConsumption()!=null){
				// txtPrevConsumption.setText(Double.toString(vo.getPrevConsumption()));
				// juehua for decimal limitation
				txtPrevConsumption.setText(df.format(vo.getPrevConsumption()));
			}else
			{
				if (pvo.getConsumption()!= null){
					// juehua for decimal limitation
					txtPrevConsumption.setText(df.format(pvo.getConsumption()));
				}else
				{
					if (vo.getLastReading()!=null)
					{
						// juehua for decimal limitation
						// llm 11/12/12 check for last reading = 0
						if (Float.parseFloat(vo.getLastReading().toString()) != 0)
							txtPrevConsumption.setText(df.format(vo.getLastReading() - vo.getInitialReading()));
						else
							txtPrevConsumption.setText("0");
					}
					else
						txtPrevConsumption.setText("0");
				}
			}

		}
		else
		{
			if (vo.getPrevConsumption() != null) {
				// juehua for decimal limitation
				txtPrevConsumption.setText(df.format(vo.getPrevConsumption()));
			} else {
				if (vo.getLastReading() != null) {
					// juehua for decimal limitation
					// llm 11/12/12 check for last reading = 0
					if (Float.parseFloat(vo.getLastReading().toString()) != 0)
						txtPrevConsumption.setText(df.format(vo.getLastReading() - vo.getInitialReading()));
					else
						txtPrevConsumption.setText("0");
				} else
					txtPrevConsumption.setText("0");
			}
		}

		//empty view
		TextView lblEmptyView31 = new TextView(this);
		lblEmptyView31.setLayoutParams(layoutParamsEmpty);

		TextView lblEmptyView3 = new TextView(this);
		lblEmptyView3.setLayoutParams(layoutParamsEmpty);

		linearLayout3.addView(lblPrevConsumption);
		linearLayout3.addView(txtPrevConsumption);
		linearLayout3.addView(lblEmptyView31);//empty view
		linearLayout3.requestLayout();
		linearLayout3.invalidate();
		linearLayout3.setVisibility(View.GONE);

		//linearlayout3 controls ends

		//linearlayout4 controls starts
		LinearLayout linearLayout4 = new LinearLayout(this);
		LinearLayout.LayoutParams layoutParamsPrev2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		linearLayout4.setLayoutParams(layoutParamsPrev2);
		layoutParamsPrev2.gravity = Gravity.LEFT;
		layoutParamsPrev2.setMargins(0, 0, 0, 0);
		linearLayout4.setTag("lnrMeterPrev2" + Integer.toString(cntbutton));

		TextView lblPrevReading = new TextView(this);
		lblPrevReading.setLayoutParams(layoutParamsPrevC);
		lblPrevReading.setTextColor(Color.BLACK);
		lblPrevReading.setTextSize(inttextsize);
		lblPrevReading.setTag("lblPrevReading" + Integer.toString(cntbutton));
		lblPrevReading.setText("Previous Reading");

		TextView txtPrevReading = new TextView(this);

		txtPrevReading.setLayoutParams(layoutParamsPrevReading);
		layoutParamsPrevReading.gravity = Gravity.LEFT;
		txtPrevReading.setTextColor(Color.BLACK);
		txtPrevReading.setTextSize(inttextsize);
		txtPrevReading.setTag("txtPrevReading" + Integer.toString(cntbutton));
		txtPrevReading.setGravity(17);


//		if (pvo!=null)
//		{
//			if (pvo.getCurrReading()!=null)
//			txtPrevReading.setText(pvo.getCurrReading());
//		}
//		else
//		{
//			if (vo.getPrevReading()!=null)
//			{txtPrevReading.setText(vo.getPrevReading());}
//			else
//			{
//				if (vo.getLastReading()!=null)
//				txtPrevReading.setText(Double.toString(vo.getLastReading()));
//				else
//				txtPrevReading.setText("0");
//
//			}
//		}


		if (pvo!=null)
		{


			if (vo.getPrevReading()!=null)
				txtPrevReading.setText(vo.getPrevReading());
			else
			{
				if (pvo.getCurrReading()!=null)
					txtPrevReading.setText(pvo.getCurrReading());
				else
				{
					if (vo.getLastReading()!=null)
						txtPrevReading.setText(Double.toString(vo.getLastReading()));
					else
						txtPrevReading.setText("0");
				}
			}


		}
		else
		{
			if (vo.getPrevReading()!=null)
			{txtPrevReading.setText(vo.getPrevReading());}
			else
			{
				if (vo.getLastReading()!=null)
					txtPrevReading.setText(Double.toString(vo.getLastReading()));
				else
					txtPrevReading.setText("0");

			}
		}
		//calculate variance start
		Double cr=0.000;
		Double c=0.000;
		Double pr=0.000;
		Double pc=0.000;
		Double va = 0.000;

		if (txtMeter2.getText().toString().length()>0)
		{
			cr= Double.parseDouble(txtMeter2.getText().toString());

			pr= Double.parseDouble(txtPrevReading.getText().toString());
			pc = Double.parseDouble(txtPrevConsumption.getText().toString());
			c=Double.parseDouble(lblConsumptionPercent.getText().toString());

			/*
			 *

			if (vo.getMeterRound() !=null )
				if (vo.getMeterRound()==true)
				{c= maxMeterValue+cr-pr;}
				else
				{c= cr-pr;}
			else
			{c= cr-pr;}
			*/
			va= (c-pc)/pc;
			va =(double)Math.round(va * 100) / 100;
			va = va*100;

			if (pc==0)
			{lblVariancePercent.setText("0");}
			else
			{lblVariancePercent.setText(Double.toString(comFunction.round3Decimals(va)));}

			double compareva =0;
			compareva= va;
			if (va<0)
				compareva= -va;

			if (compareva > consumptionalert)
			{
				lblVariancePercent.setBackgroundResource(R.color.Red);
				lblVariancePercent.setTypeface(null,Typeface.BOLD);

			}
			else
			{
				//lblVariancePercent.setBackgroundResource(R.color.bgcolor_lightpurple);
				lblVariancePercent.setTypeface(null);
			}
		}
		//calculate variance end

		Button btnCamera  = new Button(this);
		btnCamera.setLayoutParams(layoutParamsCamera);
		layoutParamsCamera.gravity = Gravity.RIGHT ;
		layoutParamsCamera.setMargins(0, 0, 10, 0);
		btnCamera.setPadding(50, 0, 0, 0);
		btnCamera.setTag("btnCamera" + Integer.toString(cntbutton));
		btnCamera.setBackgroundResource(R.drawable.camera_button_selector);
		btnCamera.setOnClickListener(onClick);

		if (scheduleVo.getStatusId()==1 )
		{
			btnCamera.setEnabled(true);
			btnCamera.setVisibility(View.VISIBLE);
			btnReset.setVisibility(View.VISIBLE);
		}
		else
		{
			if (enable ==false)
			{
				btnCamera.setVisibility(View.INVISIBLE);
				btnReset.setVisibility(View.INVISIBLE);
			}
			else
			{
				btnCamera.setVisibility(View.VISIBLE);
				btnReset.setVisibility(View.VISIBLE);
			}
		}
		linearLayout4.addView(lblPrevReading);
		linearLayout4.addView(txtPrevReading);
		linearLayout4.addView(lblEmptyView3);//empty view
		//linearLayout4.addView(btnCamera);
		linearLayout4.requestLayout();
		linearLayout4.invalidate();

		//linearlayout4 controls ends



		//linearlayout 5 start
		LinearLayout linearLayout5 = new LinearLayout(this);
		linearLayout4.setLayoutParams(layoutParamsPrev2);
		layoutParamsPrev2.gravity = Gravity.LEFT;
		layoutParamsPrev2.setMargins(0, 0, 0, 0);
		linearLayout4.setTag("lnrMeterRemarks" + Integer.toString(cntbutton));

		EditText txtRemarks = new EditText(this);
		txtRemarks.setHint("Remarks");
		txtRemarks.setHintTextColor(R.color.col_lightgrey);
		LinearLayout.LayoutParams layoutParamsRMK = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3.5f);
		layoutParamsRMK.gravity = Gravity.LEFT;
		txtRemarks.setLayoutParams(layoutParamsRMK);
		txtRemarks.setTextColor(Color.BLACK);
		txtRemarks.setTextSize(inttextsize);
		txtRemarks.setTag("txtRemarks" + Integer.toString(cntbutton));
		txtRemarks.setGravity(0);

		if (meterSendVO!=null)
			txtRemarks.setText(meterSendVO.getSRemarks());
		else
			txtRemarks.setText("");

		txtRemarks.setEnabled(enable);
		txtRemarks.setFocusable(enable);

		linearLayout5.addView(txtRemarks);
		linearLayout5.requestLayout();
		linearLayout5.invalidate();
		//linearlayout 5 ends

		TextView txtline = new TextView(this);
		LinearLayout.LayoutParams layoutParamsline = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,2);
		txtline.setLayoutParams(layoutParamsline);
		txtline.setTextSize(inttextsize);
		txtline.setText("");
		txtline.setTag("txtline" + Integer.toString(cntbutton));
		txtline.setBackgroundColor(R.color.col_lightgrey);

		linearLayoutNonCollapse.addView(linearLayout);
		linearLayoutNonCollapse.addView(linearLayoutmetertype);
		linearLayoutNonCollapse.addView(linearLayoutmeteruom);
		linearLayoutNonCollapse.addView(linearLayout4);
		linearLayoutCollapse.addView(linearLayout2);
		linearLayoutCollapse.addView(linearLayoutMeterRound);
		linearLayoutCollapse.addView(linearLayout21); //consumption
		//linearLayoutCollapse.addView(linearLayout22);

		linearLayoutCollapse.addView(linearLayout5);
		linearLayoutCollapse.addView(linearLayout3);

		linearLayoutMeterC1.addView(linearLayoutNonCollapse);
		linearLayoutMeterC2.addView(linearLayoutCollapse);

		//linearLayoutCollapse.setVisibility(View.GONE);

		//lnrMeter.addView(txtline);
		lnrMeter.requestLayout();
		lnrMeter.invalidate();

	}



	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == imgViewThumbNail[0]) {
			imgViewPreview.setImageBitmap(bitmapReplace[0]);
			imgViewPreview.setTag(0);
			txtCaption.setText(captions[0]);
		} else if (v == imgViewThumbNail[1]) {
			imgViewPreview.setImageBitmap(bitmapReplace[1]);
			imgViewPreview.setTag(1);
			txtCaption.setText(captions[1]);
		} else if (v == imgViewThumbNail[2]) {
			imgViewPreview.setImageBitmap(bitmapReplace[2]);
			imgViewPreview.setTag(2);
			txtCaption.setText(captions[2]);

		} else if (v == imgViewThumbNail[3]) {
			imgViewPreview.setImageBitmap(bitmapReplace[3]);
			imgViewPreview.setTag(3);
			txtCaption.setText(captions[3]);
		} else if (v == imgViewThumbNail[4]) {
			imgViewPreview.setImageBitmap(bitmapReplace[4]);
			imgViewPreview.setTag(4);
			txtCaption.setText(captions[4]);
		} else  if (v == imgBtnDel) // delete button of alert is selected,
		{

			for (int i = iPhoto; i < bitmapReplace.length; i++) {

				if ((i + 1) < bitmapReplace.length) // removes
				// the
				// selected
				// image
				// from
				// array
				{
					bitmapReplace[i] = bitmapReplace[i + 1];
					bitmapReplace[i + 1] = null;
					captions[i] = captions[i + 1];
					captions[i + 1] = null;
					gpsLocation[i] = gpsLocation[i + 1];
					gpsLocation[i + 1] = null;
					imageDate[i] = imageDate[i + 1];
					imageDate[i + 1] = null;

				} else {
					bitmapReplace[i] = null;
					captions[i] = null;
					gpsLocation[i] = null;
					imageDate[i] = null;

					linearLayoutMeterPhoto.setVisibility(View.GONE);
					linearLayoutPreview.setVisibility(View.GONE);
					linearLayoutCaption.setVisibility(View.GONE);
					linearLayoutThumb.setVisibility(View.GONE);
				}
			}
			dialogEdit.dismiss();
			imageCount--;
			if (imageCount < 4)
				btnCamera0.setEnabled(true);
			setImage();

		} else if (v == imgBtnNoDel) // delete button of alert is selected,
		{
			dialogEdit.dismiss();
		} else if (v == imgBtnClose) {
			dialogEdit.dismiss();
		}
		else if (v == imgBtnSave) // save button clicked.
		{

			//docalculation for last record
			TextView tR = new TextView(this);
			lastindex=1;
			tR = (TextView) this.lnrMeter.findViewWithTag("txtMeterRead" + lastindex);

			//llm 10/07/2013 fix meter round bug
			//CheckBox chk1 = new CheckBox(this);
			//chk1 = (CheckBox)this.lnrMeter.findViewWithTag("chkMeterRound" + lastindex);
			doCalculation(tR,true);

			//Toast toast1 = Toast.makeText(MonthlyMeterView.this, "meter round : "+chk1.isChecked(), Toast.LENGTH_LONG);
			//toast1.setGravity(Gravity.CENTER, 40, 60);
			//toast1.show();

			if (txtJobStart.getText() == null || "".equals(txtJobStart.getText().toString()))
			{
				DisplayUtil dUtil = new DisplayUtil();
				dUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_START_DATE_REQUIRED);

			}
			else
			{
				try {
					if ( validateFieldsforSave()) {
						if (validateImages("save"))
						{
							doSave();

							saveImage();

							requestCommand.execute();


//							if (singalStenths>=signallimit) {
//								if (syncUtil.isSynching==false)
		//used imgBUtton SAve
//								syncUtil.synchronizedSyncForVersionWithNotification(AppsConstant.VERS_UPDATE_MONTHLY_METER, AppsConstant.SUB_VERS_MONTHLY_METER_UPLOAD,
//									DbUtil.getConnection(), this.getApplicationContext(), true);
//								//MonthlyMeterView.this
//							}
//							else
//							{
//								Toast toast = Toast.makeText(MonthlyMeterView.this, "no sync", Toast.LENGTH_LONG);
//								toast.setGravity(Gravity.CENTER, 40, 60);
//								toast.show();
//							}
							Toast toast = Toast.makeText(MonthlyMeterView.this, "Meter Reading Saved!", Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 40, 60);
							toast.show();
						}
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


		} else if (String.valueOf(v.getTag()).startsWith("btnMeter") ) {

			String x = String.valueOf(v.getTag());
			x = x.replace("btnMeter", "").trim();
			txMeterValue = (TextView) this.lnrMeter.findViewWithTag("txtMeterRead" + x);

			lblConsumptionPercent=(TextView) this.lnrMeter.findViewWithTag("lblConsumptionPercent" + x);
			txtPrevConsumption=(TextView) this.lnrMeter.findViewWithTag("txtPrevConsumption" + x);
			txtPrevReading=(TextView) this.lnrMeter.findViewWithTag("txtPrevReading" + x);
			lblVariancePercent=(TextView) this.lnrMeter.findViewWithTag("lblVariancePercent" + x);
			txMeterCat= (TextView) this.lnrMeter.findViewWithTag("txtMeterCat" + x);


			if (txMeterCat.getText().toString().contains(".") && checkJobStarted())
				spinnum=Integer.parseInt( txMeterCat.getText().toString().trim().substring(0,  txMeterCat.getText().toString().indexOf(".")));
			else
				spinnum=Integer.parseInt( txMeterCat.getText().toString().trim());

			spinnum=5;
			spinnum = 10-spinnum;

			NumtoShowonSpn=txtPrevReading.getText().toString();

			if (txMeterValue != null) {
				showMeterDialog(v);
			}
		} else if (String.valueOf(v.getTag()).startsWith("btnCamera")
				|| v==btnCamera0
				) {

//			btnCamera = (Button) this.lnrMeter.findViewWithTag(v.getTag());
//			if (btnCamera != null) {
//				Integer ipos,meterno = 0;
//				ipos = Integer.parseInt(v.getTag().toString().replace("btnCamera",""));
//				txtMeterNo=(TextView) this.lnrMeter.findViewWithTag("txtMeterId" + Integer.toString(ipos));
//				meterno = Integer.parseInt(txtMeterNo.getText().toString());
//
//				goToTakePhoto(ipos,meterno);
//			}

//			Integer ipos,meterno = 0;
//			ipos = 1;
//			txtMeterNo=(TextView) this.lnrMeter.findViewWithTag("txtMeterId" + Integer.toString(ipos));
//			meterno = Integer.parseInt(txtMeterNo.getText().toString());
//
//			goToTakePhoto(ipos,meterno);
			Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

			startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);

		}
		else if (String.valueOf(v.getTag()).startsWith("btnIcon") && checkJobStarted()) {
			btnIcon = (Button) this.lnrMeter.findViewWithTag(v.getTag());
			if (btnIcon != null) {
				Integer ipos = 0;
				ipos = Integer.parseInt(v.getTag().toString().replace("btnIcon",""));
				LinearLayout linearLayoutCollapse= new LinearLayout(this);
				linearLayoutCollapse=(LinearLayout) this.lnrMeter.findViewWithTag("lnrCollapse" + Integer.toString(ipos));
//				if ( linearLayoutCollapse.getVisibility()==View.GONE)
//				{linearLayoutCollapse.setVisibility(View.VISIBLE);}
//				else
//				{linearLayoutCollapse.setVisibility(View.GONE);}


			}

		} else if (String.valueOf(v.getTag()).startsWith("btnReset") && checkJobStarted()) {

			btnReset = (Button) this.lnrMeter.findViewWithTag(v.getTag());
			if (btnReset != null) {

				String x = String.valueOf(v.getTag());
				x = x.replace("btnReset", "").trim();
				txMeterValue = (TextView) this.lnrMeter.findViewWithTag("txtMeterRead" + x);

				lblConsumptionPercent=(TextView) this.lnrMeter.findViewWithTag("lblConsumptionPercent" + x);
				txtPrevConsumption=(TextView) this.lnrMeter.findViewWithTag("txtPrevConsumption" + x);
				txtPrevReading=(TextView) this.lnrMeter.findViewWithTag("txtPrevReading" + x);
				lblVariancePercent=(TextView) this.lnrMeter.findViewWithTag("lblVariancePercent" + x);
				linearLayoutMeterRound=(LinearLayout) this.lnrMeter.findViewWithTag("linearLayoutMeterRound" + x);
				chkmeterround= (CheckBox) this.lnrMeter.findViewWithTag("chkMeterRound" + x);

				Integer ipos = 0;
				ipos = Integer.parseInt(v.getTag().toString().replace("btnReset",""));

				showResetDialog(monthlymeterno, ipos);


			}
		}
		else if (v == btnSign)
		{
			showDialog(3);
		}
		else if (v == imgViewSign)
		{
			showDialog(3);
		}

	}

	private boolean validateFields() {
		boolean allMandatoryFieldsPopulated = true;
		StringBuffer alertMessage = new StringBuffer();
		// alertMessage.append("");

		String alertHeader = "Please enter the following fields : ";
		DisplayUtil dUtil = new DisplayUtil();


		int c = 0;
		int d = 0;
		int lc = 0;

		//for (c = 0; c < lnrMeter.getChildCount(); c++) {

		try
		{

			//if (lnrMeter.getChildAt(c).getTag().toString().contains("lnrCollapse")) {
//					LinearLayout lnr = new LinearLayout(this);
//					lnr = (LinearLayout) lnrMeter.getChildAt(c);
			String strMeterRead = "";

//					for (lc= 0; lc<lnr.getChildCount(); lc++ )
//					{
			//if (lnr.getChildAt(lc).getClass().toString().contains("LinearLayout"))
			//{
			//LinearLayout lnr2 = new LinearLayout(this);
			//lnr2 = (LinearLayout) lnr.getChildAt(lc);
			//for (d = 0; d < lnr.getChildCount(); d++) {

			//if (lnr2.getChildAt(d).getTag() != null) {

			//if (lnr2.getChildAt(d).getTag().toString().contains("txtMeterRead")) {
			EditText childtxt = new EditText(this);
			//childtxt = (EditText) lnr2.getChildAt(d);
			TextView tMeterCat = new TextView(this);
			String intmeter = "";
			intmeter ="1";
			childtxt=(EditText) lnrMeter.findViewWithTag("txtMeterRead" + intmeter);
			if (childtxt.isEnabled()==true)
			{
				strMeterRead = childtxt.getText().toString();

				tMeterCat= (TextView) this.lnrMeter.findViewWithTag("txtMeterCat" + intmeter);
				String strMeterCatAlert="\nMeter Category : "+ tMeterCat.getText().toString()+"\n";;

				if ((strMeterRead.length() <= 0) )
				{
					//if (!alertMessage.toString().contains(strMeterCatAlert))
					//{alertMessage.append(strMeterCatAlert);}
					alertMessage.append("\nInvalid meter reading at meter");
				}

				int n1,n2=0;

				if (tMeterCat.getText().toString().contains("."))
				{
					n1=Integer.parseInt( tMeterCat.getText().toString().trim().substring(0, tMeterCat.getText().toString().indexOf(".")));
					n2=n1+4;
				}
				else
				{
					n1=Integer.parseInt(tMeterCat.getText().toString().trim());
					n2=n1;
				}


//											if ((strMeterRead.length() > 0) && (strMeterRead.length() != n2) )
//											{
//												if (Double.parseDouble(strMeterRead)>0)
//												{
//													if (!alertMessage.toString().contains(strMeterCatAlert))
//													{alertMessage.append(strMeterCatAlert);}
//													alertMessage.append("\nWrong number of digits at meter");
//												}
//											}
				if ((strMeterRead.length() > 0))
				{
					if (Double.parseDouble(strMeterRead)<=0)
					{
						//if (!alertMessage.toString().contains(strMeterCatAlert))
						//{alertMessage.append(strMeterCatAlert);}
						alertMessage.append("\nInvalid meter reading! ");
					}
				}
				if ((txMeterValue.getText().toString().startsWith(".")) || (txMeterValue.getText().toString().endsWith(".")))
				{
					if (Double.parseDouble(strMeterRead)>0)
					{
						//if (!alertMessage.toString().contains(strMeterCatAlert))
						//{alertMessage.append(strMeterCatAlert);}
						alertMessage.append("\nInvalid meter reading at meter" );
					}
				}



				if (tMeterCat.getText().toString().contains("."))
				{
					if ((strMeterRead.length() > 0) && !strMeterRead.contains("."))
					{
						if (Double.parseDouble(strMeterRead)>0)
						{
							//if (!alertMessage.toString().contains(strMeterCatAlert))
							//{alertMessage.append(strMeterCatAlert);}
							//	alertMessage.append("\nMissing Decimal Point at meter");
						}
					}
					else
					{
						int tmpdecimalplace =0;
						tmpdecimalplace = strMeterRead.subSequence(strMeterRead.indexOf("."), strMeterRead.length()).length()-1;
						if (tmpdecimalplace>3)
						{
							alertMessage.append("\nMaximun 3 decimal place is allowed at meter");

						}
					}
				}
				else
				{
					if ( (strMeterRead.length() > 0) && strMeterRead.contains("."))
					{
						if (Double.parseDouble(strMeterRead)>0)
						{
							//if (!alertMessage.toString().contains(strMeterCatAlert))
							//{alertMessage.append(strMeterCatAlert);}
							alertMessage.append("\nRedundant Decimal Point at meter");
						}

					}
				}
			}
			//}

			//}

			//}//end for d

			//}

			//}//end for lc


			//}
		}
		catch (Exception e1) {
		}


		//}//end for c





//		if (txtHname.getText() == null || "".equals(txtHname.getText().toString()))
//			alertMessage.append("\n - Handed By Name ");
//		if (txtHtelephone.getText() == null || "".equals(txtHtelephone.getText().toString()))
//			alertMessage.append("\n - Handed By Telephone");
//		if (txtHdate.getText() == null || "".equals(txtHdate.getText().toString()))
//			alertMessage.append("\n - Handed By Date");
//		if (bmpHsignature == null)
//			alertMessage.append("\n - Handed By Signature");

//		if (txtAname.getText() == null || "".equals(txtAname.getText().toString()))
//			alertMessage.append("\n - Witnessed By Name ");
//		if (txtAtelephone.getText() == null || "".equals(txtAtelephone.getText().toString()))
//			alertMessage.append("\n - Witnessed By Telephone ");
//		if (txtADesignation.getText() == null || "".equals(txtADesignation.getText().toString()))
//			alertMessage.append("\n - Witnessed By Designation ");

		// if (txtAdate.getText() == null
		// || "".equals(txtAdate.getText().toString()))
		// alertMessage.append("\n - Taken By Date");
//		if (bmpAsignature == null)
//			alertMessage.append("\n - Signature");

		if (this.lblUnitNo.getText().toString().length()<=0)
		{
			alertMessage.append("\nInvalid Unit No!" );

		}
		if (this.txtCustomerName.getText().toString().length()<=0)
		{
			alertMessage.append("\nInvalid Customer Name!" );

		}
		if (this.txtTenantName.getText().toString().length()<=0)
		{
			alertMessage.append("\nInvalid Tenant Name!" );

		}

		if ( comFunction.isValidEmail(txtTenantEmail.getText().toString())==false)
		{
			alertMessage.append("\nInvalid Email Address!" );

		}
		if (imgViewSign.getDrawable() == null)
			alertMessage.append("\n - Signature");




//		DropdownItemVO selectedItemVO;
//		selectedItemVO = (DropdownItemVO) this.spnMaintOfficerName.getSelectedItem();
//		if (Integer.parseInt(selectedItemVO.getItemId())<=0)
//			alertMessage.append("\n - Maintenance Officer Name  ");

		if (!"".equals(alertMessage.toString())) {
			allMandatoryFieldsPopulated = false;
			dUtil.displayAlertDialog(this, "Missing Input ", alertHeader + alertMessage.toString());
		}

		return allMandatoryFieldsPopulated;
	}


	public void doSave() throws Exception {
		Log.d(TAG, "save monthly meter - start");

		MonthlyMeterDAO dao = new MonthlyMeterDAO();
		//MonthlyMeterVO tvo = new MonthlyMeterVO();
		CommonFunction comFunction = new CommonFunction();
		DateUtil dateUtil = new DateUtil();
		try {


			String tmpDate = dateUtil.getCurrentTimeStamp().toString().replace("-", "").replace(".", "").replace(":", "").replace(" ", "").trim().substring(0, 8);

			int c = 0;
			int d = 0;
			int lc = 0;

			//for (c = 0; c < lnrMeter.getChildCount(); c++) {

//			try
//			{

//									if (lnrMeter.getChildAt(c).getTag().toString().contains("lnrCollapse")) {
//										LinearLayout lnr = new LinearLayout(this);
//										lnr = (LinearLayout) lnrMeter.getChildAt(c);
//										String strMeterRead = "";

				//for (lc= 0; lc<lnr.getChildCount(); lc++ )
				//{
				//if (lnr.getChildAt(lc).getClass().toString().contains("LinearLayout"))
				//{
				//LinearLayout lnr2 = new LinearLayout(this);
				//lnr2 = (LinearLayout) lnr.getChildAt(lc);
				//for (d = 0; d < lnr.getChildCount(); d++) {

				//if (lnr2.getChildAt(d).getTag() != null) {

				//if (lnr2.getChildAt(d).getTag().toString().contains("txtMeterRead")) {
//																TextView childtxt = new TextView(context);
//																childtxt = (EditText) lnr2.getChildAt(d);
//																strMeterRead = childtxt.getText().toString();
//																String intmeter = "";
//																intmeter = lnr2.getChildAt(d).getTag().toString().replace("txtMeterRead", "");
				String intmeter = "";
				intmeter ="1";

				txtPrevConsumption=(TextView) lnrMeter.findViewWithTag("txtPrevConsumption" + intmeter);
				txtPrevReading=(TextView) lnrMeter.findViewWithTag("txtPrevReading" + intmeter);
				txtMeterReading=(TextView) lnrMeter.findViewWithTag("txtMeterRead" + intmeter);
				lblConsumptionPercent=(TextView) lnrMeter.findViewWithTag("lblConsumptionPercent" + intmeter);
				txtMeterNo=(TextView) lnrMeter.findViewWithTag("txtMeterNo" + intmeter);
				txtMultiplyFactor=(TextView) lnrMeter.findViewWithTag("txtMultiplyFactor" + intmeter);
				txtRemarks=(TextView) lnrMeter.findViewWithTag("txtRemarks" + intmeter);
				txtMeterId=(TextView) lnrMeter.findViewWithTag("txtMeterId" + intmeter);
				chkmeterround=	(CheckBox) lnrMeter.findViewWithTag("chkMeterRound" + intmeter);



				meterSendVO = new MeterSendVO();
				meterSendVO.setSMonthlyMeterNo(String.valueOf(monthlymeterno));
				meterSendVO.setsReadingNo(intmeter);
				meterSendVO.setsMeterNo(txtMeterId.getText().toString());
				meterSendVO.setSlastupdatedDate(dateUtil.timeStamp2dateString(dateUtil.getCurrentTimeStamp(),"dd-MMM-yyyy HH:mm:ss"));
				meterSendVO.setSlastupdatedBy(main.strCurrentUserId);
				meterSendVO.setsLocationID(String.valueOf(spnLocationId));
				meterSendVO.setsChecklistResultId(String.valueOf(resultId));
				meterSendVO.setsPrevConsumption(txtPrevConsumption.getText().toString());
				meterSendVO.setsPrevReading(txtPrevReading.getText().toString());
				meterSendVO.setsCurrReading(((TextView) txtMeterReading).getText().toString());
				meterSendVO.setsConsumption(lblConsumptionPercent.getText().toString());
				meterSendVO.setsTakenByID(String.valueOf(main.iCurrentUserInfoId));
				meterSendVO.setsTenantID(String.valueOf(tenantid));
				meterSendVO.setsTenantNo(tenantno);
				meterSendVO.setsRemarks(txtRemarks.getText().toString());
				int intCheckmeterRound = chkmeterround.isChecked() ? 1 : 0;
				meterSendVO.setsMeterRound(String.valueOf(intCheckmeterRound));
				meterSendVO.setsUnitNo(lblUnitNo.getText().toString());
				meterSendVO.setsCustomerName(txtCustomerName.getText().toString());
				meterSendVO.setsTenantName(txtTenantName.getText().toString());
				meterSendVO.setsTenantEmail(txtTenantEmail.getText().toString());
				meterSendVO.setOffline(false);

				if (bmpAsignature!=null)
				{
					DateUtil dUtil = new DateUtil();
					String newImgId = meterSendVO.getsMonthlyMeterNo() + "_"+meterSendVO.getsReadingNo()+"_Sign_"
							+dUtil.getCurrentTimeStamp().toString().replace("-", "").replace(".", "").replace(":", "").replace(" ", "");
					newImgId = newImgId + ".JPG";
//					String newImgId = tvo.getMonthlyMeterNo() + "_"+tvo.getReadingNo()+"_Sign_"
//							+dUtil.getCurrentTimeStamp().toString().replace("-", "").replace(".", "").replace(":", "").replace(" ", "");
//					newImgId = newImgId + ".JPG";
					Log.d(TAG, "New Image filename is " + newImgId);
//					tvo.setAckBySigFilename(newImgId);

//					tvo.setaSignature(comFunction.convertBitmapToByteArray(bmpAsignature));

					meterSendVO.setsSignFileName(newImgId);
				}

				String encodedString = android.util.Base64.encodeToString(comFunction.convertBitmapToByteArray(bmpAsignature), Base64.DEFAULT);
				HashMap<String, String> hashMap = new HashMap<String, String>();
				hashMap.put("fileName", meterSendVO.getSSignFileName());
				hashMap.put("meterNo", meterSendVO.getSMeterNo());
				hashMap.put("monthlyMeterNo", meterSendVO.getsMonthlyMeterNo());
				hashMap.put("readingNo", meterSendVO.getsReadingNo());
				hashMap.put("terminalID", String.valueOf(spnTerminalid));
				hashMap.put("fileContents",encodedString);
				hashMap.put("checkResultID", meterSendVO.getSChecklistResultId());
			apiConnectionUtil.isExistingMeterNo(meterSendVO.getsMonthlyMeterNo(), meterSendVO.getsReadingNo(), meterSendVO.getSMeterNo(), meterSendVO.getSChecklistResultId());
			if(isExistingResult) {
				apiConnectionUtil.updateReadingHistory(meterSendVO);
				requestCommand.add(apiConnectionUtil.ImageUpload(AppsConstant.URL_UPDATE_SIGN_IMAGE, hashMap));
			} else {
				apiConnectionUtil.insertReadingHistory(meterSendVO);
				requestCommand.add(apiConnectionUtil.ImageUpload(AppsConstant.URL_INSERT_SIGN_IMAGE, hashMap));
			}
			apiConnectionUtil.getMeterSendDAO().create(meterSendVO);
			apiConnectionUtil.getImageMapDAO().create(new ImageMapVO(hashMap));
			updateScheduleItem.where().eq("checklistResult",scheduleList2VO.getChecklistResult());
			updateScheduleItem.updateColumnValue("meterSendVO",meterSendVO);
			updateScheduleItem.updateColumnValue("signatureImageMap",new ImageMapVO(hashMap));
			updateScheduleItem.update();

//				if (dao.ExistReadingno(tvo))
//					dao.updateMonthlyMeter(tvo);
//				else
//					//llm 10/12/12 not to insert when current reading is zero
//					//if (tvo.getCurrReading().toString().length()>0)
//					//if (Float.parseFloat(tvo.getCurrReading().toString())>0)
//					dao.insertMonthlyMeter(tvo);



				//}

				//}

				//}//end for d

				//}

				//}//end for lc

//
//				//}
//			}
//			catch (UnsupportedEncodingException e1) {
//				e1.printStackTrace();
//			}


			//}//end for c


//							//save sign off
//							MonthlyMeterSignOffVO mvo = new MonthlyMeterSignOffVO();
//							mvo.setMonthlyMeterNo(monthlymeterno);
//							mvo.setTakenById(main.iCurrentUserInfoId);
//							mvo.setTakenOn(dateUtil.dateString2Calendar(txtHdate.getText().toString(), DateUtil.DATEFORMAT_DD_MON_YYYY));
//							mvo.setaSignatureFileName("A" + "_" + Integer.toString(monthlymeterno) + "_" + tmpDate + ".jpg");
//
//							if (txtAname.getText() != null)
//								mvo.setaName(txtAname.getText().toString());
//							if (txtADesignation.getText() != null)
//								mvo.setaDesignation(txtADesignation.getText().toString());
//							if (this.txtAtelephone.getText() != null)
//								mvo.setaTelelphone(txtAtelephone.getText().toString());
//							if (txtAdate.getText() != null)
//								mvo.setaDate(dateUtil.dateString2Calendar(txtAdate.getText().toString(), DateUtil.DATEFORMAT_DD_MON_YYYY));
//
//							mvo.setaSignature(comFunction.convertBitmapToByteArray(bmpAsignature));
//							DropdownItemVO selectedItemVO;
//							selectedItemVO = (DropdownItemVO) spnMaintOfficerName.getSelectedItem();
//							mvo.setMaintOfficerId(Integer.parseInt(selectedItemVO.getItemId()));
//
//							if (dao.ExistSignOff(monthlymeterno))
//							{dao.updateSignoff(mvo);}
//							else
//							{dao.saveSignoff(mvo);}

//							try {
//								syncUtil.synchronizedSyncForVersionWithNotification(AppsConstant.VERS_UPDATE_MONTHLY_METER,
//										AppsConstant.SUB_VERS_MONTHLY_METER_UPLOAD, DbUtil.getConnection(), MonthlyMeterView.this, true);
//							} catch (ULjException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}

			//	finish();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d(TAG, "createNewFault - end");


	}

	public void showMeterDialog(View v) {
		showDialog(1);
	}
	public void doSign1(View v) {
		showDialog(2);
	}

	public void doSign2(View v) {
		showDialog(3);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// this method is called after invoking 'showDialog' for the first time
		// here we initiate the corresponding DateSlideSelector and return the
		// dialog to its caller

		// get todays date and the time
		final Calendar c = Calendar.getInstance();
		switch (id) {
			case 1:
				return new NumberSpinnerDialog2(this, mMeterSetListener, spinnum, NumtoShowonSpn);
			case 2: // signature 1
				return new SignatureDialog(this, mHandoverSignatureSetListener);
			case 3: // signature 2
				return new SignatureDialog(this, mAcceptSignatureSetListener);
		}

		return null;
	}
	private OnNumberSetListener mMeterSetListener = new OnNumberSetListener() {

		@Override
		public void onDateSet(NumberSpinnerDialog2 view, String value, View v) {
			// TODO Auto-generated method stub

			if (txMeterValue != null) {
				String lastReading="";
				String newreading="";
				String metercat="";

				lastReading=txtPrevReading.getText().toString();
				metercat=txMeterCat.getText().toString(); //llm 14/02/2014 fix meter max value
				newreading=(value.substring(0, value.length()-1) + "." + value.charAt(value.length()-1));

				int m = 0;
				m = Integer.parseInt(txMeterValue.getTag().toString().replace("txtMeterRead", ""));


				if (lastReading.length()>0)
				{
					if (Double.parseDouble(lastReading)>Double.parseDouble(newreading))
					{
						txMeterValue.setText(newreading);
						showMeterRoundDialog(m);

					}
					else
					{
						txMeterValue.setText(newreading);
						calVariance(newreading,false, metercat);
					}
				}
				else
				{
					txMeterValue.setText(newreading);
					calVariance(newreading,false,metercat);
				}



			}
		}
	};

	private OnSignatureSetListener mHandoverSignatureSetListener = new OnSignatureSetListener() {

		@Override
		public void onSet(SignatureDialog view, Bitmap signature, View v) {
			// TODO Auto-generated method stub
			//bmpHsignature = signature;
		}
	};

	private OnSignatureSetListener mAcceptSignatureSetListener = new OnSignatureSetListener() {

		@Override
		public void onSet(SignatureDialog view, Bitmap signature, View v) {
			// TODO Auto-generated method stub
			bmpAsignature = signature;

			imgViewSign.setImageBitmap(signature);
		}
	};

	@Override
	public void onSoftKeyboardShown(boolean isShowing) {
		// TODO Auto-generated method stub

	}

	public void getJobEndTimestamp(View v) {
		DisplayUtil disUtil = new DisplayUtil();

		/*
		 * 1. Check if start time already populated 2. Check if end time already
		 * populated 3. Check if job can be completed (skipped for demo) 4.
		 * start barcode scanning 5. post-scanning process
		 */

		//docalculation for last record
		TextView tR = new TextView(this);
		lastindex=1;
		tR = (TextView) this.lnrMeter.findViewWithTag("txtMeterRead" + lastindex);
		doCalculation(tR,true);

		if ((validateFields()) && (validateImages("update")) ){
			if (txtJobStart.getText() == null || "".equals(txtJobStart.getText().toString())) {
				disUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_START_DATE_REQUIRED);
			} else if (txtJobEnd.getText() != null && !"".equals(txtJobEnd.getText().toString())) {
				disUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_END_DATE_EXISTS);
			} else {
				Intent intent = new Intent(this, CaptureActivity.class);
				startActivityForResult(intent, REQUEST_CODE_JOB_END);
//				if (scheduleVo != null && scheduleVo.getChecklistResultId() > 0) {
//					//LLM 02032012 take out to allow schedule tn to go thru
//					//Intent intent = new Intent(this, CaptureActivity.class);
//					//startActivityForResult(intent, REQUEST_CODE_JOB_END);
//					//LLM 02032012 take out to allow schedule tn to go thru
//
//					//LLM 02032012 add in to allow schedule tn to go thru
//					DateUtil dUtil = new DateUtil();
//					Timestamp tmpTime = dUtil.getCurrentTimeStamp();
//					txtJobEnd.setText(dUtil.timeStamp2dateString2(tmpTime));
//
//					try {
//						postProcessingAfterEndTime();
//					} catch (Exception e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					//LLM 02032012 add in to allow schedule tn to go thru
//
//				} else {
//					DateUtil dUtil = new DateUtil();
//					Timestamp tmpTime = dUtil.getCurrentTimeStamp();
//					txtJobEnd.setText(dUtil.timeStamp2dateString2(tmpTime));
//
//					try {
//						postProcessingAfterEndTime();
//					} catch (Exception e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}

			}
		}
	}

	public void getJobStartTimestamp(View v) {

		MonthlyMeterBO bo = new MonthlyMeterBO(this);
		DisplayUtil disUtil = new DisplayUtil();
		/*
		 * 1. Check if start time already populated 2. check if checklist can be
		 * done today (based on revised date) 3. start barcode scanning 4.
		 * post-scanning process
		 */
		if (spnLocationId == 0 || tenantid == -1) {
			if (spnLocationId == 0)
				disUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, "Please choose location first.");
			else if (tenantid == -1)
				disUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, "Please choose tenant first.");

		} else {

			try {
				if (txtJobStart.getText() != null && !"".equals(txtJobStart.getText().toString())) {
					disUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_START_DATE_EXISTS);
				} else {
					Intent intent = new Intent(this, CaptureActivity.class);
					startActivityForResult(intent, REQUEST_CODE_JOB_START);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				disUtil.displayException(this, "Error when validating start time", e);
			}
		}
	}

	private void postProcessingAfterStartTime() throws ParseException {
		/**
		 * 1.check barcode location 2. update service start date
		 */
		// Check barcode here
		promptProgressDialogWithDelay();
		ChecklistBO bo = new ChecklistBO(this);
		DateUtil dateUtil = new DateUtil();
		DisplayUtil dispUtil = new DisplayUtil();
		Timestamp tsScannedTime;
		if (spnLocationId == 0) {

			dispUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, "Please choose location first.");

		} else {
			try {
				tsScannedTime = dateUtil.dateString2Calendar(txtJobStart.getText().toString());

				ScheduleListVO scheduleListVO = bo.updateServiceHistoryForJobStart(resultId, tsScannedTime);
				scheduleVo.setStatusId(scheduleListVO.getStatusId());
				dispUtil.displayMessage(this, "Checklist job start time captured");

				linearLayoutMeterC1.removeAllViews();
				linearLayoutMeterC1.invalidate();
				linearLayoutMeterC2.removeAllViews();
				linearLayoutMeterC2.invalidate();
				//lnrMeter.removeAllViews();
				//lnrMeter.invalidate();

				getMetersList();

				if (scheduleVo.getStatusId()!=3 && scheduleVo.getStatusId()!=0 ) //completed
				{
					if (scheduleVo.getStatusId()==1)
					{
						btnSignature2.setVisibility(View.GONE);
						//btnSign.setVisibility(View.GONE);
						this.lnrSave.setVisibility(View.GONE);
						this.btnJobEnd.setEnabled(false);
						this.btnJobStart.setEnabled(false);
						this.btnCamera0.setEnabled(false);
						this.imgViewSign.setEnabled(false);
					}

					if (txtJobStart.getText().toString().length()<=0)
					{
						btnSignature2.setVisibility(View.GONE);
						//btnSign.setVisibility(View.GONE);
						this.lnrSave.setVisibility(View.GONE);
						this.btnJobEnd.setEnabled(false);
						this.btnJobStart.setEnabled(true);
						this.btnCamera0.setEnabled(false);
						this.imgViewSign.setEnabled(false);
					}
					else
					{
						if (txtJobStart.getText().toString().length()>0 &&
								txtJobEnd.getText().toString().length()<=0)
						{
							btnSignature2.setVisibility(View.VISIBLE);
							//btnSign.setVisibility(View.VISIBLE);
							this.lnrSave.setVisibility(View.VISIBLE);
							this.btnJobEnd.setEnabled(true);
							this.btnJobStart.setEnabled(false);
							this.btnCamera0.setEnabled(true);
							this.imgViewSign.setEnabled(true);
//							linearLayoutThumb.setVisibility(View.VISIBLE);
//							linearLayoutPreview.setVisibility(View.VISIBLE);
//							linearLayoutCaption.setVisibility(View.VISIBLE);
//							this.linearLayoutMeterPhoto.setVisibility(View.VISIBLE);
						}
					}
				}

//				if (singalStenths>=signallimit){
//				if (syncUtil.isSynching==false)
//				syncUtil.synchronizedSyncForVersionWithNotification(AppsConstant.VERS_UPDATE_MONTHLY_METER, AppsConstant.SUB_VERS_MONTHLY_METER_UPLOAD,
//						DbUtil.getConnection(), this.getApplicationContext(), true);
				scheduleListSendVO = new ScheduleListSendVO();
				scheduleListSendVO.setDtRevisedStart(scheduleList2VO.getRevisedStart());
				scheduleListSendVO.setDtActualStart(dateUtil.timeStamp2dateString(tsScannedTime,"yyyy-MM-dd HH:mm:ss"));
				scheduleListSendVO.setDtActualEnd("");
				scheduleListSendVO.setStrPersonInChargeID(main.strCurrentUserId);
				scheduleListSendVO.setStrRemarks(scheduleList2VO.getRemarks());
				scheduleListSendVO.setIntChecklistTemplateID(Integer.parseInt(scheduleList2VO.getChecklistTemplateID()));
				scheduleListSendVO.setIntChecklistTemplateVersion(Integer.parseInt(scheduleList2VO.getChecklistTemplateVersion()));
				scheduleListSendVO.setIntBuildingID(0);
				scheduleListSendVO.setStrServiceDesc("0");
				scheduleListSendVO.setIntStatusID(AppsConstant.CHECKLIST_STATUS_IN_PROGRESS);
				scheduleListSendVO.setDtLastModified(scheduleList2VO.getLastModifiedDate());
				scheduleListSendVO.setIFrequencyID(0);
				scheduleListSendVO.setEquipmentID(0);
				scheduleListSendVO.setAttendedby(main.strCurrentUserId);
				scheduleListSendVO.setReadingaddtionalremarks("none");
				scheduleListSendVO.setScanLocationStart(0);
				scheduleListSendVO.setScanLocationEnd(0);
				scheduleListSendVO.setCompleteLocationCode("0");
				scheduleListSendVO.setImei("0");
				scheduleListSendVO.setIntServiceMaintProgID(0);
				scheduleListSendVO.setIntServiceID(scheduleList2VO.getStatusInt());
				scheduleListSendVO.setDtScheduledStart("2015-08-12 00:00:00");
				scheduleListSendVO.setIntChecklistResultID(Integer.parseInt(scheduleList2VO.getChecklistResult()));
				scheduleListSendVO.setOffline(false);

				updateScheduleItem.where().eq("checklistResult",scheduleList2VO.getChecklistResult());
				updateScheduleItem.updateColumnValue("actualStart",dateUtil.timeStamp2dateString(tsScannedTime,DateUtil.DATE_TIME_FORMAT));
				updateScheduleItem.updateColumnValue("status",AppsConstant.CHECKLIST_STATUS_IN_PROGRESS);
				updateScheduleItem.updateColumnValue("startScheduleListSendVO",scheduleListSendVO);
				updateScheduleItem.update();

				apiConnectionUtil.getScheduleListSendDAO().create(scheduleListSendVO);
				apiConnectionUtil.updateScheduleTime(scheduleListSendVO);
//
//				}
//				else
//				{
//					Toast toast = Toast.makeText(MonthlyMeterView.this, "no sync", Toast.LENGTH_LONG);
//					toast.setGravity(Gravity.CENTER, 40, 60);
//					toast.show();
//				}
			} catch (ULjException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Error in Post process start time", e);
				dispUtil.displayException(this, "Error in Post process start time", e);
				txtJobStart.setText("");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void postProcessingAfterEndTime() throws Exception {

		promptProgressDialogWithDelay();
		Timestamp tsScannedEndTime;
		CommonFunction comFunction = new CommonFunction();
		DateUtil dateUtil = new DateUtil();
		MonthlyMeterBO bo = new MonthlyMeterBO(this);
		DisplayUtil dispUtil = new DisplayUtil();
		SynchronizationUtils syncUtil = new SynchronizationUtils();
		MonthlyMeterDAO mdao = new MonthlyMeterDAO();

		try {

			tsScannedEndTime = dateUtil.dateString2Calendar(txtJobEnd.getText().toString());

			doSave();

			saveImage();

			requestCommand.execute();

			//bo.completeCheck(resultId, tsScannedEndTime, "", dateUtil.getCurrentTimeStamp());


			scheduleListSendVO.setDtRevisedStart(scheduleList2VO.getRevisedStart());
			if(scheduleListSendVO.getDtActualStart() == null || scheduleListSendVO.getDtActualStart().equals("") || scheduleListSendVO.getDtActualStart().length()==0) {
				scheduleListSendVO.setDtActualStart(scheduleList2VO.getActualStart());
			}
			scheduleListSendVO.setDtActualEnd(dateUtil.timeStamp2dateString(tsScannedEndTime,"yyyy-MM-dd HH:mm:ss"));
			scheduleListSendVO.setStrPersonInChargeID(scheduleList2VO.getPersonInChargeID());
			scheduleListSendVO.setStrRemarks(txtRemarks.getText().toString());
			scheduleListSendVO.setIntChecklistTemplateID(Integer.parseInt(scheduleList2VO.getChecklistTemplateID()));
			scheduleListSendVO.setIntChecklistTemplateVersion(Integer.parseInt(scheduleList2VO.getChecklistTemplateVersion()));
			scheduleListSendVO.setIntBuildingID(0);
			scheduleListSendVO.setStrServiceDesc(scheduleList2VO.getServiceMaintProgDescription());
			scheduleListSendVO.setIntStatusID(AppsConstant.CHECKLIST_STATUS_COMPLETED);
			scheduleListSendVO.setDtLastModified(dateUtil.timeStamp2dateString(dateUtil.getCurrentTimeStamp()));
			scheduleListSendVO.setIFrequencyID(Integer.parseInt(scheduleList2VO.getFrequencyID()));
			scheduleListSendVO.setEquipmentID(Integer.parseInt(scheduleList2VO.getEquipmentId()));
			scheduleListSendVO.setAttendedby(main.strCurrentUserId);
			scheduleListSendVO.setReadingaddtionalremarks("none");
			scheduleListSendVO.setScanLocationStart(0);
			scheduleListSendVO.setScanLocationEnd(0);
			scheduleListSendVO.setCompleteLocationCode("0");
			scheduleListSendVO.setImei("0");
			scheduleListSendVO.setIntServiceMaintProgID(0);
			scheduleListSendVO.setIntServiceID(scheduleList2VO.getStatusInt());
			scheduleListSendVO.setDtScheduledStart("2015-08-12 00:00:00");
			scheduleListSendVO.setIntChecklistResultID(resultId);
			scheduleListSendVO.setOffline(false);

			updateScheduleItem.where().eq("checklistResult",scheduleList2VO.getChecklistResult());
			updateScheduleItem.updateColumnValue("actualEnd",dateUtil.timeStamp2dateString(tsScannedEndTime,DateUtil.DATE_TIME_FORMAT));
			updateScheduleItem.updateColumnValue("status",AppsConstant.CHECKLIST_STATUS_COMPLETED);
			updateScheduleItem.updateColumnValue("endScheduleListSendVO",scheduleListSendVO);
			updateScheduleItem.update();

			apiConnectionUtil.getScheduleListSendDAO().create(scheduleListSendVO);
			apiConnectionUtil.updateScheduleTime(scheduleListSendVO);

			bitmapReplace = null;
			captions = null;
			gpsLocation = null;
			imageDate = null;
			iIndex = -1;
			iIndexTemp = -1;
			bitmap = null;
			//llm 03/05/2013 insert into tblSyncedMonthlyMeterService
			//mdao.insertSyncedMonthlyMeter(scheduleVo);

			//				if (singalStenths>=signallimit){
//				if (syncUtil.isSynching==false)
			//used
//			syncUtil.synchronizedSyncForVersionWithNotification(AppsConstant.VERS_UPDATE_MONTHLY_METER, AppsConstant.SUB_VERS_MONTHLY_METER_UPLOAD,
//					DbUtil.getConnection(),this.getApplicationContext(), true);
//				}
//				else
//				{
//					Toast toast = Toast.makeText(MonthlyMeterView.this, "no sync", Toast.LENGTH_LONG);
//					toast.setGravity(Gravity.CENTER, 40, 60);
//					toast.show();
//				}


			finish();


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in Post process end time", e);
			dispUtil.displayException(this, "Error in Post process end time", e);
			txtJobEnd.setText("");
		}
//		catch (ULjException e) {
//			// TODO Auto-generated catch block
//			Log.e(TAG, "Error in Post process end time", e);
//			dispUtil.displayException(this, "Error in Post process end time", e);
//			txtJobEnd.setText("");
//		}

	}


	private void initiateComponents() {
		// Retrieve parameters passed from the previous intent (i.e.
		// ScheduleView)
		DateUtil dateUtil = new DateUtil();

		try {
			if (scheduleList2VO != null) {
				if (scheduleList2VO.getActualStart() != null)
					txtJobStart.setText(scheduleList2VO.getActualStart());
				if (scheduleList2VO.getActualEnd() != null)
					txtJobEnd.setText(scheduleList2VO.getActualEnd());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// txtRemarks.setText(scheduleVo.getRemarks());

		dateUtil = null;
	}


	private void goToTakePhoto(Integer ipos, Integer meterno) {

		Intent myIntent = new Intent(context, MonthlyMeterImagesView.class);
		myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Bundle myData = new Bundle();
		myData.putString(AppsConstant.INTENT_DATA_FAULT_ID, "");
		myData.putInt(AppsConstant.INTENT_DATA_BUILDING_ID, spnLocationId);
		myData.putInt(AppsConstant.INTENT_DATA_SERVICE_RESULT_ID, resultId);
		myData.putInt(AppsConstant.INTENT_DATA_METER_NO, meterno);
		myData.putInt(AppsConstant.INTENT_DATA_MONTHLY_METER_NO, monthlymeterno);
		myData.putInt(AppsConstant.INTENT_DATA_MONTHLY_METER_STATUS, scheduleVo.getStatusId());
		myData.putInt(AppsConstant.INTENT_DATA_TERMINAL_ID, spnTerminalid);
		myData.putInt(AppsConstant.INTENT_DATA_MONTHLY_METER_READINGNO, ipos);


		myIntent.putExtras(myData);


		bitmapReplace = null;
		captions = null;
		gpsLocation = null;
		imageDate = null;

		context.startActivity(myIntent);

	}

	private void showLogoutDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(MonthlyMeterView.this);
		builder.setMessage("Are you sure you want to exit before completing this task?\nAll data will be lost!").setTitle("e-Inspection").setCancelable(true)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						bTemp = true;
						MonthlyMeterImagesVO voImg = new MonthlyMeterImagesVO();
						MonthlyMeterDAO tDAO = new MonthlyMeterDAO();


						voImg.setTerminalId(spnTerminalid);
						voImg.setMonthlyMeterNo(monthlymeterno);

						//only delete images when no reading is saved
						if (tDAO.ExistMonthlyMeterno(monthlymeterno)==false)
						{//temp take out delete image
							//tDAO.deleteImages(voImg);
						}


						//ReplaceMonthlyMeterImage.iPosition = 0;

						bitmapReplace = null;
						captions = null;
						gpsLocation = null;
						imageDate = null;

						iIndex = -1;
						iIndexTemp = -1;
						bitmap = null;

						finish();
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	public void onBackPressed() {
		if (txtJobEnd.getText().toString().equals("")) {
			showLogoutDialog();
		}
		else
		{

			bitmapReplace = null;
			captions = null;
			gpsLocation = null;
			imageDate = null;
			iIndex = -1;
			iIndexTemp = -1;
			bitmap = null;
			super.onBackPressed();
		}


	}

	private void loadAckTextBoxes(int monthlymeterno) {
		MonthlyMeterDAO tDAO = new MonthlyMeterDAO();
		MonthlyMeterSignOffVO tVO = new MonthlyMeterSignOffVO();
		DateUtil dUtil = new DateUtil();
		tVO = tDAO.SelectAckInfo(monthlymeterno);

		if (tVO.getaName() != null )
		{
//			this.txtAname.setText(tVO.getaName());
//			if (this.scheduleVo.getStatusId()!=1)
//				this.txtAdate.setText(dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp()));
//			else
//				this.txtAdate.setText(dUtil.timeStamp2dateString(tVO.getaDate()));
//
//
//			this.txtADesignation.setText(tVO.getaDesignation());
//			this.txtAtelephone.setText(tVO.getaTelelphone());
//			spnMaintOfficerName.setSelection(getPositionForId(getMaintOfficerList(spnTerminalid), String.valueOf(tVO.getMaintOfficerId())));

		}
	}
	private int getPositionForId(List<DropdownItemVO> vos, String id) {
		int returnValue = 0;
		for (DropdownItemVO vo : vos) {
			if (id.equals(vo.getItemId())) {
				return returnValue;
			}
			returnValue++;
		}
		return 0;
	}


	private boolean validateFieldsforSave() {

		boolean validateok= true;
		boolean anydataentered=false;
		boolean rtn = false;

		int c = 0;
		int d = 0;
		int lc = 0;
		StringBuffer alertMessage = new StringBuffer();
		DisplayUtil dUtil = new DisplayUtil();

		if (this.lblUnitNo.getText().toString().length()<=0)
		{
			alertMessage.append("\nInvalid Unit No!" );
			validateok=false;
		}
		if (this.txtCustomerName.getText().toString().length()<=0)
		{
			alertMessage.append("\nInvalid Customer Name!" );
			validateok=false;
		}
		if (this.txtTenantName.getText().toString().length()<=0)
		{
			alertMessage.append("\nInvalid Tenant Name!" );
			validateok=false;
		}
		if ( comFunction.isValidEmail(txtTenantEmail.getText().toString())==false)
		{
			alertMessage.append("\nInvalid Email Address!" );
			validateok=false;
		}


		//	for (c = 0; c < lnrMeter.getChildCount(); c++) {

		try
		{

			//if (lnrMeter.getChildAt(c).getTag().toString().contains("lnrCollapse")) {
			LinearLayout lnr = new LinearLayout(this);
			lnr = (LinearLayout) lnrMeter.getChildAt(c);
			String strMeterRead = "";

			//for (lc= 0; lc<lnr.getChildCount(); lc++ )
			//{
			//	if (lnr.getChildAt(lc).getClass().toString().contains("LinearLayout"))
			//	{
			LinearLayout lnr2 = new LinearLayout(this);
			lnr2 = (LinearLayout) lnr.getChildAt(lc);
			//for (d = 0; d < lnr.getChildCount(); d++) {

			//if (lnr2.getChildAt(d).getTag() != null) {

			//if (lnr2.getChildAt(d).getTag().toString().contains("txtMeterRead")) {
			EditText childtxt = new EditText(this);
			String intmeter = "1";
			childtxt=(EditText) lnrMeter.findViewWithTag("txtMeterRead" + intmeter);
			TextView tMeterCat = new TextView(this);

			if (childtxt.isEnabled()==true)
			{
				strMeterRead = childtxt.getText().toString();

				if ((strMeterRead.length() > 0) )
				{
					if (Double.parseDouble(strMeterRead)>0)
						anydataentered=true;
				}

				tMeterCat= (TextView) this.lnrMeter.findViewWithTag("txtMeterCat" + intmeter);
				String strMeterCatAlert="\n Meter Category : "+ tMeterCat.getText().toString()+"\n";


				int n1,n2=0;

				if ((strMeterRead.length() > 0)  && tMeterCat.getText().toString().contains("."))
				{
					if (Double.parseDouble(strMeterRead)>0)
					{
						n1=Integer.parseInt( tMeterCat.getText().toString().trim().substring(0, tMeterCat.getText().toString().indexOf(".")));
						n2=n1+4;
					}
				}
				else
				{
					n1=Integer.parseInt(tMeterCat.getText().toString().trim());
					n2=n1;
				}
				if ((strMeterRead.length() > 0) )//&& (strMeterRead.length()!=n2)
				{
					if (Double.parseDouble(strMeterRead)<=0)
					{
//													if (!alertMessage.toString().contains(strMeterCatAlert))
//													{alertMessage.append(strMeterCatAlert);}
						alertMessage.append("\nInvalid meter reading! " );
						validateok=false;
					}

				}
				if ((strMeterRead.startsWith(".")) || (strMeterRead.endsWith(".")))
				{
					//if (!alertMessage.toString().contains(strMeterCatAlert))
					//{alertMessage.append(strMeterCatAlert);}
					alertMessage.append("\nInvalid meter reading! ");
					validateok=false;
				}

				if (tMeterCat.getText().toString().contains("."))
				{
					if (!strMeterRead.contains("."))
					{
						//if (!alertMessage.toString().contains(strMeterCatAlert))
						//{alertMessage.append(strMeterCatAlert);}
						//alertMessage.append("\nMissing Decimal Point at meter");
						//validateok=false;
					}
					else
					{
						int tmpdecimalplace =0;
						tmpdecimalplace = strMeterRead.subSequence(strMeterRead.indexOf("."), strMeterRead.length()).length()-1;
						if (tmpdecimalplace>3)
						{
							alertMessage.append("\nMaximun 3 decimal place is allowed at meter");
							validateok=false;
						}

					}
				}
				else
				{
					if (strMeterRead.contains("."))
					{
						//if (!alertMessage.toString().contains(strMeterCatAlert))
						//{alertMessage.append(strMeterCatAlert);}
						alertMessage.append("\nRedundant Decimal Point at meter");
						validateok=false;
					}
				}

			}

			//}

			//}

			//}//end for d

			//}

			//}//end for lc


			//}
		}
		catch (Exception e1) {
		}


		//}//end for c

//		if (!(txtAname.getText() == null || "".equals(txtAname.getText().toString())))
//			anyDataEntered=true;
//
//		if (!(txtAtelephone.getText() == null || "".equals(txtAtelephone.getText().toString())))
//			anyDataEntered=true;
//
//		if (!(txtADesignation.getText() == null || "".equals(txtADesignation.getText().toString())))
//			anyDataEntered=true;
//
//		if (!(bmpAsignature == null))
//			anyDataEntered=true;

//		DropdownItemVO selectedItemVO;
//		selectedItemVO = (DropdownItemVO) this.spnMaintOfficerName.getSelectedItem();
//		if (Integer.parseInt(selectedItemVO.getItemId())>0)
//			anyDataEntered=true;


		if (validateok==false) {

			dUtil.displayAlertDialog(this, "Missing Input ",  alertMessage.toString());
		}
//		 if (anydataentered==false) {
//			 alertMessage.append("No data input!");
//			 dUtil.displayAlertDialog(this, "Missing Input ",  alertMessage.toString());
//
//		 }

//		 if (anydataentered ==true)
//			 rtn=true;

//		 if (validateok ==false)
//			 rtn=false;
//

		return validateok;
	}


	private Boolean validateImages(String forsaveorupdate)
	{
		Boolean foundimage = true;

		DisplayUtil dUtil = new DisplayUtil();
		StringBuffer alertMessage = new StringBuffer();
		try {
			ArrayList<MonthlyMeterVO> meters = bo.getMeters(spnLocationId);
			if (meters.size() > 0)
			{
				MonthlyMeterDAO dao = new MonthlyMeterDAO();
				MonthlyMeterVO hvo = new MonthlyMeterVO();
				int cntimage=0;
				int j=0;
				for (MonthlyMeterVO vo : meters)
				{
					j=j+1;
					hvo=null;
					hvo = bo.getMetersReadingHistory(vo.getMeterNo(),scheduleVo.getChecklistResultId());

					if (hvo.getMonthlyMeterNo()!=null)//got existing reading
					{	cntimage= dao.GetImagesCount(hvo);	}
					else
					{
						hvo.setMonthlyMeterNo(monthlymeterno);
						hvo.setReadingNo(j);
						cntimage= dao.GetImagesCount(hvo);
					}

					TextView txtreading= new TextView(this);
					txtreading = (TextView) this.lnrMeter.findViewWithTag("txtMeterRead" + hvo.getReadingNo());



					if (forsaveorupdate.equals("update") && bitmapReplace!=null &&  bitmapReplace[0]==null)
					{
						if (txtreading.getText().length()>0)
						{
							if (Double.parseDouble(txtreading.getText().toString())>0)
							{
								alertMessage.append("No Image found at Meter "+" ! \n");//+hvo.getReadingNo()
								foundimage=false;
							}
						}
					}
					else //insert image
					{
						if (txtreading.isEnabled()==true  && bitmapReplace!=null && bitmapReplace[0]==null)
						{
							if (txtreading.getText().length()>0)
							{
								if (Double.parseDouble(txtreading.getText().toString())>0)
								{
									alertMessage.append("No Image found at Meter "+" ! \n");//+hvo.getReadingNo()
									foundimage=false;
								}
							}
						}
					}



				}//end for

				if ( alertMessage.length()>0)
				{
					dUtil.displayAlertDialog(this, "Missing Input ",  alertMessage.toString());
					foundimage=false;
				}

			}



		} catch (ULjException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error in loading checklist", e);
			e.printStackTrace();
		}

		return foundimage;


	}


	private void showResetDialog(final int monthlymeternum, final int readingno) {
		AlertDialog.Builder builder = new AlertDialog.Builder(MonthlyMeterView.this);
		builder.setMessage("Are you sure you want to reset this meter reading?").setTitle("e-Inspection").setCancelable(true)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						bTemp = true;
//						try {
//							doSave();
//						} catch (Exception e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
						MonthlyMeterVO vo = new MonthlyMeterVO();

						MonthlyMeterDAO tDAO = new MonthlyMeterDAO();

						txMeterValue.setText("");
						lblConsumptionPercent.setText("0.000");
						lblVariancePercent.setText("0.000");
						linearLayoutMeterRound.setVisibility(View.GONE);
						chkmeterround.setChecked(false);
						//lblVariancePercent.setBackgroundResource(R.color.bgcolor_lightpurple);
						lblVariancePercent.setTypeface(null);

						try {

							vo.setMonthlyMeterNo(monthlymeternum);
							vo.setReadingNo(readingno);
							tDAO.deleteReading(vo);
						} catch (ULjException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						try{
							//delete image too
							MonthlyMeterImagesVO voImg = new MonthlyMeterImagesVO();
							voImg.setTerminalId(spnTerminalid);
							voImg.setMonthlyMeterNo(monthlymeternum);
							voImg.setReadingNo(readingno);
							tDAO.deleteImages(voImg);
							//delete image too

							//lnrMeter.removeAllViews();
							//loadMasterMeters(spnLocationId);

						} catch (ULjException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}



					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();

	}


	private void showMeterRoundDialog( final int readingno) {
		AlertDialog.Builder builder = new AlertDialog.Builder(MonthlyMeterView.this);
		builder.setMessage("Current Reading less than last reading "+", \nare you sure that meter value has turned \nback to zero after exceed the maximal digit ?").setTitle("e-Inspection").setCancelable(true)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						linearLayoutMeterRound = (LinearLayout) lnrMeter.findViewWithTag("linearLayoutMeterRound" + Integer.toString(readingno));
						linearLayoutMeterRound.setVisibility(View.VISIBLE);
						chkmeterround=(CheckBox)linearLayoutMeterRound.findViewWithTag("chkMeterRound" + Integer.toString(readingno));
						chkmeterround.setChecked(true);
						calVariance(txMeterValue.getText().toString(),true,txMeterCat.getText().toString());

					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				linearLayoutMeterRound = (LinearLayout) lnrMeter.findViewWithTag("linearLayoutMeterRound" + Integer.toString(readingno));
				linearLayoutMeterRound.setVisibility(View.GONE);
				chkmeterround=(CheckBox)linearLayoutMeterRound.findViewWithTag("chkMeterRound" + Integer.toString(readingno));
				chkmeterround.setChecked(false);
				calVariance(txMeterValue.getText().toString(),false,txMeterCat.getText().toString());

				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();

	}

	private void calVariance(String newreading,Boolean bRound, String metercat)
	{
		//txMeterValue.setText(newreading);
		Double cr, c, pr, pc, va = 0.000;
		cr= Double.parseDouble(txMeterValue.getText().toString());
		pr= Double.parseDouble(txtPrevReading.getText().toString());
		pc = Double.parseDouble(txtPrevConsumption.getText().toString());

		//llm 14/02/2014 calculate max meter value
		if (metercat !=null && metercat.length()>0)
		{

			spinnum=0;
			if (metercat.contains("."))
			{
				spinnum=Integer.parseInt( metercat.toString().trim().substring(0, metercat.toString().indexOf(".")));
				maxlen=spinnum+2;
				decnum = 1;
			}
			else
			{
				spinnum=Integer.parseInt( metercat.toString().trim());
				maxlen=spinnum;
				decnum = 0;
			}
			int n = 0;
			strzeros="";
			strmaxvalue="";
			for (n = 0; n <spinnum; n++) {
				strzeros = strzeros+"0";
			}

			strmaxvalue= "1"+strzeros;
			maxMeterValue = Integer.parseInt(strmaxvalue);



		}
		//llm 14/02/2014 calculate max meter value

		if (bRound==false)
		{c= cr-pr;}
		else
		{c= maxMeterValue-pr+Double.parseDouble(newreading);}

		va= (c-pc)/pc;
		va =(double)Math.round(va * 100) / 100;
		va = va*100;


		// juehua for decimal limitation
		// lblConsumptionPercent.setText(Double.toString(c));
		lblConsumptionPercent.setText(df.format(c));

//		if (pc==0)
//		{lblVariancePercent.setText("0");}
//		else
//		{lblVariancePercent.setText(Double.toString(va));}

//		//llm 05/06/2013
//		double compareva =0;
//		compareva= va;
//		if (va<0)
//			compareva= -va;
//	
//		if (compareva > consumptionalert)
//		{
//			lblVariancePercent.setBackgroundResource(R.color.Red);
//			lblVariancePercent.setTypeface(null,Typeface.BOLD);
//		}
//		else
//		{
//			//lblVariancePercent.setBackgroundResource(R.color.bgcolor_lightpurple);
//			lblVariancePercent.setTypeface(null);
//		}
	}

	private void doCalculation(View v, Boolean checkround)
	{
		if ( v!=null  && checkJobStarted())
		{
			String x = String.valueOf(v.getTag());
			x = x.replace("txtMeterRead", "").trim();
			txMeterValue = (TextView) this.lnrMeter.findViewWithTag("txtMeterRead" + x);
			lblConsumptionPercent=(TextView) this.lnrMeter.findViewWithTag("lblConsumptionPercent" + x);
			txtPrevConsumption=(TextView) this.lnrMeter.findViewWithTag("txtPrevConsumption" + x);
			txtPrevReading=(TextView) this.lnrMeter.findViewWithTag("txtPrevReading" + x);
			//lblVariancePercent=(TextView) this.lnrMeter.findViewWithTag("lblVariancePercent" + x);
			txMeterCat= (TextView) this.lnrMeter.findViewWithTag("txtMeterCat" + x);
			chkmeterround= (CheckBox) this.lnrMeter.findViewWithTag("chkMeterRound" + x);
			linearLayoutMeterRound=(LinearLayout) this.lnrMeter.findViewWithTag("linearLayoutMeterRound" + x);

			//llm 10/07/2013 fix meter round bug
			String lastReading="";
			String newreading="";
			String metercat="";


			lastReading=txtPrevReading.getText().toString();

			newreading=txMeterValue.getText().toString();
			metercat=txMeterCat.getText().toString();

			if ((txMeterValue.getText().length()>0) && (!txMeterValue.getText().toString().startsWith(".")) && (!txMeterValue.getText().toString().endsWith(".")))
			{

				if (txMeterValue != null) {


					int m = 0;
					m = Integer.parseInt(txMeterValue.getTag().toString().replace("txtMeterRead", ""));


					if (lastReading.length()>0)
					{
						if (Double.parseDouble(lastReading)>Double.parseDouble(newreading))
						{
//					txMeterValue.setText(newreading);
							calVariance(newreading,false,metercat );

							if (checkround==true)
							{

								if (chkmeterround.isChecked()==false)
								{
									showMeterRoundDialog(m);

								}
								else
								{
									//llm 05/06/2013
									calVariance(newreading,true,metercat);
								}
							}
							else//llm 10/07/2013 fix meter round bug
							{
								if (chkmeterround.isChecked()==true)
								{calVariance(newreading,true,metercat);}

							}


						}
						else
						{
//					txMeterValue.setText(newreading);
							calVariance(newreading,false,metercat);
						}
					}
					else
					{
//				txMeterValue.setText(newreading);
						calVariance(newreading,false,metercat);
					}



				}
			}
			else
			{
				//reset


				lblConsumptionPercent.setText("0");
				//lblVariancePercent.setText("0");
				linearLayoutMeterRound.setVisibility(View.GONE);
				//llm 10/07/2013 fix meter round bug
				if (lastReading.length()>0 && newreading.length()>0)
				{
					if (Double.parseDouble(lastReading)>Double.parseDouble(newreading))
						chkmeterround.setChecked(false);
				}

				//lblVariancePercent.setBackgroundResource(R.color.bgcolor_lightpurple);
				//lblVariancePercent.setTypeface(null);

			}
		}
	}

	private boolean checkJobStarted()
	{
		if (txtJobStart.getText() == null || "".equals(txtJobStart.getText().toString()))
		{
			DisplayUtil dUtil = new DisplayUtil();
			dUtil.displayAlertDialog(this, AppsConstant.MESSAGE_VALIDATION, AppsConstant.MESSAGE_JOB_START_DATE_REQUIRED);
			return false;
		}
		else
			return true;

	}


	public Handler dialogHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// check sync status
			// dialogWait.dismiss();

			Log.d(TAG, "dismissing dialog.............");
			// Exception ex = (Exception)
			// msg.getData().getSerializable("error");

		}
	};

	private Bitmap LoadImage(String URL, BitmapFactory.Options options, Handler handler) throws Exception {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			in.close();
		} catch (Exception e1) {
			e1.printStackTrace();
			showMessageDialog("Image not found");
			throw e1;
		}
		handler.sendEmptyMessage(0);
		return bitmap;
	}

	private InputStream OpenHttpConnection(String strURL) throws Exception {
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inputStream = httpConn.getInputStream();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return inputStream;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		DateUtil dUtil = new DateUtil();

		if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
			try {
				// To get the captured image
				//llm 20150713 for image crash
				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
						Bitmap resizedBitmappreview = scaleBitmap(thumbnail);// Bitmap.createScaledBitmap(thumbnail,
				// 400,
				// 370,
				// true);

				ImageView imgViewPreviewTemp = new ImageView(this);
				imgViewPreviewTemp.setImageBitmap(resizedBitmappreview);

//				ReplaceImage.imgViewTemp = imgViewPreviewTemp;
//				ReplaceImage.bitmapReplace = bitmap;
//				ReplaceImage.flagSetImage = true;

				Intent intentReplace = new Intent(this, CommonImageView.class);
				//intentReplace.putExtra("image", resizedBitmappreview);
				// llm 20150713 for image crash
				CommonImageView.capturedImage2 = resizedBitmappreview;

				intentReplace.putExtra("imageCount", imageCount);

				startActivityForResult(intentReplace, 123);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == CAMERA_PIC_REQUEST && resultCode != RESULT_OK) {
//			if (ReplaceImage.bitmapReplace != null) {
//				setImage(ReplaceImage.bitmapReplace);
			if (imageCount== 4)
				btnCamera0.setEnabled(false);
//			}
		}else if (requestCode == 123 && resultCode == Activity.RESULT_OK)// replaceimage.java
		{
			//llm 20150713 for image crash
			//Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
			Bitmap thumbnail = CommonImageView.capturedImage2;
			bitmapReplace[++imageCount]=thumbnail;

			captions[imageCount]=(String) data.getExtras().get("caption");
			gpsLocation[imageCount]=(String) data.getExtras().get("gpsLocation");
			imageDate[imageCount]=(String) data.getExtras().get("imageDate");
			if (imageCount== 4)
				btnCamera0.setEnabled(false);
			setImage();
		} else 	if (Activity.RESULT_OK == resultCode && (requestCode == REQUEST_CODE_JOB_START || requestCode == REQUEST_CODE_JOB_END)) {
			Bundle extras = data.getExtras();
			String timestamp = dUtil.timeStampLongValue2DateString((Long) extras.get(CaptureActivity.INTENT_RESULT_TIMESTAMP));
			String location = (String) extras.get(CaptureActivity.INTENT_RESULT_LOCATION_CODE);
			// llm 2/1/13 to add ssh record from handheld for commercial user
			CommonFunction comFunction = new CommonFunction();

			int verificationResult;
			try {
				verificationResult = comFunction.verifyBarcodeLocationByLocationId(spnLocationId,location);
				switch(verificationResult){
					case AppsConstant.BARCODE_VERIFY_LOCATION_MATCHED_WITH_CORRECT_TAG:
						if (requestCode == REQUEST_CODE_JOB_START) {
							txtJobStart.setText(timestamp);
							if (timestamp != null)
								try {
									postProcessingAfterStartTime();
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}

						if (requestCode == REQUEST_CODE_JOB_END) {
							txtJobEnd.setText(timestamp);
							if (timestamp != null)
								try {
									postProcessingAfterEndTime();
									finish();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}
						break;
					case AppsConstant.BARCODE_VERIFY_LOCATION_MISMATCHED:
						showMessageDialog("Location mismatched!" );

						break;
					case AppsConstant.BARCODE_VERIFY_UNKNOWN_LOCATION:
						showMessageDialog("Unknown Location : " + location);

						break;
				}

			} catch (ULjException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}




	}

	private Bitmap scaleBitmap(Bitmap bitmap) {
		int iWidth = bitmap.getWidth();
		int iHeight = bitmap.getHeight();
		int newWidth = 0, newHeight = 0;
		float fFactor = (float) iHeight / (float) iWidth;

		if (iWidth <= iHeight) {
			newWidth = 300;
			newHeight = (int) (newWidth * fFactor);// 370;
		} else {
			newWidth = 400;
			newHeight = (int) (newWidth * fFactor);// 370;
		}
		float fScaleWidth = ((float) newWidth) / iWidth;
		float fScaleHeight = ((float) newHeight) / iHeight;

		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(fScaleWidth, fScaleHeight);

		Bitmap resizedBitmappreview = Bitmap.createBitmap(bitmap, 0, 0, iWidth, iHeight, matrix, true);

		return resizedBitmappreview;
	}

	private void setImage() {

		for (int i = 0; i < 5; i++) {
			if (bitmapReplace[i] != null) {

				imgViewThumbNail[i].setImageBitmap(Bitmap.createScaledBitmap(bitmapReplace[i], 100, 100, true));
				imgViewThumbNail[i].setTag(i);
			} else
				imgViewThumbNail[i].setImageBitmap(null);
		}

		if (imageCount >= 0) {

			if(bitmapReplace[imageCount]!=null){
				if (linearLayoutPreview.getVisibility() != View.VISIBLE || linearLayoutThumb.getVisibility() != View.VISIBLE) {
					linearLayoutThumb.setVisibility(View.VISIBLE);
					linearLayoutPreview.setVisibility(View.VISIBLE);
					linearLayoutCaption.setVisibility(View.VISIBLE);
					linearLayoutMeterPhoto.setVisibility(View.VISIBLE);
				}
				Bitmap resizedBitmappreview = Bitmap.createScaledBitmap(bitmapReplace[imageCount], 400, 350, true);
				imgViewPreview.setImageBitmap(resizedBitmappreview);
				imgViewPreview.setTag(imageCount);
				txtCaption.setText(captions[imageCount]);
			}
		}

		else {
			imgViewPreview.setImageBitmap(null);
			linearLayoutPreview.setVisibility(View.GONE);
			linearLayoutCaption.setVisibility(View.GONE);
			linearLayoutMeterPhoto.setVisibility(View.GONE);
//			flagPhoto = false;
		}

	}


	@Override
	/**
	 * handles long click of thumbnail image.
	 * it shows dialog for edit or delete.
	 */
	public boolean onLongClick(View v) {
		try {
			// if (!bSwipe) {
			ImageView imgTemp = (ImageView) v;
			iPhoto = (Integer) imgTemp.getTag();
			// to show dialog on long click of thumbnail image.
			dialogEdit = new Dialog(this);
			Window window = dialogEdit.getWindow();
			window.requestFeature(Window.FEATURE_NO_TITLE);
			dialogEdit.setContentView(R.layout.dialogedit);
			dialogEdit.show();

			// replace and delete button of dialog
//				imgBtnDel = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogDelete);

			imgBtnNoDel = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogNoDelete);

			imgBtnClose = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogClose);

			TextView lblGPSLocation = (TextView) dialogEdit.findViewById(R.id.lblGPSLocation);
			TextView txtGPSLocation = (TextView) dialogEdit.findViewById(R.id.txtGPSLocation);

			TextView txtImageDate = (TextView) dialogEdit.findViewById(R.id.txtImageDate);

//				imgBtnDel.setOnClickListener(this);
			imgBtnNoDel.setOnClickListener(this);

			imgBtnClose.setOnClickListener(this);


			if (v == imgViewThumbNail[0]
					|| v == imgViewThumbNail[1]
					|| v == imgViewThumbNail[2]
					|| v == imgViewThumbNail[3]
					|| v == imgViewThumbNail[4]
					|| v == imgViewPreview) {
				imgBtnDel = (ImageButton) dialogEdit.findViewById(R.id.imgBtnDialogDelete);
				imgBtnDel.setOnClickListener(this);
				// To get selected image
				ImageView imgEdit = new ImageView(getBaseContext());
				Bitmap bitmapTemp =  bitmapReplace[iPhoto];
				imgEdit.setImageBitmap(bitmapTemp);

				if (gpsLocation[iPhoto] != null && !gpsLocation[iPhoto].equals(",")) {
					txtGPSLocation.setVisibility(View.VISIBLE);
					lblGPSLocation.setVisibility(View.VISIBLE);
					txtGPSLocation.setText("(" + gpsLocation[iPhoto] + ")");
				} else {
					lblGPSLocation.setVisibility(View.GONE);
					txtGPSLocation.setText("");
					txtGPSLocation.setVisibility(View.GONE);

				}
				DateUtil dUtil;
				txtImageDate.setText(imageDate[iPhoto]);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	public void saveImage() throws SQLException, URISyntaxException, IOException {
		DateUtil dUtil = new DateUtil();

		MonthlyMeterBO tBO = new MonthlyMeterBO(MonthlyMeterView.this);
//		try {


			if (bitmapReplace != null) {
				for (int i = 0; i < bitmapReplace.length; i++) {
					if (bitmapReplace[i] != null) {

						// convert bitmap to bytes before send to voImg
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmapReplace[i].compress(Bitmap.CompressFormat.JPEG, 50, bos);
						imageSendDataVO = new ImageSendDataVO();

						txtMeterNo = (TextView) this.lnrMeter.findViewWithTag("txtMeterId" + Integer.toString(1));
						int meterno = Integer.parseInt(txtMeterNo.getText().toString());
						imageSendDataVO.setTerminalID(spnTerminalid);
						imageSendDataVO.setMonthlyMeterNo(monthlymeterno);
						imageSendDataVO.setReadingNo(1);
						imageSendDataVO.setSImageFileName(String.valueOf(monthlymeterno)+"_Signature_"+String.valueOf(i)+".jpg");
						imageSendDataVO.setDateLastModifiedDate(dUtil.timeStamp2dateString(dUtil.getCurrentTimeStamp(), "yyyy-MM-dd HH:mm:ss"));
						imageSendDataVO.setIntLastModifiedByUserInfoID(main.iCurrentUserInfoId);

						if(captions[i] !=null) {
							imageSendDataVO.setSImageCaption(captions[i].toString());
						}
						imageSendDataVO.setOffline(false);

						if (gpsLocation[i] != null && !gpsLocation[i].equals(",")
								&& !gpsLocation[i].equals("")) {

							String[] gpsloc = gpsLocation[i].toString().split(",");

							imageSendDataVO.setFAltitude(Double.parseDouble(gpsloc[1]));
							imageSendDataVO.setFLongtitude(Double.parseDouble(gpsloc[0]));
						}

						String encodedString = android.util.Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
						ByteString byteString =  ByteString.decodeBase64(encodedString);
						map.put("fileName", imageSendDataVO.getSImageFileName());
						map.put("meterNo", meterSendVO.getSMeterNo());
						map.put("monthlyMeterNo", imageSendDataVO.getMonthlyMeterNo().toString());
						map.put("readingNo", imageSendDataVO.getReadingNo().toString());
						map.put("terminalID", imageSendDataVO.getTerminalID().toString());
						map.put("fileContents",encodedString);
						map.put("checkResultId",String.valueOf(resultId));
						requestCommand.add(apiConnectionUtil.ImageUpload(AppsConstant.URL_METER_IMAGE, map));
						apiConnectionUtil.getImageSendDAO().create(imageSendDataVO);
						apiConnectionUtil.getImageMapDAO().create(new ImageMapVO(map));
						//create a file to write bitmap data
						File f = new File(context.getCacheDir(), "justanotherfilename.jpg");
						f.createNewFile();


						byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bitmapdata);
						fos.flush();
						fos.close();
						apiConnectionUtil.sendImageData(f);

						updateScheduleItem.where().eq("checklistResult",scheduleList2VO.getChecklistResult());
						updateScheduleItem.updateColumnValue("meterImageMap",new ImageMapVO(map));
						updateScheduleItem.updateColumnValue("imageSendDataVO",imageSendDataVO);
						updateScheduleItem.update();
						//BO.createNewImage(voImg, i);
					}
				}
			}
			Log.d(TAG, "save fault images - end");

//		} catch (UnsupportedEncodingException ue) {
//			ue.printStackTrace();
//		}

//		iIndex = -1;
//		iIndexTemp = -1;
//		bitmap = null;
		bitmapReplace = null;
		captions = null;
		gpsLocation = null;
		imageDate = null;

	}

	private void setImage(Bitmap[] bitmaptemp) {

		for (int i = 0; i < 5; i++) {
			if (bitmaptemp[i] != null) {

				imgViewThumbNail[i].setImageBitmap(Bitmap.createScaledBitmap(bitmaptemp[i], 100, 100, true));
				imgViewThumbNail[i].setTag(i);
			} else
				imgViewThumbNail[i].setImageBitmap(null);
		}

		if (iIndex >= 0) {
			if (linearLayoutPreview.getVisibility() != View.VISIBLE || linearLayoutThumb.getVisibility() != View.VISIBLE) {
				linearLayoutThumb.setVisibility(View.VISIBLE);
				linearLayoutPreview.setVisibility(View.VISIBLE);
				linearLayoutCaption.setVisibility(View.VISIBLE);
				linearLayoutMeterPhoto.setVisibility(View.VISIBLE);
			}

			Bitmap resizedBitmappreview;
			if (monthlymeterno > 0) {
				// juehua image enhance
				// resizedBitmappreview =
				// Bitmap.createScaledBitmap(bitmaptemp[iIndex], 400, 350,
				// true);
				resizedBitmappreview = bitmaptemp[iIndex];
			} else {
				resizedBitmappreview = bitmaptemp[iIndex];
				iPreviewInd = iIndex;
			}

			imgViewPreview.setImageBitmap(resizedBitmappreview);
			imgViewPreview.setTag(iIndex);

			if (captions[iIndex]!=null)
				txtCaption.setText(captions[iIndex].toString());

		}

		else {
			imgViewPreview.setImageBitmap(null);
			linearLayoutPreview.setVisibility(View.GONE);
			linearLayoutCaption.setVisibility(View.GONE);
			linearLayoutMeterPhoto.setVisibility(View.GONE);
			flagPhoto = false;
		}

	}

	public void getMetersList() {
		apiConnectionUtil.buildRetrofit().getMeterList(main.strCurrentUserId).enqueue(new Callback<ArrayList<MonthlyMeterVO>>() {
			@Override
			public void onResponse(Call<ArrayList<MonthlyMeterVO>> call, Response<ArrayList<MonthlyMeterVO>> response) {
				apiConnectionUtil.storeMeters(response);
				meters = new ArrayList<MonthlyMeterVO>();
				for( MonthlyMeterVO vo : apiConnectionUtil.findMeter(String.valueOf(spnLocationId))){
					meters.add(vo);
				}
				loadMasterMeters();
			}

			@Override
			public void onFailure(Call<ArrayList<MonthlyMeterVO>> call, Throwable t) {
				meters = new ArrayList<MonthlyMeterVO>();
				for( MonthlyMeterVO vo : apiConnectionUtil.findMeter(String.valueOf(spnLocationId))){
					meters.add(vo);
				}
				loadMasterMeters();
			}
		});
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
		apiConnectionUtil.closeDatabaseHelper();
	}

}