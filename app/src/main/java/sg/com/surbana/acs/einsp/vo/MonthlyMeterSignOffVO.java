package sg.com.surbana.acs.einsp.vo;

import java.sql.Timestamp;

public class MonthlyMeterSignOffVO {
	private final String TAG = "SignoffVO.java";
	Integer MonthlyMeterNo;
	Integer ReadingNo;
	String aName;
	String aDesignation;
	String aTelelphone;
	Timestamp aDate;
	byte[] aSignature;
	String aSignatureFileName;
	String aEmail;
	int locationid;
	int tenantId;// 23022011 for update tenantid schedule status=new =1
	int checklistresultid;
	int takenById;
	Timestamp takenOn;
	int maintOfficerId;
	
	
	public Integer getMonthlyMeterNo() {
		return MonthlyMeterNo;
	}

	public void setMonthlyMeterNo(Integer monthlyMeterNo) {
		MonthlyMeterNo = monthlyMeterNo;
	}

	public Integer getReadingNo() {
		return ReadingNo;
	}

	public void setReadingNo(Integer readingNo) {
		ReadingNo = readingNo;
	}

	public int getLocationid() {
		return locationid;
	}

	public void setLocationid(int locationid) {
		this.locationid = locationid;
	}

	public String getaEmail() {
		return aEmail;
	}

	public void setaEmail(String aEmail) {
		this.aEmail = aEmail;
	}

	Timestamp lastUpdatedDate;
	int lastUpdatedBy;

	int terminalid;

	public int getTerminalid() {
		return terminalid;
	}

	public void setTerminalid(int terminalid) {
		this.terminalid = terminalid;
	}

	public String getaName() {
		return aName;
	}

	public void setaName(String aName) {
		this.aName = aName;
	}

	public String getaDesignation() {
		return aDesignation;
	}

	public void setaDesignation(String aDesignation) {
		this.aDesignation = aDesignation;
	}

	public String getaTelelphone() {
		return aTelelphone;
	}

	public void setaTelelphone(String aTelelphone) {
		this.aTelelphone = aTelelphone;
	}

	public Timestamp getaDate() {
		return aDate;
	}

	public void setaDate(Timestamp aDate) {
		this.aDate = aDate;
	}

	public byte[] getaSignature() {
		return aSignature;
	}

	public void setaSignature(byte[] aSignature) {
		this.aSignature = aSignature;
	}

	public String getaSignatureFileName() {
		return aSignatureFileName;
	}

	public void setaSignatureFileName(String aSignatureFileName) {
		this.aSignatureFileName = aSignatureFileName;
	}

	public Timestamp getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public int getChecklistresultid() {
		return checklistresultid;
	}

	public void setChecklistresultid(int checklistresultid) {
		this.checklistresultid = checklistresultid;
	}

	public int getTakenById() {
		return takenById;
	}

	public void setTakenById(int takenById) {
		this.takenById = takenById;
	}

	public Timestamp getTakenOn() {
		return takenOn;
	}

	public void setTakenOn(Timestamp takenOn) {
		this.takenOn = takenOn;
	}

	public int getMaintOfficerId() {
		return maintOfficerId;
	}

	public void setMaintOfficerId(int maintOfficerId) {
		this.maintOfficerId = maintOfficerId;
	}

}
