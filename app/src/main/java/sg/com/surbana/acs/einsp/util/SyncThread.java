package sg.com.surbana.acs.einsp.util;

import java.util.Date;

import sg.com.surbana.acs.einsp.main;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.ianywhere.ultralitejni12.Connection;
import com.ianywhere.ultralitejni12.SyncParms;
import com.ianywhere.ultralitejni12.ULjException;

/*
 *Change History
 ***************************************************************
 *Date				Updated By		Remarks
 ***************************************************************
 *Jun 2, 2011		Ryan			Created
 *07 Jul 2011		Ryan			Display error message when sync occur
 *22 Jul 2011	    Ryan			ensure to go to next screen only if no error occur 
 *								 	accept hanlder for progress dialog
 *29 Jul 2011		Ryan			adopt callback handler mechanism to allow caller function to decide 'post' sync process
 */
public class SyncThread extends Thread {
	private final String TAG = "SyncThread.java";

	Connection con;
	SyncParms syncParms;
	public Handler mHandler;
	ProgressDialog dialog = null;
	NotificationManager notificationManager;
	Notification notification;
	int notificationId;
	Intent nextScreen;
	Context context;

	public SyncThread(Connection c, SyncParms s, ProgressDialog d, Intent i, Context cont, Handler hand) {
		con = c;
		syncParms = s;
		dialog = d;
		nextScreen = i;
		context = cont;
		mHandler = hand;

	}

	public SyncThread(Connection c, SyncParms s, Notification n, int ni, Handler hand) {
		con = c;
		syncParms = s;
		notification = n;
		notificationId = ni;
		mHandler = hand;
	}

	public SyncThread() {

	}

	public void setSyncThreadProperties(Connection c, SyncParms s, ProgressDialog d) {
		con = c;
		syncParms = s;
		dialog = d;
	}

	public void run() {
		DisplayUtil dsUtil = new DisplayUtil();
		try {
			if (!main.NO_SYNC)
				con.synchronize(syncParms);
			System.out.println("End time is : " + new Date());
			if (mHandler != null) {
				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt(AppsConstant.HANDLER_KEY_STATUS, AppsConstant.HANDLER_VALUE_STATUS_OK);
				msg.setData(b);
				mHandler.sendMessage(msg);
			}

			if (nextScreen != null) {
				nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(nextScreen);
			}
		} catch (ULjException e) {
			System.out.println(e.getErrorCode());
			System.out.println(e.getMessage());
			//still allow to proceed if the error is due to communication to mobilink 
			// AND the version is NOT to get next ID
			//-1305 : mobilink communication error
			String version = syncParms.getVersion();
			
			if(e.getErrorCode()==-1305 && !version.contains("Next")){
				System.out.println("****View Data Locally******");
				if (nextScreen != null) {
					nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(nextScreen);
				}
				//llm 2015/01/20 show button when mobilink down
				else
				{
					if (mHandler != null) {
						Message msg = mHandler.obtainMessage(); 
						Bundle b = new Bundle();
						b.putInt(AppsConstant.HANDLER_KEY_STATUS, AppsConstant.HANDLER_VALUE_STATUS_ERROR);
						b.putSerializable(AppsConstant.HANDLER_KEY_ERROR, e);
						msg.setData(b);
						mHandler.sendMessage(msg);
					}
				}
				//llm 2015/01/20 show button when mobilink down
			
			}else{
				System.out.println("I am caching exception here");
				if (mHandler != null) {
					Message msg = mHandler.obtainMessage(); 
					Bundle b = new Bundle();
					b.putInt(AppsConstant.HANDLER_KEY_STATUS, AppsConstant.HANDLER_VALUE_STATUS_ERROR);
					b.putSerializable(AppsConstant.HANDLER_KEY_ERROR, e);
					msg.setData(b);
					mHandler.sendMessage(msg);
				}
				e.printStackTrace();
			}
		}
			catch (Exception e) {
			System.out.println(e.toString());
			System.out.println("I am caching exception here");
			if (mHandler != null) {
				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt(AppsConstant.HANDLER_KEY_STATUS, AppsConstant.HANDLER_VALUE_STATUS_ERROR);
				b.putSerializable(AppsConstant.HANDLER_KEY_ERROR, e);
				msg.setData(b);
				mHandler.sendMessage(msg);
			}
			e.printStackTrace();
		} finally {
			SynchronizationUtils.isSynching = false;
			if (dialog != null)
				dialog.dismiss();

			if (notification != null) {
				notification.tickerText = "Mobile Inspection System Synchronization completed";
				main.NOTIFICATION_MANAGER.notify(notificationId, notification);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}// sleep 3 second before removing the notificaiton
				main.NOTIFICATION_MANAGER.cancel(notificationId);
				System.out.println("==============================Cancelling notification " + notificationId);
			}
		}
	}
}
