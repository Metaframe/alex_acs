package sg.com.surbana.acs.einsp.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ALRED on 3/18/2017.
 */

@DatabaseTable(tableName = "ScheduleList2VO")
public class ScheduleList2VO implements Parcelable{
    @DatabaseField
    @SerializedName("ServiceID")
    @Expose
    private String serviceID;
    @DatabaseField
    @SerializedName("ServiceMaintProgID")
    @Expose
    private String serviceMaintProgID;
    @DatabaseField
    @SerializedName("ScheduledStart")
    @Expose
    private String scheduledStart;
    @DatabaseField
    @SerializedName("RevisedStart")
    @Expose
    private String revisedStart;
    @DatabaseField
    @SerializedName("ActualStart")
    @Expose
    private String actualStart;
    @DatabaseField
    @SerializedName("ActualEnd")
    @Expose
    private String actualEnd;
    @DatabaseField
    @SerializedName("PersonInChargeID")
    @Expose
    private String personInChargeID;
    @DatabaseField
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @DatabaseField
    @SerializedName("ChecklistTemplateID")
    @Expose
    private String checklistTemplateID;
    @DatabaseField
    @SerializedName("ChecklistTemplateVersion")
    @Expose
    private String checklistTemplateVersion;
    @DatabaseField
    @SerializedName("ChecklistResult")
    @Expose
    private String checklistResult;
    @DatabaseField
    @SerializedName("LocationID")
    @Expose
    private String locationID;
    @DatabaseField
    @SerializedName("ServiceMaintProgDescription")
    @Expose
    private String serviceMaintProgDescription;
    @DatabaseField
    @SerializedName("Status")
    @Expose
    private String status;
    @DatabaseField
    @SerializedName("LastModifiedDate")
    @Expose
    private String lastModifiedDate;
    @DatabaseField
    @SerializedName("FrequencyID")
    @Expose
    private String frequencyID;
    @DatabaseField
    @SerializedName("EquipmentId")
    @Expose
    private String equipmentId;
    @DatabaseField
    @SerializedName("AttendedBy")
    @Expose
    private String attendedBy;
    @DatabaseField
    @SerializedName("ReadingAddtionalRemarks")
    @Expose
    private String readingAddtionalRemarks;
    @DatabaseField
    @SerializedName("ScanLocationStart")
    @Expose
    private String scanLocationStart;
    @DatabaseField
    @SerializedName("ScanLocationEnd")
    @Expose
    private String scanLocationEnd;
    @DatabaseField
    @SerializedName("Column1")
    @Expose
    private String column1;
    @DatabaseField
    @SerializedName("imei")
    @Expose
    private String imei;
    @DatabaseField
    @SerializedName("return")
    @Expose
    private boolean isSuccess;
    @DatabaseField(columnName ="startScheduleListSendVO", foreign = true)
    private ScheduleListSendVO startScheduleListSendVO;
    @DatabaseField(columnName = "endScheduleListSendVO", foreign = true)
    private ScheduleListSendVO endScheduleListSendVO;
    @DatabaseField(columnName = "imageSendDataVO", foreign = true)
    private ImageSendDataVO imageSendDataVO;
    @DatabaseField(columnName = "signatureImageMap", foreign = true)
    private ImageMapVO signatureImageMap;
    @DatabaseField(columnName = "meterImageMap", foreign = true)
    private ImageMapVO meterImageMap;
    @DatabaseField(columnName = "meterSendVO", foreign = true)
    private MeterSendVO meterSendVO;

    private Image2VO image2VO;
    private MeterReading2VO meterReading2VO;

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceMaintProgID() {
        return serviceMaintProgID;
    }

    public void setServiceMaintProgID(String serviceMaintProgID) {
        this.serviceMaintProgID = serviceMaintProgID;
    }

    public String getScheduledStart() {
        return scheduledStart;
    }

    public void setScheduledStart(String scheduledStart) {
        this.scheduledStart = scheduledStart;
    }

    public String getRevisedStart() {
        return revisedStart;
    }

    public void setRevisedStart(String revisedStart) {
        this.revisedStart = revisedStart;
    }

    public String getActualStart() {
        return actualStart;
    }

    public void setActualStart(String actualStart) {
        this.actualStart = actualStart;
    }

    public String getActualEnd() {
        return actualEnd;
    }

    public void setActualEnd(String actualEnd) {
        this.actualEnd = actualEnd;
    }

    public String getPersonInChargeID() {
        return personInChargeID;
    }

    public void setPersonInChargeID(String personInChargeID) {
        this.personInChargeID = personInChargeID;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getChecklistTemplateID() {
        if(checklistTemplateID.length() > 0 && !checklistTemplateID.equals("")) {
            return checklistTemplateID;
        } else {
            return "0";
        }
    }

    public void setChecklistTemplateID(String checklistTemplateID) {
        this.checklistTemplateID = checklistTemplateID;
    }

    public String getChecklistTemplateVersion() {
        if(checklistTemplateVersion.length() > 0 && !checklistTemplateVersion.equals("")) {
            return checklistTemplateVersion;
        } else {
            return "0";
        }
    }

    public void setChecklistTemplateVersion(String checklistTemplateVersion) {
        this.checklistTemplateVersion = checklistTemplateVersion;
    }

    public String getChecklistResult() {
        return checklistResult;
    }

    public void setChecklistResult(String checklistResult) {
        this.checklistResult = checklistResult;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getServiceMaintProgDescription() {
        return serviceMaintProgDescription;
    }

    public void setServiceMaintProgDescription(String serviceMaintProgDescription) {
        this.serviceMaintProgDescription = serviceMaintProgDescription;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusInt() {
        return Integer.valueOf(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getFrequencyID() {
        return frequencyID;
    }

    public void setFrequencyID(String frequencyID) {
        this.frequencyID = frequencyID;
    }

    public String getEquipmentId() {
        if(equipmentId.length() > 0 && !equipmentId.equals("")) {
            return equipmentId;
        } else {
            return "0";
        }
    }

    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getAttendedBy() {
        return attendedBy;
    }

    public void setAttendedBy(String attendedBy) {
        this.attendedBy = attendedBy;
    }

    public String getReadingAddtionalRemarks() {
        return readingAddtionalRemarks;
    }

    public void setReadingAddtionalRemarks(String readingAddtionalRemarks) {
        this.readingAddtionalRemarks = readingAddtionalRemarks;
    }

    public String getScanLocationStart() {
        return scanLocationStart;
    }

    public void setScanLocationStart(String scanLocationStart) {
        this.scanLocationStart = scanLocationStart;
    }

    public String getScanLocationEnd() {
        return scanLocationEnd;
    }

    public void setScanLocationEnd(String scanLocationEnd) {
        this.scanLocationEnd = scanLocationEnd;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Image2VO getImage2VO() {
        return image2VO;
    }

    public void setImage2VO(Image2VO image2VO) {
        this.image2VO = image2VO;
    }

    public MeterReading2VO getMeterReading2VO() {
        return meterReading2VO;
    }

    public void setMeterReading2VO(MeterReading2VO meterReading2VO) {
        this.meterReading2VO = meterReading2VO;
    }

    public boolean getIsSuccess(){
        return isSuccess;
    }

    public void setIsSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public ScheduleListSendVO getStartScheduleListSendVO() {
        return startScheduleListSendVO;
    }

    public void setStartScheduleListSendVO(ScheduleListSendVO startScheduleListSendVO) {
        this.startScheduleListSendVO = startScheduleListSendVO;
    }

    public ScheduleListSendVO getEndScheduleListSendVO() {
        return endScheduleListSendVO;
    }

    public void setEndScheduleListSendVO(ScheduleListSendVO endScheduleListSendVO) {
        this.endScheduleListSendVO = endScheduleListSendVO;
    }

    public ImageSendDataVO getImageSendDataVO() {
        return imageSendDataVO;
    }

    public void setImageSendDataVO(ImageSendDataVO imageSendDataVO) {
        this.imageSendDataVO = imageSendDataVO;
    }

    public ImageMapVO getSignatureImageMap() {
        return signatureImageMap;
    }

    public void setSignatureImageMap(ImageMapVO signatureImageMap) {
        this.signatureImageMap = signatureImageMap;
    }

    public ImageMapVO getMeterImageMap() {
        return meterImageMap;
    }

    public void setMeterImageMap(ImageMapVO meterImageMap) {
        this.meterImageMap = meterImageMap;
    }

    public MeterSendVO getMeterSendVO() {
        return meterSendVO;
    }

    public void setMeterSendVO(MeterSendVO meterSendVO) {
        this.meterSendVO = meterSendVO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<ScheduleList2VO> CREATOR = new Creator<ScheduleList2VO>() {
        @Override
        public ScheduleList2VO createFromParcel(Parcel source) {
            ScheduleList2VO scheduleList2VO = new ScheduleList2VO();
            scheduleList2VO.serviceID = source.readString();
            scheduleList2VO.serviceMaintProgID = source.readString();
            scheduleList2VO.scheduledStart = source.readString();
            scheduleList2VO.revisedStart = source.readString();
            scheduleList2VO.actualStart = source.readString();
            scheduleList2VO.actualEnd = source.readString();
            scheduleList2VO.personInChargeID = source.readString();
            scheduleList2VO.remarks = source.readString();
            scheduleList2VO.checklistTemplateID = source.readString();
            scheduleList2VO.checklistTemplateVersion = source.readString();
            scheduleList2VO.checklistResult = source.readString();
            scheduleList2VO.locationID = source.readString();
            scheduleList2VO.serviceMaintProgDescription = source.readString();
            scheduleList2VO.status = source.readString();
            scheduleList2VO.lastModifiedDate = source.readString();
            scheduleList2VO.frequencyID = source.readString();
            scheduleList2VO.equipmentId = source.readString();
            scheduleList2VO.attendedBy = source.readString();
            scheduleList2VO.readingAddtionalRemarks = source.readString();
            scheduleList2VO.scanLocationStart = source.readString();
            scheduleList2VO.scanLocationEnd = source.readString();
            scheduleList2VO.column1 = source.readString();
            scheduleList2VO.imei = source.readString();
            scheduleList2VO.image2VO = source.readParcelable(Image2VO.class.getClassLoader());
            scheduleList2VO.meterReading2VO = source.readParcelable(MeterReading2VO.class.getClassLoader());

            return scheduleList2VO;
        }

        @Override
        public ScheduleList2VO[] newArray(int size) {
            return new ScheduleList2VO[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(serviceID);
        parcel.writeString(serviceMaintProgID);
        parcel.writeString(scheduledStart);
        parcel.writeString(revisedStart);
        parcel.writeString(actualStart);
        parcel.writeString(actualEnd);
        parcel.writeString(personInChargeID);
        parcel.writeString(remarks);
        parcel.writeString(checklistTemplateID);
        parcel.writeString(checklistTemplateVersion);
        parcel.writeString(checklistResult);
        parcel.writeString(locationID);
        parcel.writeString(serviceMaintProgDescription);
        parcel.writeString(status);
        parcel.writeString(lastModifiedDate);
        parcel.writeString(frequencyID);
        parcel.writeString(equipmentId);
        parcel.writeString(attendedBy);
        parcel.writeString(readingAddtionalRemarks);
        parcel.writeString(scanLocationStart);
        parcel.writeString(scanLocationEnd);
        parcel.writeString(column1);
        parcel.writeString(imei);
        parcel.writeParcelable(image2VO, i);
        parcel.writeParcelable(meterReading2VO, i);
    }
}
