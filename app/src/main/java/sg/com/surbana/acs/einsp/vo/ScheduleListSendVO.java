package sg.com.surbana.acs.einsp.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ALRED on 3/28/2017.
 */

@DatabaseTable(tableName = "ScheduleListSendVO")
public class ScheduleListSendVO implements Serializable {
    @DatabaseField
    @SerializedName("dtRevisedStart")
    @Expose
    private String dtRevisedStart;
    @DatabaseField
    @SerializedName("dtActualStart")
    @Expose
    private String dtActualStart;
    @DatabaseField
    @SerializedName("dtActualEnd")
    @Expose
    private String dtActualEnd;
    @DatabaseField
    @SerializedName("strPersonInChargeID")
    @Expose
    private String strPersonInChargeID;
    @DatabaseField
    @SerializedName("strRemarks")
    @Expose
    private String strRemarks;
    @DatabaseField
    @SerializedName("intChecklistTemplateID")
    @Expose
    private Integer intChecklistTemplateID;
    @DatabaseField
    @SerializedName("intChecklistTemplateVersion")
    @Expose
    private Integer intChecklistTemplateVersion;
    @DatabaseField
    @SerializedName("intBuildingID")
    @Expose
    private Integer intBuildingID;
    @DatabaseField
    @SerializedName("strServiceDesc")
    @Expose
    private String strServiceDesc;
    @DatabaseField
    @SerializedName("intStatusID")
    @Expose
    private Integer intStatusID;
    @DatabaseField
    @SerializedName("dtLastModified")
    @Expose
    private String dtLastModified;
    @DatabaseField
    @SerializedName("iFrequencyID")
    @Expose
    private Integer iFrequencyID;
    @DatabaseField
    @SerializedName("equipmentID")
    @Expose
    private Integer equipmentID;
    @DatabaseField
    @SerializedName("attendedby")
    @Expose
    private String attendedby;
    @DatabaseField
    @SerializedName("readingaddtionalremarks")
    @Expose
    private String readingaddtionalremarks;
    @DatabaseField
    @SerializedName("ScanLocationStart")
    @Expose
    private Integer scanLocationStart;
    @DatabaseField
    @SerializedName("ScanLocationEnd")
    @Expose
    private Integer scanLocationEnd;
    @DatabaseField
    @SerializedName("CompleteLocationCode")
    @Expose
    private String completeLocationCode;
    @DatabaseField
    @SerializedName("imei")
    @Expose
    private String imei;
    @DatabaseField
    @SerializedName("intServiceMaintProgID")
    @Expose
    private Integer intServiceMaintProgID;
    @DatabaseField
    @SerializedName("intServiceID")
    @Expose
    private Integer intServiceID;
    @DatabaseField
    @SerializedName("dtScheduledStart")
    @Expose
    private String dtScheduledStart;
    @DatabaseField(id = true)
    @SerializedName("intChecklistResultID")
    @Expose
    private Integer intChecklistResultID;

    @DatabaseField
    private boolean isOffline;

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    public String getDtRevisedStart() {
        return dtRevisedStart;
    }

    public void setDtRevisedStart(String dtRevisedStart) {
        this.dtRevisedStart = dtRevisedStart;
    }

    public String getDtActualStart() {
        return dtActualStart;
    }

    public void setDtActualStart(String dtActualStart) {
        this.dtActualStart = dtActualStart;
    }

    public String getDtActualEnd() {
        return dtActualEnd;
    }

    public void setDtActualEnd(String dtActualEnd) {
        this.dtActualEnd = dtActualEnd;
    }

    public String getStrPersonInChargeID() {
        return strPersonInChargeID;
    }

    public void setStrPersonInChargeID(String strPersonInChargeID) {
        this.strPersonInChargeID = strPersonInChargeID;
    }

    public String getStrRemarks() {
        return strRemarks;
    }

    public void setStrRemarks(String strRemarks) {
        this.strRemarks = strRemarks;
    }

    public Integer getIntChecklistTemplateID() {
        return intChecklistTemplateID;
    }

    public void setIntChecklistTemplateID(Integer intChecklistTemplateID) {
        this.intChecklistTemplateID = intChecklistTemplateID;
    }

    public Integer getIntChecklistTemplateVersion() {
        return intChecklistTemplateVersion;
    }

    public void setIntChecklistTemplateVersion(Integer intChecklistTemplateVersion) {
        this.intChecklistTemplateVersion = intChecklistTemplateVersion;
    }

    public Integer getIntBuildingID() {
        return intBuildingID;
    }

    public void setIntBuildingID(Integer intBuildingID) {
        this.intBuildingID = intBuildingID;
    }

    public String getStrServiceDesc() {
        return strServiceDesc;
    }

    public void setStrServiceDesc(String strServiceDesc) {
        this.strServiceDesc = strServiceDesc;
    }

    public Integer getIntStatusID() {
        return intStatusID;
    }

    public void setIntStatusID(Integer intStatusID) {
        this.intStatusID = intStatusID;
    }

    public String getDtLastModified() {
        return dtLastModified;
    }

    public void setDtLastModified(String dtLastModified) {
        this.dtLastModified = dtLastModified;
    }

    public Integer getIFrequencyID() {
        return iFrequencyID;
    }

    public void setIFrequencyID(Integer iFrequencyID) {
        this.iFrequencyID = iFrequencyID;
    }

    public Integer getEquipmentID() {
        return equipmentID;
    }

    public void setEquipmentID(Integer equipmentID) {
        this.equipmentID = equipmentID;
    }

    public String getAttendedby() {
        return attendedby;
    }

    public void setAttendedby(String attendedby) {
        this.attendedby = attendedby;
    }

    public String getReadingaddtionalremarks() {
        return readingaddtionalremarks;
    }

    public void setReadingaddtionalremarks(String readingaddtionalremarks) {
        this.readingaddtionalremarks = readingaddtionalremarks;
    }

    public Integer getScanLocationStart() {
        return scanLocationStart;
    }

    public void setScanLocationStart(Integer scanLocationStart) {
        this.scanLocationStart = scanLocationStart;
    }

    public Integer getScanLocationEnd() {
        return scanLocationEnd;
    }

    public void setScanLocationEnd(Integer scanLocationEnd) {
        this.scanLocationEnd = scanLocationEnd;
    }

    public String getCompleteLocationCode() {
        return completeLocationCode;
    }

    public void setCompleteLocationCode(String completeLocationCode) {
        this.completeLocationCode = completeLocationCode;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getIntServiceMaintProgID() {
        return intServiceMaintProgID;
    }

    public void setIntServiceMaintProgID(Integer intServiceMaintProgID) {
        this.intServiceMaintProgID = intServiceMaintProgID;
    }

    public Integer getIntServiceID() {
        return intServiceID;
    }

    public void setIntServiceID(Integer intServiceID) {
        this.intServiceID = intServiceID;
    }

    public String getDtScheduledStart() {
        return dtScheduledStart;
    }

    public void setDtScheduledStart(String dtScheduledStart) {
        this.dtScheduledStart = dtScheduledStart;
    }

    public Integer getIntChecklistResultID() {
        return intChecklistResultID;
    }

    public void setIntChecklistResultID(Integer intChecklistResultID) {
        this.intChecklistResultID = intChecklistResultID;
    }
}
